//
//  AppDelegate.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/17/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "AppDelegate.h"
#import "Utility.h"
#import "Crashlytics/Crashlytics.h"
#import "ME_Logger.h"
#import "ME_Launcher.h"
#import "FlixPakClient.h"
#import "NavigationController.h"
#import "MainVC.h"
#import "UserManager.h"

// iphone launch client
#import "MainViewController.h"

// choose version of app.
#import "ChooseVersionVC.h"

@interface AppDelegate()

@property (nonatomic, strong) MainVC *mainVC;
@property (nonatomic, strong) NavigationController *navC;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Crashlytics startWithAPIKey:@"ebe521821af24fb6d90aaa02a343ec7975c7b349"];

    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    [[UINavigationBar appearance] setTintColor:[UIColor lightGrayColor]];
    // Send the dimensions to Parse along with the 'search' event
    NSMutableDictionary *dimensions = [NSMutableDictionary dictionary];
    if (IS_IPAD) {
        [dimensions setObject:@"iPad" forKey:@"device"];
    } else {
        [dimensions setObject:@"iPhone-iPod" forKey:@"device"];
        if (IS_FOUR_INCH_IPHONE) {
            [dimensions setObject:@"4 inch device" forKey:@"screenSize"];
        } else [dimensions setObject:@"3.5 inch device" forKey:@"screenSize"];
    }
    
//    [self setupReachability];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startNormalVersion) name:NOTIFICATION_START_NEW_APP_VERSION object:nil];
//    self.mainVC = [[MainViewController alloc] init];
//    [self.mainVC.view setFrame:self.window.bounds];
    self.window.backgroundColor = [UIColor colorWithWhite:0.2f alpha:1.0f];
    self.navC = [[NavigationController alloc] initWithRootViewController:self.mainVC];
    self.navC.view.backgroundColor = self.window.backgroundColor;
    [self.navC setNavigationBarHidden:YES];
    [self.window setRootViewController:self.navC];
    [self.window addSubview:self.navC.view];
    [self.window makeKeyAndVisible];
    
    NSDictionary *defaultPrefs = @{
                                   @"fav-interests":@[],
                                   @"lastOpenedTabId":@-1,
                                   @"autoplayOnWiFi":@1,
                                   @"autoplayOnCellular":@0,
                                   @"lastEpisodesForDramaIdDict":@{},
                                   @"ratingsDict":@{},
                                   @"viewsDict":@{},
                                   @"fav-channels":@[]
                                   };
    
    [[ME_Launcher singleton] showLauncherWithAppName:@"Pakistani videos" shortId:@"pv" appUrl:@"itms-apps://itunes.apple.com/app/id485585283" defaultPrefs:defaultPrefs rootViewController:self.navC withCompletion:^{
        [[ME_Logger singleton] log:@"App Launch success."];
        [self startAppWithNavC:self.navC];
    }];
    return YES;
}

// TODO: Choose interests in V 5.0, localisation in v5.1
// on first launch or on reset,
// ask user to choose app language. English or Urdu
// should be able to change app language from settings app on device.
// should be able to reset settings from settings app on device.
// ask user to choose interests.
// allow to rearrange interests easily.
// load interests in tabs.
// each tab is a browse response for that category.
// interests are categories: should be
// News, Dramas, Morning Shows, Movies, Stage Dramas, Music Videos, Cooking Shows etc.
// server should be able to send responses in Urdu as well.
// All content titles should be added in urdu as well.
// croner should prepare responses in urdu as well.

- (void)startAppWithNavC:(UINavigationController *)navC {
    
    // if language and interests are not selected yet, show option to select those, otherwise proceed with app launch.
    
    self.mainVC = [[MainVC alloc] initWithNibName:@"MainVC" bundle:nil];
    [self.mainVC.view setFrame:navC.view.bounds];
    [navC pushViewController:self.mainVC animated:YES];
}

//-(void) startNormalVersion {
//    self.mainVC = [[MainViewController alloc] init];
//    [self.mainVC.view setFrame:self.window.bounds];
//    UINavigationController *navC = [[UINavigationController alloc] initWithRootViewController:self.mainVC];
//    [navC setNavigationBarHidden:YES];
//    [self.window setRootViewController:navC];
//    [self.window addSubview:navC.view];
//}

- (void)applicationWillResignActive:(UIApplication *)application
{
//    BOOL loggedOut = [[UserManager sharedManager] logoutCurrentUserIfNeedsPinAccess];
//    
//    if (loggedOut) {
//        NSNotification *notif = [NSNotification notificationWithName:NOTIFICATION_SWITCH_USER_SELECTED object:nil];
//        [[NSNotificationCenter defaultCenter] postNotification:notif];
//    }
    
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
//    NSNotification *noti = [NSNotification notificationWithName:NOTIFICATION_USER_LOG_OUT object:nil];
//    [[NSNotificationCenter defaultCenter] postNotification:noti];
//    
    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
//    [self checkUpdatedVersions];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    NSNotification *noti2 = [NSNotification notificationWithName:@"reload-if-needed" object:nil];
    [[NSNotificationCenter defaultCenter] postNotification:noti2];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - reachability related

//-(void) setupReachability {
//    Reachability *reachability = [Reachability reachabilityWithHostname:@"www.google.com"];
//    reachability.reachableBlock = ^(Reachability *reachability) {
//
//        NSLog(@"internet connected.");
////        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_INTERNET_CONNECTED object:nil];
//    };
//    reachability.unreachableBlock = ^(Reachability *reachability) {
//
//        NSLog(@"No internet connection.");
////        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NO_INTERNET_CONNECTIVITY object:nil];
//    };
//    // Start Monitoring
//    [reachability startNotifier];
//}

#pragma mark - app update check

//-(void) checkUpdatedVersions {

//    [[DataManager sharedManager] readDictionarywithUrl:@"http://www.anmaple.com/VideoLibrary/versions.json" completion:^(NSMutableDictionary *jsonDict, NSError *error) {
//        if (error) {
//            NSLog(@"Failed to read versions JSON");
//        } else {
//            if (jsonDict) {
//                
////                NSLog(@"Version Dictionary: %@", jsonDict);
//                float latestVersion = [[jsonDict valueForKey:@"LatestAppVersion"] floatValue];
//                float lastSupportedVersion = [[jsonDict valueForKey:@"LastSupportedVersion"] floatValue];
//                float appVersion = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] floatValue];
//                
////                NSLog(@"App version: %.1f, Last Supported Version: %.1f, Latest Version: %.1f", appVersion, lastSupportedVersion, latestVersion);
//                
//                if (appVersion < latestVersion) {
//                    if (appVersion >= lastSupportedVersion) {
//                        [self updatedVersionExists:[NSString stringWithFormat:@"%.1f", latestVersion]];
//                    } else {
//                        [self unsupportedVersion:[NSString stringWithFormat:@"%.1f", latestVersion]];
//                    }
//                } else {
//                    NSString *message = jsonDict[@"message"];
//                    if (message) {
//                        if ([message isEqualToString:@""]) {
//                            [[NSUserDefaults standardUserDefaults] setObject:message forKey:@"LAST_MESSAGE"];
//                            [[NSUserDefaults standardUserDefaults] synchronize];
//                        } else if (![[self lastMessage] isEqualToString:message]) {
//                            [self showMessage:message];
//                            [[NSUserDefaults standardUserDefaults] setObject:message forKey:@"LAST_MESSAGE"];
//                            [[NSUserDefaults standardUserDefaults] synchronize];
//                        }
//                    }
//                }
//            }
//        }
//    }];
//}

//-(NSString *) lastMessage {
//    return [[NSUserDefaults standardUserDefaults] objectForKey:@"LAST_MESSAGE"];
//}

//-(BOOL) shouldDisplayUpdateMessageForSupportedVersion:(BOOL)supported {
//    int numberOfTimesMessageWasDisplayed = [[NSUserDefaults standardUserDefaults] integerForKey:@"KEY_NUM_UPDATE_MESSAGE"];
//    if (numberOfTimesMessageWasDisplayed > (supported?3:5)) {
//        return NO;
//    }
//    
//    NSString *lastMessageDateAsString = [[NSUserDefaults standardUserDefaults] objectForKey:@"KEY_DATE_LAST_MESSAGE"];
//    
//    NSDateFormatter *df = [[NSDateFormatter alloc] init];
//    [df setDateFormat:@"dd MM yy"];
//    NSString *todayDateAsString = [df stringFromDate:[NSDate date]];
//    
//    if ([lastMessageDateAsString isEqualToString:todayDateAsString]) {
//        return NO;
//    }
//    
//    numberOfTimesMessageWasDisplayed++;
//    [[NSUserDefaults standardUserDefaults] setValue:todayDateAsString forKey:@"KEY_DATE_LAST_MESSAGE"];
//    [[NSUserDefaults standardUserDefaults] setInteger:numberOfTimesMessageWasDisplayed forKey:@"KEY_NUM_UPDATE_MESSAGE"];
//    return YES;
//    return NO;
//}

//-(void) updatedVersionExists:(NSString *)updatedVersionNumber {

//    if (![self shouldDisplayUpdateMessageForSupportedVersion:YES]) {
//        return;
//    }
//    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"App update available" message:[NSString stringWithFormat:@"An updated version (%@) is available. Update now?", updatedVersionNumber] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Update", nil];
//    [alert show];
//}

//-(void) showMessage:(NSString *)message {
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//    [alert show];
//}

//-(void) unsupportedVersion:(NSString *)updatedVersionNumber {

//    // unsupported version message will be displayed 5 times. once per day.
//    if (![self shouldDisplayUpdateMessageForSupportedVersion:NO]) {
//        return;
//    }
//    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unsupported app version" message:[NSString stringWithFormat:@"Your app version is no longer supported. Please update to version %@", updatedVersionNumber] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Update", nil];
//    [alert show];
//}

//-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
//    if (buttonIndex == 1) {
//        NSString *url = [[DataManager sharedManager] appURL];
//        [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
//    }
//}

#pragma mark - Methods to deal with an access control, service, info dictionary received from notification.

#define FP_Response_Dict_Info               @"FP_Response_Dict_Info"
#define FP_Response_Dict_Service            @"FP_Response_Dict_Service"
#define FP_Response_Dict_Access_Control     @"FP_Response_Dict_Access_Control"
#define FP_Response_Dict_Ad_Prefs           @"FP_Response_Dict_Ad_Prefs"
#define FP_Response_Dict_Prefs              @"FP_Response_Dict_Prefs"



@end
