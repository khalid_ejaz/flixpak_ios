//
//  Utility.h
//  ContentManager
//
//  Created by Malik Khalid Ejaz on 2013-10-05.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^UtilityURLAndErrorBlock)(NSString *url, NSError *error);
@interface Utility : NSObject

// string tools

+(NSString *)stringByRemovingWhiteSpaces:(NSString *)string;

// color tools
+(UIColor *) colorFromHexString:(NSString *)hexString;
+(NSString *) stringForColor:(UIColor *)color;
+(UIColor *) darkerColorForColor:(UIColor *)color;
+(UIColor *) lighterColorForColor:(UIColor *)color;

// image tools
+(void) compressImage:(UIImage *)image withCompletion:(UtilityURLAndErrorBlock)completion;
+(UIImage*)scaleImage:(UIImage *)image toSize:(CGSize)size;
+(UIImage *) placeHolderImage;

// general messages.
+(void) showDone;
+(void) showError:(NSError *)error;

// date parse
+(NSString *) friendlyDateStringFromDate:(NSDate *)date;
+(NSString *) dateStringFromDate:(NSDate *)date;
+(NSString *) mySQLCompatibleDateStringFromDate:(NSDate *)date;
+(NSDate *) dateFromDateString:(NSString *)dateString;
+(NSDate *) dateFromUNIXDateString:(NSString *)unixDateString;
+(NSDate *) dateDaysAgo:(NSInteger)daysAgo;
+(NSString *) publishedAtStringFromString:(NSString *)dateString;

+(NSString *) durationStringFromYoutubeDuration:(NSString *)duration;
+(NSString *) durationSinceTime:(NSString *)time;

// file paths
+(NSString *) documentsDirPathForFile:(NSString *)fileName;
+(NSString *) cachesDirPathForFile:(NSString *)fileName;

// ratings
+(NSString *) ratingStarsStringForRatings:(int) ratings;

@end


#ifndef PakTV_constants_h
#define PakTV_constants_h

#define IS_IPAD         [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad
#define OS_VERSION      [[UIDevice currentDevice].systemVersion floatValue]
#define IS_FOUR_INCH_IPHONE ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 568.0)
#define IS_IPHONE_6 ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 1334.0)
#define IS_IPHONE_6_PLUS ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 1920.0)

#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width

#define IS_PORTRAIT     !UIDeviceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])
#define VIDEO_TILE_MARGIN   10

// notifications.

#define NOTIFICATION_START_NEW_APP_VERSION  @"NOTIFICATION_START_NEW_APP_VERSION"

#define NOTIFICATION_NO_INTERNET_CONNECTIVITY           @"NOTIFICATION_NO_INTERNET_CONNECTIVITY"
#define NOTIFICATION_INTERNET_CONNECTED                 @"NOTIFICATION_INTERNET_CONNECTED"
#define NOTIFICATION_RELOAD_IF_NEEDED                   @"NOTIFICATION_RELOAD_IF_NEEDED"
#define NOTIFICATION_USER_LOG_OUT                       @"NOTIFICATION_USER_LOG_OUT"
#define NOTIFICATION_TOGGLE_MENU                        @"NOTIFICATION_TOGGLE_MENU"
#define NOTIFICATION_CHANGED_PERSONALISED_CONTENT       @"NOTIFICATION_CHANGED_PERSONALISED_CONTENT"
#define NOTIFICATION_CHANGED_FAVS_FROM_SIDE_MENU        @"NOTIFICATION_CHANGED_FAVS_FROM_SIDE_MENU"
#define NOTIFICATION_TOGGLE_FAV_ON_FORUM        @"NOTIFICATION_TOGGLE_FAV_ON_FORUM"
#define NOTIFICATION_TOGGLE_FAV_ON_CHANNEL              @"NOTIFICATION_TOGGLE_FAV_ON_CHANNEL"
#define NOTIFICATION_SHOW_PREVIEW_IMAGES                @"NOTIFICATION_SHOW_PREVIEW_IMAGES"
#define NOTIFICATION_FLASH_TAG                          @"NOTIFICATION_FLASH_TAG"
#define NOTIFICATION_CHOOSE_FAVS                        @"NOTIFICATION_CHOOSE_FAVS"
#define NOTIFICATION_CHOOSE_WATCH_LATER                 @"NOTIFICATION_CHOOSE_WATCH_LATER"
#define NOTIFICATION_TUTORIAL_FINISHED                  @"NOTIFICATION_TUTORIAL_FINISHED"
#define NOTIFICATION_PLAY_EPISODE                       @"NOTIFICATION_PLAY_EPISODE"
#define NOTIFICATION_SHOW_ALL_EPISODES                  @"NOTIFICATION_SHOW_ALL_EPISODES"
#define NOTIFICATION_USER_ACCOUNT_SELECTED              @"NOTIFICATION_USER_ACCOUNT_SELECTED"
#define NOTIFICATION_EXPLORE_SELECTED_FROM_CAT_CELL     @"NOTIFICATION_EXPLORE_SELECTED_FROM_CAT_CELL"
#define NOTIFICATION_PLAY_VIDEO @"NOTIFICATION_PLAY_VIDEO"

// will use this notification to show login screen when coming back from background and the user needs pin access.
#define NOTIFICATION_SWITCH_USER_SELECTED       @"NOTIFICATION_SWITCH_USER_SELECTED"

// colors.

#define VERY_DARK       32
#define DARK            64
#define MEDIUM          128
#define LIGHT           192
#define VERY_LIGHT      224

#define i0  0
#define i1  31
#define i2  63
#define i3  95
#define i4  127
#define i5  159
#define i6  191
#define i7  223
#define i8  255

#define GRAY_WITH_PERCENT(perc)     [UIColor colorWithRed:perc/100.0 green:perc/100.0 blue:perc/100.0 alpha:1.0]

#define BOOKMARK_LIGHT      RGB(i1,i1,i1);      //PLAIN_COLOR_BLUE_LIGHT
#define BOOKMARK_DARK       RGB(i1,i1,i1);      //PLAIN_COLOR_BLUE_DARK

#define KID_RED             RGBA(i7,i2,i2,1.0);
#define KID_BLUE            RGB(i2,i2,i7);
#define KID_GREEN           RGB(i2,i7,i2);

#define PLAIN_COLOR_BLUE_DARK           RGB(DARK, MEDIUM, MEDIUM);
#define PLAIN_COLOR_BLUE_LIGHT          RGB(DARK, DARK, LIGHT);

#define PLAIN_COLOR_RED_DARK            RGB(MEDIUM, DARK, DARK);
#define PLAIN_COLOR_RED_LIGHT           RGB(LIGHT, DARK, DARK);

#define PLAIN_COLOR_GREEN_DARK          RGB(DARK, MEDIUM, DARK);
#define PLAIN_COLOR_GREEN_LIGHT         RGB(DARK, LIGHT, DARK);

#define PLAIN_COLOR_LIGHT_BLACK         RGB(72.0, 72.0, 72.0);
#define PLAIN_COLOR_BLACK               RGB(52.0, 52.0, 52.0);
#define PLAIN_COLOR_DARKER_BLACK        RGB(30.0, 30.0, 30.0);
#define PLAIN_COLOR_ALMOST_BLACK        RGB(15.0, 15.0, 15.0);

#define PLAIN_COLOR_ALMOST_WHITE        RGB(240.0, 240.0, 240.0);
#define PLAIN_COLOR_WHITE               RGB(220.0, 220.0, 220.0);
#define PLAIN_COLOR_DARKER_WHITE        RGB(180.0, 180.0, 180.0);

#define RGB(r,g,b)  [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:0.8];
#define RGBA(r,g,b,a)  [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a];


#endif
