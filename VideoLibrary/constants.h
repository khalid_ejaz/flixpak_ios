//
//  constants.h
//  PakVideos
//
//  Created by Malik Ejaz on 2016-09-08.
//  Copyright © 2016 Malik Ejaz. All rights reserved.
//

#ifndef constants_h
#define constants_h

#define kIPHONE_VIDEO_VIEW_WIDTH        300
#define kIPHONE_VIDEO_VIEW_HEIGHT       218
#define kIPAD_VIDEO_VIEW_WIDTH          300//320
#define kIPAD_VIDEO_VIEW_HEIGHT         218//240

#define VIDEO_VIEW_MARGIN   5

#define TABLE_CELL_REUSE_ID @"tableCellReuseId"

#define COLLECTION_VIEW_CELL_REUSE_ID   @"collectionCellReuseId"

#define IS_IPAD         [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad
#define OS_VERSION      [[UIDevice currentDevice].systemVersion floatValue]
#define IS_FOUR_INCH_IPHONE ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 568.0)
#define IS_IPHONE_6 ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 1334.0)
#define IS_IPHONE_6_PLUS ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 1920.0)

#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width

#define IS_PORTRAIT     !UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])

#define ITEM_HEIGHT IS_IPAD?kIPAD_VIDEO_VIEW_HEIGHT:kIPHONE_VIDEO_VIEW_HEIGHT
#define ITEM_WIDTH IS_IPAD?kIPAD_VIDEO_VIEW_WIDTH:kIPHONE_VIDEO_VIEW_WIDTH
#define ITEM_WIDTH_LANDSCAPE (IS_IPAD?kIPAD_VIDEO_VIEW_WIDTH:kIPHONE_VIDEO_VIEW_WIDTH)*0.8
#define ROW_HEIGHT IS_IPAD?(kIPAD_VIDEO_VIEW_HEIGHT):(kIPHONE_VIDEO_VIEW_HEIGHT)
// COLORS

#define NAV_BAR_COLOR   [UIColor colorWithRed:73/255.0 green:125/255.0 blue:25/255.0 alpha:1]
#define PASTEL_GREEN_COLOR   [UIColor colorWithRed:(146/255.0) green:211/255.0 blue:110/255.0 alpha:1.0]
#define PASTEL_RED_COLOR   [UIColor colorWithRed:255/255.0 green:105/255.0 blue:97/255.0 alpha:1]
#define PASTEL_BLUE_COLOR   [UIColor colorWithRed:119/255.0 green:258/255.0 blue:203/255.0 alpha:1]

#endif /* constants_h */
