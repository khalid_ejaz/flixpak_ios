//
//  main.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/17/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}