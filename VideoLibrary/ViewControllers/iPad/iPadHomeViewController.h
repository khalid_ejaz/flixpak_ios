//
//  iPadHomeViewController.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/25/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "RootViewController.h"
#import "SwitchControl.h"

@interface iPadHomeViewController : RootViewController <SwitchControlDelegate>

@property (nonatomic, strong) SwitchControl *switchControl;

-(void) updateIfNeeded;
-(void) removePlayerNavC;

@end
