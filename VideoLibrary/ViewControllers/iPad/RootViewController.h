//
//  RootViewController.h
//  ContentManager
//
//  Created by Malik Khalid Ejaz on 2013-10-05.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"
#import "AskPinView.h"
#import "ANMapleBusyView.h"
#import "StyleManager.h"

@class PFQuery;

@interface RootViewController : UIViewController <UIAlertViewDelegate, PinViewDelegate>

@property (nonatomic, assign) BOOL firstLoadDone;

// queries management.
-(void) addQuery:(PFQuery *) query;
-(void) cancelQueries;

// query calls go through self for easy cancellation.
-(void) readContentsWithCategoryId:(NSString *)catId completion:(ArrayAndErrorBlock)completion;
-(void) readContentWithId:(NSString *)contentId completion:(ParseObjectAndErrorBlock)completion;
-(void) readCategoriesWithParentCategoryId:(NSString *) parentCatId completion:(ArrayAndErrorBlock)completion;
-(void) readClassicCategoriesWithCompletion:(ArrayAndErrorBlock)completion;
-(void) readAllCategoriesWithParentCategoryId:(NSString *) parentCatId completion:(ArrayAndErrorBlock)completion;
-(void) readCategoryWithId:(NSString *) catId completion:(ParseObjectAndErrorBlock)completion;
-(void) readRootCategoriesWithChildOnly:(BOOL)childOnly completion:(ArrayAndErrorBlock)completion;

-(void) findByDateString:(NSString *)dateString contentIds:(NSArray *)contentIds orderBy:(NSString *)sortField ascending:(BOOL)ascending completion:(ArrayAndErrorBlock)completion;
-(void) findByDateString:(NSString *)dateString categoryId:(NSString *)categoryId orderBy:(NSString *)sortField ascending:(BOOL)ascending completion:(ArrayAndErrorBlock)completion;
-(void) findByContentId:(NSString *)contentId orderBy:(NSString *)sortField ascending:(BOOL)ascending skip:(int)skip completion:(ArrayAndErrorBlock)completion;
-(void) findByEpisodeTitle:(NSString *)title categoryId:(NSString *)categoryId contentId:(NSString *)contentId completion:(ArrayAndErrorBlock)completion;

// category update reads.
-(void) updatesWithCategoryId:(NSString *)categoryId skip:(int)skip completion:(ArrayAndErrorBlock)completion;

// content update reads.
-(void) updatesWithContentIds:(NSArray *)contentIds completion:(ArrayAndErrorBlock)completion;

// youtube channel reads.
-(void) readChannelsWithCategoryId:(NSString *)categoryId completion:(ArrayAndErrorBlock)completion;

// mapping reads.
-(void) readMappingsWithCompletion:(ArrayAndErrorBlock)completion;

// version reads.
-(void) readVersionsWithCompletion:(ArrayAndErrorBlock)completion;

// Forum reads.
-(void) readForumsWithCompletion:(ArrayAndErrorBlock)completion;
// user messages and errors.
// TODO: add alert view designs matching app UI.
-(void) showError:(NSError *) error;
-(void) showAlertWithTitle:(NSString *)title message:(NSString *)message;
-(void) showTipForAction:(NSString *)action;
-(void) showGeneralError;
-(void) showNetworkError;
-(void) showDone;
-(void) showInternetNeededIfNotAvailable;

// uploader progress view.
-(void) showProgressViewWithTitle:(NSString *)title;
-(void) addProgressMessage:(NSString *)message color:(UIColor *)color;
-(void) hideProgressView;
-(void) hideProgressViewIfNoRedMessages;
-(void) saveProgressMessageAsName:(NSString *)name AndReset:(BOOL)reset;

// blocking overlay related.
-(void) showOverlay;
-(void) hideOverlay;

// pin view related.
-(void) askPinWithUser:(User *)user;
-(void) removePinView;

// spinner related.
-(void) showSpinner;
-(void) hideSpinner;
-(void) keepBusyIndicatorOnTop;
-(void) showLightWeightSpinnerGray:(BOOL) gray;
-(void) showLightWeightSpinnerGray:(BOOL) gray atPoint:(CGPoint) point;
-(void) hideLightWeightSpinner;

// helper methods.
-(BOOL) isPortrait;

@end
