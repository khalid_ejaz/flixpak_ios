//
//  RightSidePanelViewController.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/25/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "RightSidePanelViewController.h"
#import "SwitchOption.h"
#import "Utility.h"
#import "BrowseViewController.h"
#import "WatchLaterVC.h"

#define SWITCH_OPTION_BROWSE            @"browse"
#define SWITCH_OPTION_WATCH_LATER       @"watchLater"
#define SWITCH_OPTION_LIST_FAVORITES    @"listFavorites"

@interface RightSidePanelViewController ()
@property (unsafe_unretained, nonatomic) IBOutlet UIView *switchContainerView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *contentView;

@property (nonatomic, strong) UINavigationController *navController;

@property (nonatomic, strong) BrowseViewController *browseVC;
@property (nonatomic, strong) BrowseViewController *listFavsVC;
@property (nonatomic, strong) WatchLaterVC *watchLaterVC;

@end

@implementation RightSidePanelViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addNavigationController];
    [self setupSwitchControl];
}

-(void) addNavigationController {
    
    UIViewController *rootVC = [[UIViewController alloc] init];
    [rootVC.view setFrame:self.contentView.bounds];
    [rootVC.view setBackgroundColor:[UIColor blackColor]];
    
    self.navController = [[UINavigationController alloc] initWithRootViewController:rootVC];
    [self.navController.view setFrame:self.contentView.bounds];
    [self.contentView addSubview:self.navController.view];
    [self addChildViewController:self.navController];
}
-(void) setupSwitchControl {
    
    SwitchOption *opt1 = [[SwitchOption alloc] initWithDisplayText:@"Browse" value:@"browse" ascending:NO];
    SwitchOption *opt2 = [[SwitchOption alloc] initWithDisplayText:@"Watch Later" value:@"watchLater" ascending:NO];
    SwitchOption *opt3 = [[SwitchOption alloc] initWithDisplayText:@"List Favorites" value:@"listFavorites" ascending:NO];
    
    NSArray *switchOptions = @[opt1, opt2, opt3];
    
    self.switchControl = [[NSBundle mainBundle] loadNibNamed:@"SwitchControl" owner:self options:nil][0];
    [self.switchControl setFrame:self.switchContainerView.bounds];
//    self.switchControl.delegate = self;
    [self.switchContainerView addSubview:self.switchControl];
    
    [self.switchControl populateWithSwitchOptions:switchOptions controlBackColor:GRAY_WITH_PERCENT(6) optionBackColor:GRAY_WITH_PERCENT(13) textColor:GRAY_WITH_PERCENT(66) showingSort:NO];
}

-(void) loadBrowser {
    if (!self.browseVC) {
        self.browseVC = [[BrowseViewController alloc] initWithNibName:@"BrowseViewController" bundle:nil];
        [self.browseVC setSelectedCategoryId:@"root"];
    }
    [self.navController popToRootViewControllerAnimated:NO];
    [self.navController pushViewController:self.browseVC animated:YES];
}

-(void) loadFavsList {
    if (!self.listFavsVC) {
        self.listFavsVC = [[BrowseViewController alloc] initWithNibName:@"BrowseViewController" bundle:nil];
    }
    
    [self.listFavsVC setShowingLocalFavs:YES];
    [self.navController popToRootViewControllerAnimated:NO];
    [self.navController pushViewController:self.listFavsVC animated:YES];
}

-(void) loadWatchLaterList {
    if (!self.watchLaterVC) {
        self.watchLaterVC = [[WatchLaterVC alloc] initWithNibName:@"ViewerVC" bundle:nil];
    }
    
    [self.navController popToRootViewControllerAnimated:NO];
    [self.navController pushViewController:self.watchLaterVC animated:YES];
    [self.watchLaterVC loadData];
}

#pragma mark - switch control delegate

-(void) switchControl:(id)control selectedOption:(SwitchOption *)option {
    if ([option.value isEqualToString:SWITCH_OPTION_BROWSE]) {
        [self loadBrowser];
    } else if ([option.value isEqualToString:SWITCH_OPTION_LIST_FAVORITES]) {
        [self loadFavsList];
    } else if ([option.value isEqualToString:SWITCH_OPTION_WATCH_LATER]) {
        [self loadWatchLaterList];
    }
}

@end
