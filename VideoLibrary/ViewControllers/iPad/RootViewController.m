//
//  RootViewController.m
//  ContentManager
//
//  Created by Malik Khalid Ejaz on 2013-10-05.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import "RootViewController.h"
#import <Reachability.h>
#import "Utility.h"
#import "UserManager.h"

@interface RootViewController ()

@property (nonatomic, strong) NSMutableArray *queries;
@property (nonatomic, strong) UIView *progressOverlayView;

@property (nonatomic, strong) UIView *blockingOverlayView;

@property (nonatomic, strong) AskPinView *pinView;

// spinner related
@property (strong, nonatomic) UIActivityIndicatorView *lightWeightSpinner;
@property (strong, nonatomic) ANMapleBusyView *spinner;

@end

@implementation RootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.queries = [NSMutableArray array];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self cancelQueries];
}

-(void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    if (self.pinView) {
        self.pinView.center = CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2 - 100);
    }
}

#pragma mark - Queries related

-(void) addQuery:(PFQuery *) query {
    [self.queries addObject:query];
//    NSLog(@"Active queries: %ld", (unsigned long)self.queries.count);
}

-(void) removeQuery:(PFQuery *)query {
    [self.queries removeObject:query];
//    NSLog(@"Active queries: %ld", (unsigned long)self.queries.count);
}

-(void) cancelQueries {
//    for (PFQuery *query in self.queries) {
//        [query cancel];
//    }
//    self.queries = [NSMutableArray array];
}

#pragma mark - Parse object read methods. go through here so can be easily cancelled.
#pragma mark - Content reads.

-(void) readContentsWithCategoryId:(NSString *)catId completion:(ArrayAndErrorBlock)completion {
//    __block PFQuery *query = [Content readContentsWithCategoryId:catId completion:^(NSArray *objects, NSError *error) {
//        completion(objects, error);
//        [self removeQuery:query];
//    }];
//    [self addQuery:query];
}

-(void) readContentWithId:(NSString *)contentId completion:(ParseObjectAndErrorBlock)completion {
//    [Content readContentWithId:contentId completion:completion];
}

#pragma mark - Category reads.

-(void) readCategoriesWithParentCategoryId:(NSString *) parentCatId completion:(ArrayAndErrorBlock)completion {
//    __block PFQuery *query = [ContentCategory readAllCategoriesWithParentCategoryId:parentCatId completion:^(NSArray *objects, NSError *error) {
//        completion(objects, error);
//        [self removeQuery:query];
//    }];
//    [self addQuery:query];
}

-(void) readClassicCategoriesWithCompletion:(ArrayAndErrorBlock)completion {
//    __block PFQuery *query = [ContentCategory readClassicCategoriesWithCompletion:^(NSArray *objects, NSError *error) {
//        completion(objects, error);
//        [self removeQuery:query];
//    }];
//    [self addQuery:query];
}

-(void) readAllCategoriesWithParentCategoryId:(NSString *) parentCatId completion:(ArrayAndErrorBlock)completion {
//    __block PFQuery *query = [ContentCategory readAllCategoriesWithParentCategoryId:parentCatId completion:^(NSArray *objects, NSError *error) {
//        completion(objects, error);
//        [self removeQuery:query];
//    }];
//    [self addQuery:query];
}

-(void) readCategoryWithId:(NSString *) catId completion:(ParseObjectAndErrorBlock)completion {
//    __block PFQuery *query = [ContentCategory readCategoryWithId:catId completion:^(PFObject *object, NSError *error) {
//        completion(object, error);
//        [self removeQuery:query];
//    }];
//    [self addQuery:query];
}

-(void) readRootCategoriesWithChildOnly:(BOOL)childOnly completion:(ArrayAndErrorBlock)completion {
//    __block PFQuery *query = [ContentCategory readRootCategoriesWithChildOnly:childOnly completion:^(NSArray *objects, NSError *error) {
//        completion(objects, error);
//        [self removeQuery:query];
//    }];
//    [self addQuery:query];
}

#pragma mark - Episode reads.

-(void) findByEpisodeTitle:(NSString *)title categoryId:(NSString *)categoryId contentId:(NSString *)contentId completion:(ArrayAndErrorBlock)completion {
//    __block PFQuery *query = [Episode findByEpisodeTitle:title categoryId:categoryId contentId:contentId completion:^(NSArray *objects, NSError *error) {
//        completion(objects, error);
//        [self removeQuery:query];
//    }];
//    [self addQuery:query];
}

-(void) findByDateString:(NSString *)dateString categoryId:(NSString *)categoryId orderBy:(NSString *)sortField ascending:(BOOL)ascending completion:(ArrayAndErrorBlock)completion {
//    __block PFQuery *query = [Episode findByDateString:dateString categoryId:categoryId orderBy:sortField ascending:ascending completion:^(NSArray *objects, NSError *error) {
//        completion(objects, error);
//        [self removeQuery:query];
//    }];
//    [self addQuery:query];
}

-(void) findByDateString:(NSString *)dateString contentIds:(NSArray *)contentIds orderBy:(NSString *)sortField ascending:(BOOL)ascending completion:(ArrayAndErrorBlock)completion {
//    __block PFQuery *query = [Episode findByDateString:dateString contentIds:contentIds orderBy:sortField ascending:ascending completion:^(NSArray *objects, NSError *error) {
//        completion(objects,error);
//        [self removeQuery:query];
//    }];
//    [self addQuery:query];
}

-(void) findByContentId:(NSString *)contentId orderBy:(NSString *)sortField ascending:(BOOL)ascending skip:(int)skip completion:(ArrayAndErrorBlock)completion {
//    __block PFQuery *query = [Episode findByContentId:contentId orderBy:sortField ascending:ascending skip:skip completion:^(NSArray *objects, NSError *error) {
//        completion(objects, error);
//        [self removeQuery:query];
//    }];;
//    [self addQuery:query];
}

#pragma mark - Category update reads.

-(void) updatesWithCategoryId:(NSString *)categoryId skip:(int)skip completion:(ArrayAndErrorBlock)completion {
//    __block PFQuery *query = [CategoryUpdate updatesWithCategoryId:categoryId skip:skip completion:^(NSArray *objects, NSError *error) {
//        [self removeQuery:query];
    
        // make unique.
//        objects = [[DataManager sharedManager] uniqueObjectsFromArray:objects withKeyColumn:KEY_CAT_UPDATE_UPDATED_ON];
        
//        completion(objects, error);
//    }];
//    [self addQuery:query];
}

#pragma mark - Content update reads.

-(void) updatesWithContentIds:(NSArray *)contentIds completion:(ArrayAndErrorBlock)completion {
//    __block PFQuery *query = [ContentUpdate updatesWithContentIds:contentIds completion:^(NSArray *objects, NSError *error) {
//        [self removeQuery:query];
//        completion(objects, error);
//    }];
//    [self addQuery:query];
}

#pragma mark - Youtube channel reads

-(void) readChannelsWithCategoryId:(NSString *)categoryId completion:(ArrayAndErrorBlock)completion {
//    __block PFQuery *query = [YoutubeChannel readChannelsWithCategoryId:categoryId completion:^(NSArray *objects, NSError *error) {
//        completion(objects, error);
//        [self removeQuery:query];
//    }];
//    [self addQuery:query];
}

#pragma mark - Version reads.

-(void) readVersionsWithCompletion:(ArrayAndErrorBlock)completion {
//    __block PFQuery *query = [Version readVersionsWithCompletion:^(NSArray *objects, NSError *error) {
//        [self removeQuery:query];
//        completion(objects, error);
//    }];
//    [self addQuery:query];
}

#pragma mark - Forum Reads

-(void) readForumsWithCompletion:(ArrayAndErrorBlock)completion {
//    __block PFQuery *query = [Forum readForumsWithCompletion:^(NSArray *objects, NSError *error) {
//        [self removeQuery:query];
//        completion(objects, error);
//    }];
//    [self addQuery:query];
}

#pragma mark - user messages.

-(void) showError:(NSError *) error {
    [self showNetworkError];
//    if (error.localizedRecoverySuggestion) {
//        [self showAlertWithTitle:@"An error occured" message:error.localizedDescription];
//    } else {
//        [self showAlertWithTitle:@"An error occured" message:error.description];
//    }
}

-(void) showDone {
    [self showAlertWithTitle:@"" message:@"Done !"];
}

-(void) showGeneralError {
    [self showAlertWithTitle:@"Oops" message:@"Something went wrong!"];
    [self hideSpinner];
}

-(void) showNetworkError {
    [self showAlertWithTitle:@"Oops" message:@"A network error occured. Please check your internet connectivity and try again."];
    [self hideSpinner];
}

-(void) showInternetNeededIfNotAvailable {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable) {
        // do work here if the user has a valid connection
        [self showNetworkError];
    }
}

-(void) showTipForAction:(NSString *)action {
    NSString *tip = [[UserManager sharedManager] TipForAction:action];
    if (!tip) {
        return;
    } else [self showAlertWithTitle:@"Tip" message:tip];
}

-(void) showAlertWithTitle:(NSString *)title message:(NSString *)message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}

#pragma mark - alert view delegate

-(void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
//    if ([[DataManager sharedManager] uploaderApp]) {
//        [self.progressView addDoneButton];
//    }
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
}


#pragma mark - progress view delegate

-(void) remove {
    [self hideProgressView];
}

#pragma mark - pin related.

-(void) askPinWithUser:(User *)user {
    
    if (self.pinView) {
        [self removePinView];
        return;
    }
    
    // TODO: not 5 minutes passed from 3 incorrect attempts
    if (NO) {
        
    }
    else {
        self.pinView = [[NSBundle mainBundle] loadNibNamed:@"AskPinView" owner:self options:nil][0];
        self.pinView.center = self.view.center;
        [self showOverlay];
        [self.view addSubview:self.pinView];
        [self.pinView setUser:user delegate:self];
    }
}

-(void) removePinView {
    [self hideOverlay];
    [self.pinView removeFromSuperview];
    self.pinView = nil;
}

#pragma mark - pin view delegate

-(void) pinViewAllowedAccessToUser:(User *)user {
    abort();
}

-(void) pinViewdeniedAccessToUser:(User *)user {
    abort();
}

-(void) pinViewWantsToShowMessage:(NSString *)message {
    [self showAlertWithTitle:@"" message:message];
}

#pragma mark - blocking overlay related.

-(void) showOverlay {
    if (!self.blockingOverlayView) {
        self.blockingOverlayView = [[UIView alloc] init];
        self.blockingOverlayView.frame = self.view.bounds;
        [self.blockingOverlayView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        self.blockingOverlayView.backgroundColor = GRAY_WITH_PERCENT(20);
        self.blockingOverlayView.alpha = 0.1;
        [self.view addSubview:self.blockingOverlayView];
        [UIView animateWithDuration:0.3 animations:^{
            self.blockingOverlayView.alpha = 0.7;
        }];
    }
}

-(void) hideOverlay {
    if (!self.blockingOverlayView) {
        return;
    } else {
        
        [UIView animateWithDuration:0.3 animations:^{
            self.blockingOverlayView.alpha = 0.1;
        } completion:^(BOOL finished) {
            [self.blockingOverlayView removeFromSuperview];
            self.blockingOverlayView = nil;
        }];
    }
}

#pragma mark - auto rotation related.

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(BOOL)shouldAutorotate
{
    return YES;
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}
#endif

#pragma mark - busy spinner related.

-(void) setupSpinner {
    self.spinner = [[NSBundle mainBundle] loadNibNamed:@"ANMapleBusyView" owner:self options:nil][0];
}

-(void) showSpinner {
    if (!self.spinner) {
        [self setupSpinner];
    }
    
    if ([self showingSpinner]) {
        return;
    }
    [self.spinner setFrame:self.view.bounds];
    [self.view addSubview:self.spinner];
    [self.spinner startSpinner];
//    [self.view bringSubviewToFront:self.adPlaceholderView];
}

-(void) hideSpinner {
    if (![self showingSpinner] | !self.spinner) {
        return;
    }
    
    [self.spinner stopSpinner];
}

-(void) keepBusyIndicatorOnTop {
//    [self.view bringSubviewToFront:self.spinner];
//    [self.view bringSubviewToFront:self.adPlaceholderView];
}

-(BOOL) showingSpinner {
    return (self.spinner.superview != nil);
}

#pragma mark - helper methods.

-(BOOL) isPortrait {
    return ([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortrait || [[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortraitUpsideDown);
//    return (self.interfaceOrientation == UIInterfaceOrientationPortrait | self.interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}

@end
