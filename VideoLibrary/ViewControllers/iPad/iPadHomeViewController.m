//
//  iPadHomeViewController.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/25/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "iPadHomeViewController.h"
#import "BrowseViewController.h"
#import "ContentBrowserViewController.h"
#import "WatchLaterVC.h"
#import "CustomVC.h"
#import "YoutubeVideosVC.h"
#import "CategoryViewerVC.h"
#import "PlayerVC.h"
#import "PlayerNavC.h"
#import "ForumWebVC.h"

#import "UserManager.h"
#import "ME_AdManager.h"

#define HOME_HEADER_HEIGHT          64
#define CONTENT_VIEW_WIDTH          704
#define SIDE_PANEL_WIDTH            320

#define SIDE_PANEL_GAP              2

@interface iPadHomeViewController ()
@property (unsafe_unretained, nonatomic) IBOutlet UIView *headerView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *favContentView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *watchLaterContentView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *noFavsView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *noWatchLaterView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *watchLaterHeader;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *rightPanelView;

@property (unsafe_unretained, nonatomic) IBOutlet UIView *switchControlView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *contentView;

@property (nonatomic, strong) BrowseViewController *favBrowser;
@property (nonatomic, strong) CustomVC *customVC;
@property (nonatomic, strong) CategoryViewerVC *catVC;
@property (nonatomic, strong) YoutubeVideosVC *youtubeVC;
@property (nonatomic, strong) PlayerNavC *playerNavC;

@property (nonatomic, strong) BrowseViewController *browseVC;
@property (nonatomic, strong) UINavigationController *browseNavC;
@property (nonatomic, strong) BrowseViewController *listFavsVC;
@property (nonatomic, strong) UINavigationController *listFavsNavC;
@property (nonatomic, strong) WatchLaterVC *watchLaterVC;

@property (nonatomic, assign) BOOL animateFrameChange;
@property (nonatomic, assign) BOOL allowRightPanelToggle;

@end

@implementation iPadHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.headerView.backgroundColor = [[UserManager sharedManager] colorForUser];
    self.allowRightPanelToggle = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showTipForWatchLater) name:@"NOTIFICATION_ADDED_TO_WATCH_LATER" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FavouriteChangedFromSideMenu:) name:NOTIFICATION_CHANGED_FAVS_FROM_SIDE_MENU object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(favouriteChannelChanged:) name:NOTIFICATION_TOGGLE_FAV_ON_CHANNEL object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(favouriteForumChanged:) name:NOTIFICATION_TOGGLE_FAV_ON_FORUM object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(personalisedContentChanged:) name:NOTIFICATION_CHANGED_PERSONALISED_CONTENT object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playEpisode:) name:NOTIFICATION_PLAY_EPISODE object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playVideo:) name:NOTIFICATION_PLAY_VIDEO object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showPreviousEpisodes:) name:NOTIFICATION_SHOW_ALL_EPISODES object:nil];
    
    [self setupSwitchControl];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.headerView.backgroundColor = [[UserManager sharedManager] colorForUser];
    if (self.switchControl) {
        [self.switchControl changeBackColorTo:[Utility darkerColorForColor:[[UserManager sharedManager] colorForUser]] OptionBackColor:[[UserManager sharedManager] colorForUser] textColor:GRAY_WITH_PERCENT(80)];
    }
    
//    if ([[UserManager sharedManager] favouritesChanged]) {
//        if (self.customVC) {
//            [self.customVC loadData];
//        }
//    }
    
    if (self.firstLoadDone) {
        [self updateIfNeeded];
    } else self.firstLoadDone = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self.switchControl reLayout];
}

#pragma mark - IBActions

- (IBAction)chooseFavsAction:(id)sender {
    NSNotification *notification = [NSNotification notificationWithName:NOTIFICATION_CHOOSE_FAVS object:nil];
    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

- (IBAction)toggleMenu:(id)sender {
    NSNotification *notification = [NSNotification notificationWithName:NOTIFICATION_TOGGLE_MENU object:nil];
    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

#pragma mark - Switch Control related

-(void) setupSwitchControl {
    [self hideSwitch];
    
    NSArray *switchOptions = [[UserManager sharedManager] switchOptionsFromFavCategories];
    if (switchOptions.count == 0) {
        self.noFavsView.hidden = NO;
    } else {
        self.noFavsView.hidden = YES;
    }
    
    if (!self.switchControl) {
        self.switchControl = [[NSBundle mainBundle] loadNibNamed:@"SwitchControl" owner:self options:nil][0];
        [self.switchControl setFrame:self.switchControlView.bounds];
        self.switchControl.delegate = self;
        [self.switchControlView addSubview:self.switchControl];
        
        NSInteger index = 0;
        NSString *lastTab = [[UserManager sharedManager] getLastSelectedHomeTab];
        for (int i=0; i<switchOptions.count; i++) {
            SwitchOption *option = switchOptions[i];
            if ([option.value isEqualToString:lastTab]) {
                index = i;
                break;
            }
        }
        
        [self.switchControl populateWithSwitchOptions:switchOptions controlBackColor:[[UserManager sharedManager] darkerColorForUser] optionBackColor:[[UserManager sharedManager] colorForUser] textColor:GRAY_WITH_PERCENT(80) showingSort:NO selectIndex:index];
        
    } else {
        self.switchControl.hidden = NO;
    }
}

-(void) hideSwitch {
    if (self.switchControl) {
        self.switchControl.hidden = YES;
    }
}

-(void) removeAllViewControllers {
    for (UIViewController *viewController in self.childViewControllers) {
        [viewController removeFromParentViewController];
        [viewController.view removeFromSuperview];
    }
    self.noFavsView.hidden = NO;
}

#pragma mark - switch control delegate

-(void) switchControl:(id)control selectedOption:(SwitchOption *)option {
    
    for (UIViewController *viewController in self.childViewControllers) {
        [viewController removeFromParentViewController];
        [viewController.view removeFromSuperview];
    }
    
    // TODO: save selected option.
    [[UserManager sharedManager] saveLastSelectedHomeTab:option.value];
    
    if ([option.value isEqualToString:SWITCH_OPTION_BROWSE]) {
        // show browse.
        
        self.browseVC = [[BrowseViewController alloc] initWithNibName:@"BrowseViewController" bundle:nil];
        [self.browseVC setSelectedCategoryId:@"root"];
        [self.browseVC.view setFrame:self.contentView.bounds];
        
        self.browseNavC = [[UINavigationController alloc] initWithRootViewController:self.browseVC];
        [self.browseNavC.view setFrame:self.contentView.bounds];
        
        [self.contentView addSubview:self.browseNavC.view];
        
        [self addChildViewController:self.browseNavC];
        
    } else if ([option.value isEqualToString:SWITCH_OPTION_WATCH_LATER]) {
        // show watch later.
        
        self.watchLaterVC = [[WatchLaterVC alloc] initWithNibName:@"ViewerVC" bundle:nil];
        [self.watchLaterVC.view setFrame:self.contentView.bounds];
        
        [self.contentView addSubview:self.watchLaterVC.view];
        [self addChildViewController:self.watchLaterVC];

        [self.watchLaterVC loadData];
    } else if ([option.value isEqualToString:SWITCH_OPTION_LIST_FAVORITES]) {
        // show list of favorites.
        
        self.listFavsVC = [[BrowseViewController alloc] initWithNibName:@"BrowseViewController" bundle:nil];
        [self.listFavsVC.view setFrame:self.contentView.bounds];
        
        self.listFavsNavC = [[UINavigationController alloc] initWithRootViewController:self.listFavsVC];
        [self.listFavsNavC.view setFrame:self.contentView.bounds];
        [self.contentView addSubview:self.listFavsNavC.view];
        [self addChildViewController:self.listFavsNavC];
        
        [self.listFavsVC setShowingLocalFavs:YES];
    } else if ([option.value isEqualToString:SWITCH_VALUE_FAV]) {
        // show personalized.
        
        self.customVC = [[CustomVC alloc] initWithNibName:@"ViewerVC" bundle:nil];
        [self.customVC.view setFrame:self.contentView.bounds];
        [self.contentView addSubview:self.customVC.view];
        
        [self addChildViewController:self.customVC];
        [self.customVC loadData];
        
    } else if ([option.value rangeOfString:@"*YT_"].location != NSNotFound) {
        // show youtube channel.
        
        NSString *channelId = [option.value stringByReplacingOccurrencesOfString:@"*YT_" withString:@""];
        
        self.youtubeVC = [[YoutubeVideosVC alloc] initWithNibName:@"ViewerVC" bundle:nil];
        [self.youtubeVC.view setFrame:self.contentView.bounds];
        [self.contentView addSubview:self.youtubeVC.view];
        
        [self addChildViewController:self.youtubeVC];
        [self.youtubeVC setChannelId:channelId];
        [self.youtubeVC setShowingChannel:YES];
        [self.youtubeVC loadData];
        
    } else if ([option.value rangeOfString:@"*web_"].location != NSNotFound) {
        
        NSString *forumURL = [option.value stringByReplacingOccurrencesOfString:@"*web_" withString:@""];
        
        ForumWebVC *forumWebVC = [[ForumWebVC alloc] initWithNibName:@"ForumWebVC" bundle:nil];
        [forumWebVC.view setFrame:self.contentView.bounds];
        [forumWebVC setForumURL:forumURL];
        [self.contentView addSubview:forumWebVC.view];
        [self addChildViewController:forumWebVC];
        
    } else {
        // show category.
        
        self.catVC = [[CategoryViewerVC alloc] initWithNibName:@"ViewerVC" bundle:nil];
        [self.catVC setCanShowFavs:YES];
        [self.catVC.view setFrame:self.contentView.bounds];
        [self.contentView addSubview:self.catVC.view];
        
        [self.catVC setCategoryId:option.value];
        [self addChildViewController:self.catVC];
        [self.catVC loadData];
    }
}

-(void) gotOptionForSwitchControl:(id)control {
    [self setupSwitchControl];
    self.noFavsView.hidden = YES;
}

-(void) removedAllOptionsForSwitchControl:(id)control {
    [self hideSwitch];
    [self removeAllViewControllers];
}

#pragma mark - notification selectors

-(void) showTipForWatchLater {
    [self showTipForAction:TIP_ACTION_WATCH_LATER];
}

-(void) FavouriteChangedFromSideMenu:(NSNotification *)notification {
//    ContentCategory *category = notification.userInfo[@"category"];
//    
//    NSString *selected = notification.userInfo[@"selected"];
//    if ([selected isEqualToString:@"yes"]) {
//        SwitchOption *option = [[SwitchOption alloc] initWithDisplayText:category.categoryName value:category.objectId ascending:NO];
//        [self.switchControl addOption:option];
//        [self showTipForAction:TIP_ACTION_TAG_CATEGORY];
//    } else {
//        [self.switchControl removeOptionByValue:category.objectId];
//    }
}

-(void) favouriteChannelChanged:(NSNotification *)notification {
//    YoutubeChannel *channel = notification.userInfo[@"channel"];
//    NSString *selected = notification.userInfo[@"selected"];
//    
//    if ([selected isEqualToString:@"yes"]) {
//        SwitchOption *opt = [[SwitchOption alloc] initWithDisplayText:[[UserManager sharedManager] switchDisplayTextForYoutubeChannelName:channel.channelName] value:[[UserManager sharedManager] switchValueForYoutubeChannelId:channel.channelId] ascending:YES];
//        [self.switchControl addOption:opt];
//    } else {
//        [self.switchControl removeOptionByValue:[[UserManager sharedManager] switchValueForYoutubeChannelId:channel.channelId]];
//    }
//    if (selected) {
//        [self showTipForAction:TIP_ACTION_TAG_CHANNEL];
//    }
}

-(void) favouriteForumChanged:(NSNotification *)notification {
//    Forum *forum = notification.userInfo[@"forum"];
//    NSString *selected = notification.userInfo[@"selected"];
//    
//    if ([selected isEqualToString:@"yes"]) {
//        SwitchOption *opt = [[SwitchOption alloc] initWithDisplayText:[[UserManager sharedManager] switchDisplayTextForYoutubeChannelName:forum.forumName] value:[[UserManager sharedManager] switchValueForForumURL:forum.forumURL] ascending:YES];
//        [self.switchControl addOption:opt];
//    } else {
//        [self.switchControl removeOptionByValue:[[UserManager sharedManager] switchValueForForumURL:forum.forumURL]];
//    }
//    if (selected) {
//        [self showTipForAction:TIP_ACTION_TAG_FORUM];
//    }
}

-(void) personalisedContentChanged:(NSNotification *)notification {
    
//    if ([self.switchControl containsOptionWithValue:SWITCH_VALUE_FAV]) {
//        if ([[[UserManager sharedManager] favContents] allKeys].count == 0) {
//            // removed all personalised content.
//            [self.switchControl removeOptionByValue:SWITCH_VALUE_FAV];
//        }
//        // will auto reload when user selects the tab.
//        return;
//    } else {
//        if ([[[UserManager sharedManager] favContents] allKeys].count != 0) {
//            // removed all personalised content.
//            [self.switchControl addOption:[[SwitchOption alloc] initWithDisplayText:HEART_SYMBOL value:SWITCH_VALUE_FAV ascending:NO]];
//        }
//    }
}

-(void) playEpisode:(NSNotification *)notification {
//    [self removePlayerNavC];
//    
//    [[ANAdManager sharedManager] removeAds];
//    Episode *episode = notification.userInfo[@"Episode"];
//    PlayerVC *playerVC = [[PlayerVC alloc] initWithNibName:@"PlayerVC" bundle:nil];
//    [playerVC setEpisode:episode];
//    [playerVC.view setFrame:self.view.bounds];
//    self.playerNavC = [[PlayerNavC alloc] initWithRootViewController:playerVC];
//    self.playerNavC.view.frame = self.view.bounds;
//    self.playerNavC.navigationBarHidden = YES;
//    [self presentViewController:self.playerNavC animated:YES completion:nil];
}

-(void) removePlayerNavC {
//    if (self.playerNavC) {
//        if (self.playerNavC.view.window) {
//            [self.playerNavC dismissViewControllerAnimated:YES completion:nil];
//        }
//    }
}

-(void) playVideo:(NSNotification *)notification {
    [self removePlayerNavC];
    [[ME_AdManager sharedManager] removeAds];
    
    YoutubeVideo *video = notification.userInfo[@"video"];
    PlayerVC *playerVC = [[PlayerVC alloc] initWithNibName:@"PlayerVC" bundle:nil];
    [playerVC setVideo:video];
    [playerVC.view setFrame:self.navigationController.view.bounds];
    self.playerNavC = [[PlayerNavC alloc] initWithRootViewController:playerVC];
    self.playerNavC.view.frame = self.view.bounds;
    self.playerNavC.navigationBarHidden = YES;
//    [self presentModalViewController:self.playerNavC animated:YES];
    [self presentViewController:self.playerNavC animated:YES completion:nil];
//    [self.navigationController pushViewController:playerVC animated:YES];
}

-(void) showPreviousEpisodes:(NSNotification *)notification {
//    Episode *episode = notification.userInfo[@"Episode"];
//    ContentBrowserViewController *contentVC = [[ContentBrowserViewController alloc] initWithNibName:@"ContentBrowserViewController" bundle:nil];
//    [contentVC setContentId:episode.contentId];
//    [contentVC.view setFrame:self.view.bounds];
//    [self.navigationController pushViewController:contentVC animated:YES];
}

#pragma mark - versions related

-(void) reloadIfVisible {
    if (self.catVC) {
        [self.catVC loadData];
    }
}

-(void) updateIfNeeded {
    
    if (!self.catVC) {
        return;
    }
    NSString *categoryId = self.catVC.categoryId;
    
//    [Version readVersionsWithCompletion:^(NSArray *objects, NSError *error) {
//        if (!error) {
//            
//            if ([[UserManager sharedManager] versionsDict].allKeys.count == 0) {
//                [self updateDictionaryWithVersions:objects alreadyUpdated:NO];
//            } else {
//                for (Version *version in objects) {
//                    if ([version.categoryId isEqualToString:categoryId]) {
//                        int ver = [version.versionNumber integerValue];
//                        int existingVer = [[[[UserManager sharedManager] versionsDict] objectForKey:categoryId] integerValue];
//                        if (ver > existingVer) {
//                            NSLog(@"update needed");
//                            [self updateDictionaryWithVersions:objects alreadyUpdated:NO];
//                        } else [self updateDictionaryWithVersions:objects alreadyUpdated:YES];
//                    }
//                }
//            }
//        }
//    }];
}

-(void) updateDictionaryWithVersions:(NSArray *)versions alreadyUpdated:(BOOL)updated {
//    NSMutableDictionary *versionsDict = [[UserManager sharedManager] versionsDict];
//    for (Version *version in versions) {
//        [versionsDict setObject:version.versionNumber forKey:version.categoryId];
//    }
//    
//    if (!updated) {
//        [self reloadIfVisible];
//    }
//    [[UserManager sharedManager] saveVersions];
}

@end
