//
//  iPhoneHomeViewController.h
//  PakTV
//
//  Created by Malik Khalid Ejaz on 2013-09-24.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import "RootViewController.h"
#import "SideMenuViewController.h"
#import "SwitchControl.h"

@class Content;

@interface iPhoneHomeViewController : RootViewController <SwitchControlDelegate>

@property (nonatomic, strong) Content *content;
@property (nonatomic, strong) NSString *contentCategoryType;

@property (nonatomic, assign) BOOL showingWatchLaterContent;

-(void) updateIfNeeded;
-(void) removePlayerNavC;

@end
