//
//  ViewController.m
//  PakTV
//
//  Created by Malik Khalid Ejaz on 2013-09-24.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import "MainViewController.h"
#import "Utility.h"
#import "iPhoneHomeViewController.h"
#import "iPadHomeViewController.h"
#import "SideMenuViewController.h"
#import "BrowseViewController.h"
#import "WatchLaterViewController.h"
#import "YoutubeSearchVC.h"
#import "ME_AdManager.h"
#import "UserManager.h"
#import "MoreAppsVC.h"
#import "ForumListVC.h"
#import "UserAccountsVC.h"
#import "PlayerNavC.h"
#import "DataManager.h"

#import <QuartzCore/QuartzCore.h>

#import "PreviewView.h"

// tutorial.
#import "WelcomeVC.h"

#define TOUCH_DRAG_OFFSET   10
#define DRAG_RATIO_TO_TRIGGER_MENU_OPEN_OR_CLOSE    2   // 1 is 1/4, 2 is half, 3 is 3/4.
#define SIDE_MENU_WIDTH     240

#define USER_ACTION_SWITCH_USER     @"switchUser"
#define USER_ACTION_BROWSE          @"browse"
#define USER_ACTION_SEARCH_YOUTUBE  @"searchYouTube"

#define USER_ACTION_UPLOADER        @"uploader"
#define USER_ACTION_SYNC            @"sync"
#define USER_ACTION_CHANNEL_MANAGER @"channelManager"


@interface MainViewController ()

@property (nonatomic, strong) iPhoneHomeViewController *homeVC;
@property (nonatomic, strong) iPadHomeViewController *ipadHomeVC;
@property (nonatomic, strong) SideMenuViewController *sideMenuVC;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *overlayView;

@property (nonatomic, strong) UINavigationController *navC;

// image preview related.
@property (nonatomic, strong) UIButton *previewImagesBackButton;
@property (nonatomic, strong) PreviewView *previewView;
@property (nonatomic, strong) UILabel *adPlaceHolderLabel;

@property BOOL menuIsOpen;
@property BOOL menuDisabled;

// menu drag out by user related
@property CGPoint touchStartPoint;

@property (nonatomic, strong) NSString *action;

@property (nonatomic, assign) BOOL canShowAds;

@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.action = @"";
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showNoConnection) name:NOTIFICATION_NO_INTERNET_CONNECTIVITY object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideNoConnection) name:NOTIFICATION_INTERNET_CONNECTED object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadOnHomeIfNeeded) name:NOTIFICATION_RELOAD_IF_NEEDED object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startNormalAppFlow) name:NOTIFICATION_TUTORIAL_FINISHED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(toggleMenu) name:NOTIFICATION_TOGGLE_MENU object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sideMenuBrowseSelected) name:NOTIFICATION_CHOOSE_FAVS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sideMenuWatchLaterSelected) name:NOTIFICATION_CHOOSE_WATCH_LATER object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMain) name:NOTIFICATION_USER_ACCOUNT_SELECTED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showPreviewImages:) name:NOTIFICATION_SHOW_PREVIEW_IMAGES object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(exploreCategory:) name:NOTIFICATION_EXPLORE_SELECTED_FROM_CAT_CELL object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(switchUserSelectedToRestrictAccess) name:NOTIFICATION_SWITCH_USER_SELECTED object:nil];
    
//    if ([[UserManager sharedManager] isFirstLaunch]) {
//        self.menuDisabled = YES;
//        [self startFirstLaunchFlow];
//    }
//    else {
        [self startNormalAppFlow];
//    }
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.canShowAds) {
        [[ME_AdManager sharedManager] showAdOnViewController:self atPosition:AD_POSITION_BOTTOM];
    }
}

-(void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [[ME_AdManager sharedManager] layoutAdView];
    
    CGRect overlayFrame = self.overlayView.frame;
    overlayFrame.size.height = self.view.bounds.size.height - [[ME_AdManager sharedManager] adHeight];
    self.overlayView.frame = overlayFrame;
    
    [self layoutHomeView];
    
    CGRect sideMenuFrame = self.sideMenuVC.view.frame;
    sideMenuFrame.size.height = self.view.bounds.size.height - [[ME_AdManager sharedManager] adHeight];
    [self.sideMenuVC.view setFrame:sideMenuFrame];

    [self resetPreviewViewFrame];
    
    self.adPlaceHolderLabel.frame = CGRectMake(0, self.view.bounds.size.height - [[ME_AdManager sharedManager] adHeight], self.view.bounds.size.width, [[ME_AdManager sharedManager] adHeight]);
}

-(void) layoutHomeView {
    CGRect homeFrame = self.navC.view.frame;
    homeFrame.size.height = self.view.bounds.size.height - [[ME_AdManager sharedManager] adHeight];
    homeFrame.size.width = self.view.bounds.size.width;
    
    self.navC.view.frame = homeFrame;
    
    if (IS_IPAD) {
        self.ipadHomeVC.view.frame = self.navC.view.bounds;
    } else {
        self.homeVC.view.frame = self.navC.view.bounds;
    }
}

#pragma mark - gesture recognizer related

- (IBAction)swipedRight:(id)sender {
    if (self.menuDisabled) {
        return;
    }
    if (!self.menuIsOpen) {
        [self showSideMenu];
    }
}

- (IBAction)swipedLeft:(id)sender {
    if (self.menuDisabled) {
        return;
    }
    if (self.menuIsOpen) {
        [self hideSideMenu];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isKindOfClass:[UISlider class]]) {
        // prevent recognizing touches on the slider
        return NO;
    }
    return YES;
}

- (IBAction)hideMenuAction:(id)sender {
    [self hideSideMenu];
}

#pragma mark - side menu open close related

-(void) toggleMenu {
    if (self.menuDisabled) {
        return;
    }
    if (self.menuIsOpen) {
        [self hideSideMenu];
    } else [self showSideMenu];
}


-(void) showSideMenu {
//    NSLog(@"Home VC: %@", IS_IPAD?self.ipadHomeVC.view:self.homeVC.view);
//    NSLog(@"NavC: %@", self.navC.view);
    [self.sideMenuVC reloadTable];
    [UIView animateWithDuration:0.3 animations:^{
        CGRect homeViewFrame = self.navC.view.frame;
        homeViewFrame.origin.x = SIDE_MENU_WIDTH;// self.view.bounds.size.width*3/4;
        self.navC.view.frame = homeViewFrame;
    } completion:^(BOOL finished) {
        self.menuIsOpen = TRUE;
    }];
    [self.view bringSubviewToFront:self.overlayView];
}

-(void) hideSideMenu {
    [UIView animateWithDuration:0.3 animations:^{
        CGRect homeViewFrame = self.navC.view.frame;
        homeViewFrame.origin.x = 0;
        self.navC.view.frame = homeViewFrame;
    } completion:^(BOOL finished) {
        self.menuIsOpen = FALSE;
    }];
    [self.view sendSubviewToBack:self.overlayView];
}

-(void) resetHomeViewFrame {
    if (self.menuIsOpen) {
        [self showSideMenu];
    } else [self hideSideMenu];
}

#pragma mark - slide out side menu when user drags

//-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    //    self.touchesMoving = TRUE;
//    if (self.menuDisabled) {
//        return;
//    }
//    self.touchStartPoint = [[touches anyObject] locationInView:self.view];
//}

//-(void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
//    if (self.menuDisabled) {
//        return;
//    }
//    CGPoint movedToPoint = [[touches anyObject] locationInView:self.view];
//    NSInteger distance =  movedToPoint.x - self.touchStartPoint.x;
//    if (abs(distance) > TOUCH_DRAG_OFFSET) {
//        CGRect homeViewFrame = self.navC.view.frame;
//        
//        if (self.menuIsOpen) {
//            homeViewFrame.origin.x = SIDE_MENU_WIDTH + distance;// self.view.bounds.size.width*3/4 + distance;
//        } else {
//            homeViewFrame.origin.x = distance;
//        }
//        if (homeViewFrame.origin.x < 0) {
//            homeViewFrame.origin.x = 0;
//        }
//        self.navC.view.frame = homeViewFrame;
//    }
//}

//-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
//    if (self.menuDisabled) {
//        return;
//    }
//    CGPoint endedAtPoint = [[touches anyObject] locationInView:self.view];
//    NSInteger distance = endedAtPoint.x - self.touchStartPoint.x;
//    NSInteger triggerDistance = self.view.bounds.size.width*DRAG_RATIO_TO_TRIGGER_MENU_OPEN_OR_CLOSE/4;
//    if (abs(distance) > triggerDistance) {
//        if (distance > 0) {
//            [self showSideMenu];
//        } else [self hideSideMenu];
//    } else {
//        [self resetHomeViewFrame];
//    }
//}

#pragma mark - side menu open/close delegate

-(void) closeMenu {
    [self hideSideMenu];
}

-(void) sideMenuSearchYoutubeSelected {
//    if ([[UserManager sharedManager] currentUser].childUser) {
//        self.action = USER_ACTION_SEARCH_YOUTUBE;
//        [self askPinWithUser:[[UserManager sharedManager] currentUser]];
//    } else {
//        [self allowSearchYouTube];
//    }
}

-(void) allowSearchYouTube {
    self.menuDisabled = NO;
    [self closeMenu];
    
    // Send the dimensions to Parse along with the 'search' event
//    [PFAnalytics trackEvent:@"searchYoutube" dimensions:@{@"event":@"open"}];
    
    YoutubeSearchVC *searchVC = [[YoutubeSearchVC alloc] initWithNibName:@"YoutubeSearchVC" bundle:nil];
    [searchVC.view setFrame:self.navC.view.bounds];
    [self.navC pushViewController:searchVC animated:YES];
    [self showTipForAction:TIP_ACTION_SEARCH_YOUTUBE];
}

-(void) sideMenuMoreAppsSelected {
    [self closeMenu];
    
    // Send the dimensions to Parse along with the 'search' event
//    [PFAnalytics trackEvent:@"moreApps" dimensions:@{@"event":@"open"}];
    
    MoreAppsVC *moreAppsVc = [[MoreAppsVC alloc] initWithNibName:@"MoreAppsVC" bundle:nil];
    [moreAppsVc.view setFrame:self.navC.view.bounds];
    [self addChildViewController:moreAppsVc];
    [self.navC pushViewController:moreAppsVc animated:YES];
}

-(void) sideMenuBrowseSelected {
//    if ([[UserManager sharedManager] currentUser].childUser) {
//        self.action = USER_ACTION_BROWSE;
//        [self askPinWithUser:[[UserManager sharedManager] currentUser]];
//    } else {
//        [self allowBrowse];
//    }
}

-(void) allowBrowse {
    self.menuDisabled = NO;
    [self closeMenu];
    
    // Send the dimensions to Parse along with the 'search' event
//    [PFAnalytics trackEvent:@"browse" dimensions:@{@"event":@"open"}];
    
    [self.navC popToRootViewControllerAnimated:NO];
    [self askFavs];
    
    [self showTipForAction:TIP_ACTION_BROWSE];
}

-(void) sideMenuListFavouritesSelected {
    [self.navC popToRootViewControllerAnimated:NO];
    
    BrowseViewController *browseVC = [[BrowseViewController alloc] initWithNibName:@"BrowseViewController" bundle:nil];
    
    browseVC.showingLocalFavs = YES;
    [self.navC pushViewController:browseVC animated:YES];
    [self hideSideMenu];
    [self showTipForAction:TIP_ACTION_FAVS];
}

-(void) sideMenuWatchLaterSelected {

    [self.navC popToRootViewControllerAnimated:NO];
    
    WatchLaterViewController *watchLaterVC = [[WatchLaterViewController alloc] initWithNibName:@"WatchLaterViewController" bundle:nil];
    
    [self.navC pushViewController:watchLaterVC animated:YES];
    [self hideSideMenu];
}

-(void) sideMenuHomeSelected {
    [self closeMenu];
    [self.navC popToRootViewControllerAnimated:YES];
}

-(void) sideMenuForumsSelected {
    [self closeMenu];
    [self.navC popToRootViewControllerAnimated:NO];
    
    ForumListVC *forumVC = [[ForumListVC alloc] initWithNibName:@"ForumListVC" bundle:nil];
    [self.navC pushViewController:forumVC animated:YES];
}

-(void) switchUserSelectedToRestrictAccess {
    [self closeMenu];
    [self.navC popToRootViewControllerAnimated:NO];
    
    UserAccountsVC *accountsVC = [[UserAccountsVC alloc] initWithNibName:@"UserAccountsVC" bundle:nil];
    [self.navC pushViewController:accountsVC animated:NO];
}

-(void) sideMenuSwitchUserSelected {
    
//    if ([[UserManager sharedManager] currentUser].childUser) {
//        self.menuDisabled = YES;
//        self.action = USER_ACTION_SWITCH_USER;
//        [self askPinWithUser:[[UserManager sharedManager] currentUser]];
//    } else {
//        [self allowSwitchUser];
//    }
}

-(void) allowSwitchUser {
    [self closeMenu];
    [self.navC popToRootViewControllerAnimated:NO];
    
    UserAccountsVC *accountsVC = [[UserAccountsVC alloc] initWithNibName:@"UserAccountsVC" bundle:nil];
    [self.navC pushViewController:accountsVC animated:YES];
}

-(void) sideMenuShareSelected:(CGRect)rect {
//    if (!IS_IPAD) {
//        [self closeMenu];
//    }
    
    // TODO: share implementation
    NSURL *url = [NSURL URLWithString:[[DataManager sharedManager] appURL]];
    NSString *text = [NSString stringWithFormat:@"Check this great app: %@", [[DataManager sharedManager] appName]];
    [self shareText:text andImage:nil andUrl:url rect:rect];
    
    // Send the dimensions to Parse along with the 'search' event
//    [PFAnalytics trackEvent:@"share" dimensions:@{@"event":@"open"}];
}

- (void)shareText:(NSString *)text andImage:(UIImage *)image andUrl:(NSURL *)url rect:(CGRect)rect
{
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    if (text) {
        [sharingItems addObject:text];
    }
    if (image) {
        [sharingItems addObject:image];
    }
    if (url) {
        [sharingItems addObject:url];
    }
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    if ( [activityController respondsToSelector:@selector(popoverPresentationController)] ) {
        // iOS8
        activityController.popoverPresentationController.sourceView = self.view;
        activityController.popoverPresentationController.sourceRect = rect;
    }
    [self presentViewController:activityController animated:YES completion:nil];
}

#pragma mark - pin view delegate

-(void) pinViewAllowedAccessToUser:(User *)user {
    self.menuDisabled = NO;
    [self removePinView];
    if ([self.action isEqualToString:USER_ACTION_SWITCH_USER]) {
        [self allowSwitchUser];
    } else if ([self.action isEqualToString:USER_ACTION_BROWSE]) {
        [self allowBrowse];
    } else if ([self.action isEqualToString:USER_ACTION_SEARCH_YOUTUBE]) {
        [self allowSearchYouTube];
    }
}

-(void) pinViewdeniedAccessToUser:(User *)user {
    [self removePinView];
    self.menuDisabled = NO;
    return;
}

#pragma mark - launch flow related

-(void) addSideMenu {
    if (self.sideMenuVC) {
        // called again after selecting user account. dont need to add menu again.
        [self showUserName];
        return;
    }
    self.sideMenuVC = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController" bundle:nil];
    CGRect sideMenuFrame = self.sideMenuVC.view.frame;
    sideMenuFrame.size.width = SIDE_MENU_WIDTH;
    sideMenuFrame.size.height = self.view.bounds.size.width - [[ME_AdManager sharedManager] adHeight];
    [self.sideMenuVC.view setFrame:sideMenuFrame];
    [self.view addSubview:self.sideMenuVC.view];
    self.sideMenuVC.openCloseDelegate = self;
    [self addChildViewController:self.sideMenuVC];
    [self showUserName];
    
}

-(void) showUserName {
//    self.sideMenuVC.userNameLabel.text = [NSString stringWithFormat:@"Welcome %@!", [[UserManager sharedManager] currentUser].name];
//    [self.sideMenuVC.view setBackgroundColor:[[UserManager sharedManager] colorForUser]];
}

-(void) askToCreateUserName {
    if (self.navC) {
        [self.navC.view removeFromSuperview];
        [self.navC removeFromParentViewController];
        self.navC = nil;
    }
    
    UserAccountsVC *accountsVC = [[UserAccountsVC alloc] initWithNibName:@"UserAccountsVC" bundle:nil];
    accountsVC.mustCreateUserName = YES;
    
    self.navC = [[UINavigationController alloc] initWithRootViewController:accountsVC];
    [self.navC.view setFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - [[ME_AdManager sharedManager] adHeight])];
    self.navC.navigationBarHidden = YES;
    [self.view addSubview:self.navC.view];
    [self addChildViewController:self.navC];
    
    [self showTipForAction:TIP_ACTION_CREATE_ACCOUNT];
}

-(void) startNormalAppFlow {
//    self.canShowAds = YES;
//    if (![[UserManager sharedManager] currentUser]) {
//        if (![[UserManager sharedManager] haveUserAccounts]) {
//
//            [[UserManager sharedManager] addUserAccount:@"User" color:[[UserManager sharedManager] colorForUser] pin:@"" childAccount:NO];
//            [[UserManager sharedManager] setCurrentUser:[[UserManager sharedManager] users][0]];
////            [self showSpinner];
//            [self readClassicCategoriesWithCompletion:^(NSArray *objects, NSError *error) {
//                if (!error && objects.count > 0) {
//                    for (ContentCategory *category in objects) {
//                        //                if ([category.categoryName isEqualToString:@"Pakistani TV Shows"] || [category.categoryName isEqualToString:@"Pakistani TV Dramas"]) {
//                        [category saveAsFavourite];
//                        //                }
//                    }
//                }
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    if(self.canShowAds) {
//                        [[ME_AdManager sharedManager] showAdOnViewController:self atPosition:AD_POSITION_BOTTOM];
//                        [self setupAdPlaceHolderView];
//                    }
//                    
//                    [self showMain];
//                });
//            }];
//            
//            
//        } else {
//            [self askToCreateUserName];
//            return;
//        }
//    }
//    if(self.canShowAds) {
//        [[ME_AdManager sharedManager] showAdOnViewController:self atPosition:AD_POSITION_BOTTOM];
//        [self setupAdPlaceHolderView];
//    }
//
//    [self showMain];
}

-(void) setupAdPlaceHolderView {
    self.adPlaceHolderLabel = [[UILabel alloc] init];
    self.adPlaceHolderLabel.frame = CGRectMake(0, self.view.bounds.size.height - [[ME_AdManager sharedManager] adHeight], self.view.bounds.size.width, [[ME_AdManager sharedManager] adHeight]);
    self.adPlaceHolderLabel.text = [[DataManager sharedManager] appName];
    
    self.adPlaceHolderLabel.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:24.0f];
    self.adPlaceHolderLabel.textColor = GRAY_WITH_PERCENT(80);
    self.adPlaceHolderLabel.shadowColor = GRAY_WITH_PERCENT(6);
    self.adPlaceHolderLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    self.adPlaceHolderLabel.backgroundColor = GRAY_WITH_PERCENT(20);
    self.adPlaceHolderLabel.textAlignment = NSTextAlignmentCenter;
    self.adPlaceHolderLabel.minimumFontSize = 10;
    [self.adPlaceHolderLabel setAdjustsFontSizeToFitWidth:YES];
    self.adPlaceHolderLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.adPlaceHolderLabel];
}

-(void) showMain {
    
    self.menuDisabled = NO;
    if (self.navC) {
        [self.navC.view removeFromSuperview];
        [self.navC removeFromParentViewController];
        self.navC = nil;
    }
    if (IS_IPAD) {
        [self showMainForIPad];
    } else {
        [self showMainForIPhone];
    }
}

-(void) showMainForIPad {
    [self addSideMenu];
    
    self.ipadHomeVC = [[iPadHomeViewController alloc] initWithNibName:@"iPadHomeViewController" bundle:nil];
    self.ipadHomeVC.view.frame = self.view.bounds;
    self.navC = [[UINavigationController alloc] initWithRootViewController:self.ipadHomeVC];
    [self.navC.view setFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - [[ME_AdManager sharedManager] adHeight])];
    self.ipadHomeVC.view.frame = self.navC.view.bounds;
    self.navC.navigationBarHidden = YES;
    [self.view addSubview:self.navC.view];
    [self addChildViewController:self.navC];
}

-(void) showMainForIPhone {
    
    [self addSideMenu];
    
    self.homeVC = [[iPhoneHomeViewController alloc] initWithNibName:@"iPhoneHomeViewController" bundle:nil];
    self.navC = [[UINavigationController alloc] initWithRootViewController:self.homeVC];
    [self.navC.view setFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - [[ME_AdManager sharedManager] adHeight])];
    self.homeVC.view.frame = self.navC.view.bounds;
    self.navC.navigationBarHidden = YES;
    [self.view addSubview:self.navC.view];
    [self addChildViewController:self.navC];
}

-(void) startFirstLaunchFlow {
    //    [self askFavs];
    WelcomeVC *welcomeVC = [[WelcomeVC alloc] initWithNibName:@"WelcomeVC" bundle:nil];
    self.navC = [[UINavigationController alloc] initWithRootViewController:welcomeVC];
    self.navC.navigationBarHidden = YES;
    [self.navC.view setFrame:self.view.bounds];
    [self.view addSubview:self.navC.view];
    [self addChildViewController:self.navC];
}

-(void) askFavs {
    BrowseViewController *categoriesTableVC = [[BrowseViewController alloc] initWithNibName:@"BrowseViewController" bundle:nil];
    [categoriesTableVC setCategory:nil];
    
    if (!self.navC) {
        self.navC = [[UINavigationController alloc] initWithRootViewController:categoriesTableVC];
        [self.navC setNavigationBarHidden:YES];
        [self.view addSubview:self.navC.view];
        [self addChildViewController:self.navC];
    } else {
        [self.navC pushViewController:categoriesTableVC animated:YES];
    }
}

-(void) exploreCategory:(NSNotification *)notification {
    ContentCategory *category = notification.userInfo[@"category"];
    if (category) {
        BrowseViewController *browseVC = [[BrowseViewController alloc] initWithNibName:@"BrowseViewController" bundle:nil];
        [browseVC setCategory:category];
        [self.navC pushViewController:browseVC animated:YES];
    }
}

-(void) showPreviewImages:(NSNotification *)notif {
    
    if (self.navC) {
        [self.navC.view setUserInteractionEnabled:NO];
        [self.sideMenuVC.view setUserInteractionEnabled:NO];
    }
    
    self.menuDisabled = YES;
    
    if (self.previewView) {
        [self.previewView removeFromSuperview];
        self.previewView = nil;
    }
    
    if (self.previewImagesBackButton) {
        [self.previewImagesBackButton removeFromSuperview];
        self.previewImagesBackButton = nil;
    }
    
    self.previewImagesBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.previewImagesBackButton.backgroundColor = [UIColor blackColor];
    self.previewImagesBackButton.alpha = 0.6f;
    [self.previewImagesBackButton setTitle:@"" forState:UIControlStateNormal];
    [self.previewImagesBackButton setTitle:@"" forState:UIControlStateHighlighted];
    [self.previewImagesBackButton setFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - [[ME_AdManager sharedManager] adHeight])];
    [self.previewImagesBackButton addTarget:self action:@selector(hidePreviewImages) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.previewImagesBackButton];
    self.previewView = [[NSBundle mainBundle] loadNibNamed:@"PreviewView" owner:self options:nil][0];
    [self.previewView popuateWithDictionary:notif.userInfo];
    self.previewView.layer.cornerRadius = 10.0f;
    self.previewView.layer.masksToBounds = YES;
    self.previewView.layer.borderColor = GRAY_WITH_PERCENT(15).CGColor;
    self.previewView.layer.borderWidth = 2.0;
    
    CGRect frame;
    if (IS_IPAD) {
        frame = CGRectMake(0, 0, 440, 330);
        
    } else {
        frame = CGRectMake(0, 0, 300, 228);
    }
    self.previewView.frame = frame;
    self.previewView.center = self.view.center;
    frame = self.previewView.frame;
    
    [self.view addSubview:self.previewView];
    
    self.previewView.frame = CGRectMake(self.view.center.x-1, self.view.center.y-1, 2, 2);
    [UIView animateWithDuration:0.3 animations:^{
        self.previewView.frame = frame;
    }];
}

-(void) resetPreviewViewFrame {
    if (self.previewView) {
        self.previewView.frame = CGRectMake(self.view.bounds.size.width/2 - self.previewView.bounds.size.width/2, self.view.bounds.size.height/2 - self.previewView.bounds.size.height/2 - [[ME_AdManager sharedManager] adHeight]/2, self.previewView.bounds.size.width, self.previewView.bounds.size.height);
    }
    if (self.previewImagesBackButton) {
        [self.previewImagesBackButton setFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - [[ME_AdManager sharedManager] adHeight])];
    }
}

-(void) hidePreviewImages {
    self.menuDisabled = NO;
    self.previewView.removing = YES;
    [self.navC.view setUserInteractionEnabled:YES];
    [self.sideMenuVC.view setUserInteractionEnabled:YES];
    [self.previewView removeFromSuperview];
    self.previewView = nil;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.previewImagesBackButton.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self.previewImagesBackButton removeFromSuperview];
        self.previewImagesBackButton = nil;
    }];
}

#pragma mark - reload if came back from backgrounded state

-(void) reloadOnHomeIfNeeded {
    if (self.navC) {
        if (self.navC.viewControllers.count == 1) {
            UIViewController *controller = self.navC.viewControllers[0];
            if (IS_IPAD) {
                if ([controller isKindOfClass:[iPadHomeViewController class]]) {
                    [(iPadHomeViewController *)controller updateIfNeeded];
                }
            } else {
                if ([controller isKindOfClass:[iPhoneHomeViewController class]]) {
                    [(iPhoneHomeViewController *)controller updateIfNeeded];
                }
            }
        }
    }
}

#pragma mark - internet connectivity related.

//-(void) showNoConnection {
//    NSLog(@"Network is unreachable.");
//}
//
//-(void) hideNoConnection {
//    NSLog(@"Network is reachable.");
//}


@end
