//
//  ChooseVersionVC.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2014-03-25.
//  Copyright (c) 2014 Malik Khalid Ejaz. All rights reserved.
//

#import "ChooseVersionVC.h"
#import "Utility.h"

@interface ChooseVersionVC ()

@end

@implementation ChooseVersionVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)classicAction:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Leaving app now" message:@"App store will open now for the classic version. You can download it for free there." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    [alert show];
    
    
}

- (IBAction)newAction:(id)sender {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstRun"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSNotification *notif = [NSNotification notificationWithName:NOTIFICATION_START_NEW_APP_VERSION object:nil];
    [[NSNotificationCenter defaultCenter] postNotification:notif];
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 1) {
        NSString* url;
//        if (OS_VERSION >= 7) {
            url = @"itms-apps://itunes.apple.com/app/id524728157";
//        } else {
//            url = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=524728157";
//        }
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
    }
}


#pragma mark - Auto rotation related.
// do not allow landscape view here.

-(BOOL) shouldAutorotate {
    return NO;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}

//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation // iOS 6 autorotation fix
//{
//    return UIInterfaceOrientationLandscapeRight;
//}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
