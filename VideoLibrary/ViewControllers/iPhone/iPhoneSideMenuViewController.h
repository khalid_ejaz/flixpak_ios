//
//  iPhoneSideMenuViewController.h
//  PakTV
//
//  Created by Malik Khalid Ejaz on 2013-09-24.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ContentCategory;

@protocol SideMenuDelegate <NSObject>

-(void) sideMenuCategorySelected:(ContentCategory *)selectedCategory;

@end

@protocol SideMenuOpenCloseDelegate <NSObject>

-(void) closeMenu;
-(void) sideMenuBrowseSelected;
-(void) sideMenuCategorySelected:(ContentCategory *)selectedCategory;

@end

@interface iPhoneSideMenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) id<SideMenuDelegate> delegate;
@property (nonatomic, weak) id<SideMenuOpenCloseDelegate> openCloseDelegate;

@end
