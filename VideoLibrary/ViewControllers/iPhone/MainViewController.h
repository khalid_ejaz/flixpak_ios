//
//  ViewController.h
//  PakTV
//
//  Created by Malik Khalid Ejaz on 2013-09-24.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import "SideMenuViewController.h"

@interface MainViewController : RootViewController <SideMenuOpenCloseDelegate>

@end
