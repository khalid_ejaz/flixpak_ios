//
//  ChooseVersionVC.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2014-03-25.
//  Copyright (c) 2014 Malik Khalid Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseVersionVC : UIViewController <UIAlertViewDelegate>

@end
