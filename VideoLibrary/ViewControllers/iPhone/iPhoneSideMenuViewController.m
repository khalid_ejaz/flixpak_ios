//
//  iPhoneSideMenuViewController.m
//  PakTV
//
//  Created by Malik Khalid Ejaz on 2013-09-24.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import "iPhoneSideMenuViewController.h"
#import "ME_AdManager.h"
#import "SideMenuCell.h"
#import "CategoryViewCell.h"
#import "DataManager.h"

typedef enum {
    AppSpecificMenuOptionTVShows,
    AppSpecificMenuOptionTVDramas,
    AppSpecificMenuOptionInterests,
    AppSpecificMenuOptionsTotal
}AppSpecificMenuOption;

#define SECTION_HEADER_HEIGHT   8
#define CELL_HEIGHT 32
#define MENU_CELL_ID    @"menuCellId"
#define CATEGORIES_CELL_ID  @"categoriesCellId"

#define SIDE_MENU_TEXT_SHARE            @"Share"
#define SIDE_MENU_TEXT_RATE_OR_REVIEW   @"Rate / Write a review"
#define SIDE_MENU_TEXT_CONTACT_SUPPORT  @"Contact Support"
#define SIDE_MENU_TEXT_MORE_APPS        @"More Apps"

#define SIDE_MENU_TEXT_HOME             @"Home"
#define SIDE_MENU_TEXT_FAV              @"Favorites"
#define SIDE_MENU_TEXT_BROWSE           @"Browse"

// Share sub options.
#define SIDE_MENU_TEXT_SHARE_TELL_A_FRIEND  @"Tell a friend"
#define SIDE_MENU_TEXT_SHARE_SHARE_ON_TWITTER  @"Share on twitter"
#define SIDE_MENU_TEXT_SHARE_SHARE_ON_FACEBOOK  @"Share on facebook"

@interface iPhoneSideMenuViewController ()

@property (nonatomic, strong) NSArray *sideMenuChoices;
@property (weak, nonatomic) IBOutlet UITableView *choicesTableView;

@property (nonatomic, strong) NSArray *categories;

@property (nonatomic, strong) NSArray *defaultMenuOptions;
@property (nonatomic, strong) NSArray *specialMenuOptions;

@end

@implementation iPhoneSideMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    self.specialMenuOptions = @[SIDE_MENU_TEXT_HOME, SIDE_MENU_TEXT_FAV, SIDE_MENU_TEXT_BROWSE];
//    
//    self.defaultMenuOptions = @[@{SIDE_MENU_TEXT_SHARE: @[SIDE_MENU_TEXT_SHARE_TELL_A_FRIEND, SIDE_MENU_TEXT_SHARE_SHARE_ON_TWITTER, SIDE_MENU_TEXT_SHARE_SHARE_ON_FACEBOOK]}, SIDE_MENU_TEXT_RATE_OR_REVIEW, SIDE_MENU_TEXT_CONTACT_SUPPORT, SIDE_MENU_TEXT_MORE_APPS];
//    self.choicesTableView.backgroundColor = [UIColor clearColor];
//    [self.choicesTableView registerNib:[UINib nibWithNibName:@"SideMenuCell" bundle:nil] forCellReuseIdentifier:MENU_CELL_ID];
//    [self.choicesTableView registerNib:[UINib nibWithNibName:@"CategoryViewCell" bundle:nil] forCellReuseIdentifier:CATEGORIES_CELL_ID];
//    self.choicesTableView.separatorColor = [UIColor darkGrayColor];
//    
//    [[DataManager sharedManager] readCategoriesWithCompletionBlock:^(NSArray *array, NSError *error) {
//        if (error) {
//            abort();
//        } else {
//            _categories = array;
//            [self.choicesTableView reloadData];
//        }
//    }];
//    [self.choicesTableView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.choicesTableView.bounds.size.width, SECTION_HEADER_HEIGHT)];
    
    header.backgroundColor = [UIColor clearColor];
    //    UIImageView *imageView = [[UIImageView alloc] initWithFrame:header.bounds];
    //    [imageView setImage:[UIImage imageNamed:@"menu_section_header_bg"]];
    //    [header addSubview:imageView];
    return header;
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    //    if (section == 0) {
    //        // make a big enough header so the menu is in the middle.
    //        int numberOfMenuItems = 9;
    //        int numberOfSections = 3;
    //        int menuHeight = numberOfMenuItems * CELL_HEIGHT + numberOfSections * SECTION_HEADER_HEIGHT;
    //        int diff = self.view.bounds.size.height - [ANAdManager sharedManager].adHeight - menuHeight;
    //        return diff/2;
    //    }
    return SECTION_HEADER_HEIGHT;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return CELL_HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.specialMenuOptions.count;
    }
    if (section == 1) {
        return self.categories.count;
    } else if (section == 2) {
        return self.defaultMenuOptions.count;
    }
    
    abort();
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        static NSString *cellId = @"cellId";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        }
        
        cell.textLabel.text = [self.specialMenuOptions objectAtIndex:indexPath.row];
        return cell;
    }
    
    if (indexPath.section == 1) {
        CategoryViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CATEGORIES_CELL_ID];
        ContentCategory *category = [self.categories objectAtIndex:indexPath.row];
        [cell populateWithCategory:category];
        return cell;
    } else if (indexPath.section == 2) {
        SideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:MENU_CELL_ID];
        NSString *menuText = @"";
        BOOL subOptions = FALSE;
        
        if ([[self.defaultMenuOptions objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]) {
            menuText = [self.defaultMenuOptions objectAtIndex:indexPath.row];
        } else {
            NSDictionary *menuOptionWithSubMenus = [self.defaultMenuOptions objectAtIndex:indexPath.row];
            menuText = [[menuOptionWithSubMenus allKeys] objectAtIndex:0];
            subOptions = TRUE;
        }
        
        UIImage *menuArt = nil;
        
        cell.menuTextLabel.text = menuText;
        
        if (indexPath.row == AppSpecificMenuOptionsTotal) {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        } else {
            cell.menuArtView.image = menuArt;
        }
        
        
        return cell;
    }
    
    abort();
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            // go home.
        } else if (indexPath.row == 1) {
            // show favourites.
        } else if (indexPath.row == 2) {
            // browse.
            [self.openCloseDelegate sideMenuBrowseSelected];
        }
    }
    
    if (indexPath.section == 1) {
        ContentCategory *category = [self.categories objectAtIndex:indexPath.row];
        [self.openCloseDelegate closeMenu];
        [self.openCloseDelegate sideMenuCategorySelected:category];
//        [self.delegate sideMenuCategorySelected:category];
        return;
    } else if (indexPath.section == 2) {
        
        return;
    }
}

#pragma mark - helper methods


@end
