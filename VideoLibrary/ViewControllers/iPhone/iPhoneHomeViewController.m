//
//  iPhoneHomeViewController.m
//  PakTV
//
//  Created by Malik Khalid Ejaz on 2013-09-24.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import "iPhoneHomeViewController.h"
#import "Utility.h"
#import "StyledCategoryViewController.h"
#import "ContentBrowserViewController.h"
#import "ShowView.h"
#import "YoutubeVideo.h"
#import "DataManager.h"
#import "UserManager.h"
#import "SwitchOption.h"
#import "PlayerNavC.h"
#import "ForumWebVC.h"

#import "CategoryViewerVC.h"
#import "CustomVC.h"
#import "WatchLaterVC.h"
#import "ContentViewerVC.h"
#import "PlayerVC.h"
#import "YoutubeVideosVC.h"

#define SHOW_CELL_REUSE_ID  @"showCellReuseId"
//#define HEADER_HEIGHT       20

@interface iPhoneHomeViewController ()

@property (nonatomic, strong) StyledCategoryViewController *styledCatVC;

@property (nonatomic, strong) CategoryViewerVC *catVC;
@property (nonatomic, strong) CustomVC *customVC;
@property (nonatomic, strong) WatchLaterVC *watchLaterVC;
@property (nonatomic, strong) ContentViewerVC *contentVC;
@property (nonatomic, strong) YoutubeVideosVC *youtubeVC;
@property (nonatomic, strong) PlayerNavC *playerNavC;

@property (unsafe_unretained, nonatomic) IBOutlet UIView *headerView;

@property (unsafe_unretained, nonatomic) IBOutlet UIView *contentView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *bookmarkControlView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *tagButton;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *homeLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *lineHiderView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *backButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *menuButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *noFavsView;
@property (unsafe_unretained, nonatomic) IBOutlet UIBarButtonItem *menuBarButton;

@property (nonatomic, strong) SwitchControl *switchControl;

@end

@implementation iPhoneHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0) {
        self.lineHiderView.hidden = NO;
    } else self.lineHiderView.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(flashTagButton) name:NOTIFICATION_FLASH_TAG object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showTipForWatchLater) name:@"NOTIFICATION_ADDED_TO_WATCH_LATER" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FavouriteChangedFromSideMenu:) name:NOTIFICATION_CHANGED_FAVS_FROM_SIDE_MENU object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(personalisedContentChanged:) name:NOTIFICATION_CHANGED_PERSONALISED_CONTENT object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playEpisode:) name:NOTIFICATION_PLAY_EPISODE object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playVideo:) name:NOTIFICATION_PLAY_VIDEO object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(favouriteChannelChanged:) name:NOTIFICATION_TOGGLE_FAV_ON_CHANNEL object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(favouriteForumChanged:) name:NOTIFICATION_TOGGLE_FAV_ON_FORUM object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showPreviousEpisodes:) name:NOTIFICATION_SHOW_ALL_EPISODES object:nil];
    
    if (self.showingWatchLaterContent) {
        CGRect contentFrame = self.contentView.frame;
        contentFrame.origin.y = self.bookmarkControlView.frame.origin.y;
        contentFrame.size.height += self.bookmarkControlView.frame.size.height;
        self.contentView.frame = contentFrame;
        
        [self.bookmarkControlView removeFromSuperview];
        
        self.backButton.hidden = NO;
        self.menuButton.hidden = YES;
        
        self.watchLaterVC = [[WatchLaterVC alloc] initWithNibName:@"ViewerVC" bundle:nil];
        self.watchLaterVC.view.frame = self.contentView.bounds;
        [self.contentView addSubview:self.watchLaterVC.view];
        [self addChildViewController:self.watchLaterVC];
        
        self.homeLabel.text = @"Watch Later List";
        self.tagButton.hidden = YES;
        
        [self.watchLaterVC loadData];
    } else if (self.content) {
        
        CGRect contentFrame = self.contentView.frame;
        contentFrame.origin.y = self.bookmarkControlView.frame.origin.y;
        contentFrame.size.height += self.bookmarkControlView.frame.size.height;
        self.contentView.frame = contentFrame;
        
        [self.bookmarkControlView removeFromSuperview];
        
        self.backButton.hidden = NO;
        self.menuButton.hidden = YES;
        
        self.contentVC = [[ContentViewerVC alloc] initWithNibName:@"ViewerVC" bundle:nil];
        self.contentVC.view.frame = self.contentView.bounds;
        [self.contentView addSubview:self.contentVC.view];
        [self addChildViewController:self.contentVC];
//        self.homeLabel.text = self.content.name;
        [self.contentVC setCategoryType:self.contentCategoryType];
        [self.contentVC setContent:self.content];

        [self.contentVC loadData];
        
    } else {
        [self setupSwitchControl];
    }
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.headerView.backgroundColor = [[UserManager sharedManager] colorForUser];
    if (self.switchControl) {
        [self.switchControl changeBackColorTo:[Utility darkerColorForColor:[[UserManager sharedManager] colorForUser]] OptionBackColor:[[UserManager sharedManager] colorForUser] textColor:GRAY_WITH_PERCENT(80)];
    }
    
    self.navigationController.navigationBarHidden = YES;
    if (self.navigationController.viewControllers.count > 1 || IS_IPAD) {
        self.menuBarButton.title = @"←";
    } else {
        self.menuBarButton.title = @"☰";
    }
    
    if ([[UserManager sharedManager] favouritesChanged]) {
        if (self.customVC) {
            [self.customVC loadData];
        }
    }
    
    if (self.firstLoadDone) {
        [self updateIfNeeded];
    } else self.firstLoadDone = YES;
}

-(void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.styledCatVC.view.frame = self.contentView.bounds;
    
    CGRect contentFrame = self.contentView.frame;
    contentFrame.size.height = self.view.bounds.size.height - self.headerView.bounds.size.height;
    self.contentView.frame = contentFrame;
    
    [self.switchControl reLayout];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setupSwitchControl {
    [self hideSwitch];
    
    NSArray *switchOptions = [[UserManager sharedManager] switchOptionsFromFavCategories];
    if (switchOptions.count == 0) {
        self.noFavsView.hidden = NO;
        self.bookmarkControlView.hidden = YES;
    } else {
        self.noFavsView.hidden = YES;
        self.bookmarkControlView.hidden = NO;
    }
    
    if (!self.switchControl) {
        self.switchControl = [[NSBundle mainBundle] loadNibNamed:@"SwitchControl" owner:self options:nil][0];
        self.switchControl.delegate = self;
        [self.switchControl setFrame:self.bookmarkControlView.bounds];
        [self.bookmarkControlView addSubview:self.switchControl];
        
        NSInteger index = 0;
        NSString *lastTab = [[UserManager sharedManager] getLastSelectedHomeTab];
        for (int i=0; i<switchOptions.count; i++) {
            SwitchOption *option = switchOptions[i];
            if ([option.value isEqualToString:lastTab]) {
                index = i;
                break;
            }
        }
        
        [self.switchControl populateWithSwitchOptions:switchOptions controlBackColor:[[UserManager sharedManager] darkerColorForUser] optionBackColor:[[UserManager sharedManager] colorForUser] textColor:GRAY_WITH_PERCENT(80) showingSort:NO selectIndex:index];
        
    } else {
        self.switchControl.hidden = NO;
    }
}

-(void) hideSwitch {
    if (self.switchControl) {
        self.switchControl.hidden = YES;
    }
}

-(void) removeAllViewControllers {
    for (UIViewController *viewController in self.childViewControllers) {
        [viewController removeFromParentViewController];
        [viewController.view removeFromSuperview];
    }
    self.noFavsView.hidden = NO;
}

-(void) flashTagButton {
    [UIView animateWithDuration:0.3 animations:^{
        CGAffineTransform transform = CGAffineTransformMakeScale(2.5, 2.5);
        self.tagButton.transform = transform;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3 animations:^{
            CGAffineTransform transform = CGAffineTransformMakeScale(1.0, 1.0);
            self.tagButton.transform = transform;
        }];
    }];
}

#pragma mark - IBActions

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)watchLaterAction:(id)sender {
    NSNotification *notification = [NSNotification notificationWithName:NOTIFICATION_CHOOSE_WATCH_LATER object:nil];
    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

- (IBAction)toggleMenu:(id)sender {
    
    if (IS_IPAD) {
        if (self.navigationController.viewControllers.count == 1) {
//            [self dismissModalViewControllerAnimated:YES];
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            [self backAction:nil];
        }
        return;
    }
    
    if (self.navigationController.viewControllers.count == 1) {
        NSNotification *notification = [NSNotification notificationWithName:NOTIFICATION_TOGGLE_MENU object:nil];
        [[NSNotificationCenter defaultCenter] postNotification:notification];
    } else {
        [self backAction:nil];
    }
}

- (IBAction)chooseFavsAction:(id)sender {
    NSNotification *notification = [NSNotification notificationWithName:NOTIFICATION_CHOOSE_FAVS object:nil];
    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

#pragma mark - switch control delegate

-(void) switchControl:(id)control selectedOption:(SwitchOption *)option {
    
    // save selected option.
    [[UserManager sharedManager] saveLastSelectedHomeTab:option.value];
    
    if ([option.value isEqualToString:SWITCH_VALUE_FAV]) {
        // show personalized.
        if (self.catVC) {
            [self.catVC.view removeFromSuperview];
            [self.catVC removeFromParentViewController];
        }
        
        self.customVC = [[CustomVC alloc] initWithNibName:@"ViewerVC" bundle:nil];
        [self.customVC.view setFrame:self.contentView.bounds];
        [self.contentView addSubview:self.customVC.view];
        
        [self addChildViewController:self.customVC];
        [self.customVC loadData];
        
    } else if ([option.value rangeOfString:@"*YT_"].location != NSNotFound) {
        // show youtube channel.
        
        NSString *channelId = [option.value stringByReplacingOccurrencesOfString:@"*YT_" withString:@""];
        
        self.youtubeVC = [[YoutubeVideosVC alloc] initWithNibName:@"ViewerVC" bundle:nil];
        [self.youtubeVC.view setFrame:self.contentView.bounds];
        [self.contentView addSubview:self.youtubeVC.view];
        [self.youtubeVC setShowingChannel:YES];
        [self addChildViewController:self.youtubeVC];
        [self.youtubeVC setChannelId:channelId];
        [self.youtubeVC loadData];
        
    } else if ([option.value rangeOfString:@"*web_"].location != NSNotFound) {
        
        NSString *forumURL = [option.value stringByReplacingOccurrencesOfString:@"*web_" withString:@""];
        
        ForumWebVC *forumWebVC = [[ForumWebVC alloc] initWithNibName:@"ForumWebVC" bundle:nil];
        [forumWebVC.view setFrame:self.contentView.bounds];
        [forumWebVC setForumURL:forumURL];
        [self.contentView addSubview:forumWebVC.view];
        [self addChildViewController:forumWebVC];
        
    } else {
        // show category.
        if (self.catVC) {
            [self.catVC.view removeFromSuperview];
            [self.catVC removeFromParentViewController];
        }
        
        self.catVC = [[CategoryViewerVC alloc] initWithNibName:@"ViewerVC" bundle:nil];
        [self.catVC setCanShowFavs:YES];
        [self.catVC.view setFrame:self.contentView.bounds];
        [self.contentView addSubview:self.catVC.view];
        
        [self.catVC setCategoryId:option.value];
        [self addChildViewController:self.catVC];
        [self.catVC loadData];
    }
}

-(void) gotOptionForSwitchControl:(id)control {
    [self setupSwitchControl];
}

-(void) removedAllOptionsForSwitchControl:(id)control {
    [self hideSwitch];
    [self removeAllViewControllers];
}

#pragma mark - notification selectors

-(void) showTipForWatchLater {
    [self showTipForAction:TIP_ACTION_WATCH_LATER];
}

-(void) FavouriteChangedFromSideMenu:(NSNotification *)notification {
//    ContentCategory *category = notification.userInfo[@"category"];
//    
//    NSString *selected = notification.userInfo[@"selected"];
//    if ([selected isEqualToString:@"yes"]) {
//        SwitchOption *option = [[SwitchOption alloc] initWithDisplayText:category.categoryName value:category.objectId ascending:NO];
//        [self.switchControl addOption:option];
//        [self showTipForAction:TIP_ACTION_TAG_CATEGORY];
//    } else {
//        [self.switchControl removeOptionByValue:category.objectId];
//    }
}

-(void) favouriteChannelChanged:(NSNotification *)notification {
//    YoutubeChannel *channel = notification.userInfo[@"channel"];
//    NSString *selected = notification.userInfo[@"selected"];
//    
//    if ([selected isEqualToString:@"yes"]) {
//        SwitchOption *opt = [[SwitchOption alloc] initWithDisplayText:[[UserManager sharedManager] switchDisplayTextForYoutubeChannelName:channel.channelName] value:[[UserManager sharedManager] switchValueForYoutubeChannelId:channel.channelId] ascending:YES];
//        [self.switchControl addOption:opt];
//    } else {
//        [self.switchControl removeOptionByValue:[[UserManager sharedManager] switchValueForYoutubeChannelId:channel.channelId]];
//    }
//    if (selected) {
//        [self showTipForAction:TIP_ACTION_TAG_CHANNEL];
//    }
}

-(void) favouriteForumChanged:(NSNotification *)notification {
//    Forum *forum = notification.userInfo[@"forum"];
//    NSString *selected = notification.userInfo[@"selected"];
//    
//    if ([selected isEqualToString:@"yes"]) {
//        SwitchOption *opt = [[SwitchOption alloc] initWithDisplayText:[[UserManager sharedManager] switchDisplayTextForYoutubeChannelName:forum.forumName] value:[[UserManager sharedManager] switchValueForForumURL:forum.forumURL] ascending:YES];
//        [self.switchControl addOption:opt];
//    } else {
//        [self.switchControl removeOptionByValue:[[UserManager sharedManager] switchValueForForumURL:forum.forumURL]];
//    }
//    if (selected) {
//        [self showTipForAction:TIP_ACTION_TAG_FORUM];
//    }
}

-(void) personalisedContentChanged:(NSNotification *)notification {
    
    if ([self.switchControl containsOptionWithValue:SWITCH_VALUE_FAV]) {
        if ([[[UserManager sharedManager] favContents] allKeys].count == 0) {
            // removed all personalised content.
            [self.switchControl removeOptionByValue:SWITCH_VALUE_FAV];
        }
        // will auto reload when user selects the tab.
        return;
    } else {
        if ([[[UserManager sharedManager] favContents] allKeys].count != 0) {
            // removed all personalised content.
            [self.switchControl addOption:[[SwitchOption alloc] initWithDisplayText:HEART_SYMBOL value:SWITCH_VALUE_FAV ascending:NO]];
        }
    }
}

-(void) playEpisode:(NSNotification *)notification {
    [self removePlayerNavC];
    
    Episode *episode = notification.userInfo[@"Episode"];
    PlayerVC *playerVC = [[PlayerVC alloc] initWithNibName:@"PlayerVC" bundle:nil];
    [playerVC setEpisode:episode];
    [playerVC.view setFrame:self.navigationController.view.bounds];
    self.playerNavC = [[PlayerNavC alloc] initWithRootViewController:playerVC];
    self.playerNavC.view.frame = self.view.bounds;
    self.playerNavC.navigationBarHidden = YES;
//    [self presentModalViewController:self.playerNavC animated:YES];
    [self presentViewController:self.playerNavC animated:YES completion:nil];
    
    //    [self.navigationController pushViewController:playerVC animated:YES];
}

-(void) removePlayerNavC {
    if (self.playerNavC) {
        if (self.playerNavC.view.window) {
//            [self.playerNavC dismissModalViewControllerAnimated:YES];
            [self.playerNavC dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

-(void) playVideo:(NSNotification *)notification {
    
    [self removePlayerNavC];
    
    YoutubeVideo *video = notification.userInfo[@"video"];
    PlayerVC *playerVC = [[PlayerVC alloc] initWithNibName:@"PlayerVC" bundle:nil];
    [playerVC setVideo:video];
    [playerVC.view setFrame:self.navigationController.view.bounds];
    self.playerNavC = [[PlayerNavC alloc] initWithRootViewController:playerVC];
    self.playerNavC.view.frame = self.view.bounds;
    self.playerNavC.navigationBarHidden = YES;
//    [self presentModalViewController:self.playerNavC animated:YES];
    [self presentViewController:self.playerNavC animated:YES completion:nil];
    
//    [self.navigationController pushViewController:playerVC animated:YES];
}

-(void) showPreviousEpisodes:(NSNotification *)notification {
    Episode *episode = notification.userInfo[@"Episode"];
    ContentBrowserViewController *contentVC = [[ContentBrowserViewController alloc] initWithNibName:@"ContentBrowserViewController" bundle:nil];
//    [contentVC setContentId:episode.contentId];
    [contentVC.view setFrame:self.view.bounds];
    [self.navigationController pushViewController:contentVC animated:YES];
}
#pragma mark - versions related

-(void) reloadIfVisible {
    if (self.catVC) {
        [self.catVC loadData];
    }
}

-(void) updateIfNeeded {
    
    if (!self.catVC) {
        return;
    }
    NSString *categoryId = self.catVC.categoryId;
    
//    [Version readVersionsWithCompletion:^(NSArray *objects, NSError *error) {
//        if (!error) {
//            
//            if ([[UserManager sharedManager] versionsDict].allKeys.count == 0) {
//                [self updateDictionaryWithVersions:objects alreadyUpdated:NO];
//            } else {
//                for (Version *version in objects) {
//                    if ([version.categoryId isEqualToString:categoryId]) {
//                        int ver = [version.versionNumber integerValue];
//                        int existingVer = [[[[UserManager sharedManager] versionsDict] objectForKey:categoryId] integerValue];
//                        if (ver > existingVer) {
//                            NSLog(@"update needed");
//                            [self updateDictionaryWithVersions:objects alreadyUpdated:NO];
//                        } else [self updateDictionaryWithVersions:objects alreadyUpdated:YES];
//                    }
//                }
//            }
//        }
//    }];
}

-(void) updateDictionaryWithVersions:(NSArray *)versions alreadyUpdated:(BOOL)updated {
//    NSMutableDictionary *versionsDict = [[UserManager sharedManager] versionsDict];
//    for (Version *version in versions) {
//        [versionsDict setObject:version.versionNumber forKey:version.categoryId];
//    }
//    
//    if (!updated) {
//        [self reloadIfVisible];
//    }
//    [[UserManager sharedManager] saveVersions];
}

@end
