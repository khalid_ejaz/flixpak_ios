//
//  ChannelAgreementVC.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-02-03.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "ChannelAgreementVC.h"
#import "PublisherCategoryVC.h"

@interface ChannelAgreementVC ()

@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *channelId;
@property (nonatomic, strong) NSString *channelTitle;
@property (nonatomic, strong) NSString *videoUrl;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation ChannelAgreementVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    // Do any additional setup after loading the view from its nib.
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.contentSize.width, 500)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)setCode:(NSString *)code videoUrl:(NSString *)url channelId:(NSString *)channelId channelTitle:(NSString *)title {
    _code = code;
    _videoUrl = url;
    _channelId = channelId;
    _channelTitle = title;
}

- (IBAction)agreeAction:(id)sender {
    if (_code && _channelId && _channelTitle) {
        PublisherCategoryVC *publisherCategoryVC = [[PublisherCategoryVC alloc] initWithNibName:@"PublisherCategoryVC" bundle:nil];
        [publisherCategoryVC.navigationItem setTitle:self.channelTitle];
        [publisherCategoryVC setCode:self.code videoUrl:self.videoUrl channelId:self.channelId channelTitle:self.channelTitle];
        [self.navigationController pushViewController:publisherCategoryVC animated:YES];
    }
}

@end
