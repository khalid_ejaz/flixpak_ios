//
//  ChannelAgreementVC.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-02-03.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "AppRootViewController.h"

@interface ChannelAgreementVC : AppRootViewController

- (void)setCode:(NSString *)code videoUrl:(NSString *)url channelId:(NSString *)channelId channelTitle:(NSString *)title;

@end
