//
//  iPhoneSideMenuViewController.m
//  PakTV
//
//  Created by Malik Khalid Ejaz on 2013-09-24.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import "SideMenuViewController.h"
#import "ME_AdManager.h"
#import "SideMenuCell.h"
#import "CategoryViewCell.h"
#import "DataManager.h"
#import "Utility.h"

typedef enum {
    AppSpecificMenuOptionTVShows,
    AppSpecificMenuOptionTVDramas,
    AppSpecificMenuOptionInterests,
    AppSpecificMenuOptionsTotal
}AppSpecificMenuOption;

#define SECTION_HEADER_HEIGHT           8

#define CELL_HEIGHT                     44
#define CATEGORY_CELL_HEIGHT            40

#define MENU_CELL_ID                    @"menuCellId"
#define CATEGORIES_CELL_ID              @"categoriesCellId"

#define SIDE_MENU_TEXT_SHARE            @"Share"
#define SIDE_MENU_TEXT_RATE_OR_REVIEW   @"Rate / Write a review"
#define SIDE_MENU_TEXT_CONTACT_SUPPORT  @"Contact Support"
#define SIDE_MENU_TEXT_MORE_APPS        @"More Apps"

#define SIDE_MENU_TEXT_HOME             @"Home"
#define SIDE_MENU_TEXT_SWITCH_ACCOUNT   @"Switch User"
#define SIDE_MENU_TEXT_FAV              @"Your Favorites"
#define SIDE_MENU_TEXT_WATCH_LATER      @"Watch Later"
#define SIDE_MENU_TEXT_BROWSE           @"Browse"
#define SIDE_MENU_TEXT_SEARCH_YOUTUBE   @"Search YouTube"
#define SIDE_MENU_TEXT_FORUM            @"Forums"
#define SIDE_MENU_TEXT_ABOUT            @"About"

// Share sub options.
#define SIDE_MENU_TEXT_SHARE_TELL_A_FRIEND          @"Tell a friend"
#define SIDE_MENU_TEXT_SHARE_SHARE_ON_TWITTER       @"Share on twitter"
#define SIDE_MENU_TEXT_SHARE_SHARE_ON_FACEBOOK      @"Share on facebook"

@interface SideMenuViewController ()

@property (nonatomic, strong) NSArray *sideMenuChoices;
@property (unsafe_unretained, nonatomic) IBOutlet UITableView *choicesTableView;

@property (nonatomic, strong) NSArray *defaultMenuOptions;
@property (nonatomic, strong) NSArray *defaultMenuSymbols;

@property (nonatomic, strong) NSArray *specialMenuOptions;
@property (nonatomic, strong) NSArray *specialMenuSymbols;

@end

@implementation SideMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.specialMenuOptions = @[SIDE_MENU_TEXT_HOME, SIDE_MENU_TEXT_SWITCH_ACCOUNT, SIDE_MENU_TEXT_BROWSE, SIDE_MENU_TEXT_SEARCH_YOUTUBE, SIDE_MENU_TEXT_FAV, SIDE_MENU_TEXT_WATCH_LATER, SIDE_MENU_TEXT_FORUM];
    
    self.specialMenuSymbols = @[@"⌂", @"👱", @"📖", @"🔍", @"•", @"🔖", @"👥"];
    
    // client and iphone app
    self.defaultMenuOptions = @[@{SIDE_MENU_TEXT_SHARE: @[SIDE_MENU_TEXT_SHARE_TELL_A_FRIEND, SIDE_MENU_TEXT_SHARE_SHARE_ON_TWITTER, SIDE_MENU_TEXT_SHARE_SHARE_ON_FACEBOOK]}, SIDE_MENU_TEXT_RATE_OR_REVIEW, SIDE_MENU_TEXT_CONTACT_SUPPORT, SIDE_MENU_TEXT_MORE_APPS, SIDE_MENU_TEXT_ABOUT];
    
    self.defaultMenuSymbols = @[@"☍", @"✭", @"✉", @"ℹ", @"i"];
    self.choicesTableView.backgroundColor = [UIColor clearColor];
    [self.choicesTableView registerNib:[UINib nibWithNibName:@"SideMenuCell" bundle:nil] forCellReuseIdentifier:MENU_CELL_ID];
    [self.choicesTableView registerNib:[UINib nibWithNibName:@"CategoryViewCell" bundle:nil] forCellReuseIdentifier:CATEGORIES_CELL_ID];
    self.choicesTableView.separatorColor = [UIColor darkGrayColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// needed so favourite star/unstar are correctly displayed from latest data.
-(void) reloadTable {
    [self.choicesTableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.choicesTableView.bounds.size.width, SECTION_HEADER_HEIGHT)];
    
    header.backgroundColor = PLAIN_COLOR_ALMOST_BLACK;
    return header;
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 0.01f;
    }
    return SECTION_HEADER_HEIGHT;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        return CATEGORY_CELL_HEIGHT;
    }
    return CELL_HEIGHT;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.specialMenuOptions.count;
    } else if (section == 1) {
        return self.defaultMenuOptions.count;
    }
    
    abort();
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        SideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:MENU_CELL_ID];
        NSString *menuText = @"";
        BOOL subOptions = FALSE;
        
        if ([[self.specialMenuOptions objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]) {
            menuText = [self.specialMenuOptions objectAtIndex:indexPath.row];
        } else {
            NSDictionary *menuOptionWithSubMenus = [self.specialMenuOptions objectAtIndex:indexPath.row];
            menuText = [[menuOptionWithSubMenus allKeys] objectAtIndex:0];
            subOptions = TRUE;
        }
        
        UIImage *menuArt = nil;
        
        cell.menuTextLabel.text = menuText;
        
        if (indexPath.row == AppSpecificMenuOptionsTotal) {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        } else {
            cell.menuArtView.image = menuArt;
        }
        cell.symbolLabel.text = [self.specialMenuSymbols objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.textColor = PLAIN_COLOR_WHITE;
        cell.backgroundColor = PLAIN_COLOR_LIGHT_BLACK;
        
        return cell;
    }
    
    if (indexPath.section == 1) {
        SideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:MENU_CELL_ID];
        NSString *menuText = @"";
        BOOL subOptions = FALSE;
        
        if ([[self.defaultMenuOptions objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]) {
            menuText = [self.defaultMenuOptions objectAtIndex:indexPath.row];
        } else {
            NSDictionary *menuOptionWithSubMenus = [self.defaultMenuOptions objectAtIndex:indexPath.row];
            menuText = [[menuOptionWithSubMenus allKeys] objectAtIndex:0];
            subOptions = TRUE;
        }
        UIImage *menuArt = nil;
        
        cell.menuTextLabel.text = menuText;
        
        if (indexPath.row == AppSpecificMenuOptionsTotal) {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        } else {
            cell.menuArtView.image = menuArt;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.textColor = PLAIN_COLOR_WHITE;
        cell.backgroundColor = PLAIN_COLOR_LIGHT_BLACK;
        cell.symbolLabel.text = [self.defaultMenuSymbols objectAtIndex:indexPath.row];
        return cell;
    }
    
    abort();
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            // home.
            [self.openCloseDelegate sideMenuHomeSelected];
        }  else if (indexPath.row == 1) {
            // switch user.
            [self.openCloseDelegate sideMenuSwitchUserSelected];
        } else if (indexPath.row == 2) {
            // browse.
            [self.openCloseDelegate sideMenuBrowseSelected];
        } else if (indexPath.row == 3) {
            // search youtube.
            [self.openCloseDelegate sideMenuSearchYoutubeSelected];
        } else if (indexPath.row == 4) {
            // list favourites.
            [self.openCloseDelegate sideMenuListFavouritesSelected];
        } else if (indexPath.row == 5) {
            // watch later.
            [self.openCloseDelegate sideMenuWatchLaterSelected];
        } else if (indexPath.row == 6) {
            // forums.
            [self.openCloseDelegate sideMenuForumsSelected];
        }
    } else if (indexPath.section == 1) {
        //TODO: default menu options.
        if (indexPath.row == 0) {
            // share.
            
            CGRect rectOfCellInTableView = [tableView rectForRowAtIndexPath:indexPath];
            CGRect rectOfCellInSuperview = [tableView convertRect:rectOfCellInTableView toView:[[tableView superview] superview]];
//            rectOfCellInSuperview.origin.y += self.choicesTableView.frame.origin.y;
            
//            NSLog(@"Y of Cell is: %f", rectOfCellInSuperview.origin.y);
            
            [self.openCloseDelegate sideMenuShareSelected:rectOfCellInSuperview];
            
        } else if (indexPath.row == 1) {
            // rate the app.
            [self.openCloseDelegate closeMenu];
            NSString* url;
//            if (OS_VERSION >= 7) {
                url = @"itms-apps://itunes.apple.com/app/id485585283";
//            } else {
//                url = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=485585283";
//            }
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
        } else if (indexPath.row == 2) {
            [self.openCloseDelegate closeMenu];
            //contact support.
            
            if ([MFMailComposeViewController canSendMail]) {
                // Show the composer
                // Email Subject
                NSString *emailTitle = [[DataManager sharedManager] appName];
                // Email Content
                NSString *messageBody = [NSString stringWithFormat:@"<br><br><br><br>Sent from <a href=%@>%@</a>", [[DataManager sharedManager] appURL] , [[DataManager sharedManager] appName]];
                // To address
                NSArray *toRecipents = [NSArray arrayWithObject:@"support@anmaple.com"];
                
                MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
                mc.mailComposeDelegate = self;
                [mc setSubject:emailTitle];
                [mc setMessageBody:messageBody isHTML:YES];
                [mc setToRecipients:toRecipents];
                
                // Present mail view controller on screen
                [self presentViewController:mc animated:YES completion:NULL];
            } else {
                // Handle the error
                [self showAlertWithTitle:@"Error" message:@"Can not send email. No accounts setup."];
                return;
            }
            
            
        } else if (indexPath.row == 3) {
            [self.openCloseDelegate closeMenu];
            // more apps.
            [self.openCloseDelegate sideMenuMoreAppsSelected];
        } else if (indexPath.row == 4) {
            [self.openCloseDelegate closeMenu];
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Leaving app now" message:@"App store will open now for the classic version. You can download it for free there." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
//            [alert show];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Pak TV Shows, Dramas n more - v4.1" message:@"By Malik Khalid Ejaz." delegate:self cancelButtonTitle:@"that's nice" otherButtonTitles:nil];
            [alert show];
            
        }
        return;
    }
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 1) {
        NSString* url;
//        if (OS_VERSION >= 7) {
            url = @"itms-apps://itunes.apple.com/app/id524728157";
//        } else {
//            url = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=524728157";
//        }
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
//    [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - helper methods


@end
