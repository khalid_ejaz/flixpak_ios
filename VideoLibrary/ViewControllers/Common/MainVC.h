//
//  MainVC.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-16.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "AppRootViewController.h"
#import "TabView.h"
#import "SectionsTableViewController.h"

@interface MainVC : AppRootViewController <TabViewDelegate, SectionVCDelegate>

- (void)resetLastUpdate;

@end
