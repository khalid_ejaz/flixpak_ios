//
//  StyledCategoryViewController.m
//  PakTV
//
//  Created by Malik Khalid Ejaz on 11/15/2013.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import "StyledCategoryViewController.h"
#import "SwitchOption.h"
#import "ShowView.h"
#import "LoadMoreCell.h"
#import "DataManager.h"
#import "UserManager.h"
#import "Utility.h"

#define SHOW_CELL_REUSE_ID      @"showCellReuseId"
#define LOAD_MORE_CELL_REUSE_ID @"loadMoreReuseId"

#define DOWN_ARROW              @"⇢"
#define UP_ARROW                @"⇣"

#define HEADER_HEIGHT                   20
#define SEPARATOR_HEIGHT                3
#define SORT_SWITCH_HEIGHT              54
#define COLLAPSE_EXPAND_BUTTON_WIDTH    40

@interface StyledCategoryViewController ()
@property (unsafe_unretained, nonatomic) IBOutlet UITableView *catTableView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *switchControlContainerView;
@property (unsafe_unretained, nonatomic) IBOutlet UISegmentedControl *sortSwitcher;
@property (unsafe_unretained, nonatomic) IBOutlet UIScrollView *scrollView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *busyOverlayView;
@property (unsafe_unretained, nonatomic) IBOutlet UIActivityIndicatorView *busyWheel;

@property (nonatomic, assign) BOOL tieSwitchWithTable;
@property (nonatomic, assign) BOOL switchVisible;

// for category data & personalised data.
@property (nonatomic, strong) NSMutableDictionary *episodesDictionary;
@property (nonatomic, strong) NSArray *dates;

// for personalised data.
@property (nonatomic, strong) NSMutableArray *dateStrings;

// for content data.
@property (nonatomic, strong) NSMutableArray *episodes;
@property (nonatomic, strong) NSMutableArray *episodeDates;
@property (nonatomic, strong) NSMutableDictionary *episodeInDateDictionary;


@property (nonatomic, strong) NSMutableDictionary *sortOptions;

@property (nonatomic, strong) NSMutableIndexSet *collapsedIndices;

@property (nonatomic, assign) BOOL loading;
@property (nonatomic, assign) NSInteger selectedLoadIndex;
@property (nonatomic, strong) NSString *sortField;

@property (nonatomic, strong) ContentCategory *category;

@property (nonatomic, assign) BOOL showingContent;

@end

@implementation StyledCategoryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.catTableView registerNib:[UINib nibWithNibName:@"ShowView" bundle:nil] forCellReuseIdentifier:SHOW_CELL_REUSE_ID];
    [self.catTableView registerNib:[UINib nibWithNibName:@"LoadMoreCell" bundle:nil] forCellReuseIdentifier:LOAD_MORE_CELL_REUSE_ID];
    
    self.collapsedIndices = [NSMutableIndexSet indexSet];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self hideSortSwitch];
    
    if (self.showingWatchLaterContent) {
        [self clearData];
        self.showingWatchLaterContent = YES;
        self.episodes = [[[DataManager sharedManager] getSortedArrayForLocalFavEpisodes:[[UserManager sharedManager] favEpisodes]] mutableCopy];
        
        [self setupSortSwitcher];
        [self.catTableView reloadData];
        [self hideBusyView];
    }
}

-(void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self hideSortSwitch];
}

-(void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    CGRect tableRect = self.view.bounds;
    tableRect.origin.y = SORT_SWITCH_HEIGHT;
    [self.catTableView setFrame:tableRect];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void) setupSortSwitcher {
//    self.sortOptions = [NSMutableDictionary dictionary];
//    if (self.showingContent) {
//        if ([self.contentCategoryType isEqualToString:CATEGORY_TYPE_DATE]) {
//            [self.sortOptions setObject:@{@"title":@"Date",@"value":KEY_EPISODE_ADDED_ON} forKey:@"0"];
//            [self.sortOptions setObject:@{@"title":@"Ratings",@"value":KEY_CONTENT_RATING_POINTS} forKey:@"1"];
//            [self.sortOptions setObject:@{@"title":@"Views",@"value":KEY_EPISODE_VIEWS} forKey:@"2"];
//        } else if ([self.contentCategoryType isEqualToString:CATEGORY_TYPE_EPISODE]) {
//            [self.sortOptions setObject:@{@"title":@"Episode",@"value":KEY_EPISODE_NUMBER} forKey:@"0"];
//            [self.sortOptions setObject:@{@"title":@"Ratings",@"value":KEY_CONTENT_RATING_POINTS} forKey:@"1"];
//            [self.sortOptions setObject:@{@"title":@"Views",@"value":KEY_EPISODE_VIEWS} forKey:@"2"];
//        } else {
//            [self.sortOptions setObject:@{@"title":@"Ratings",@"value":KEY_CONTENT_RATING_POINTS} forKey:@"0"];
//            [self.sortOptions setObject:@{@"title":@"Views",@"value":KEY_EPISODE_VIEWS} forKey:@"1"];
//            [self.sortOptions setObject:@{@"title":@"Alphabetical",@"value":KEY_EPISODE_CONTENT_NAME} forKey:@"2"];
//        }
//    } else if (self.showingWatchLaterContent) {
//        [self.sortOptions setObject:@{@"title":@"Order added",@"value":KEY_EPISODE_SAVED_AT} forKey:@"0"];
//        [self.sortOptions setObject:@{@"title":@"Ratings",@"value":KEY_CONTENT_RATING_POINTS} forKey:@"1"];
//        [self.sortOptions setObject:@{@"title":@"Views",@"value":KEY_EPISODE_VIEWS} forKey:@"2"];
//        
//    } else if (self.showingPersonalized) {
//        [self.sortOptions setObject:@{@"title":@"Latest",@"value":KEY_EPISODE_ADDED_ON} forKey:@"0"];
//        [self.sortOptions setObject:@{@"title":@"Ratings",@"value":KEY_CONTENT_RATING_POINTS} forKey:@"1"];
//        [self.sortOptions setObject:@{@"title":@"Views",@"value":KEY_EPISODE_VIEWS} forKey:@"2"];
//        
//    } else {
//        [self.sortOptions setObject:@{@"title":@"Ratings",@"value":KEY_CONTENT_RATING_POINTS} forKey:@"0"];
//        [self.sortOptions setObject:@{@"title":@"Views",@"value":KEY_EPISODE_VIEWS} forKey:@"1"];
//        [self.sortOptions setObject:@{@"title":@"Alphabetical",@"value":KEY_EPISODE_CONTENT_NAME} forKey:@"2"];
//    }
//    
//    for (int i=0; i<3; i++) {
//        NSString *key = [NSString stringWithFormat:@"%d", i];
//        [self.sortSwitcher setTitle:[self.sortOptions objectForKey:key][@"title"] forSegmentAtIndex:i];
//    }
//    self.sortSwitcher.selectedSegmentIndex = 0;
//    self.sortField = [self.sortOptions objectForKey:@"0"][@"value"];
}

#pragma mark - property setters that trigger data load.

-(void) setCategoryId:(NSString *)categoryId {

//    [self clearData];
//    self.collapsedIndices = [NSMutableIndexSet indexSet];
//    
//    if ([categoryId isEqualToString:SWITCH_VALUE_FAV]) {
//        
//        [self updatesWithContentIds:[[UserManager sharedManager] getContentIdsFromFavContent] completion:^(NSArray *objects, NSError *error) {
//            if (error) {
//                [self showError:error];
//            } else {
//                for (ContentUpdate *update in objects) {
//                    if ([self.dateStrings containsObject:update.updatedOn]) {
//                        continue;
//                    }
//                    [self.dateStrings addObject:update.updatedOn];
//                }
//                if (self.dateStrings.count > 0) {
//                    self.dateString = [self.dateStrings objectAtIndex:0];
//                    [self loadPersonalisedContent];
//                }
//            }
//            [self hideBusyView];
//        }];
//        return;
//    }
//    _categoryId = categoryId;
//    self.sortField = nil;
//    [self setupSortSwitcher];
//    [self readCategoryWithId:categoryId completion:^(PFObject *category, NSError *error) {
//        if (error) {
//            [self showError:error];
//        } else {
//            _category = (ContentCategory *)category;
//            [self loadCategoryContent];
//        }
//    }];
}

-(void) setContent:(Content *)content {
    [self clearData];
    _content = content;
    self.showingContent = YES;
    [self setupSortSwitcher];
    [self loadContent];
}

-(void) setContentCategoryType:(NSString *)contentCategoryType {
    [self clearData];
    _contentCategoryType = contentCategoryType;
    self.showingContent = YES;
    [self setupSortSwitcher];
}

#pragma mark - IBActions

- (IBAction)sortChanged:(id)sender {
    
//    self.sortField = [self sortFieldAtIndex:self.sortSwitcher.selectedSegmentIndex];
//    
//    if (!self.sortField) {
//        abort();
//    }
//    
//    if (self.showingWatchLaterContent) {
//        self.episodes = [[[DataManager sharedManager] sortObjects:self.episodes orderBy:self.sortField ascending:([self.sortField isEqualToString:KEY_EPISODE_SAVED_AT] || [self.sortField isEqualToString:KEY_EPISODE_CONTENT_NAME] || [self.sortField isEqualToString:KEY_EPISODE_NUMBER])] mutableCopy];
//        [self.catTableView reloadData];
//    }
//    
//    if (self.showingContent) {
//        self.episodes = [[[DataManager sharedManager] sortObjects:self.episodes orderBy:self.sortField ascending:([self.sortField isEqualToString:KEY_EPISODE_CONTENT_NAME] || [self.sortField isEqualToString:KEY_EPISODE_NUMBER])] mutableCopy];
//        [self extractDatesFromEpisodes];
//        [self.catTableView reloadData];
//        
//    } else if (self.categoryId || self.showingPersonalized) {
//        
//        NSMutableDictionary *tempDict = self.episodesDictionary;
//        self.episodesDictionary = [NSMutableDictionary dictionary];
//        [self.catTableView reloadData];
//        
//        [[DataManager sharedManager] sortArraysInsideDictionary:tempDict orderBy:self.sortField ascending:([self.sortField isEqualToString:KEY_EPISODE_CONTENT_NAME] || [self.sortField isEqualToString:KEY_EPISODE_ADDED_ON]) completion:^(NSMutableDictionary *dictionary, NSError *error) {
//            self.episodesDictionary = dictionary;
//            [self.catTableView reloadData];
//        }];
//    }
}

#pragma mark - data load

-(void) loadContent {
//    self.loading = YES;
//    
//    [self findByContentId:self.content.objectId orderBy:self.sortField completion:^(NSArray *episodes, NSError *error) {
//        if (error) {
//            [self showError:error];
//            
//        } else {
//            self.episodes = [episodes mutableCopy];
//            [self extractDatesFromEpisodes];
//            [self.catTableView reloadData];
//        }
//        [self hideBusyView];
//        self.loading = NO;
//    }];
}

-(void) loadCategoryContent {
    self.loading = YES;
//    if (!self.dateString) {
//        // if have a datestring show for that date string. otherwise use dates array.
//        [self updatesWithCategoryId:self.categoryId skip:0 completion:^(NSArray *objects, NSError *error) {
//            if (error) {
//                [self showError:error];
//            } else {
//                _dates = objects;
//                if (objects.count > 0) {
//                    CategoryUpdate *day = [objects objectAtIndex:0];
//                    // TODO: check why below line was like
//                    // self.dateString = day.updatedOn;
//                    self.dateString = [Utility dateStringFromDate:day.updatedOn];
//                    [self loadCategoryContent];
//                }
//            }
//        }];
//    } else {
//        [self findByDateString:self.dateString categoryId:self.categoryId orderBy:self.sortField ascending:YES completion:^(NSArray *episodes, NSError *error) {
//            if (error) {
//                [self showError:error];
//                
//            } else {
//                if (episodes.count > 0) {
//                    [self.episodesDictionary setObject:episodes forKey:[NSString stringWithFormat:@"%ld", (long)self.selectedLoadIndex]];
//                }
//            }
//            if (self.episodesDictionary.allKeys.count > 0) {
//                [self hideSortSwitch];
//                if (self.episodesDictionary.allKeys.count == 1) {
//                    [self.catTableView reloadData];
//                } else {
//                    NSMutableIndexSet *set = [NSMutableIndexSet indexSetWithIndex:self.selectedLoadIndex];
//                    [self.catTableView reloadSections:set withRowAnimation:UITableViewRowAnimationAutomatic];
//                }
//            }
//            
//            [self hideBusyView];
//            self.loading = NO;
//        }];
//    }
}

-(void) loadPersonalisedContent {
//    self.loading = YES;
//    self.showingPersonalized = YES;
//    [self setupSortSwitcher];
//    [self findByDateString:self.dateString categoryIds:[[UserManager sharedManager] getCategoryIdsFromFavContent] contentIds:[[UserManager sharedManager] getContentIdsFromFavContent] orderBy:self.sortField completion:^(NSArray *episodes, NSError *error) {
//        if (error) {
//            [self showError:error];
//            
//        } else {
//            if (episodes.count > 0) {
//                [self.episodesDictionary setObject:episodes forKey:[NSString stringWithFormat:@"%ld", (long)self.selectedLoadIndex]];
//            }
//        }
//        if (self.episodesDictionary.allKeys.count > 0) {
//            [self hideSortSwitch];
//            if (self.episodesDictionary.allKeys.count == 1) {
//                [self.catTableView reloadData];
//            } else {
//                NSMutableIndexSet *set = [NSMutableIndexSet indexSetWithIndex:self.selectedLoadIndex];
//                [self.catTableView reloadSections:set withRowAnimation:UITableViewRowAnimationAutomatic];
//            }
//        }
//        
//        self.loading = NO;
//    }];
}

#pragma mark - helper methods

-(void) clearData {
    
    [self cancelQueries];
    [self showBusyView];
    [self hideSortSwitch];
    
    self.episodesDictionary = [NSMutableDictionary dictionary];
    self.dates = nil;
    self.dateStrings = [NSMutableArray array];
    self.dateString = nil;
    
    self.episodes = [NSMutableArray array];
    self.episodeDates = [NSMutableArray array];
    self.episodeInDateDictionary = [NSMutableDictionary dictionary];
    self.sortOptions = [NSMutableDictionary dictionary];
    
    self.collapsedIndices = [NSMutableIndexSet indexSet];
    self.loading = NO;
    self.selectedLoadIndex = 0;
    self.sortField = nil;
    self.category = nil;
    
    self.showingContent = NO;
    self.showingPersonalized = NO;
    self.showingWatchLaterContent = NO;
    
    [self.catTableView reloadData];
}

-(NSString *) sortFieldAtIndex:(NSInteger)index {
    NSString *key = [NSString stringWithFormat:@"%ld", (long)index];
    return [self.sortOptions objectForKey:key][@"value"];
}

-(void) extractDatesFromEpisodes {
//    self.episodeDates = [NSMutableArray array];
//    self.episodeInDateDictionary = [NSMutableDictionary dictionary];
//    for (Episode *episode in self.episodes) {
//        if ([self.episodeDates containsObject:episode.dateString]) {
//            NSInteger epis = [[self.episodeInDateDictionary objectForKey:episode.dateString] integerValue];
//            [self.episodeInDateDictionary setObject:[NSNumber numberWithInteger:(epis + 1)] forKey:episode.dateString];
//            continue;
//        }
//        [self.episodeInDateDictionary setObject:[NSNumber numberWithInteger:1] forKey:episode.dateString];
//        [self.episodeDates addObject:episode.dateString];
//    }
}

-(void) loadPreviosUpdateDay:(id)sender {
//    if (self.loading) {
//        return;
//    }
//    NSInteger section = [(UIButton *)sender tag];
//    
//    if (self.showingPersonalized) {
//        if (section == self.dateStrings.count) {
//            return;
//        } else {
//            self.selectedLoadIndex = section;
//            self.dateString = [self.dateStrings objectAtIndex:section];
//            [self loadPersonalisedContent];
//        }
//    } else {
//        if (section == self.dates.count) {
//            return;
//        } else {
//            self.selectedLoadIndex = section;
//            CategoryUpdate *update = [self.dates objectAtIndex:section];
//            self.dateString = [Utility dateStringFromDate:update.updatedOn];
//            [self loadCategoryContent];
//        }
//    }
}

-(NSInteger) numberOfRowsInSection:(NSInteger)section {
    if (self.showingContent) {
        NSString *date = [self.episodeDates objectAtIndex:section];
        NSNumber *num = [self.episodeInDateDictionary objectForKey:date];
        return [num integerValue];
    } else {
        int rows =  1;
        
        NSString *key = [NSString stringWithFormat:@"%ld", (long)section];
        NSArray *episodes = [self.episodesDictionary objectForKey:key];
        if (episodes) {
            return episodes.count;
        }
        return rows;
    }
}

# pragma mark - categories table view delegate methods.

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (self.showingWatchLaterContent) {
        return 1;
    }
    if (self.showingContent) {
        return self.episodeDates.count;
    }
    if (self.showingPersonalized) {
        return self.dateStrings.count;
    } else if(self.category) {
        return self.dates.count;
    }
    return 0;
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.showingWatchLaterContent) {
        return 0.1;
    }
    return HEADER_HEIGHT;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
//    if (self.showingWatchLaterContent) {
        return nil;
//    }
    
//    CGFloat height = HEADER_HEIGHT;
//    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, height)];
//    
//    [headerView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
//    headerView.backgroundColor = PLAIN_COLOR_BLACK;
//    
//    UILabel *sectionHeaderLabel = [[UILabel alloc] initWithFrame:CGRectMake(4, 0, tableView.bounds.size.width, height)];
//    sectionHeaderLabel.backgroundColor = PLAIN_COLOR_BLACK;
//    sectionHeaderLabel.textColor = PLAIN_COLOR_DARKER_WHITE;
//    sectionHeaderLabel.shadowColor = PLAIN_COLOR_DARKER_BLACK;
//    sectionHeaderLabel.shadowOffset = CGSizeMake(2.0, 2.0);
//    sectionHeaderLabel.textAlignment = NSTextAlignmentCenter;
//    sectionHeaderLabel.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:14.0f];
//    [sectionHeaderLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
//    [headerView addSubview:sectionHeaderLabel];
//    
//    if (self.showingContent) {
//        sectionHeaderLabel.text = [NSString stringWithFormat:@"Added On: %@", [[DataManager sharedManager] friendlyDateFromString:[self.episodeDates objectAtIndex:section]]];
//    } else {
//        if (self.showingPersonalized) {
//            NSString *date = [self.dateStrings objectAtIndex:section];
//            sectionHeaderLabel.text = [NSString stringWithFormat:@"Added On: %@", [[DataManager sharedManager] friendlyDateFromString:date]];
//        } else {
//            CategoryUpdate *update = [self.dates objectAtIndex:section];
//            sectionHeaderLabel.text = [NSString stringWithFormat:@"Added On: %@", [[DataManager sharedManager] friendlyDateFromString:update.updatedOn]];
//        }
//    }
//    
//    BOOL shouldShowButton = FALSE;
//    if (self.showingContent) {
//        shouldShowButton = TRUE;
//    } else {
//        NSString *key = [NSString stringWithFormat:@"%ld",(long)section];
//        NSArray *episodes = [self.episodesDictionary objectForKey:key];
//        if (episodes) {
//            shouldShowButton = TRUE;
//        }
//    }
//    if (shouldShowButton) {
//        // add collapse/expand buttons.
//        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//        button.frame = CGRectMake(tableView.bounds.size.width - COLLAPSE_EXPAND_BUTTON_WIDTH, 0, COLLAPSE_EXPAND_BUTTON_WIDTH, height);
//        [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
//        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
//        [button.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:16.0]];
//        if ([self isSectionCollapsed:section]) {
//            [button setTitle:DOWN_ARROW forState:UIControlStateNormal];
//        } else {
//            [button setTitle:UP_ARROW forState:UIControlStateNormal];
//        }
//        [button addTarget:self action:@selector(expandCollapseSection:) forControlEvents:UIControlEventTouchUpInside];
//        [button setTag:section];
//        [button setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin];
//        [headerView addSubview:button];
//    }
//    
//    return headerView;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.showingWatchLaterContent) {
        return self.episodes.count;
    }
    
    if ([self isSectionCollapsed:section]) {
        return 0;
    }
    
    return [self numberOfRowsInSection:section];
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.showingContent || self.showingWatchLaterContent) {
        return 81;
    }
    
    NSString *key = [NSString stringWithFormat:@"%ld",(long)indexPath.section];
    NSArray *episodes = [self.episodesDictionary objectForKey:key];
    if (episodes) {
        return 81;
    } else return 44;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.showingWatchLaterContent) {
        Episode *episode = [self.episodes objectAtIndex:indexPath.row];
        
        ShowView *showView = [tableView dequeueReusableCellWithIdentifier:SHOW_CELL_REUSE_ID];
        [showView populateCellWithShow:episode enableContentButton:YES showContentImage:YES];
        return showView;
    } else if (self.showingContent) {
        
        NSInteger index = indexPath.row;
        for (NSInteger ind = 0; ind < indexPath.section; ind ++) {
            int i = [tableView numberOfRowsInSection:ind];
            index += i;
        }
//        for (NSInteger ind=indexPath.section; ind > 0; ind--) {
//            index += [tableView numberOfRowsInSection:ind];
//        }
        
        Episode *episode = [self.episodes objectAtIndex:index];
        
        ShowView *showView = [tableView dequeueReusableCellWithIdentifier:SHOW_CELL_REUSE_ID];
        [showView populateCellWithShow:episode enableContentButton:YES showContentImage:YES];
        
        return showView;
    } else {
        
        NSString *key = [NSString stringWithFormat:@"%ld",(long)indexPath.section];
        NSArray *episodes = [self.episodesDictionary objectForKey:key];
        if (episodes) {
            
            Episode *episode = [episodes objectAtIndex:indexPath.row];
            
            ShowView *showView = [tableView dequeueReusableCellWithIdentifier:SHOW_CELL_REUSE_ID];
            [showView populateCellWithShow:episode enableContentButton:YES showContentImage:YES];
            
            return showView;
        } else {
            LoadMoreCell *cell = [tableView dequeueReusableCellWithIdentifier:LOAD_MORE_CELL_REUSE_ID];
//            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.loadMoreButton.tag = indexPath.section;
            cell.loadMoreButton.hidden = NO;
            [cell.activityIndicator stopAnimating];
            [cell.loadMoreButton addTarget:self action:@selector(loadPreviosUpdateDay:) forControlEvents:UIControlEventTouchUpInside];
            
            if (indexPath.section == 0) {
                cell.loadMoreButton.hidden = YES;
                [cell.activityIndicator startAnimating];
            }
            
            return cell;
        }
    }
    abort();
}

#pragma mark - table sections collapse/expand related

-(void) expandCollapseSection:(id) sender {
    UIButton *button = (UIButton *)sender;
    NSInteger section = [button tag];
    if ([self isSectionCollapsed:section]) {
        [button setTitle:UP_ARROW forState:UIControlStateNormal];
        [self expandSection:section];
    } else {
        [button setTitle:DOWN_ARROW forState:UIControlStateNormal];
        [self collapseSection:section];
    }
}

-(void) collapseSection:(NSInteger)section {
    NSArray *indices = [self indicesOfRowsInSection:section];
    if (indices) {
        [self.collapsedIndices addIndex:section];
        [self.catTableView deleteRowsAtIndexPaths:indices withRowAnimation:UITableViewRowAnimationTop];
    }
}

-(void) expandSection:(NSInteger)section {
    NSArray *indices = [self indicesOfRowsInSection:section];
    if (indices) {
        [self.collapsedIndices removeIndex:section];
        [self.catTableView insertRowsAtIndexPaths:indices withRowAnimation:UITableViewRowAnimationTop];
    }
}

-(BOOL) isSectionCollapsed:(NSInteger)section {
    if ([self.collapsedIndices containsIndex:section]) {
        return YES;
    }
    return NO;
}

-(NSArray *) indicesOfRowsInSection:(NSInteger)section {
    NSInteger rows = [self numberOfRowsInSection:section];
    if (rows == 0) {
        return nil;
    }
    
    NSMutableArray *indices = [NSMutableArray array];
    for (int i=0; i<rows; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:section];
        [indices addObject:indexPath];
    }
    return indices;
}

#pragma mark - scroll view delegate and show/hide sort switch related

-(void) hideSortSwitch {
    [self.scrollView setContentOffset:CGPointMake(0, SORT_SWITCH_HEIGHT) animated:YES];
    self.switchVisible = NO;
}

-(void) showSortSwitch {
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    self.switchVisible = YES;
}

-(BOOL) sortSwitchVisible {
    if (self.scrollView.contentOffset.y < SORT_SWITCH_HEIGHT) {
        return YES;
    }
    return NO;
}

-(void) scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (scrollView == self.catTableView) {
        self.tieSwitchWithTable = TRUE;
    }
}

-(void) scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.catTableView && self.tieSwitchWithTable) {
        if (scrollView.contentOffset.y < 0) {
            if (self.switchVisible) {
                self.scrollView.contentOffset = CGPointMake(0, scrollView.contentOffset.y);
            } else {
                self.scrollView.contentOffset = CGPointMake(0, SORT_SWITCH_HEIGHT + scrollView.contentOffset.y);
            }
        }
    }
}

-(void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (scrollView == self.catTableView) {
        self.tieSwitchWithTable = FALSE;
        if (scrollView.contentOffset.y > 5) {
            [self hideSortSwitch];
        } else if (scrollView.contentOffset.y < -5) {
            [self showSortSwitch];
        }
    }
}

#pragma mark - busy view related

-(void) showBusyView {
    self.busyOverlayView.hidden = NO;
    self.busyOverlayView.alpha = 0.0;
    [UIView animateWithDuration:0.1f animations:^{
        self.busyOverlayView.alpha = 0.5;
    }];
}

-(void) hideBusyView {
    
//    [self.busyWheel stopAnimating];
    self.busyOverlayView.alpha = 0.5;
    [UIView animateWithDuration:0.3f animations:^{
        self.busyOverlayView.alpha = 0.0;
    } completion:^(BOOL finished) {
        self.busyOverlayView.hidden = YES;
    }];
}

@end
