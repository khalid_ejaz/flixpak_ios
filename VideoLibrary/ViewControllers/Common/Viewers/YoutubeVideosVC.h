//
//  YoutubeVideosVC.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/1/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "ViewerVC.h"

@class YCSeachVideoResult;

@protocol YouTubeVideosVCDelegate <NSObject>

@optional
-(void) hideKeyboard;

@end

@interface YoutubeVideosVC : ViewerVC <UIScrollViewDelegate, UITextFieldDelegate>

@property (nonatomic, strong) NSString *channelId;
@property (nonatomic, strong) YCSeachVideoResult *videoResult;

@property (nonatomic, strong) NSString *searchText;
@property (nonatomic, strong) NSString *presetSortOrder;
@property (nonatomic, unsafe_unretained) id<YouTubeVideosVCDelegate> delegate;

// hides channel button in rows. already showing channel;
@property (nonatomic, assign) BOOL showingChannel;

@end
