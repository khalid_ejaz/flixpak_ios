//
//  CustomVC.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/24/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "CustomVC.h"

@interface CustomVC ()

@property (nonatomic, strong) NSArray *favContentIds;
@property (nonatomic, strong) NSArray *updates;
@property (nonatomic, strong) NSMutableDictionary *episodesDictionary;

@end

@implementation CustomVC

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void) createDataSources {
    self.favContentIds = [[UserManager sharedManager] getContentIdsFromFavContent];
    self.updates = nil;
    self.episodesDictionary = [NSMutableDictionary dictionary];
}

-(NSArray *) sortOptions {
//    SwitchOption *option1 = [[SwitchOption alloc] initWithDisplayText:@"Latest" value:KEY_EPISODE_ADDED_ON ascending:YES];
//    SwitchOption *option2 = [[SwitchOption alloc] initWithDisplayText:@"Rating" value:KEY_CONTENT_RATING_POINTS ascending:YES];
//    SwitchOption *option3 = [[SwitchOption alloc] initWithDisplayText:@"Views" value:KEY_EPISODE_VIEWS ascending:YES];
//    return @[option1, option2, option3];
    return nil;
}

#pragma mark - data load

-(void) loadUpdates {
    [self updatesWithContentIds:self.favContentIds completion:^(NSArray *objects, NSError *error) {
        if (error) {
            [self showNetworkError];
            return;
        } else {
            _updates = objects;
            if (_updates.count > 0) {
                [self loadEpisodesForDateString:[self dateStringForSection:0] section:0];
            }
        }
    }];
}

-(void) loadEpisodesForDateString:(NSString *)dateString section:(NSInteger)section {
    [self findByDateString:dateString contentIds:self.favContentIds orderBy:self.selectedSortOption.value ascending:self.selectedSortOption.ascending completion:^(NSArray *objects, NSError *error) {
        if (error) {
            [self showNetworkError];
            [self resetLoadMoreCellAtSection:section];
            return;
        } else {
            [_episodesDictionary setObject:objects forKey:dateString];
            if (_episodesDictionary.allKeys.count == 1) {
                [self hideSpinner];
                [self.tableView reloadData];
            } else {
                if (objects.count == 0) {
                    [self resetLoadMoreCellAtSection:section];
                }
                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
        }
    }];
}

// load more event
-(void) loadSection:(id)sender {
    NSInteger section = [(UIButton *)sender tag];
    NSString *dateString = [self dateStringForSection:section];
    [self loadEpisodesForDateString:dateString section:section];
}

#pragma mark - helper methods

-(NSString *) dateStringForSection:(NSInteger)section {
    if (section == _updates.count) {
        abort();
    }
    return [self keyForSection:section];
}

-(NSString *)keyForSection:(NSInteger)section {
//    ContentUpdate *update = _updates[section];
//    return [Utility dateStringFromDate:update.updatedOn];
    return nil;
}

-(NSInteger) numberOfEpisodesForSection:(NSInteger)section {
    NSInteger count = [[self episodesForSection:section] count];
    return count;
}

-(NSArray *) episodesForSection:(NSInteger)section {
    return self.episodesDictionary[[self keyForSection:section]];
}

#pragma mark - sort changed

-(void) sortWithColumn:(NSString *)column ascending:(BOOL)ascending {
    if (_updates.count == 0) {
        [self loadUpdates];
        
    } else {
        // do local sort.
        [[DataManager sharedManager] sortArraysInsideDictionary:self.episodesDictionary orderBy:column ascending:ascending completion:^(NSMutableDictionary *dictionary, NSError *error) {
            if (error) {
                [self showGeneralError];
                return;
            } else {
                _episodesDictionary = dictionary;
                [self.tableView reloadData];
            }
        }];
    }
}

#pragma mark - table view data source

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return _updates.count;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    CGFloat height = HEADER_HEIGHT;
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, height)];
    
    [headerView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    headerView.backgroundColor = PLAIN_COLOR_BLACK;
    
    UILabel *sectionHeaderLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, height)];
    sectionHeaderLabel.backgroundColor = PLAIN_COLOR_BLACK;
    sectionHeaderLabel.textColor = PLAIN_COLOR_DARKER_WHITE;
    sectionHeaderLabel.shadowColor = PLAIN_COLOR_DARKER_BLACK;
    sectionHeaderLabel.shadowOffset = CGSizeMake(2.0, 2.0);
    sectionHeaderLabel.textAlignment = NSTextAlignmentCenter;
    sectionHeaderLabel.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:14.0f];
    [sectionHeaderLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [headerView addSubview:sectionHeaderLabel];
    
    sectionHeaderLabel.text = [NSString stringWithFormat:@"Added On: %@", [[DataManager sharedManager] friendlyDateFromString:[self dateStringForSection:section]]];
    
    return headerView;
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return HEADER_HEIGHT;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self numberOfEpisodesForSection:indexPath.section] == 0) {
        return 44;
    }
    
    return IS_IPAD? IPAD_SHOW_CELL_HEIGHT: IPHONE_ROW_CELL_HEIGHT;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger rows = [self numberOfEpisodesForSection:section];
    if (rows == 0) {
        rows = 1;
    }
    
    return rows;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self numberOfEpisodesForSection:indexPath.section] == 0) {
        // not loaded. show load more.
        LoadMoreCell *cell = [tableView dequeueReusableCellWithIdentifier:LOAD_MORE_CELL_ID];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.loadMoreButton.tag = indexPath.section;
        cell.loadMoreButton.hidden = NO;
        [cell.activityIndicator stopAnimating];
        [cell.loadMoreButton addTarget:self action:@selector(loadSection:) forControlEvents:UIControlEventTouchUpInside];
        
        if (indexPath.section == 0) {
            cell.loadMoreButton.hidden = YES;
            [cell.activityIndicator startAnimating];
        }
        
        return cell;
    } else {
        // loaded. show episode.
        Episode *episode = [[self episodesForSection:indexPath.section] objectAtIndex:indexPath.row];
        
        VideoItemCell *cell = [tableView dequeueReusableCellWithIdentifier:VIDEO_ITEM_CELL_ID];
        [cell populateWithEpisode:episode contentButtonEnabled:YES showContentImage:YES];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}


@end
