//
//  ViewerVC.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/24/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "RootViewController.h"
#import "UserManager.h"
#import "SwitchControl.h"
#import "SwitchOption.h"
#import "ShowView.h"
#import "LargeShowViewCell.h"
#import "VideoItemCell.h"
#import "LoadMoreCell.h"
#import "Utility.h"
#import <PSTCollectionView.h>

#define LARGE_SHOW_CELL_ID              @"largeShowCellId"
#define SHOW_CELL_ID                    @"showCellId"
#define LOAD_MORE_CELL_ID               @"loadMoreCellId"
#define YOUTUBE_VIDEO_CELL_ID           @"youtubeVideoCellId"
#define YOUTUBE_LARGE_VIDEO_CELL_ID     @"largeYoutubeVideoCellId"
#define VIDEO_ITEM_CELL_ID              @"videoItemCellId"

#define IPAD_SHOW_CELL_HEIGHT       156
#define IPHONE_ROW_CELL_HEIGHT      161
#define HEADER_HEIGHT   20

@interface ViewerVC : RootViewController <SwitchControlDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, PSTCollectionViewDataSource, PSTCollectionViewDelegate, UIActionSheetDelegate>

@property (nonatomic, assign) BOOL hasMore;
@property (nonatomic, assign) int skip;

@property (nonatomic, strong) PSTCollectionView *collectionView;    // TODO: when done remove tableview.
@property (unsafe_unretained, nonatomic) IBOutlet UITableView *tableView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *searchView;
@property (unsafe_unretained, nonatomic) IBOutlet UITextField *searchTextField;

@property (nonatomic, strong) NSMutableIndexSet *collapsedIndices;

@property (weak, nonatomic) IBOutlet UIButton *filterFavButton;
@property (nonatomic, strong) SwitchOption *selectedSortOption;

// called from HomeViewController to start loading first data.
-(void) loadData;
-(void) createDataSources;

// override to call after loaddata is complete. (must override loadData for this).
-(void) setSortControl;

// override to provide sort options.
-(NSArray *) sortOptions;
-(void) sortWithColumn:(NSString *)column ascending:(BOOL)ascending;

// removes search view. some vcs may not need to show search view.
-(void) removeSearchView;

// override must if using search.
-(void) searchAction:(NSString *)searchText;
-(void) searchAllAction:(NSString *)searchText;

// call to add collapse button.
-(void) addCollapseButtonToHeader:(UIView *)header section:(NSInteger)section;

// call to check if number of rows in section should be zero. (if collapsed).
-(BOOL) isSectionCollapsed:(NSInteger)section;

// once load is failed. re-enable load button and hide busy wheel.
-(void) resetLoadMoreCellAtSection:(NSInteger)section;

- (CGSize)collectionView:(PSTCollectionView *)collectionView layout:(PSTCollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;

@end
