//
//  YoutubeVideosVC.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/1/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "YoutubeVideosVC.h"
#import "YoutubeVideo.h"
#import "YCSeachVideoResult.h"
#import "DataManager.h"
#import "YoutubeVideoCell.h"
#import "LargeYoutubeVideoCell.h"
#import "Utility.h"

#define SEARCH_ORDER_DATE           @"date"
#define SEARCH_ORDER_RATING         @"rating"
#define SEARCH_ORDER_RELEVANCE      @"relevance"
#define SEARCH_ORDER_TITLE          @"title"
#define SEARCH_ORDER_VIEW_COUNT     @"viewCount"

#define PREV_BUTTON_TAG     1
#define NEXT_BUTTON_TAG     2

@interface YoutubeVideosVC ()

@property (nonatomic, strong) NSString *uploadId;

@property (nonatomic, strong) UITextField *searchField;
@property (nonatomic, strong) NSString *searchOrder;
@property (nonatomic, assign) BOOL showingSearchResults;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@end

@implementation YoutubeVideosVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.tableHeaderView = nil;
    self.showingSearchResults = NO;
    self.searchOrder = SEARCH_ORDER_DATE;
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(loadData) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:refreshControl];
    refreshControl.tintColor = [UIColor whiteColor];
    self.refreshControl = refreshControl;
    [self removeSearchView];
    
    if(self.presetSortOrder) {
        for (SwitchOption *option in self.sortOptions) {
            if ([option.value isEqualToString:self.presetSortOrder]) {
                self.selectedSortOption = option;
                self.searchOrder = self.presetSortOrder;
                break;
            }
        }
    }
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.collectionView reloadData];
}

-(NSArray *) sortOptions {
    SwitchOption *opt2 = [[SwitchOption alloc] initWithDisplayText:@"Relevance" value:SEARCH_ORDER_RELEVANCE ascending:YES];
    SwitchOption *opt1 = [[SwitchOption alloc] initWithDisplayText:@"Date" value:SEARCH_ORDER_DATE ascending:YES];
    SwitchOption *opt3 = [[SwitchOption alloc] initWithDisplayText:@"Rating" value:SEARCH_ORDER_RATING ascending:YES];
    SwitchOption *opt4 = [[SwitchOption alloc] initWithDisplayText:@"View Count" value:SEARCH_ORDER_VIEW_COUNT ascending:YES];
    return @[opt1, opt2, opt3, opt4];
}

-(void) loadData {
    if (!self.channelId && !self.searchText) {
        [self showGeneralError];
        return;
    }
    
    if (self.channelId) {
        self.showingChannel = YES;
        [self showSpinner];
        [self setupHeaderView];
        [self findUploadsIdByChannelId:self.channelId];
    } else {
        [self setupHeaderView];
        [self searchActionWithPageToken:nil];
    }
}

-(void) setSortControl {
    // no sort control;
}

-(void) setupHeaderView {
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,(self.channelId)?77:33)];
    headerView.backgroundColor = GRAY_WITH_PERCENT(15);
    [headerView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    
    // only add search bar if there is a channel id. otherwise it is being shown form video result form YouTube search VC.
    if (self.channelId) {
        UIView *searchBackgroundView = [[UIView alloc] init];
        searchBackgroundView.frame = CGRectMake(0, 0, self.tableView.bounds.size.width, IS_IPAD?44:38);
        searchBackgroundView.backgroundColor = GRAY_WITH_PERCENT(15);
        searchBackgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        
        UITextField *searchField = [[UITextField alloc] init];
        searchField.frame = CGRectMake(4, IS_IPAD?12:4, searchBackgroundView.bounds.size.width-8, 30);
        searchField.borderStyle = UITextBorderStyleRoundedRect;
        searchField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        searchField.backgroundColor = GRAY_WITH_PERCENT(70);
        searchField.textColor = GRAY_WITH_PERCENT(20);
        searchField.font = [UIFont fontWithName:@"STHeitiTC-Medium  " size:16];
        searchField.placeholder = @"Search in channel";
        searchField.autocorrectionType = UITextAutocorrectionTypeNo;
        searchField.textAlignment = NSTextAlignmentCenter;
        [searchField setClearButtonMode:UITextFieldViewModeAlways];
        searchField.enablesReturnKeyAutomatically = YES;
        searchField.delegate = self;
        self.searchField = searchField;
        
        [searchBackgroundView addSubview:searchField];
        [headerView addSubview:searchBackgroundView];
    }
    
    SwitchControl *control = [[NSBundle mainBundle] loadNibNamed:@"SwitchControl" owner:self options:nil][0];
    [control setFrame:CGRectMake(8, self.channelId?(IS_IPAD?42:35):0, self.tableView.bounds.size.width-16, 33)];
    [control populateWithSwitchOptions:self.sortOptions controlBackColor:GRAY_WITH_PERCENT(15) optionBackColor:GRAY_WITH_PERCENT(15) textColor:GRAY_WITH_PERCENT(60) showingSort:NO];
    control.delegate = self;
    control.autoresizingMask = UIViewAutoresizingFlexibleWidth;

    [headerView addSubview:control];
    self.tableView.tableHeaderView = headerView;
    [self.tableView setContentOffset:CGPointMake(0, self.channelId?76:33) animated:NO];
}

-(void) searchActionWithPageToken:(NSString *)pageToken {
    if (!self.showingSearchResults) {
        self.showingSearchResults = YES;
        self.videoResult = nil;
        [self showSpinner];
//        [self.tableView reloadData];
    }
    NSString *searchString;
    if (self.searchText) {
        searchString = self.searchText;
    } else {
        searchString = [Utility stringByRemovingWhiteSpaces:self.searchField.text];
    }
    [[DataManager sharedManager] findVideosByChannelId:self.channelId searchString:searchString order:self.searchOrder pageToken:pageToken completion:^(YCSeachVideoResult *result, NSError *error) {
        if (error) {
            [self showError:error];
        } else {
            if (self.videoResult.videoIds.count == 0) {
                [self hideSpinner];
                [self.refreshControl endRefreshing];
                _videoResult = result;
            } else {
                [self.videoResult.videoIds addObjectsFromArray:result.videoIds];
                [self.videoResult.youtubeVideos addObjectsFromArray:result.youtubeVideos];
                _videoResult.nextPageToken = result.nextPageToken;
                _videoResult.prevPageToken = result.prevPageToken;
                //                _videoResult = result;
            }
            [self loadVideoDetailsWithResult:self.videoResult];
        }
    }];
}

-(void) prevAction {
    if (self.showingSearchResults) {
        [self searchActionWithPageToken:self.videoResult.prevPageToken];
        return;
    }
    [self findVideosByPageToken:self.videoResult.prevPageToken];
}

-(void) nextAction {
    if (self.showingSearchResults) {
        [self searchActionWithPageToken:self.videoResult.nextPageToken];
        return;
    }
    [self findVideosByPageToken:self.videoResult.nextPageToken];
}

-(void)findUploadsIdByChannelId:(NSString *)channelId {
    [[DataManager sharedManager] findUploadsIdByChannelId:channelId completion:^(NSArray *objects, NSError *error) {
        if (error) {
            [self showError:error];
            [self.refreshControl endRefreshing];
        } else {
            NSLog(@"Upload playlist ids: %@", objects);
            if (objects.count > 0) {
                _uploadId = [NSString stringWithFormat:@"%@",objects[0]];
                [self findVideosByPageToken:nil];
            } else {
                [self hideSpinner];
                [self.refreshControl endRefreshing];
            }
        }
    }];
}

-(void) findVideosByPageToken:(NSString *)pageToken {
    [[DataManager sharedManager] findVideosByUploadId:self.uploadId pageToken:pageToken completion:^(YCSeachVideoResult *result, NSError *error) {
        if (error) {
            [self showError:error];
            [self.refreshControl endRefreshing];
        } else {
            if ((self.videoResult.videoIds.count == 0 || result.videoIds.count == 0) || pageToken == nil) {
                [self hideSpinner];
                [self.refreshControl endRefreshing];
                _videoResult = result;
            } else {
                [self.videoResult.videoIds addObjectsFromArray:result.videoIds];
                [self.videoResult.youtubeVideos addObjectsFromArray:result.youtubeVideos];
                _videoResult.nextPageToken = result.nextPageToken;
                _videoResult.prevPageToken = result.prevPageToken;
//                _videoResult = result;
            }
            [self loadVideoDetailsWithResult:self.videoResult];
        }
    }];
}

-(void) loadVideoDetailsWithResult:(YCSeachVideoResult *)result {
    
    NSArray *videoIdsWithoutDetail = [result videoIdsThatHaveNoDetail];
    
    [[DataManager sharedManager] findVideoDetailByVideoIds:videoIdsWithoutDetail completion:^(NSArray *objects, NSError *error) {
        if (error) {
            [self showError:error];
            [self.refreshControl endRefreshing];
        } else {
            if (objects.count == videoIdsWithoutDetail.count) {
                int startAtIndex = self.videoResult.videoIds.count - videoIdsWithoutDetail.count;
                for (int i=0; i<objects.count; i++) {
                    NSDictionary *itemDict = objects[i];
                    YoutubeVideo *video = result.youtubeVideos[i+startAtIndex];
                    [video populateVideoDetail:itemDict];
                }
                
                [self hideSpinner];
                [self.refreshControl endRefreshing];
            } else {
                [self showNetworkError];
            }
//            if (result.nextPageToken) {
//                self.tableView.tableFooterView = [self createFooterView];
//            }
            
//            if (result.prevPageToken) {
//                self.tableView.tableHeaderView = [self createHeaderView];
//            }
            [self.collectionView reloadData];
//            [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
        }
    }];
    
}

#pragma mark - collection view data source

-(NSInteger) numberOfSectionsInCollectionView:(PSTCollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(PSTCollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (!self.videoResult) {
        return 0;
    }
    
    int results = self.videoResult.youtubeVideos.count;
    if (results != 0 && results%10 == 0 && self.videoResult.nextPageToken != nil) {
        return self.videoResult.youtubeVideos.count + 1;
    } else return self.videoResult.youtubeVideos.count;
}

- (PSTCollectionViewCell *)collectionView:(PSTCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.videoResult.youtubeVideos.count) {
        LoadMoreCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:LOAD_MORE_CELL_ID forIndexPath:indexPath];
        [cell.loadMoreButton addTarget:self action:@selector(nextAction) forControlEvents:UIControlEventTouchUpInside];
        //        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.loadMoreButton.tag = indexPath.section;
        cell.loadMoreButton.hidden = NO;
        [cell.activityIndicator stopAnimating];
        [cell reset];
        [cell.loadMoreButton setTitle:@"More..." forState:UIControlStateNormal];
        return cell;
    } else {
        VideoItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:VIDEO_ITEM_CELL_ID forIndexPath:indexPath];
        [cell populateWithVideo:self.videoResult.youtubeVideos[indexPath.row] showChannelTitleButton:!self.showingChannel];
        //        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

- (CGSize)collectionView:(PSTCollectionView *)collectionView layout:(PSTCollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return [VideoItemCell sizeForVideoItem];
    
    //    if (!IS_IPAD) {
    //        if ([self isPortrait]) {
    //            return CGSizeMake(IPHONE_VIDEO_THUMBNAIL_WIDTH, IPHONE_VIDEO_THUMBNAIL_HEIGHT);
    //        } else return CGSizeMake(IPHONE_VIDEO_THUMBNAIL_WIDTH_LANDSCAPE, IPHONE_VIDEO_THUMBNAIL_HEIGHT);
    //    } else {
    //        if ([self isPortrait]) {
    //            return CGSizeMake(IPAD_VIDEO_THUMBNAIL_WIDTH, IPAD_SHOW_CELL_HEIGHT);
    //        } else {
    //            return CGSizeMake(IPAD_VIDEO_THUMBNAIL_WIDTH_LANDSCAPE, IPAD_VIDEO_THUMBNAIL_HEIGHT_LANDSCAPE);
    //        }
    //    }
}


#pragma mark - table view delegate

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (!self.videoResult) {
        return 0;
    }
    
    int results = self.videoResult.youtubeVideos.count;
    if (results != 0 && results%10 == 0 && self.videoResult.nextPageToken != nil) {
        return self.videoResult.youtubeVideos.count + 1;
    } else return self.videoResult.youtubeVideos.count;
    
//    if (self.videoResult && self.videoResult.youtubeVideos.count!= 0 && self.videoResult.nextPageToken != nil) {
//        
//        return self.videoResult.youtubeVideos.count+1;
//    }
//    return self.videoResult.youtubeVideos.count;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.videoResult.youtubeVideos.count) {
        return 44; // load more cell height.
    }
    
    return IS_IPAD?IPAD_SHOW_CELL_HEIGHT:IPHONE_ROW_CELL_HEIGHT;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.videoResult.youtubeVideos.count) {
        LoadMoreCell *cell = [tableView dequeueReusableCellWithIdentifier:LOAD_MORE_CELL_ID];
        [cell.loadMoreButton addTarget:self action:@selector(nextAction) forControlEvents:UIControlEventTouchUpInside];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.loadMoreButton.tag = indexPath.section;
        cell.loadMoreButton.hidden = NO;
        [cell.activityIndicator stopAnimating];
        [cell reset];
        [cell.loadMoreButton setTitle:@"More..." forState:UIControlStateNormal];
        return cell;
    } else {
        VideoItemCell *cell = [tableView dequeueReusableCellWithIdentifier:VIDEO_ITEM_CELL_ID];
        [cell populateWithVideo:self.videoResult.youtubeVideos[indexPath.row] showChannelTitleButton:!self.showingChannel];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

#pragma mark - scroll view delegate

-(void) scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (!self.searchField) {
        if (self.delegate) {
            if ([self.delegate respondsToSelector:@selector(hideKeyboard)]) {
                [self.delegate hideKeyboard];
            }
        }
    }
    
    if ([self.searchField isFirstResponder]) {
        [self.searchField resignFirstResponder];
    }
}

#pragma mark - text field delegate

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.searchField) {
        
//        NSString *searchString = [Utility stringByRemovingWhiteSpaces:self.searchField.text];
//        if (searchString.length < 3) {
//            return NO;
//        }
        
        [self.searchField resignFirstResponder];
        self.showingSearchResults = NO;
        [self searchActionWithPageToken:nil];
    }
    return NO;
}

#pragma mark - sort control

-(void) switchControl:(id)control selectedOption:(SwitchOption *)option {
    self.searchOrder = option.value;
    NSString *searchString;
    if (self.searchText) {
        searchString = self.searchText;
    } else {
        searchString = [Utility stringByRemovingWhiteSpaces:self.searchField.text];
        [self.searchField resignFirstResponder];
    }
    
//    if (searchString.length < 3) {
//        return;
//    }
    
    self.showingSearchResults = NO;
    [self searchActionWithPageToken:nil];
}

-(void) sortWithColumn:(NSString *)column ascending:(BOOL)ascending {
    self.searchOrder = column;
    NSString *searchString;
    if (self.searchText) {
        searchString = self.searchText;
    } else {
        searchString = [Utility stringByRemovingWhiteSpaces:self.searchField.text];
        [self.searchField resignFirstResponder];
    }
    self.showingSearchResults = NO;
    [self searchActionWithPageToken:nil];
}

@end
