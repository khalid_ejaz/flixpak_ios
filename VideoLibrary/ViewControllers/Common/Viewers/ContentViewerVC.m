//
//  ContentViewerVC.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/24/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "ContentViewerVC.h"

@interface ContentViewerVC ()

@property (nonatomic, strong) NSMutableArray *episodes;

@end

@implementation ContentViewerVC

-(void) viewDidLoad {
    [super viewDidLoad];
    [self removeSearchView];
}

-(void) loadData {
    // sort control triggers load by auto selecting first option.
//    self.hasMore = YES;
//    [self createDataSources];
//    [self showSpinner];
//    [self.collectionView reloadData];
//    if (!self.categoryType) {
//        [self readCategoryWithId:self.content.categoryId completion:^(PFObject *object, NSError *error) {
//            if (error) {
//                [self showNetworkError];
//                return;
//            } else if(!object) {
//                [self showGeneralError];
//                return;
//            } else {
//                self.categoryType = [(ContentCategory *)object categoryType];
//                [self setSortControl];
//                if (!IS_IPAD) {
//                    [self.tableView setContentOffset:CGPointMake(0, 44)];
//                }
//            }
//        }];
//    } else {
//        [self setSortControl];
//        if (!IS_IPAD) {
//            [self.tableView setContentOffset:CGPointMake(0, 44)];
//        }
//    }
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.collectionView reloadData];
}

-(void) createDataSources {
    self.episodes = nil;
}

-(NSArray *) sortOptions {
    
//    SwitchOption *option1;
//    
//    if (self.categoryType) {
//        if ([self.categoryType isEqualToString:CATEGORY_TYPE_DATE]) {
//            option1 = [[SwitchOption alloc] initWithDisplayText:@"Date" value:KEY_EPISODE_ADDED_ON ascending:NO];
//        } else if ([self.categoryType isEqualToString:CATEGORY_TYPE_EPISODE]) {
//            option1 = [[SwitchOption alloc] initWithDisplayText:@"Episode" value:KEY_EPISODE_NUMBER ascending:YES];
//        } else {
//            option1 = [[SwitchOption alloc] initWithDisplayText:@"Alphabetical" value:KEY_EPISODE_CONTENT_NAME ascending:YES];
//        }
//    } else {
//        
//    }
//    
//    SwitchOption *option2 = [[SwitchOption alloc] initWithDisplayText:@"Rating" value:KEY_CONTENT_RATING_POINTS ascending:YES];
//    SwitchOption *option3 = [[SwitchOption alloc] initWithDisplayText:@"Views" value:KEY_EPISODE_VIEWS ascending:YES];
//    
//    return @[option1, option2, option3];
    return nil;
}

#pragma mark - data load

-(void) LoadWithSkip {
//    self.skip += MAX_RESULTS_EPISODES;
//    [self loadEpisodes];
}

-(void) loadEpisodes {
//    SwitchOption *defaultOption = [[self sortOptions] objectAtIndex:0];
//    [self findByContentId:self.content.objectId orderBy:defaultOption.value ascending:defaultOption.ascending skip:self.skip completion:^(NSArray *objects, NSError *error) {
//        if (error) {
//            [self showGeneralError];
//            return ;
//        } else {
//            if (objects.count > 0) {
//                if (_episodes.count > 0) {
//                    [_episodes addObjectsFromArray:objects];
//                } else {
//                    _episodes = [objects mutableCopy];
//                }
//                if (objects.count < MAX_RESULTS_EPISODES) {
//                    self.hasMore = NO;
//                }
//            } else {
//                self.hasMore = NO;
//            }
//            [self hideSpinner];
//            [self.collectionView reloadData];
//        }
//    }];
}

#pragma mark - collection view data source

- (NSInteger)collectionView:(PSTCollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (self.episodes.count == 0)
        return 0;
    if (!self.hasMore)
        return self.episodes.count;
    return self.episodes.count + 1;
}

-(NSInteger) numberOfSectionsInCollectionView:(PSTCollectionView *)collectionView {
    return 1;
}

- (PSTCollectionViewCell *)collectionView:(PSTCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.episodes.count) {
        LoadMoreCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:LOAD_MORE_CELL_ID forIndexPath:indexPath];
        [cell.loadMoreButton addTarget:self action:@selector(LoadWithSkip) forControlEvents:UIControlEventTouchUpInside];
        //        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.loadMoreButton.tag = indexPath.section;
        cell.loadMoreButton.hidden = NO;
        [cell.activityIndicator stopAnimating];
        [cell.loadMoreButton setTitle:@"More..." forState:UIControlStateNormal];
        return cell;
    } else {
        Episode *episode = [self.episodes objectAtIndex:indexPath.row];
        
        VideoItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:VIDEO_ITEM_CELL_ID forIndexPath:indexPath];
        [cell populateWithEpisode:episode contentButtonEnabled:NO showContentImage:NO];
        [cell hideFavButton];
        //        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

- (CGSize)collectionView:(PSTCollectionView *)collectionView layout:(PSTCollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return [VideoItemCell sizeForVideoItem];
}

#pragma mark - table view data source

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.episodes.count) {
        return 44;
    }
    return IS_IPAD? IPAD_SHOW_CELL_HEIGHT: IPHONE_ROW_CELL_HEIGHT;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.episodes.count == 0)
        return 0;
    if (!self.hasMore)
        return self.episodes.count;
    return self.episodes.count + 1;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.episodes.count) {
        LoadMoreCell *cell = (LoadMoreCell *)[tableView dequeueReusableCellWithIdentifier:LOAD_MORE_CELL_ID];
        [cell.loadMoreButton addTarget:self action:@selector(LoadWithSkip) forControlEvents:UIControlEventTouchUpInside];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.loadMoreButton.tag = indexPath.section;
        cell.loadMoreButton.hidden = NO;
        [cell.activityIndicator stopAnimating];
        [cell.loadMoreButton setTitle:@"More..." forState:UIControlStateNormal];
        return (UITableViewCell *)cell;
    } else {
        Episode *episode = [self.episodes objectAtIndex:indexPath.row];
        
        VideoItemCell *cell = (VideoItemCell *)[tableView dequeueReusableCellWithIdentifier:VIDEO_ITEM_CELL_ID];
        [cell populateWithEpisode:episode contentButtonEnabled:NO showContentImage:NO];
        [cell hideFavButton];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return (UITableViewCell *)cell;
    }
}

#pragma mark - sort changed

-(void) sortWithColumn:(NSString *)column ascending:(BOOL)ascending {
    if (_episodes.count == 0) {
        [self loadEpisodes];
        
    } else {
        // do local sort.
        self.episodes = [[[DataManager sharedManager] sortObjects:self.episodes orderBy:self.selectedSortOption.value ascending:self.selectedSortOption.ascending] mutableCopy];
        [self.collectionView reloadData];
    }
}

@end
