//
//  StyledCategoryViewController.h
//  PakTV
//
//  Created by Malik Khalid Ejaz on 11/15/2013.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"

@class Content;

@interface StyledCategoryViewController : RootViewController <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>

// category
@property (nonatomic, strong) NSString *categoryId;
@property (nonatomic, strong) NSString *dateString;

// content
@property (nonatomic, strong) Content *content;
@property (nonatomic, strong) NSString *contentCategoryType;

// watch later
@property (nonatomic, assign) BOOL showingWatchLaterContent;

// personalized.
@property (nonatomic, assign) BOOL showingPersonalized;

@end
