//
//  ContentViewerVC.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/24/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "ViewerVC.h"

@interface ContentViewerVC : ViewerVC

@property (nonatomic, strong) Content *content;
@property (nonatomic, strong) NSString *categoryType;

@property (nonatomic, assign) BOOL showContentImageInCell;

@end
