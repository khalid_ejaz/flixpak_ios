//
//  CategoryViewerVC.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/24/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "CategoryViewerVC.h"
#import "BrowseViewController.h"
#import "ThumbnailShowView.h"

@interface CategoryViewerVC ()

@property (nonatomic, strong) ContentCategory *category;

@property (nonatomic, strong) NSMutableDictionary *episodesDictionary;
@property (nonatomic, strong) NSMutableArray *updates;
@property (nonatomic, strong) NSMutableDictionary *filteredEpisodesDictionary;

@property (nonatomic, assign) BOOL favsOnly;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

// search related helpers
@property (nonatomic, assign) NSInteger lastSearchLength;
@property (nonatomic, assign) BOOL showingFilteredEpisodes;
@property (nonatomic, strong) NSString *searchString;

@end

@implementation CategoryViewerVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.lastSearchLength = 0;
//    if (self.categoryId && self.canShowFavs) {
//        if ([[UserManager sharedManager] hasFavContentForCategoryId:self.categoryId]) {
//            // TODO: check if user prefered it off, otherwise set to YES.
//            self.favsOnly = YES;
//        }
//    } else
//    self.canShowFavs = NO;
}

-(void) viewWillAppear:(BOOL)animated {
    BOOL firstLoad = self.firstLoadDone;
    [super viewWillAppear:animated];
    [self showTipForAction:TIP_ACTION_APP_LAUNCH];
    
    if (!firstLoad) {
        UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
        [refreshControl addTarget:self action:@selector(refreshControlPulled) forControlEvents:UIControlEventValueChanged];
        [self.collectionView addSubview:refreshControl];
        refreshControl.tintColor = [UIColor whiteColor];
        self.refreshControl = refreshControl;
    }
    [self.collectionView reloadData];
}

-(void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    if (!(IS_IPAD)) {
        if (self.tableView.contentOffset.y == 0) {
            [self.tableView setContentOffset:CGPointMake(0, self.tableView.tableHeaderView.bounds.size.height)];
        }
    }
}

#pragma mark - data related

-(NSArray *) sortOptions {
//    SwitchOption *option1 = [[SwitchOption alloc] initWithDisplayText:@"Rating" value:KEY_CONTENT_RATING_POINTS ascending:NO];
//    SwitchOption *option2 = [[SwitchOption alloc] initWithDisplayText:@"Views" value:KEY_EPISODE_VIEWS ascending:NO];
//    SwitchOption *option3 = [[SwitchOption alloc] initWithDisplayText:@"Alphabetical" value:KEY_EPISODE_CONTENT_NAME ascending:YES];
//    return @[option1, option2, option3];
    return nil;
}

-(void) createDataSources {
    _updates = [NSMutableArray array];
    _filteredEpisodesDictionary = [NSMutableDictionary dictionary];
    _episodesDictionary = [NSMutableDictionary dictionary];
}

-(void)refreshControlPulled {
    [self loadData];
//    [self loadCategory];
}

-(void) loadCategory {
    if (!_categoryId) {
        [self showGeneralError];
        return;
    }
    [self showSpinner];
    [self readCategoryWithId:_categoryId completion:^(PFObject *object, NSError *error) {
        if (error) {
            [self showNetworkError];
            return;
        } else if (!object) {
            [self showGeneralError];
            return;
        }
        _category = (ContentCategory *)object;
        [self loadUpdates];
    }];
}

-(void) LoadWithSkip {
//    self.skip += MAX_RESULTS_CATEGORY_UPDATES;
//    [self loadUpdates];
}

-(void) loadUpdates {
//    [self updatesWithCategoryId:_categoryId skip:self.skip completion:^(NSArray *objects, NSError *error) {
//        if (error) {
//            [self showNetworkError];
//            return;
//        } else {
//            if (objects.count > 0) {
//                if (_updates.count > 0) {
//                    [_updates addObjectsFromArray:objects];
//                    [self loadEpisodesForDateString:[self dateStringForSection:self.skip] section:self.skip];
//                } else {
//                    _updates = [objects mutableCopy];
//                    if (_updates.count > 0) {
//                        [self loadEpisodesForDateString:[self dateStringForSection:0] section:0];
//                    }
//                }
//                if (objects.count < MAX_RESULTS_CATEGORY_UPDATES) {
//                    self.hasMore = NO;
//                }
//            } else self.hasMore = NO;
////            [self hideSpinner];
//        }
//    }];
}

-(void) loadEpisodesForDateString:(NSString *)dateString section:(NSInteger)section {
    [self findByDateString:dateString categoryId:_categoryId orderBy:self.selectedSortOption.value ascending:self.selectedSortOption.ascending completion:^(NSArray *objects, NSError *error) {
        if (error) {
            [self showNetworkError];
            [self resetLoadMoreCellAtSection:section];
            return;
        } else {
            [_episodesDictionary setObject:objects forKey:dateString];
            [self createFilteredEpisodesDictionaryWithObjects:objects dateString:dateString];
            if (_episodesDictionary.allKeys.count == 1) {
                [self hideSpinner];
                [self.refreshControl endRefreshing];
                [self.collectionView reloadData];
            } else {
                if (objects.count == 0) {
                    [self resetLoadMoreCellAtSection:section];
                }
                [self.collectionView reloadData];
//                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
        }
    }];
}

-(void) updateFilteredEpisodesDictionary {
    if (!self.canShowFavs) {
        _filteredEpisodesDictionary = self.filteredEpisodesDictionary;
        return;
    }
    
    if (self.favsOnly) {
        for (NSString *dateString in self.episodesDictionary.allKeys) {
            NSArray *objects = self.episodesDictionary[dateString];
            if (objects.count > 0) {
                [self createFilteredEpisodesDictionaryWithObjects:objects dateString:dateString];
            } else [self.filteredEpisodesDictionary setObject:nil forKey:dateString];
        }
    } else _filteredEpisodesDictionary = [self.episodesDictionary mutableCopy];
}

-(void) createFilteredEpisodesDictionaryWithObjects:(NSArray *)objects dateString:(NSString *)dateString {
    
//    if (self.showingFilteredEpisodes) {
//        objects = [[DataManager sharedManager] filterArray:objects SearchString:self.searchString field:KEY_EPISODE_CONTENT_NAME];
//    }
//    
//    if (!self.canShowFavs) {
//        [self.filteredEpisodesDictionary setObject:objects forKey:dateString];
//        return;
//    }
//    
//    if (!self.favsOnly) {
//        [self.filteredEpisodesDictionary setObject:objects forKey:dateString];
//        return;
//    }
//    
//    NSArray *favIds = [[UserManager sharedManager] getFavContentIdsForCategoryId:self.categoryId];
//    
//    if (favIds.count == 0) {
//        [self.filteredEpisodesDictionary removeObjectForKey:dateString];
//        [self.collectionView reloadData];
//        return;
//    }
//    
//    NSMutableArray *filteredObjects = [NSMutableArray array];
//    for (Episode *episode in objects) {
//        if ([favIds containsObject:episode.contentId]) {
//            [filteredObjects addObject:episode];
//        }
//    }
//    
//    [self.filteredEpisodesDictionary setObject:filteredObjects forKey:dateString];
}

-(NSString *) dateStringForSection:(NSInteger)section {
    if (section == _updates.count) {
        abort();
    }
    return [self keyForSection:section];
}

// load more event
-(void) loadSection:(id)sender {
    NSInteger section = [(UIButton *)sender tag];
    NSString *dateString = [self dateStringForSection:section];
    [self loadEpisodesForDateString:dateString section:section];
}

-(NSString *)keyForSection:(NSInteger)section {
//    if([_updates count] <= section) return @"";
//    CategoryUpdate *update = _updates[section];
//    return [Utility dateStringFromDate:update.updatedOn];
    return nil;
}

-(NSInteger) numberOfEpisodesForSection:(NSInteger)section {
    NSArray *array = [self episodesForSection:section];
    if(!array) {
        return 0;
    }
    NSInteger count = [array count];
    return count;
}

-(NSArray *) episodesForSection:(NSInteger)section {
    if ([[self.filteredEpisodesDictionary allKeys] count] == 0) {
        return nil;
    }
    return self.filteredEpisodesDictionary[[self keyForSection:section]];
}

-(void) toggleFavOnly:(id)sender {
    UIButton *favButton = (UIButton *)sender;
    
    if (!favButton.selected && [[UserManager sharedManager] favContents].count == 0) {
        [self showAlertWithTitle:@"No favourites in this category" message:@"Tap ♡ to mark content as favourite. You can also choose 'Browse' from side menu to go through all content and mark favourites."];
        return;
    }
    
    favButton.selected = !favButton.selected;
    self.favsOnly = favButton.selected;
    [self updateFilteredEpisodesDictionary];
    [self.collectionView reloadData];
}

#pragma mark - collection view data source

- (NSInteger)collectionView:(PSTCollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (section == _updates.count) {
        return 1;
    }
    
    if ([self isSectionCollapsed:section]) {
        return 0;
    }
    
    NSInteger rows = [self numberOfEpisodesForSection:section];
    NSString *key = [self keyForSection:section];
    NSArray *loadedObjects = self.episodesDictionary[key];
    if (rows == 0 && loadedObjects.count == 0) {
        rows = 1;
    }
    
    return rows;
}

-(NSInteger) numberOfSectionsInCollectionView:(PSTCollectionView *)collectionView {
    if (_updates.count == 0) {
        return 0;
    }
    
    if (!self.hasMore) {
        return _updates.count;
    }
    return _updates.count + 1;
}

- (PSTCollectionViewCell *)collectionView:(PSTCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == _updates.count) {
        LoadMoreCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:LOAD_MORE_CELL_ID forIndexPath:indexPath];
        [cell.loadMoreButton removeTarget:self action:@selector(loadSection:) forControlEvents:UIControlEventTouchUpInside];
        [cell.loadMoreButton addTarget:self action:@selector(LoadWithSkip) forControlEvents:UIControlEventTouchUpInside];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.loadMoreButton.tag = indexPath.section;
        cell.loadMoreButton.hidden = NO;
        [cell.activityIndicator stopAnimating];
        [cell.loadMoreButton setTitle:@"More..." forState:UIControlStateNormal];
        return cell;
    }
    
    if ([self numberOfEpisodesForSection:indexPath.section] == 0) {
        // not loaded. show load more.
        LoadMoreCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:LOAD_MORE_CELL_ID forIndexPath:indexPath];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.loadMoreButton.tag = indexPath.section;
        cell.loadMoreButton.hidden = NO;
        [cell.activityIndicator stopAnimating];
        [cell.loadMoreButton removeTarget:self action:@selector(LoadWithSkip) forControlEvents:UIControlEventTouchUpInside];
        [cell.loadMoreButton addTarget:self action:@selector(loadSection:) forControlEvents:UIControlEventTouchUpInside];
        
        if (indexPath.section == 0) {
            cell.loadMoreButton.hidden = YES;
            [cell.activityIndicator startAnimating];
        }
        
        [cell.loadMoreButton setTitle:[NSString stringWithFormat:@"Load: %@", [[DataManager sharedManager] friendlyDateFromString:[self dateStringForSection:indexPath.section]]] forState:UIControlStateNormal];
        return cell;
    } else {
        // loaded. show episode.
        Episode *episode = [[self episodesForSection:indexPath.section] objectAtIndex:indexPath.row];
        
        VideoItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:VIDEO_ITEM_CELL_ID forIndexPath:indexPath];
        [cell populateWithEpisode:episode contentButtonEnabled:YES showContentImage:NO];
        //        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row == 0) {
        }
        return cell;
    }
}

- (CGSize)collectionView:(PSTCollectionView *)collectionView layout:(PSTCollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == _updates.count) {
        return CGSizeMake(collectionView.bounds.size.width - 2*[VideoItemCell marginForVideoItem], 44);
    }
    
    if ([self numberOfEpisodesForSection:indexPath.section] == 0) {
        CGSize size = CGSizeMake(collectionView.bounds.size.width - 2*[VideoItemCell marginForVideoItem], 44);
        return size;
    }
    
    return [VideoItemCell sizeForVideoItem];
    
//    if (!IS_IPAD) {
//        if ([self isPortrait]) {
//            return CGSizeMake(IPHONE_VIDEO_THUMBNAIL_WIDTH, IPHONE_VIDEO_THUMBNAIL_HEIGHT);
//        } else return CGSizeMake(IPHONE_VIDEO_THUMBNAIL_WIDTH_LANDSCAPE, IPHONE_VIDEO_THUMBNAIL_HEIGHT);
//    } else {
//        if ([self isPortrait]) {
//            return CGSizeMake(IPAD_VIDEO_THUMBNAIL_WIDTH, IPAD_SHOW_CELL_HEIGHT);
//        } else {
//            return CGSizeMake(IPAD_VIDEO_THUMBNAIL_WIDTH_LANDSCAPE, IPAD_VIDEO_THUMBNAIL_HEIGHT_LANDSCAPE);
//        }
//    }
}

#pragma mark - table view data source

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    if (_updates.count == 0) {
        return 0;
    }
    
    if (!self.hasMore) {
        return _updates.count;
    }
    return _updates.count + 1;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    return nil;
    
    if (section == _updates.count) {
        return nil;
    }
    
    CGFloat height = HEADER_HEIGHT;
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, height)];
    
    [headerView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    headerView.backgroundColor = [[UserManager sharedManager] colorForUser];//PLAIN_COLOR_BLACK;
//    headerView.backgroundColor = PLAIN_COLOR_BLACK;
    
    UILabel *sectionHeaderLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, height)];
    sectionHeaderLabel.backgroundColor = headerView.backgroundColor;//PLAIN_COLOR_BLACK;
    sectionHeaderLabel.textColor = PLAIN_COLOR_ALMOST_WHITE;
    sectionHeaderLabel.shadowColor = PLAIN_COLOR_DARKER_BLACK;
    sectionHeaderLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    sectionHeaderLabel.textAlignment = NSTextAlignmentCenter;
    sectionHeaderLabel.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:12.0f];
    [sectionHeaderLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [headerView addSubview:sectionHeaderLabel];
    if (IS_IPAD) {
        sectionHeaderLabel.text = [NSString stringWithFormat:@"Added On: %@", [[DataManager sharedManager] friendlyDateFromString:[self dateStringForSection:section]]];
    } else {
        sectionHeaderLabel.text = [[DataManager sharedManager] friendlyDateFromString:[self dateStringForSection:section]];
    }
    
//    if (self.canShowFavs && section == 0) {
//        UIButton *favButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        [favButton setTitleColor:GRAY_WITH_PERCENT(80) forState:UIControlStateNormal];
//        [favButton setTitleColor:GRAY_WITH_PERCENT(66) forState:UIControlStateHighlighted];
////        [favButton setBackgroundColor:sectionHeaderLabel.backgroundColor];
//        favButton.backgroundColor = headerView.backgroundColor;
//        favButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:14.0f];
//        favButton.frame = CGRectMake(0, 0, 100, height);
//        [favButton addTarget:self action:@selector(toggleFavOnly:) forControlEvents:UIControlEventTouchUpInside];
//        [favButton setTitle:@"♥ Favs Only" forState:UIControlStateSelected];
//        [favButton setTitle:@"♡ Showing All" forState:UIControlStateNormal];
//        favButton.selected = self.favsOnly;
//        [headerView addSubview:favButton];
//    }
    
    [self addCollapseButtonToHeader:headerView section:section];
    headerView.alpha = 0.9f;
    return headerView;
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.0f;
    if (section == _updates.count) {
        return 0.0f;
    }
    return HEADER_HEIGHT;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == _updates.count) {
        return 44;
    }
    
    if ([self numberOfEpisodesForSection:indexPath.section] == 0) {
        return 44;
    }
    
    return IS_IPAD? IPAD_SHOW_CELL_HEIGHT: IPHONE_ROW_CELL_HEIGHT;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == _updates.count) {
        return 1;
    }
    
    if ([self isSectionCollapsed:section]) {
        return 0;
    }
    
    NSInteger rows = [self numberOfEpisodesForSection:section];
    NSString *key = [self keyForSection:section];
    NSArray *loadedObjects = self.episodesDictionary[key];
    if (rows == 0 && loadedObjects.count == 0) {
        rows = 1;
    }
    
    return rows;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == _updates.count) {
        LoadMoreCell *cell = [tableView dequeueReusableCellWithIdentifier:LOAD_MORE_CELL_ID];
        [cell.loadMoreButton removeTarget:self action:@selector(loadSection:) forControlEvents:UIControlEventTouchUpInside];
        [cell.loadMoreButton addTarget:self action:@selector(LoadWithSkip) forControlEvents:UIControlEventTouchUpInside];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.loadMoreButton.tag = indexPath.section;
        cell.loadMoreButton.hidden = NO;
        [cell.activityIndicator stopAnimating];
        [cell.loadMoreButton setTitle:@"More..." forState:UIControlStateNormal];
        return cell;
    }
    
    if ([self numberOfEpisodesForSection:indexPath.section] == 0) {
        // not loaded. show load more.
        LoadMoreCell *cell = [tableView dequeueReusableCellWithIdentifier:LOAD_MORE_CELL_ID];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.loadMoreButton.tag = indexPath.section;
        cell.loadMoreButton.hidden = NO;
        [cell.activityIndicator stopAnimating];
        [cell.loadMoreButton removeTarget:self action:@selector(LoadWithSkip) forControlEvents:UIControlEventTouchUpInside];
        [cell.loadMoreButton addTarget:self action:@selector(loadSection:) forControlEvents:UIControlEventTouchUpInside];
        
        if (indexPath.section == 0) {
            cell.loadMoreButton.hidden = YES;
            [cell.activityIndicator startAnimating];
        }
        
        [cell.loadMoreButton setTitle:[NSString stringWithFormat:@"Load: %@", [[DataManager sharedManager] friendlyDateFromString:[self dateStringForSection:indexPath.section]]] forState:UIControlStateNormal];
        return cell;
    } else {
        // loaded. show episode.
        Episode *episode = [[self episodesForSection:indexPath.section] objectAtIndex:indexPath.row];
        
        VideoItemCell *cell = [tableView dequeueReusableCellWithIdentifier:VIDEO_ITEM_CELL_ID];
        [cell populateWithEpisode:episode contentButtonEnabled:YES showContentImage:YES];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
}

#pragma mark - sort changed

-(void) sortWithColumn:(NSString *)column ascending:(BOOL)ascending {
    if (_updates.count == 0) {
        [self loadCategory];
    } else {
        // do local sort.
        [self showSpinner];
        [[DataManager sharedManager] sortArraysInsideDictionary:self.filteredEpisodesDictionary orderBy:column ascending:ascending completion:^(NSMutableDictionary *dictionary, NSError *error) {
            if (error) {
                [self showGeneralError];
                return;
            } else {
                _filteredEpisodesDictionary = dictionary;
                [self.collectionView reloadData];
            }
            [self hideSpinner];
        }];
    }
}

#pragma mark - search related.

-(void) searchAllAction:(NSString *)searchText {
    self.searchString = nil;
    self.lastSearchLength = 0;
    BrowseViewController *browseVC = [[BrowseViewController alloc] initWithNibName:@"BrowseViewController" bundle:nil];
    [browseVC setCategory:self.category];
    [browseVC setSearchText:searchText];
    [self.navigationController pushViewController:browseVC animated:YES];
    
    [self clearSearch];
}

-(void) clearSearch {
    self.searchTextField.text = @"";
    _filteredEpisodesDictionary = [self.episodesDictionary mutableCopy];
    self.showingFilteredEpisodes = NO;
    [self.collectionView reloadData];
}

-(void) searchAction:(NSString *)searchText {
//    self.searchString = searchText;
//    if (searchText.length == 0) {
//        [self clearSearch];
//        return;
//    }
//    
//    NSMutableDictionary *dictionaryToSearchIn;
//    if (searchText.length > self.lastSearchLength) {
//        dictionaryToSearchIn = self.filteredEpisodesDictionary;
//    } else dictionaryToSearchIn = self.episodesDictionary;
//    
//    self.lastSearchLength = searchText.length;
//    
//    [[DataManager sharedManager] filterArraysInsideDictionary:dictionaryToSearchIn searchString:searchText field:KEY_EPISODE_CONTENT_NAME completion:^(NSMutableDictionary *dictionary, NSError *error) {
//        if (dictionary && !error) {
//            self.showingFilteredEpisodes = YES;
//            self.filteredEpisodesDictionary = dictionary;
//            [self.collectionView reloadData];
//        }
//    }];
}

@end
