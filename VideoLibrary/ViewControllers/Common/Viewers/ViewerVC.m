//
//  ViewerVC.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/24/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "ViewerVC.h"
#import "Utility.h"
#import "LargeShowViewCell.h"
#import "YoutubeVideoCell.h"
#import "LargeYoutubeVideoCell.h"

@interface ViewerVC ()

#define DOWN_ARROW              @"⇢"
#define UP_ARROW                @"⇣"

#define COLLAPSE_EXPAND_BUTTON_WIDTH    40

@property (unsafe_unretained, nonatomic) IBOutlet UIView *sortHeaderView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *sortContainerView;
@property (weak, nonatomic) IBOutlet UIButton *sortOptionsButton;

@property (nonatomic, assign) BOOL searchRemoved;
@property (nonatomic, strong) SwitchControl *control;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@end

@implementation ViewerVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) createDataSources {
    abort();
}

-(NSArray *) sortOptions {
    abort();
}

- (IBAction)showSortOptions:(id)sender {
    if(!self.sortOptions) {
        return;
    }
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Sort:"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                          destructiveButtonTitle:nil
                                               otherButtonTitles:[(SwitchOption *)(self.sortOptions[0]) displayText], [(SwitchOption *)(self.sortOptions[1]) displayText], [(SwitchOption *)(self.sortOptions[2]) displayText], (self.sortOptions.count==4)? [(SwitchOption *)(self.sortOptions[3]) displayText]:nil, nil];
    
    [action  showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex < self.sortOptions.count) {
        SwitchOption *option = self.sortOptions[buttonIndex];
        self.selectedSortOption = option;
        [self sortWithColumn:option.value ascending:option.ascending];
    }
}

- (IBAction)filterFavAction:(id)sender {
    [self toggleFavOnly:sender];
}

-(void) toggleFavOnly:(id)sender {
    abort();
    // have to implement this method.
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.searchTextField addTarget:self action:@selector(searchTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"VideoItemCell" bundle:nil] forCellReuseIdentifier:VIDEO_ITEM_CELL_ID];
    [self.tableView registerNib:[UINib nibWithNibName:@"YoutubeVideoCell" bundle:nil] forCellReuseIdentifier:YOUTUBE_VIDEO_CELL_ID];
    [self.tableView registerNib:[UINib nibWithNibName:@"LargeYoutubeVideoCell" bundle:nil] forCellReuseIdentifier:YOUTUBE_LARGE_VIDEO_CELL_ID];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ShowView" bundle:nil] forCellReuseIdentifier:SHOW_CELL_ID];
    [self.tableView registerNib:[UINib nibWithNibName:@"LoadMoreCell" bundle:nil] forCellReuseIdentifier:LOAD_MORE_CELL_ID];
    [self.tableView registerNib:[UINib nibWithNibName:@"LargeShowViewCell" bundle:nil] forCellReuseIdentifier:LARGE_SHOW_CELL_ID];
    
    self.collapsedIndices = [NSMutableIndexSet indexSet];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - setup

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.firstLoadDone) {
        return;
    } else self.firstLoadDone = YES;
    
    [self setupCollectionView];
    if(self.sortOptionsButton) {
        [[StyleManager sharedManager] styleButtons:@[self.sortOptionsButton]];
    }
    if (self.filterFavButton) {
        [[StyleManager sharedManager] styleButtons:@[self.filterFavButton]];
    }
}

-(void) loadData {
    self.skip = 0;
    self.hasMore = YES;
    // sort control triggers load by auto selecting first option.
    [self createDataSources];
    [self.tableView reloadData];
    [self setSortControl];
    if (!IS_IPAD) {
        [self.tableView setContentOffset:CGPointMake(0, self.tableView.tableHeaderView.bounds.size.height)];
    }
}

-(void) setSortControl {
    NSArray *sortOptions = [self sortOptions];
    [self clearSortControl];
    
    if (sortOptions.count == 0) {
        // should not need to call with 0 options.
        abort();
    }
    
    [self addSwitchControl];
}

-(void) clearSortControl {
    if (self.control) {
        [self.control removeFromSuperview];
        self.control = nil;
    }
}

-(void) addSwitchControl {
    self.control = [[NSBundle mainBundle] loadNibNamed:@"SwitchControl" owner:self options:nil][0];
    [self.control setFrame:self.sortContainerView.bounds];
    [self.sortContainerView addSubview:self.control];
    self.control.delegate = self;
    [self.control populateWithSwitchOptions:[self sortOptions] controlBackColor:GRAY_WITH_PERCENT(6) optionBackColor:GRAY_WITH_PERCENT(15) textColor:GRAY_WITH_PERCENT(66) showingSort:YES];
}

-(void) resetLoadMoreCellAtSection:(NSInteger)section {
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:section];
    UITableViewCell *cell = [self tableView:self.tableView cellForRowAtIndexPath:indexPath];
    if ([cell isKindOfClass:[LoadMoreCell class]]) {
        LoadMoreCell *loadMoreCell = (LoadMoreCell *)cell;
        [loadMoreCell.activityIndicator stopAnimating];
        loadMoreCell.loadMoreButton.hidden = NO;
    }
}

-(void) removeSearchView {
    [self.searchView removeFromSuperview];
    self.searchRemoved = YES;
    UIView *tableHeader = self.tableView.tableHeaderView;
    CGRect headerFrame = tableHeader.frame;
    headerFrame.size.height = 34;
    tableHeader.frame = headerFrame;
    
    CGRect sortContainerFrame = self.sortContainerView.frame;
    sortContainerFrame.origin.y = 0;
    self.sortContainerView.frame = sortContainerFrame;

    self.tableView.tableHeaderView = tableHeader;
    self.contentView.frame = self.view.bounds;
}

#pragma mark --
#pragma mark - PST Collection View implementation

-(PSTCollectionViewLayout *) flowLayout {
    
    if (UIDeviceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
        return [self landscapeFlowLayout];
    } else return [self portraitFlowLayout];
}

-(PSTCollectionViewLayout *) landscapeFlowLayout {
    PSTCollectionViewFlowLayout *layout =[[PSTCollectionViewFlowLayout alloc] init];
    [layout setScrollDirection:PSTCollectionViewScrollDirectionVertical];
    
    CGFloat margin = [VideoItemCell marginForVideoItem];
    [layout setMinimumLineSpacing:margin];
    [layout setMinimumInteritemSpacing:margin];
    [layout setSectionInset:UIEdgeInsetsMake(margin, margin, margin, margin)];
    return layout;
}

-(PSTCollectionViewLayout *) portraitFlowLayout {
    PSTCollectionViewFlowLayout *layout =[[PSTCollectionViewFlowLayout alloc] init];
    [layout setScrollDirection:PSTCollectionViewScrollDirectionVertical];
    
    if (IS_IPAD) {
        return [self landscapeFlowLayout];
    }
    
    [layout setMinimumInteritemSpacing:0];
    [layout setMinimumLineSpacing:0];
    [layout setSectionInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    return layout;
}

-(void) setupCollectionView {
    
    self.collectionView = [[PSTCollectionView alloc] initWithFrame:self.contentView.bounds collectionViewLayout:[self flowLayout]];
    
    [_collectionView setDelegate:self];
    [_collectionView setDataSource:self];
    [_collectionView setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    [_collectionView setBackgroundColor:GRAY_WITH_PERCENT(80)];
    
    [_collectionView registerNib:[UINib nibWithNibName:@"VideoItemCell" bundle:nil] forCellWithReuseIdentifier:VIDEO_ITEM_CELL_ID];
//    [_collectionView registerNib:[UINib nibWithNibName:@"YoutubeVideoCell" bundle:nil] forCellWithReuseIdentifier:YOUTUBE_VIDEO_CELL_ID];
//    [_collectionView registerNib:[UINib nibWithNibName:@"LargeYoutubeVideoCell" bundle:nil] forCellWithReuseIdentifier:YOUTUBE_LARGE_VIDEO_CELL_ID];
//    
//    [_collectionView registerNib:[UINib nibWithNibName:@"ShowView" bundle:nil] forCellWithReuseIdentifier:SHOW_CELL_ID];
    [_collectionView registerNib:[UINib nibWithNibName:@"LoadMoreCell" bundle:nil] forCellWithReuseIdentifier:LOAD_MORE_CELL_ID];
//    [_collectionView registerNib:[UINib nibWithNibName:@"LargeShowViewCell" bundle:nil] forCellWithReuseIdentifier:LARGE_SHOW_CELL_ID];
    
//    [_collectionView registerNib:[UINib nibWithNibName:@"ChannelView" bundle:nil] forCellWithReuseIdentifier:CHANNEL_CELL_REUSE_ID];
//    [_collectionView registerNib:[UINib nibWithNibName:@"CategoryViewCell" bundle:nil] forCellWithReuseIdentifier:CATEGORY_CELL_REUSE_ID];
//    [_collectionView registerNib:[UINib nibWithNibName:@"ContentViewCell" bundle:nil] forCellWithReuseIdentifier:CONTENT_CELL_REUSE_ID];
//    [_collectionView registerNib:[UINib nibWithNibName:@"TileShowView" bundle:nil] forCellWithReuseIdentifier:SHOW_CELL_ID];
    [_collectionView registerClass:[PSTCollectionReusableView class] forSupplementaryViewOfKind:PSTCollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    
    
    [self.contentView addSubview:self.collectionView];
    
    self.collectionView.alwaysBounceVertical = YES;
}

// all derived view controllers need to override following four methods for data source.

- (CGSize)collectionView:(PSTCollectionView *)collectionView layout:(PSTCollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    //    if (indexPath.section == 0 || indexPath.section == 1) {
    //        return CGSizeMake(90, 120);
    //    } else if (indexPath.section == 2) {
    //        return CGSizeMake(self.view.bounds.size.width - 2*TILE_MARGIN, 44);
    //    } else return CGSizeMake(IS_IPAD?220:140,IS_IPAD?140:90);
    return CGSizeMake(120, 80);
}

- (NSInteger)collectionView:(PSTCollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return 0;
}

-(NSInteger) numberOfSectionsInCollectionView:(PSTCollectionView *)collectionView {
    return 0;
}

- (PSTCollectionViewCell *)collectionView:(PSTCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

#pragma mark - sort control delegate

-(void) switchControl:(id)control selectedOption:(SwitchOption *)option {
    self.selectedSortOption = option;
    [self sortWithColumn:option.value ascending:option.ascending];
}

#pragma mark - table view data source // empty implementation. override in subclass.

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LoadMoreCell *cell = (LoadMoreCell *)[tableView dequeueReusableCellWithIdentifier:LOAD_MORE_CELL_ID];
    return (UITableViewCell *)cell;
}

#pragma mark - table sections collapse/expand related

-(void) expandCollapseSection:(id) sender {
    UIButton *button = (UIButton *)sender;
    NSInteger section = [button tag];
    if ([self isSectionCollapsed:section]) {
        [button setTitle:UP_ARROW forState:UIControlStateNormal];
        [self expandSection:section];
    } else {
        [button setTitle:DOWN_ARROW forState:UIControlStateNormal];
        [self collapseSection:section];
    }
}

-(void) collapseSection:(NSInteger)section {
    NSArray *indices = [self indicesOfRowsInSection:section];
    if (indices) {
        [self.collapsedIndices addIndex:section];
        [self.tableView deleteRowsAtIndexPaths:indices withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

-(void) expandSection:(NSInteger)section {
    [self.collapsedIndices removeIndex:section];
    NSArray *indices = [self indicesOfRowsInSection:section];
    if (indices) {
        [self.tableView insertRowsAtIndexPaths:indices withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

-(BOOL) isSectionCollapsed:(NSInteger)section {
    if ([self.collapsedIndices containsIndex:section]) {
        return YES;
    }
    return NO;
}

-(NSArray *) indicesOfRowsInSection:(NSInteger)section {
    NSInteger rows = [self tableView:self.tableView numberOfRowsInSection:section];
    if (rows == 0) {
        return nil;
    }
    
    NSMutableArray *indices = [NSMutableArray array];
    for (int i=0; i<rows; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:section];
        [indices addObject:indexPath];
    }
    return indices;
}

-(void) addCollapseButtonToHeader:(UIView *)header section:(NSInteger)section {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(self.tableView.bounds.size.width - COLLAPSE_EXPAND_BUTTON_WIDTH, 0, COLLAPSE_EXPAND_BUTTON_WIDTH, [self tableView:self.tableView heightForHeaderInSection:section]);
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [button.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:14.0]];
    if ([self isSectionCollapsed:section]) {
        [button setTitle:DOWN_ARROW forState:UIControlStateNormal];
    } else {
        [button setTitle:UP_ARROW forState:UIControlStateNormal];
    }
    [button addTarget:self action:@selector(expandCollapseSection:) forControlEvents:UIControlEventTouchUpInside];
    [button setTag:section];
    [button setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin];
    [header addSubview:button];
}

#pragma mark - scroll view delegate

-(void) scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.searchRemoved) {
        return;
    }
    if ([self.searchTextField isFirstResponder]) {
        [self.searchTextField resignFirstResponder];
    }
}

#pragma mark - search related.

-(void) searchAction:(NSString *)searchText {
    // should override.
    abort();
}

-(void) searchAllAction:(NSString *)searchText {
    // should override.
    abort();
}

#pragma mark - text field delegate

-(void) searchTextFieldDidChange:(id)sender {
    NSString *searchText = [Utility stringByRemovingWhiteSpaces:self.searchTextField.text];
    [self searchAction:searchText];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    NSString *searchText = [Utility stringByRemovingWhiteSpaces:textField.text];
    [self searchAllAction:searchText];
    [self.searchTextField resignFirstResponder];
    return NO;
}

#pragma mark - force relayout on collection view when device orientation changes

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [[self collectionView] setCollectionViewLayout:[self flowLayout]];
//    [self.collectionView performBatchUpdates:nil completion:nil];
    [self.collectionView reloadData];
    self.collectionView.contentOffset = CGPointMake(0, self.collectionView.contentOffset.y);
//    NSLog(@"CollectionView content offset x: %d", self.collectionView.contentOffset.x);
}

@end
