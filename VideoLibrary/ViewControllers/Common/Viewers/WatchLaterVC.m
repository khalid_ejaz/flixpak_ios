//
//  WatchLaterVC.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/24/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "WatchLaterVC.h"
#import "YoutubeVideo.h"

@interface WatchLaterVC ()

@property (nonatomic, strong) NSArray *videos;
@property (nonatomic, strong) NSArray *episodes;

@end

@implementation WatchLaterVC

-(void) viewDidLoad {
    [super viewDidLoad];
    [self removeSearchView];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.collectionView reloadData];
}

-(void) createDataSources {
    self.episodes = nil;
    self.videos = nil;
}

-(NSArray *) sortOptions {
//    SwitchOption *option1 = [[SwitchOption alloc] initWithDisplayText:@"Order Added" value:KEY_EPISODE_SAVED_AT ascending:YES];
//    SwitchOption *option2 = [[SwitchOption alloc] initWithDisplayText:@"Rating" value:KEY_CONTENT_RATING_POINTS ascending:NO];
//    SwitchOption *option3 = [[SwitchOption alloc] initWithDisplayText:@"Views" value:KEY_EPISODE_VIEWS ascending:NO];
//    return @[option1, option2, option3];
    return nil;
}

-(void) loadEpisodesAndVideos {
    self.episodes = [[DataManager sharedManager] getSortedArrayForLocalFavEpisodes:[[UserManager sharedManager] favEpisodes]];
    self.videos = [[DataManager sharedManager] getSortedArrayForLocalFavVideos:[[UserManager sharedManager] favVideos]];
    [self hideSpinner];
    [self.tableView reloadData];
}

#pragma mark - collection view data source

- (CGSize)collectionView:(PSTCollectionView *)collectionView layout:(PSTCollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    return [super collectionView:collectionView layout:collectionViewLayout sizeForItemAtIndexPath:indexPath];
    return [VideoItemCell sizeForVideoItem];
}

- (NSInteger)collectionView:(PSTCollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (section == 0) {
        return self.episodes.count;
    } else if (section == 1) {
        return self.videos.count;
    }
    return 0;
}

-(NSInteger) numberOfSectionsInCollectionView:(PSTCollectionView *)collectionView {
    return 2;
}

- (PSTCollectionViewCell *)collectionView:(PSTCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        Episode *episode = [self.episodes objectAtIndex:indexPath.row];
        VideoItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:VIDEO_ITEM_CELL_ID forIndexPath:indexPath];
        [cell populateWithEpisode:episode contentButtonEnabled:YES showContentImage:YES];
        return cell;
    } else {
        YoutubeVideo *video = [self.videos objectAtIndex:indexPath.row];
        VideoItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:VIDEO_ITEM_CELL_ID forIndexPath:indexPath];
        [cell populateWithVideo:video showChannelTitleButton:YES];
        return cell;
    }
    return nil;
}

#pragma mark - table view data source

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (self.episodes.count == 0 && section == 0) {
        return nil;
    }
    
    if (self.videos.count == 0 && section == 0) {
        return nil;
    }
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 30)];
    [headerView setBackgroundColor:GRAY_WITH_PERCENT(20)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:headerView.bounds];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setTextColor:GRAY_WITH_PERCENT(80)];
    [label setBackgroundColor:headerView.backgroundColor];
    [label setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    
    [headerView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [headerView addSubview:label];
    
    if (section == 0) {
        label.text = @"From video categories";
    } else if (section == 1) {
        label.text = @"From YouTube channels";
    }
    
    return label;
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.episodes.count == 0 && section == 0) {
        return 0;
    }
    
    if (self.videos.count == 0 && section == 0) {
        return 0;
    }
    return 30;
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return IS_IPAD? IPAD_SHOW_CELL_HEIGHT: IPHONE_ROW_CELL_HEIGHT;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.episodes.count;
    } else if (section == 1) {
        return self.videos.count;
    }
    return 0;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        Episode *episode = [self.episodes objectAtIndex:indexPath.row];
        VideoItemCell *cell = [tableView dequeueReusableCellWithIdentifier:VIDEO_ITEM_CELL_ID];
        [cell populateWithEpisode:episode contentButtonEnabled:YES showContentImage:YES];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else {
        YoutubeVideo *video = [self.videos objectAtIndex:indexPath.row];
        VideoItemCell *cell = [tableView dequeueReusableCellWithIdentifier:VIDEO_ITEM_CELL_ID];
        [cell populateWithVideo:video showChannelTitleButton:YES];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

#pragma mark - sort changed

-(void) sortWithColumn:(NSString *)column ascending:(BOOL)ascending {
    if (_episodes.count == 0) {
        [self loadEpisodesAndVideos];
        
    } else {
        // do local sort.
        self.episodes = [[DataManager sharedManager] sortObjects:self.episodes orderBy:self.selectedSortOption.value ascending:self.selectedSortOption.ascending];
        
        if ([column isEqualToString:KEY_VIDEO_SAVED_AT]) {
            self.videos = [[DataManager sharedManager] sortObjects:self.videos orderBy:self.selectedSortOption.value ascending:self.selectedSortOption.ascending];
        }
        
        [self.tableView reloadData];
    }
}

@end
