//
//  AppRootViewController.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-18.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "ME_ParentViewController.h"
#import "ME_AdManager.h"

@interface AppRootViewController : ME_ParentViewController

@end
