//
//  ForumWebVC.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/14/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "RootViewController.h"

@class Forum;

@interface ForumWebVC : RootViewController <UIWebViewDelegate, UITextFieldDelegate>

@property (nonatomic, strong) Forum *forum;
@property (nonatomic, strong) NSString *forumURL;

@end
