//
//  BrowseInterestVC.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-02-04.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "BrowseInterestVC.h"
#import "FlixPakClient.h"
#import "ME_Logger.h"
#import "FP_Response.h"
#import "HoriScrollView.h"
#import "ME_PrefsManager.h"
#import "Utility.h"
#import "PlayVC.h"
#import "NavigationController.h"
#import "AddUserChannelVC.h"

@interface BrowseInterestVC ()

//@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) FP_Response *mainResponse;
@property (nonatomic, strong) NSNumber *selectedCategoryId;
@property (nonatomic, strong) NSString *selectedCategoryTitle;
@property (nonatomic, strong) SectionsTableViewController *sectionsVC;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (nonatomic, strong) PlayVC *playerVC;
@property (weak, nonatomic) IBOutlet UIView *shareInviteView;

@end

@implementation BrowseInterestVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    // Do any additional setup after loading the view from its nib.
    [self resetViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.playerVC = nil;
    [self reloadData];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self layout];
}

- (void)layout {
    CGRect frame = self.contentView.frame;
    frame.size.height = self.view.frame.size.height - frame.origin.y - [[ME_AdManager sharedManager] adHeight];
    frame.size.width = self.view.frame.size.width;
    self.contentView.frame = frame;
    [self.sectionsVC.view setFrame:self.contentView.bounds];
}

- (NSString *)screenName {
    return @"browse";
}

- (void)resetViews {
    // setup tab view.
//    _selectedCategoryId = nil;
//    self.tabs = nil;
//    self.tabs = [NSMutableArray array];
//    [self.horiScrollView removeAllViews];
//    self.horiScrollView.showsVerticalScrollIndicator = NO;
//    self.horiScrollView.clipsToBounds = YES;
//    self.horiScrollView.contentOffset = CGPointMake(0, 0);
    
    // setup grid view.
//    UINib *cellNib = [UINib nibWithNibName:@"NibCell" bundle:nil];
//    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"cvCell"];
//    
//    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
//    [flowLayout setItemSize:CGSizeMake(200, 200)];
//    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
//    self.automaticallyAdjustsScrollViewInsets = NO;
//    [self.collectionView setCollectionViewLayout:flowLayout];
}

- (void)setSelectedCategory:(NSDictionary *)category {
    self.selectedCategoryId = category[@"_id"];
    self.selectedCategoryTitle = category[@"title"];
}

- (void)reloadData {
    // load selected category.
    [self loadCategory:self.selectedCategoryId];
}

- (IBAction)shareAction:(id)sender {
    AddUserChannelVC *addUserChannelVC = [[AddUserChannelVC alloc] initWithNibName:@"AddUserChannelVC" bundle:nil];
    [addUserChannelVC.navigationItem setTitle:@"Promote your channel"];
    [addUserChannelVC setShowingOnHome:NO];
    [self.navigationController pushViewController:addUserChannelVC animated:YES];
}

- (void)loadCategory:(NSNumber *)categoryId {
    self.shareInviteView.hidden = YES;
    [self showBlockingWaitIndicatorWithMessage:@"Loading"];
    [[FlixPakClient singleton] getBrowseWithQueryParams:@{@"category_id":categoryId} completion:^(NSDictionary *dictionary, NSError *error) {
        if (dictionary) {
            [[ME_Logger singleton] log:[dictionary description]];
            [self showResponse:dictionary];
        } else if (error) {
            [[ME_Logger singleton] logError:error];
        } else [[ME_Logger singleton] log:@"Empty response for GET main." level:ME_Log_Level_WARN];
        [self hideBlockingWaitIndicator];
    }];
}

- (void)showResponse:(NSDictionary *)response {
    [self removeSectionVC];
    NSDictionary *dict = response[@"data"];
    NSArray *sections = dict[@"sections"];
    if (sections && (sections.count > 0)) {
        self.sectionsVC = [[SectionsTableViewController alloc] initWithStyle:UITableViewStylePlain];
        self.sectionsVC.delegate = self;
        [self.contentView addSubview:self.sectionsVC.view];
        [self.sectionsVC.view setFrame:self.contentView.bounds];
        [self.sectionsVC setResponse:response];
    } else {
        self.shareInviteView.hidden = NO;
    }
}

- (void)removeSectionVC {
    [self.sectionsVC.view removeFromSuperview];
    [self.sectionsVC removeFromParentViewController];
    self.sectionsVC = nil;
}

#pragma mark - Section VC delegate

- (void)sectionViewSelectedVideoAtIndex:(NSInteger)videoIndex section:(NSDictionary *)section {
    self.playerVC = [[PlayVC alloc] initWithNibName:@"PlayVC" bundle:nil];
    self.playerVC.view.frame = self.navigationController.view.bounds;
    [self.navigationController pushViewController:self.playerVC animated:YES];
    [self.playerVC playVideosInSection:section startingIndex:videoIndex];
}

@end
