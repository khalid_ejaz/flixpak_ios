//
//  iPhoneSideMenuViewController.h
//  PakTV
//
//  Created by Malik Khalid Ejaz on 2013-09-24.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import "RootViewController.h"
#import <MessageUI/MessageUI.h>

@class ContentCategory, SwitchOption;

@protocol SideMenuOpenCloseDelegate <NSObject>

-(void) closeMenu;
-(void) sideMenuHomeSelected;
-(void) sideMenuSwitchUserSelected;
-(void) sideMenuBrowseSelected;
-(void) sideMenuListFavouritesSelected;
-(void) sideMenuWatchLaterSelected;
-(void) sideMenuSearchYoutubeSelected;
-(void) sideMenuShareSelected:(CGRect)rect;
-(void) sideMenuMoreAppsSelected;
-(void) sideMenuForumsSelected;

@end

@interface SideMenuViewController : RootViewController <UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate>

@property (nonatomic, unsafe_unretained) id<SideMenuOpenCloseDelegate> openCloseDelegate;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *userNameLabel;

-(void) reloadTable;

@end
