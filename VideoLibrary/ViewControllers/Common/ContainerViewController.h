//
//  ContainerViewController.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2013-04-20.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TVShow;

@interface ContainerViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>
{
    NSMutableArray *_showViews;
    CGSize _showViewSize;
}
@property (assign) id delegate;
@property (nonatomic, strong) UIView *headerView;

@property (nonatomic, strong) UITableView *contentTable;

-(void) setupShowViewsWithArray:(NSArray *) showsArray;
-(void) setupShowViewsWithNameWise:(BOOL) nameWise FavOnly:(BOOL) favOnly;
-(void) setupShowViewsForAllEpisodes:(NSArray *)episodeArray inShow:(NSString *) showName;
-(void) reloadData;

@end
