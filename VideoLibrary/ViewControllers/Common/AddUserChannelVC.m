//
//  AddUserChannelVC.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-02-03.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "AddUserChannelVC.h"
#import "ChannelAgreementVC.h"
#import "DataManager.h"
#import "ME_PrefsManager.h"
#import "ME_Logger.h"
#import "HowSharingWorksVC.h"

@interface AddUserChannelVC ()

@property (weak, nonatomic) IBOutlet UILabel *codeLabel;
@property (weak, nonatomic) IBOutlet UITextField *videoUrlField;
@property (weak, nonatomic) IBOutlet UILabel *stepOneInstructionLabel;
@property (weak, nonatomic) IBOutlet UILabel *footerLabel;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *videoUrl;
@property (nonatomic, strong) NSString *channelId;
@property (nonatomic, strong) NSString *channelTitle;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, assign) int addChannel;
@property (nonatomic, assign) BOOL onHome;
@property (nonatomic, assign) BOOL singleVideo;


@end

@implementation AddUserChannelVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    // Do any additional setup after loading the view from its nib.
    
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, 600)];
    
    NSString *code = [[ME_PrefsManager singleton] stringPrefForKey:@"Add-Channel-Code"];
    if (!code || [code isEqualToString:@""]) {
        u_int32_t num1 = arc4random_uniform(9000) + 1000;
        u_int32_t num2 = arc4random_uniform(9000) + 1000;
        code = [NSString stringWithFormat:@"FlixPak-%d-%d", num1, num2];
        [[ME_PrefsManager singleton] saveStringPrefs:code forKey:@"Add-Channel-Code"];
    }
    self.code = code;
    self.codeLabel.text = self.code;
    
    self.addChannel = 0;
    // TODO: remove the following line to check for code in video description.
    self.addChannel = 1;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    self.stepOneInstructionLabel.text = @"Write the following code in the video description of the video to share. Code:";
    self.footerLabel.text = @"Once we verify that you own this video and it matches interests of this app, your video will be shared in the appropriate interest category.";
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.onHome) {
        self.navigationController.navigationBarHidden = YES;
    }
}

- (IBAction)howAction:(id)sender {
    HowSharingWorksVC *howSharingWorksVC = [[HowSharingWorksVC alloc] initWithNibName:@"HowSharingWorksVC" bundle:nil];
    [howSharingWorksVC.navigationItem setTitle:@"Your videos in this app"];
    [self.navigationController pushViewController:howSharingWorksVC animated:YES];
}

- (void)setupForASingleVideo {
    _singleVideo = YES;
}

- (IBAction)verifyOwnership:(id)sender {
    //verify that the user owns the said video channel.
    [self.videoUrlField resignFirstResponder];
    NSString *code = self.codeLabel.text;
    
//    https://www.youtube.com/watch?v=dY8v0j6uEJc
//    https://youtu.be/RrS-zd8VhlU
    
    NSString *url = self.videoUrlField.text;
    url = [url stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([url isEqualToString:@""]) {
        [self.videoUrlField becomeFirstResponder];
        return;
    }
    
    if ([url rangeOfString:@"youtube"].location == NSNotFound && [url rangeOfString:@"youtu.be"].location == NSNotFound) {
        [self showMessage:@"failed to detect a valid video url" title:@"error"];
        [self.videoUrlField becomeFirstResponder];
        return;
    }
    
    url = [url stringByReplacingOccurrencesOfString:@"https://m.youtube.com/watch?v=" withString:@""];
    url = [url stringByReplacingOccurrencesOfString:@"http://m.youtube.com/watch?v=" withString:@""];
    
    url = [url stringByReplacingOccurrencesOfString:@"https://www.youtube.com/watch?v=" withString:@""];
    url = [url stringByReplacingOccurrencesOfString:@"https://youtu.be/" withString:@""];
    
    url = [url stringByReplacingOccurrencesOfString:@"http://www.youtube.com/watch?v=" withString:@""];
    url = [url stringByReplacingOccurrencesOfString:@"http://youtu.be/" withString:@""];
    
    if ([url rangeOfString:@"?"].location != NSNotFound) {
        url = [url substringToIndex:[url rangeOfString:@"?"].location];
    }
    
    if ([url length] != 11) {
        [[ME_Logger singleton] log:[NSString stringWithFormat:@"Add user channel - could not parse video url: %@", url] level:ME_Log_Level_ERROR];
        [self showMessage:@"failed to detect a valid video url" title:@"error"];
        return;
    }
    
    self.channelId = nil;
    self.channelTitle = nil;
    self.videoUrl = url;
    [self showBlockingWaitIndicatorWithMessage:@"Please wait..."];
    [[DataManager sharedManager] findVideoDetailByVideoIds:@[url] completion:^(NSArray *objects, NSError *error) {
        BOOL checked = NO;
        
        if (objects) {
            if (objects.count > 0) {
                NSDictionary *detail = objects[0];
                if ([detail.allKeys containsObject:@"snippet"]) {
                    NSDictionary *snippet = detail[@"snippet"];
                    if ([snippet.allKeys containsObject:@"description"]) {
                        NSString *description = snippet[@"description"];
                        
                        if ([snippet.allKeys containsObject:@"channelId"]) {
                            self.channelId = snippet[@"channelId"];
                        }
                        
                        if ([snippet.allKeys containsObject:@"channelTitle"]) {
                            self.channelTitle = snippet[@"channelTitle"];
                        }
                        
                        if ([[description lowercaseString] containsString:[code lowercaseString]]) {
                            // verified ownership. code confirmed.
                            
                            if (self.channelId && self.channelTitle) {
                                [self hideBlockingWaitIndicator];
//                                NSString *message = [NSString stringWithFormat:@"Channel '%@' ownership verified.", self.channelTitle];
//                                [self showMessage:message title:@"Success"];
                                [self ownershipVerified];
                                checked = YES;
                            }
                        }
//                        else if (self.addChannel == 1) {
//                            [self hideBlockingWaitIndicator];
//                            [self ownershipVerified];
//                            checked = YES;
//                        }
                        else {
                            checked = YES;
                            [self hideBlockingWaitIndicator];
                            [self showMessage:@"We could not find the code in this video's description" title:@"Not found"];
                            return;
                        }
                    }
                }
            }
        }
        if (!checked) {
            [self hideBlockingWaitIndicator];
            [self showMessage:@"failed to detect a valid video url" title:@"error"];
        }
    }];
}

- (void)setShowingOnHome:(BOOL)onHome {
    _onHome = onHome;
}

- (void)ownershipVerified {
    ChannelAgreementVC *agreementVC = [[ChannelAgreementVC alloc] initWithNibName:@"ChannelAgreementVC" bundle:nil];
    [agreementVC.navigationItem setTitle:@"Terms of use"];
    [agreementVC setCode:self.code videoUrl:self.videoUrl channelId:self.channelId channelTitle:self.channelTitle];
    NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:[[self navigationController] viewControllers]];
    [viewControllers removeLastObject];
    [viewControllers addObject:agreementVC];
    [[self navigationController] setViewControllers:viewControllers animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self verifyOwnership:nil];
    return NO;
}

//- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    [self.view endEditing:YES];
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
