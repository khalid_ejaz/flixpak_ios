//
//  SectionsTableViewController.m
//  PakVideos
//
//  Created by Malik Ejaz on 2016-09-15.
//  Copyright © 2016 Malik Ejaz. All rights reserved.
//

#import "SectionsTableViewController.h"
//#import "PlayerViewController.h"
#import "constants.h"
//#import "Client.h"
#import "FlixPakClient.h"

@interface SectionsTableViewController ()

@property (nonatomic, strong) NSDictionary *response;
@property (nonatomic, strong) NSArray *sections;

@end

@implementation SectionsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
//    self.view.backgroundColor = [UIColor colorWithWhite:0.57f alpha:1.0f];
    self.view.backgroundColor = [UIColor blackColor];
    self.tableView.backgroundColor = self.view.backgroundColor;
    [self.tableView registerNib:[UINib nibWithNibName:@"SectionRow" bundle:nil] forCellReuseIdentifier:TABLE_CELL_REUSE_ID];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.tableView setBounces:NO];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
}

#pragma mark - data load

//- (void)setRoute:(NSString *)route {
//    _route = route;
//    if (!route) {
//        _response = nil;
//        _sections = nil;
//        [self.tableView reloadData];
//    } else {
//        [self loadData];
//    }
//}

//- (void)loadData {
//    [[FlixPakClient singleton] getRoute:self.route withCompletion:^(NSDictionary *dictionary, NSError *error) {
//        if (dictionary) {
//            //            NSLog(@"Dictionary in response: %@", dictionary);
//            [self getDataFromResponseDictionary:dictionary];
//        }
//        if (error) {
//            // TODO: show error.
//            NSLog(@"Error: %@", error);
//        }
//        [self.tableView reloadData];
//        if (self.sections.count == 0) {
//            NSNotification *notif = [NSNotification notificationWithName:@"remove_sections" object:nil userInfo:nil];
//            [[NSNotificationCenter defaultCenter] postNotification:notif];
//        }
//    }];
//}

- (void)setResponse:(NSDictionary *)response {
    _response = response;
    [self getDataFromResponseDictionary:response];
}

- (void)getDataFromResponseDictionary:(NSDictionary *)dictionary {
    NSDictionary *dict = dictionary[@"data"];
    [self setSections:dict[@"sections"]];
    [self.tableView reloadData];
}

// TODO: Add ability to section rows to reload selective data (only for the row itself.) The api route will be provided in section dictionary by the server.

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return ROW_HEIGHT + 10;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SectionRow *cell = (SectionRow *)[tableView dequeueReusableCellWithIdentifier:TABLE_CELL_REUSE_ID];
    NSDictionary *section = self.sections[indexPath.section];
    [cell setSection:section];
    cell.delegate = self;
    cell.backgroundColor = self.view.backgroundColor;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 25;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSDictionary *sec = self.sections[section];
    SectionHeaderView *header = [[NSBundle mainBundle] loadNibNamed:@"SectionHeaderView" owner:self options:nil][0];
    [header setHeading:sec[@"heading"]];
//    [header setBackgroundColor:[UIColor colorWithWhite:0.5f alpha:1.0f]];
//    CGRect frame = CGRectMake(0, 0, tableView.frame.size.width, 30);
//    header.frame = frame;
//    [header setBackgroundColor:[UIColor lightGrayColor]];
    return header;
}

#pragma mark - Section Row delegate

- (void)sectionRowSelectedVideoAtIndex:(NSInteger)videoIndex inSection:(NSDictionary *)section {
    [self.delegate sectionViewSelectedVideoAtIndex:videoIndex section:section];
    if ([section.allKeys containsObject:@"channel_id"]) {
        NSString *channelId = section[@"channel_id"];
        NSString *form = [NSString stringWithFormat:@"channel_id=%@", channelId];
        [[FlixPakClient singleton] POSTRoute:@"channel-view" form:form withCompletion:nil];
    }
    // if youtube channel, save viewed.
    
    //    PlayerViewController *playerVC = [[PlayerViewController alloc] initWithNibName:@"PlayerViewController" bundle:nil];
//    [playerVC setSection:section];
//    [playerVC setVideoIndex:videoIndex];
//    [self.navigationController pushViewController:playerVC animated:YES];
}

@end
