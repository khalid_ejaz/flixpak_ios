//
//  SectionsTableViewController.h
//  PakVideos
//
//  Created by Malik Ejaz on 2016-09-15.
//  Copyright © 2016 Malik Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SectionRow.h"
#import "SectionHeaderView.h"

@protocol SectionVCDelegate <NSObject>

// when user taps on a video thumbnail to start playing a video.
- (void)sectionViewSelectedVideoAtIndex:(NSInteger)videoIndex section:(NSDictionary *)section;

@end

@interface SectionsTableViewController : UITableViewController <SectionRowDelegate>
@property (nonatomic, unsafe_unretained) id<SectionVCDelegate> delegate;

- (void)setResponse:(NSDictionary *)response;

@end
