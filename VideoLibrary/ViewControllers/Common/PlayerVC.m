//
//  PlayerVC.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/29/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

@import AVFoundation;

#import "PlayerVC.h"
#import "Utility.h"
#import "YoutubeVideo.h"
#import "DataManager.h"
#import "UserManager.h"
#import "CustomSearchResultViewController.h"
#import "StyleManager.h"
#import "ME_AdManager.h"

#define IPAD_VIDEO_WIDTH        768
#define IPAD_VIDEO_HEIGHT       576

#define IPHONE_VIDEO_WIDTH      320
#define IPHONE_VIDEO_HEIGHT     240

@interface PlayerVC ()

@property (weak, nonatomic) IBOutlet YTPlayerView *ytPlayerView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *playerView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *favButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *backButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *reportButton;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *contentNameLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *watchLaterButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *headerView;
@property (unsafe_unretained, nonatomic) IBOutlet UIWebView *webView;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *episodeNameLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *ratingView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *nextButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *prevButton;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *titleLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *searchButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *searchView;

@property (unsafe_unretained, nonatomic) IBOutlet UIButton *buttonStar1;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *buttonStar2;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *buttonStar3;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *buttonStar4;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *buttonStar5;
@property (nonatomic, strong) NSArray *videos;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *partLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *wheel;

@property (nonatomic, assign) BOOL firstLoad;
@property (nonatomic, assign) int part;

@property (nonatomic, assign) int ratingPointsToAdd;
@property (nonatomic, strong) NSDate *viewOpenDate;

@property (nonatomic, strong) Content *content;

@property (nonatomic, assign) BOOL JustStartedPlaying;

@end

@implementation PlayerVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&sessionError];
    
    self.JustStartedPlaying = NO;
//    [[StyleManager sharedManager] styleHeaderButtons:@[self.backButton, self.watchLaterButton]];
    self.ratingPointsToAdd = 0;
    self.viewOpenDate = [NSDate date];
//    [self.webView setBackgroundColor:[UIColor clearColor]];
    self.webView.opaque = NO;
    self.webView.allowsInlineMediaPlayback = YES;
    self.webView.mediaPlaybackRequiresUserAction = FALSE;
    self.webView.mediaPlaybackAllowsAirPlay = YES;
    self.part = 1;
    
    // disable scrolling on webview.
    for (UIView *view in self.webView.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            UIScrollView *scrollView = (UIScrollView *)view;
            scrollView.scrollEnabled = NO;
        }
    }
    
    [self setupStarButtonActions];
    
    self.headerView.backgroundColor = [[UserManager sharedManager] colorForUser];
//    if (IS_IPAD) {
//        [self.webView setScalesPageToFit:YES];
//    }
    self.webView.contentMode = UIViewContentModeTop;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // partial implementation to find youtube video url from video id by parsing xml response.
//    [[DataManager sharedManager] findURLForVideoID:self.video.videoId completion:^(NSString *string, NSError *error) {
//        if (error) {
//            NSLog(@"Error: %@", [error description]);
//        } else {
//            NSLog(@"Found URL: %@", string);
//        }
//    }];
    
//    [self showTipForAction:TIP_ACTION_PLAY_VIDEO];
//    [[ME_AdManager sharedManager] showAdOnViewController:self atPosition:AD_POSITION_BOTTOM];
//    if (self.firstLoadDone) {
//        return;
//    }
//    self.firstLoadDone = YES;
//    
//    [self showInternetNeededIfNotAvailable];
//    
//    if (self.episode) {
//        self.reportButton.hidden = NO;
//        self.episodeNameLabel.text = self.episode.contentName;
//        
//        if ([self.episode.title rangeOfString:@"Episode "].location == NSNotFound && [self.episode.title rangeOfString:@"_"].location == NSNotFound) {
//            self.contentNameLabel.text = self.episode.title;
//        } else self.contentNameLabel.text = self.episode.contentName;
//        
//        if ([self.episode.title rangeOfString:@"_"].location == NSNotFound) {
//            self.titleLabel.text = self.episode.title;
//        } else {
//            self.titleLabel.text = [Utility friendlyDateStringFromDate:self.episode.addedOn];
//        }
//        self.videos =  [[DataManager sharedManager] longLinks:self.episode.ytlinks];
//            self.webView.hidden = NO;
//            [self showVideos:[self htmlStringForVideos]];
//        [self showUserRatings];
//        [self setupHeaderView];
//        self.favButton.selected = [Content isFavouriteByContentId:self.episode.contentId];
//        self.watchLaterButton.selected = [self.episode isFavourite];
//    } else if (self.video) {
//        self.favButton.hidden = YES;
//        self.contentNameLabel.hidden = YES;
//        self.episodeNameLabel.text = self.video.title;
//        self.videos = @[self.video.embedURL];
//        self.reportButton.hidden = YES;
//        
//            self.webView.hidden = NO;
//            [self showVideos:[self htmlStringForVideos]];
//        
//        self.nextButton.hidden = YES;
//        self.prevButton.hidden = YES;
//        self.partLabel.hidden = YES;
//        for (UIButton *button in [self starButtons]) {
//            button.hidden = YES;
//        }
//        self.favButton.hidden = YES;
//        self.watchLaterButton.selected = [self.video isFavourite];
//    }
}

- (void)playWithYouTubePlayer:(NSString *)vid {
    self.webView.hidden = YES;
    NSDictionary *playerVars = @{
                                 @"playsinline" : @1,
                                 @"rel":[NSNumber numberWithInt:0],
                                 };
    
    [self.ytPlayerView loadWithVideoId:vid playerVars:playerVars];
    
//    [self.ytPlayerView loadVideoById:vid startSeconds:seekTime suggestedQuality:kYTPlaybackQualityHighRes];
    self.ytPlayerView.delegate = self;
}

-(void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    [[ME_AdManager sharedManager] layoutAdView];
    
    CGRect playerRect = self.playerView.frame;
    playerRect.size.height = self.view.bounds.size.height - self.headerView.bounds.size.height - 1 - [[ME_AdManager sharedManager] adHeight];
    self.playerView.frame = playerRect;
    
    CGRect webRect = self.webView.frame;
    webRect.size.height = self.playerView.bounds.size.height - self.searchView.bounds.size.height;
    webRect.size.width = self.view.bounds.size.width;

//    CGSize size;
//    if(IS_PORTRAIT) {
//        size = CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.width*0.5625);
//    } else size = CGSizeMake(self.view.bounds.size.height*1.777, self.view.bounds.size.height);
//    webRect.size = size;
    self.webView.frame = webRect;
    
//    NSString *js = [NSString stringWithFormat:@"document.getElementByClass('youtube-player').height=%f;document.getElementByClass('youtube-player').height=%f;",webRect.size.height,webRect.size.width];
//    [self.webView stringByEvaluatingJavaScriptFromString:js];
    UIView *browserView;
    for(UIView *subview in self.webView.scrollView.subviews)
    {
        NSString * className = NSStringFromClass([subview class]);
        if([className isEqualToString:@"UIWebBrowserView"])
        {
            //YES ! Just grab a reference to it :)
            browserView  = subview;
            
            //Set the frame of the view to fit your needs
            break;
        }
    }
    if(browserView) {
        [browserView setFrame:self.webView.bounds];
//        [self.webView.scrollView scrollRectToVisible:CGRectMake(0, 1, 1, 1) animated:NO];
    }
    
    self.ytPlayerView.frame = webRect;
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[ME_AdManager sharedManager] removeAds];
}

- (IBAction)backAction:(id)sender {
    
//    [self.ytPlayerView pauseVideo];
//    
//    if (self.video) {
//        
////        [self.navigationController dismissModalViewControllerAnimated:YES];
//        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
////        [self.navigationController popViewControllerAnimated:YES];
//        return;
//    }
//    
//    BOOL needToSave = FALSE;
//    if (![self.episode isDirty]) {
//        if (self.ratingPointsToAdd > 0) {
//            [self.episode incrementKey:KEY_EPISODE_TOTAL_RATINGS byAmount:[NSNumber numberWithInteger:1]];
//            [self.episode incrementKey:KEY_EPISODE_RATING_POINTS byAmount:[NSNumber numberWithInteger:self.ratingPointsToAdd]];
//            needToSave = YES;
//            [self disableStarButtons];
//        }
//        
//        if (![[UserManager sharedManager] alreadyMakedEpisodeAsViewed:self.episode]) {
//            NSDate *dateNow = [NSDate date];
//            double openTime = [self.viewOpenDate timeIntervalSince1970];
//            double backTime = [dateNow timeIntervalSince1970];
//            if (backTime - openTime > 60) {
//                [self.episode incrementKey:KEY_EPISODE_VIEWS byAmount:[NSNumber numberWithInteger:1]];
//                [[UserManager sharedManager] saveViewedEpisode:self.episode];
//                needToSave = YES;
//            }
//        }
//        if (needToSave) {
//            [self.episode saveEventually];
//        }
//    }
//
////    [self.navigationController dismissModalViewControllerAnimated:YES];
//    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
////    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - video load related

-(CGSize) videoSize {
    
    CGRect webRect = self.webView.frame;
//    webRect.size.height = self.view.bounds.size.height -self.headerView.bounds.size.height - self.searchView.bounds.size.height - 90;//[[ANAdManager sharedManager] adHeight];
//    webRect.size.width = self.view.bounds.size.width;
//    CGSize size = CGSizeMake(IS_IPAD?self.webView.bounds.size.width:IPHONE_VIDEO_WIDTH, IS_IPAD?webRect.size.height:IPHONE_VIDEO_HEIGHT);
    CGSize size;
    CGFloat width;
    CGFloat height;
    if(IS_PORTRAIT) {
        width = webRect.size.width;
        height = webRect.size.width*0.5625;
        if(height > webRect.size.height)height = webRect.size.height;
        
        size = CGSizeMake(width, height);
    } else {
        width = webRect.size.height*1.777;
        height = webRect.size.height;
        if(width > webRect.size.width)width = webRect.size.width;
        size = CGSizeMake(width, height);
    }
    return size;
}

-(void) showVideos:(NSString *)url
{
    self.firstLoad = TRUE;
    [self.webView loadHTMLString:url baseURL:nil];
}

- (BOOL)isYouTubeVideo {
    BOOL dailyMotion = FALSE;
    BOOL youtubeVideo = TRUE;
    int i = self.part-1;
    
    NSString *link = [self.videos objectAtIndex:i];
    link = [link stringByReplacingOccurrencesOfString:@"http://www.youtube.com/embed/" withString:@""];
    NSRange aRange = [link rangeOfString:@"dailymotion"];
    if(aRange.location != NSNotFound)
    {
        dailyMotion = TRUE;
        youtubeVideo = false;
    }
    return youtubeVideo;
}

-(NSString *) htmlStringForVideos {
    
    NSString *header = [NSString stringWithFormat: @"<html><body marginheight='0' marginwidth='0' >"];
    NSString *body = @"";
    int i = self.part-1;
    NSString *p;
    BOOL dailyMotion = FALSE;
    BOOL youtubeVideo = TRUE;
    
    NSString *link = [self.videos objectAtIndex:i];
    link = [link stringByReplacingOccurrencesOfString:@"https://www.youtube.com/embed/" withString:@""];
    NSRange aRange = [link rangeOfString:@"dailymotion"];
    if(aRange.location != NSNotFound)
    {
        dailyMotion = TRUE;
        youtubeVideo = false;
    }
    
    CGSize videoSize = [self videoSize];
    if(!dailyMotion) {
        // youtube video.
        
        // older implementation where video is embedded in the web view.
        p = [NSString stringWithFormat:@"<iframe webkit-playsinline title='YouTube video player' class='youtube-player' type='text/html' width='100%%' height='98%%' align='top' allowfullscreen='false' src='%@?rel=0&feature=player_detailpage&playsinline=1&autoplay=1' frameborder='0'></iframe>",[self.videos objectAtIndex:i]];
    } else {
        // dailymotion video.
        p = [NSString stringWithFormat:@"<iframe webkit-playsinline name='player' type='text/html' width='%f' height='%f' align='top' src='%@?autoplay=1;' frameborder='0' allowfullscreen='false'></iframe>",videoSize.width,videoSize.height,[self.videos objectAtIndex:i]];
    }
    if (self.videos.count == 1) {
//        self.nextButton.hidden = YES;
//        self.prevButton.hidden = YES;
    } else {
//        self.nextButton.hidden = NO;
//        [self.label setText:[NSString stringWithFormat:@"%@ (%@) - Part %d/%d",self.showName,self.showDate,(i+1),totalparts]];
    }
    body = [body stringByAppendingString:p];
    //}
    NSString *footer = [NSString stringWithFormat: @"<br></body></html>"];
    NSString *st = [NSString stringWithFormat:@"%@%@%@",header,body,footer];
    return st;
}

#pragma mark - ratings related

-(void) showUserRatings {
//    // if not rated yet. show avg ratings.
//    int userRating = [[UserManager sharedManager] userRatingsForEpisode:self.episode];
//    if (userRating > 0) {
//        [self showStarsWithRatings:userRating];
//        [self disableStarButtons];
//    } else {
//        [self showStarsWithRatings:[self.episode getAverageRating]];
//        [self enableStarButtons];
//    }
}

-(NSArray *)starButtons {
    return @[self.buttonStar1, self.buttonStar2, self.buttonStar3, self.buttonStar4, self.buttonStar5];
}

-(void) disableStarButtons {
    NSArray *starButtons = [self starButtons];
    for (UIButton *button in starButtons) {
        button.enabled = NO;
    }
}

-(void) enableStarButtons {
    NSArray *starButtons = [self starButtons];
    for (UIButton *button in starButtons) {
        button.enabled = YES;
    }
}

-(void) showStarsWithRatings:(int)rating {
    switch (rating) {
        case 0:
            [self setEmptyStarOnButtons:@[self.buttonStar1, self.buttonStar2, self.buttonStar3, self.buttonStar4, self.buttonStar5]];
            break;
        case 1:
            [self setEmptyStarOnButtons:@[self.buttonStar2, self.buttonStar3, self.buttonStar4, self.buttonStar5]];
            [self setFullStarOnButtons:@[self.buttonStar1]];
            break;
        case 2:
            [self setEmptyStarOnButtons:@[self.buttonStar3, self.buttonStar4, self.buttonStar5]];
            [self setFullStarOnButtons:@[self.buttonStar1, self.buttonStar2]];
            break;
        case 3:
            [self setEmptyStarOnButtons:@[self.buttonStar4, self.buttonStar5]];
            [self setFullStarOnButtons:@[self.buttonStar1, self.buttonStar2, self.buttonStar3]];
            break;
        case 4:
            [self setEmptyStarOnButtons:@[self.buttonStar5]];
            [self setFullStarOnButtons:@[self.buttonStar1, self.buttonStar2, self.buttonStar3, self.buttonStar4]];
            break;
        case 5:
            [self setFullStarOnButtons:@[self.buttonStar1, self.buttonStar2, self.buttonStar3, self.buttonStar4, self.buttonStar5]];
            break;
        default:
            break;
    }
}

-(void) rateAction:(id)sender {
    int tag = [(UIButton *)sender tag];
    [[UserManager sharedManager] saveRatings:tag forEpisode:self.episode];
    [self showStarsWithRatings:tag];
    self.ratingPointsToAdd = tag;

    UIColor *goldColor = [UIColor colorWithRed:252.0/255.0 green:183.0/255.0 blue:28.0/255.0 alpha:1.0f];
    
    NSArray *buttons = [self starButtons];
    for (int i=0; i<tag; i++) {
        UIButton *button = buttons[i];
        [button setTitleColor:goldColor forState:UIControlStateNormal];
    }
}

-(void) setupStarButtonActions {
    NSArray *starButtons = @[self.buttonStar1,self.buttonStar2,self.buttonStar3,self.buttonStar4,self.buttonStar5];
    for (UIButton *button in starButtons) {
        [button addTarget:self action:@selector(rateAction:) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void) setEmptyStarOnButtons:(NSArray *)buttons {
    for (UIButton *button in buttons) {
        [button setTitle:@"☆" forState:UIControlStateNormal];
    }
}

-(void) setFullStarOnButtons:(NSArray *)buttons {
    for (UIButton *button in buttons) {
        [button setTitle:@"★" forState:UIControlStateNormal];
    }
}

#pragma mark - parts related
- (IBAction)prevAction:(id)sender {
    self.part --;
    if (self.part == 1) {
        self.prevButton.hidden = YES;
    }
    self.nextButton.hidden = NO;
    [self showVideos:[self htmlStringForVideos]];
    [self setPartLabelText];
}
- (IBAction)nextAction:(id)sender {
    self.part++;
    if (self.part == self.videos.count) {
        self.nextButton.hidden = YES;
    }
    self.prevButton.hidden = NO;
    [self showVideos:[self htmlStringForVideos]];
    [self setPartLabelText];
}

-(void) setupHeaderView {
    if (self.videos.count == 1) {
        self.partLabel.hidden = YES;
        self.prevButton.hidden = YES;
        self.nextButton.hidden = YES;
        return;
    } else {
        [self setPartLabelText];
        self.favButton.hidden = YES;
        self.nextButton.hidden = NO;
        self.prevButton.hidden = YES;
    }
}

-(void) setPartLabelText {
    self.partLabel.text = [NSString stringWithFormat:@"%d of %d", self.part, self.videos.count];
}

# pragma mark - WebView Delegate methods.

-(BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
//    if ([[UserManager sharedManager] currentUser].childUser) {
        return self.firstLoad;
//    }
//    return YES;
}

-(void) webViewDidFinishLoad:(UIWebView *)webView
{
    self.firstLoad = FALSE;
    [self.wheel stopAnimating];
}

#pragma mark - IBActions Fav/watch later related

- (IBAction)favAction:(id)sender {
//    self.favButton.selected = !self.favButton.selected;
//    if (self.favButton.selected) {
//        if (self.content) {
//            [self.content saveAsFavourite];
//            [[UserManager sharedManager] addCategoryAsFavouriteById:self.content.categoryId];
//        } else {
//            [self readContentWithId:self.episode.contentId completion:^(PFObject *object, NSError *error) {
//                if (error) {
//                    [self showAlertWithTitle:@"Network Error" message:@"Could not mark as favourite."];
//                } else {
//                    if (!object) {
//                        [self showGeneralError];
//                    } else {
//                        Content *content = (Content *)object;
//                        [content saveAsFavourite];
//                        self.content = content;
//                        
//                        [[UserManager sharedManager] addCategoryAsFavouriteById:self.content.categoryId];
////                        NSNotification *notification = [NSNotification notificationWithName:NOTIFICATION_CHANGED_PERSONALISED_CONTENT object:nil];
////                        [[NSNotificationCenter defaultCenter] postNotification:notification];
//                    }
//                }
//            }];
//        }
//    } else {
//        [Content saveAsNotFavouriteByContentId:self.episode.contentId];
////        NSNotification *notification = [NSNotification notificationWithName:NOTIFICATION_CHANGED_PERSONALISED_CONTENT object:nil];
////        [[NSNotificationCenter defaultCenter] postNotification:notification];
//    }
}

#pragma mark - 
#pragma mark - Report related.

- (IBAction)reportAction:(id)sender {
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Report:"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                          destructiveButtonTitle:nil
                                               otherButtonTitles:@"Report broken link", @"Report Copyright violation", @"Report inapproprate content", nil];
    
    [action  showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            // report broken link.
            [self reportLinkBroken:YES copyright:NO inappropriate:NO];
            break;
        case 1:
            // report copyright violation.
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.youtube.com/copyright_complaint_form"]];
            [self reportLinkBroken:NO copyright:YES inappropriate:NO];
            break;
        case 2:
            // report inapproprate video.
            [self reportLinkBroken:NO copyright:NO inappropriate:YES];
            break;
        default:
            break;
    }
}

-(void) reportLinkBroken:(BOOL)broken copyright:(BOOL)copyright inappropriate:(BOOL)inappropriate {
//    PFObject *report = [PFObject objectWithClassName:@"Report"];
//    report[@"ytLinks"] = self.episode.ytlinks?self.episode.ytlinks:@"";
//    report[@"dmLinks"] = self.episode.dmlinks?self.episode.dmlinks:@"";
//    
//    report[@"categoryId"] = self.episode.categoryId?self.episode.categoryId:@"";
//    report[@"contentId"] = self.episode.contentId?self.episode.contentId:@"";
//    report[@"contentName"] = self.episode.contentName?self.episode.contentName:@"";
//    report[@"dealthWith"] = @NO;
//    report[@"episodeNumber"] = self.episode.episodeNumber?self.episode.episodeNumber:@"";
//    report[@"date"] = self.episode.dateString?self.episode.dateString:@"";
//    
//    if(broken) report[@"reason"] = @"broken";
//    else if(copyright) report[@"reason"] = @"copyright";
//    else if(inappropriate) report[@"reason"] = @"inappropriate";
//    
//    [report saveInBackground];
//    
//    [self showAlertWithTitle:@"Thank You" message:@"This video has been reported."];
}

#pragma mark -
#pragma mark - Share related.

- (IBAction)shareAction:(id)sender {
    // Create the item to share (in this example, a url)
//    NSString *embedURL;
//    if (self.episode) {
//        NSString *vid = self.episode.ytlinks[0];
//        if ([vid rangeOfString:@"dailymotion"].location == NSNotFound) {
//            embedURL = [[DataManager sharedManager] getYoutubeEmbedLinkFromVID:vid];
//        } else embedURL = self.episode.ytlinks[0];
//    } else {
//        embedURL = self.video.embedURL;
//    }
//    NSURL *url = [NSURL URLWithString:embedURL];
//        // TODO: share implementation
//    [self shareText:@"Check this video" andImage:nil andUrl:url];
}

- (void)shareText:(NSString *)text andImage:(UIImage *)image andUrl:(NSURL *)url
{
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    if (text) {
        [sharingItems addObject:text];
    }
    if (image) {
        [sharingItems addObject:image];
    }
    if (url) {
        [sharingItems addObject:url];
    }
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    if ( [activityController respondsToSelector:@selector(popoverPresentationController)] ) {
        // iOS8
        activityController.popoverPresentationController.sourceView = self.shareButton;
    }
    [self presentViewController:activityController animated:YES completion:nil];
}

- (IBAction)watchLaterAction:(id)sender {
//    self.watchLaterButton.selected = !self.watchLaterButton.selected;
//    if (self.episode) {
//        if (self.watchLaterButton.selected) {
//            [self.episode saveAsFavourite];
//        } else [self.episode saveAsNotFavourite];
//    } else if (self.video) {
//        if (self.watchLaterButton.selected) {
//            [self.video saveAsFavourite];
//        } else [self.video saveAsNotFavourite];
//    }
}

#pragma mark - search broken/alternate video related

- (IBAction)searchOnYouTubeAction:(id)sender {
//    if ([[UserManager sharedManager] currentUser].childUser) {
//        [self askPinWithUser:[[UserManager sharedManager] currentUser]];
//    } else {
//        [self allowSearchOnYoutube];
//    }
}

-(void) allowSearchOnYoutube {
//    if (self.episode) {
//        if ([self.episode.title rangeOfString:@"_"].location == NSNotFound) {
//            [self searchByEpisodeAction];
//        } else {
//            NSString *friendlyDate = [Utility friendlyDateStringFromDate:self.episode.addedOn];
//            NSString *searchString = [NSString stringWithFormat:@"%@ %@",self.episode.contentName, friendlyDate];
//            [self searchWithString:searchString];
//        }
//    } else if (self.video) {
//        [self searchWithString:self.video.title];
//    }
}

- (void)searchByEpisodeAction {
//    NSString *EpisodeNumber;
//    if ([self.episode.title rangeOfString:@"Episode"].location == NSNotFound) {
//        EpisodeNumber = @" ";
//    } else {
//        EpisodeNumber = self.episode.title;
//    }
//    NSString *searchString = [NSString stringWithFormat:@"%@ %@",self.episode.contentName, EpisodeNumber];
//    [self searchWithString:searchString];
}

-(void) searchWithString:(NSString *)searchString {
    //    searchString = [Utility youtubeSearchUrlForSearchText:searchString];
    CustomSearchResultViewController *resultVC = [[CustomSearchResultViewController alloc] initWithNibName:@"CustomSearchResultViewController" bundle:nil];
    [resultVC setSearchString:searchString];
    [self.navigationController pushViewController:resultVC animated:YES];
}

#pragma mark - pin view delegate methods

-(void) pinViewAllowedAccessToUser:(User *)user {
    [self removePinView];
    [self allowSearchOnYoutube];
}

-(void) pinViewdeniedAccessToUser:(User *)user {
    [self removePinView];
}

#pragma mark - touches related.

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if([self.wheel isAnimating]) {
        [self.wheel stopAnimating];
    }
    
    UITouch *touch = [touches anyObject];
    
    if (touch.view == self.ratingView) {
        return;
    }
    
    self.ratingView.hidden = !self.ratingView.hidden;
}

#pragma mark - YTPlayer View delegate

- (void)playerViewDidBecomeReady:(YTPlayerView *)playerView {
    self.JustStartedPlaying = YES;
    [playerView playVideo];
}

- (void)playerView:(YTPlayerView *)playerView didChangeToState:(YTPlayerState)state {
    switch (state) {
        case kYTPlayerStatePlaying:
            NSLog(@"Started playback");
            if (self.JustStartedPlaying) {
                CGFloat seekTime = [[UserManager sharedManager] lastPlaybackSeekTimeForVID:[self vidFromPlayerView:playerView]];
                [self.ytPlayerView seekToSeconds:seekTime allowSeekAhead:YES];
                self.JustStartedPlaying = NO;
            }
            break;
        case kYTPlayerStatePaused:
            NSLog(@"Paused playback");
            [[UserManager sharedManager] savePlayBackSeekTimeForVID:[self vidFromPlayerView:playerView] seekTime:playerView.currentTime];
            break;
        case kYTPlayerStateEnded: {
                NSLog(@"Finished playing.");
                [[UserManager sharedManager] removePlayBackSeekTimeForVID:[self vidFromPlayerView:playerView]];
            }
            break;
        default:
            break;
    }
}

- (NSString *)vidFromPlayerView:(YTPlayerView *)playerView {
    NSString *vid = [[[playerView.videoUrl absoluteString] lastPathComponent] stringByReplacingOccurrencesOfString:@"watch?v=" withString:@""];
    return vid;
}

@end
