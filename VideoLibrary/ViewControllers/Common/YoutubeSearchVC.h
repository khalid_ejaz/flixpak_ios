//
//  YoutubeSearchVC.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/7/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "RootViewController.h"
#import "YCSearchResultView.h"
#import "SwitchControl.h"
#import "YoutubeVideosVC.h"

@interface YoutubeSearchVC : RootViewController <YCSearchResultItemDelegate, UITextFieldDelegate, SwitchControlDelegate, YouTubeVideosVCDelegate, UIActionSheetDelegate>

@end
