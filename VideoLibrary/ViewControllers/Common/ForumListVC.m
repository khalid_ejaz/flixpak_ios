//
//  ForumListVC.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/14/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "ForumListVC.h"
#import "ForumViewCell.h"
#import "ForumWebVC.h"
#import "Utility.h"
#import "DataManager.h"
#import "UserManager.h"

#define FORUM_CELL_ID   @"forumCellId"

@interface ForumListVC ()

@property (unsafe_unretained, nonatomic) IBOutlet UIView *headerView;
@property (nonatomic, strong) NSArray *forums;
@property (nonatomic, strong) NSMutableArray *filteredForums;

@property (unsafe_unretained, nonatomic) IBOutlet UITableView *tableView;
@property (unsafe_unretained, nonatomic) IBOutlet UITextField *searchTextField;

@end

@implementation ForumListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView registerNib:[UINib nibWithNibName:@"ForumViewCell" bundle:nil] forCellReuseIdentifier:FORUM_CELL_ID];
    [self.searchTextField addTarget:self action:@selector(searchTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    self.headerView.backgroundColor = [[UserManager sharedManager] colorForUser];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.firstLoadDone) {
        return;
    } else self.firstLoadDone = YES;
    
    [self loadData];
}

-(void) loadData {
    [self showSpinner];
    [self readForumsWithCompletion:^(NSArray *objects, NSError *error) {
        if (error) {
            [self showNetworkError];
        } else if (objects.count == 0) {
            [self showGeneralError];
        } else {
            self.forums = objects;
            self.filteredForums = [self.forums mutableCopy];
            [self.tableView reloadData];
            [self hideSpinner];
        }
    }];
}

#pragma mark - IBAction

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - table view data source

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.filteredForums.count;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ForumViewCell *cell = [tableView dequeueReusableCellWithIdentifier:FORUM_CELL_ID];
    Forum *forum = [self.filteredForums objectAtIndex:indexPath.row];
    [cell populateWithForum:forum];
    return cell;
}

#pragma mark - table view delegate

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Forum *forum = [self.filteredForums objectAtIndex:indexPath.row];
    ForumWebVC *forumVC = [[ForumWebVC alloc] initWithNibName:@"ForumWebVC" bundle:nil];
    [forumVC setForum:forum];
    [self.navigationController pushViewController:forumVC animated:YES];
}

#pragma mark - scroll view delegate

-(void) scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([self.searchTextField isFirstResponder]) {
        [self.searchTextField resignFirstResponder];
    }
}

#pragma mark - text field related

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    [self.searchTextField resignFirstResponder];
    return NO;
}

-(void) searchTextFieldDidChange:(id)sender {
//    NSString *filterString = [Utility stringByRemovingWhiteSpaces:self.searchTextField.text];
//    if (filterString.length == 0) {
//        if (self.forums.count != self.filteredForums.count) {
//            self.filteredForums = [self.forums mutableCopy];
//            [self.tableView reloadData];
//        }
//    } else {
//        NSMutableArray *filteredArray = [[DataManager sharedManager] filterArray:self.forums SearchString:filterString field:KEY_FORUM_NAME];
//        if (filteredArray.count != self.filteredForums.count) {
//            self.filteredForums = filteredArray;
//            [self.tableView reloadData];
//        }
//    }
}

@end
