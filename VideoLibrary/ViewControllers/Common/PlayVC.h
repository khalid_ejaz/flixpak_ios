//
//  PlayVC.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-17.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "AppRootViewController.h"
#import "YTPlayerView.h"

@interface PlayVC : AppRootViewController <UICollectionViewDelegate, UICollectionViewDataSource, YTPlayerViewDelegate>

- (void)playVideosInSection:(NSDictionary *)section startingIndex:(NSInteger)index;

@end
