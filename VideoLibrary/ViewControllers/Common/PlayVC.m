//
//  PlayVC.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-17.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "PlayVC.h"
#import "PlayerOverlayVideoCell.h"
#import "Utility.h"
#import "ME_PrefsManager.h"
#import "ME_ConfigManager.h"
#import "FlixPakClient.h"

@interface PlayVC ()

@property (weak, nonatomic) IBOutlet UIView *playerContainerView;
@property (weak, nonatomic) IBOutlet YTPlayerView *playerView;
@property (weak, nonatomic) IBOutlet UICollectionView *latestCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *episodesCollectionView;
@property (weak, nonatomic) IBOutlet UIView *moreView;
@property (weak, nonatomic) IBOutlet UIView *episodeView;
@property (weak, nonatomic) IBOutlet UIView *latestView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *episodesSpinner;
@property (weak, nonatomic) IBOutlet UILabel *allEpisodesLabel;

@property (weak, nonatomic) IBOutlet UILabel *videoTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *videoSubtitleLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet UIButton *episodesButton;

@property (nonatomic, strong) NSDictionary *section;
@property (nonatomic, strong) NSDictionary *episodesSection;
@property (nonatomic, strong) NSArray *latestVideos;
@property (nonatomic, strong) NSArray *allEpisodes;
@property (nonatomic, strong) NSDictionary *selectedDrama;
@property (nonatomic, assign) NSInteger playingIndex;
@property (nonatomic, assign) NSInteger episodeIndex;

@property (weak, nonatomic) IBOutlet UIView *starsView;
@property (weak, nonatomic) IBOutlet UIButton *oneStarButton;
@property (weak, nonatomic) IBOutlet UIButton *twoStarButton;
@property (weak, nonatomic) IBOutlet UIButton *threeStarButton;
@property (weak, nonatomic) IBOutlet UIButton *fourStarButton;
@property (weak, nonatomic) IBOutlet UIButton *fiveStarButton;

@property (nonatomic, assign) NSNumber *starRating;
@property (nonatomic, strong) NSString *typeForRating;
@property (nonatomic, strong) NSString *vid;
@property (nonatomic, strong) NSNumber *_id;

@property (nonatomic, strong) NSTimer *timer;

@end

@implementation PlayVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.playerView.delegate = self;
    // add ads.
    
    UINib *cellNib = [UINib nibWithNibName:@"PlayerOverlayVideoCell" bundle:nil];
    [self.latestCollectionView registerNib:cellNib forCellWithReuseIdentifier:@"cvCell"];
    [self.episodesCollectionView registerNib:cellNib forCellWithReuseIdentifier:@"cvCell"];
    
    [self.episodesCollectionView setCollectionViewLayout:[self collectionViewLayout]];
    [self.latestCollectionView setCollectionViewLayout:[self collectionViewLayout]];
    
    self.episodesCollectionView.backgroundColor = [UIColor clearColor];
    self.latestCollectionView.backgroundColor = [UIColor clearColor];
    
    self.playerView.backgroundColor = [UIColor blackColor];
    self.playerView.hidden = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.episodeIndex = -1;
    self.starRating = 0;
}

- (UICollectionViewFlowLayout *)collectionViewLayout {
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(240, 135)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [flowLayout setMinimumLineSpacing:8.0f];
    [flowLayout setMinimumInteritemSpacing:0.0f];
    [flowLayout setSectionInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
    return flowLayout;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self playVideoAtPlayingIndex];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self layout];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.playerView stopVideo];
    [self stopAutoStopTimer];
}

- (void)loadEpisodesWithDramaId:(NSString *)dramaId {
    self.episodeView.hidden = NO;
    self.allEpisodes = nil;
    self.episodesSection = nil;
    [self.episodesCollectionView reloadData];
    [self.episodesSpinner startAnimating];
    [[FlixPakClient singleton] getRoute:@"detail" queryParams:@{@"drama_id":dramaId} withCompletion:^(NSDictionary *dictionary, NSError *error) {
        [self.episodesSpinner stopAnimating];
        if (dictionary) {
            if ([dictionary.allKeys containsObject:@"data"]) {
                self.episodesSection = dictionary[@"data"];
                if ([self.episodesSection.allKeys containsObject:@"sections"]) {
                    NSArray *seasons = self.episodesSection[@"sections"];
                    if (seasons.count > 0) {
                        NSDictionary *firstSeason = seasons[0];
                        if ([firstSeason.allKeys containsObject:@"episodes"]) {
                            NSArray *episodes = firstSeason[@"episodes"];
                            self.allEpisodes = episodes;
                            [self.episodesCollectionView reloadData];
                            
                            // find last episode played and play next episode.
                            NSDictionary *dict = [[ME_PrefsManager singleton] dictionaryPrefsForKey:@"lastEpisodesForDramaIdDict"];
                            NSString *key = [NSString stringWithFormat:@"%@",dramaId];
                            if ([dict.allKeys containsObject:key]) {
                                int lastEp = [dict[key] intValue];
                                NSNumber *nextEp = [NSNumber numberWithInt:(lastEp+1)];
                                
                                BOOL nextEpisodeFound = NO;
                                for (int i=0; i<episodes.count; i++) {
                                    NSDictionary *episode = episodes[i];
                                    NSNumber *episodeNumber = episode[@"episode"];
                                    if ([episodeNumber isEqual:nextEp]) {
                                        self.episodeIndex = i;
                                        [self playVideoAtEpisodeIndex];
                                        nextEpisodeFound = YES;
                                        break;
                                    }
                                }
                                
                                // next episode not found. play last episode again.
                                if (!nextEpisodeFound) {
                                    self.episodeIndex = episodes.count - 1;
                                    [self playVideoAtEpisodeIndex];
                                }
                            } else {
                                self.episodeIndex = 0;
                                [self playVideoAtEpisodeIndex];
                            }
                        }
                    }
                }
            }
        }
    }];
}

- (NSString *)screenName {
    return @"player";
}

- (void)layout {
    // resize player to fill screen - ads.
    CGRect frame = self.playerContainerView.frame;
    frame.size.height = self.view.frame.size.height - frame.origin.y - [[ME_AdManager sharedManager] adHeight];
    frame.size.width = self.view.frame.size.width;
    self.playerContainerView.frame  = frame;
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rewindAction:(id)sender {
    float time = [self.playerView currentTime] - 7;
    [self.playerView seekToSeconds:time allowSeekAhead:YES];
}

- (IBAction)forwardAction:(id)sender {
    float time = [self.playerView currentTime] + 7;
    [self.playerView seekToSeconds:time allowSeekAhead:YES];
}

- (IBAction)prevVideoAction:(id)sender {
    [self stopAutoStopTimer];
    [self playPrevVideo];
}

- (IBAction)nextVideoAction:(id)sender {
    [self stopAutoStopTimer];
    [self playNextVideo];
}

- (void)playNextVideo {
    if (self.episodeIndex == -1) {
        self.playingIndex++;
        [self playVideoAtPlayingIndex];
    } else {
        self.episodeIndex++;
        [self playVideoAtEpisodeIndex];
    }
}

- (void)playPrevVideo {
    if (self.episodeIndex == -1) {
        self.playingIndex--;
        [self playVideoAtPlayingIndex];
    } else {
        self.episodeIndex--;
        [self playVideoAtEpisodeIndex];
    }
}

- (IBAction)playListAction:(id)sender {
    [self toggleCollectionView];
}

- (IBAction)oneStarAction:(id)sender {
    int rating = 1;
    self.starRating = [NSNumber numberWithInt:rating];
    [self showStars:rating];
    [self saveRating];
}

- (IBAction)twoStarAction:(id)sender {
    int rating = 2;
    self.starRating = [NSNumber numberWithInt:rating];
    [self showStars:rating];
    [self saveRating];
}

- (IBAction)threeStarAction:(id)sender {
    int rating = 3;
    self.starRating = [NSNumber numberWithInt:rating];
    [self showStars:rating];
    [self saveRating];
}

- (IBAction)fourStarAction:(id)sender {
    int rating = 4;
    self.starRating = [NSNumber numberWithInt:rating];
    [self showStars:rating];
    [self saveRating];
}

- (IBAction)fiveStarAction:(id)sender {
    int rating = 5;
    self.starRating = [NSNumber numberWithInt:rating];
    [self showStars:rating];
    [self saveRating];
}

-(NSArray *)starButtons {
    return @[self.oneStarButton, self.twoStarButton, self.threeStarButton, self.fourStarButton, self.fiveStarButton];
}

-(void) disableStarButtons {
    NSArray *starButtons = [self starButtons];
    for (UIButton *button in starButtons) {
        button.enabled = NO;
    }
}

- (void)enableStarButtons {
    NSArray *starButtons = [self starButtons];
    for (UIButton *button in starButtons) {
        button.enabled = YES;
    }
}

-(void) resetStarButtons {
    NSArray *starButtons = [self starButtons];
    for (UIButton *button in starButtons) {
        button.enabled = YES;
        [button setTitle:@"☆" forState:UIControlStateNormal];
        [button setSelected:NO];
    }
}

- (void)showStars:(int)stars {
    if (stars >= 5)
        stars = 5;
    
    NSArray *buttons = [self starButtons];;
    for (int i=0; i<stars; i++) {
        UIButton *b = buttons[i];
        [b setTitle:@"★" forState:UIControlStateNormal];
    }
    
    for (int i=stars; i<5; i++) {
        UIButton *b = buttons[i];
        [b setTitle:@"☆" forState:UIControlStateNormal];
    }
}

- (void)playVideosInSection:(NSDictionary *)section startingIndex:(NSInteger)index {
    _section = section;
    _latestVideos = section[@"videos"];
    _playingIndex = index;
    [self.spinner startAnimating];
    [self.latestCollectionView reloadData];
}

- (void)playVideoAtEpisodeIndex {
    if (self.episodeIndex < 0) self.episodeIndex = 0;
    if (self.episodeIndex >= self.allEpisodes.count) {
        self.episodeIndex = -1;
        [self playNextVideo];
    } else {
        NSDictionary *episode = self.allEpisodes[self.episodeIndex];
        NSMutableDictionary *lastEpisodeForDramaDict = [[[ME_PrefsManager singleton] dictionaryPrefsForKey:@"lastEpisodesForDramaIdDict"] mutableCopy];
        [lastEpisodeForDramaDict setObject:episode[@"episode"] forKey:[NSString stringWithFormat:@"%@",self.selectedDrama[@"id"]]];
        [[ME_PrefsManager singleton] saveDictionaryPrefs:lastEpisodeForDramaDict forKey:@"lastEpisodesForDramaIdDict"];
        self.videoSubtitleLabel.text = [NSString stringWithFormat:@"Episode %@", episode[@"episode"]];
        [self playVideo:episode[@"video_link"]];
        [self.episodesCollectionView reloadData];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.episodeIndex inSection:0];
        [self.episodesCollectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionLeft];
        [self.spinner startAnimating];
        [self performSelector:@selector(hideCollectionView) withObject:nil afterDelay:1.0f];
    }
}

- (void)playVideoAtPlayingIndex {
    // get vid at playing index in videos array and start playing video.
    if (self.playingIndex < 0) self.playingIndex = 0;
    
    if (self.playingIndex < self.latestVideos.count) {
        
        NSDictionary * video = self.latestVideos[_playingIndex];
        self.videoTitleLabel.text = video[@"title"];
        self.videoSubtitleLabel.text = video[@"subtitle"];
        self.allEpisodesLabel.text = [NSString stringWithFormat:@"ALL EPISODES - %@", self.videoTitleLabel.text];
//        [self playVideo:video[@"vid"]];
        // if its a drama, episode will be selected and played otherwise video will be played below.
        [self showAvgRatings];
        
        [self.latestCollectionView reloadData];
        self.latestCollectionView.hidden = NO;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.playingIndex inSection:0];
        [self.latestCollectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionLeft];
        
        // load episodes if showing drama.
        self.episodeView.hidden = YES;
        if (self.section) {
            if ([self.section.allKeys containsObject:@"kind"]) {
                NSString *kind = self.section[@"kind"];
                if ([kind isEqualToString:@"App Category"] || [kind isEqualToString:@"Browse"]) {
                    if ([self.section.allKeys containsObject:@"type"]) {
                        NSString *type = self.section[@"type"];
                        _typeForRating = type;
                        if ([type isEqualToString:@"Drama"]) {
                            NSArray *videos = self.section[@"videos"];
                            if (self.playingIndex >= 0 && self.playingIndex < videos.count) {
                                NSDictionary *selectedDrama = videos[self.playingIndex];
                                _selectedDrama = selectedDrama;
                                if ([selectedDrama.allKeys containsObject:@"id"]) {
                                    NSString *dramaId = selectedDrama[@"id"];
                                    __id = selectedDrama[@"id"];
                                    [self loadEpisodesWithDramaId:dramaId];
                                    return;
                                }
                            }
                        } else if ([type isEqualToString:@"Daily_Show"]){
                            __id = video[@"daily_show_update_id"];
                        } else if ([type isEqualToString:@"Titled_Content"] || [type isEqualToString:@"Movie"]) {
                            __id = video[@"id"];
                        } else if ([type isEqualToString:@"User Channel"]) {
                            __id = video[@"id"];
                        }
                    }
                }
            }
        }
        
        // start non drama video immediately otherwise would have returned above.
        [self playVideo:video[@"vid"]];
    } else {
        self.playingIndex = 0;
        if (self.latestVideos.count > 0) {
            [self playVideoAtPlayingIndex];
        }
    }
    [self performSelector:@selector(hideCollectionView) withObject:nil afterDelay:1.0f];
}

- (void)hideCollectionView {
    if (self.moreView.hidden) return;
    [self toggleCollectionView];
//    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideCollectionView) object:nil];
//    [UIView animateWithDuration:0.5f animations:^{
//        self.moreView.alpha = 0.0f;
//    } completion:^(BOOL finished) {
//        self.moreView.hidden = YES;
//        self.moreView.alpha = 1.0f;
//    }];
}

- (void)gotoNextVideo {
    // if WiFi is connected, auto play next video.
    // show overlay videos and select next video as playing.
    NSNumber *autoplayOnWifi = [[ME_PrefsManager singleton] numberPrefForKey:@"autoplayOnWiFi"];
    NSNumber *autoplayOnCellular = [[ME_PrefsManager singleton] numberPrefForKey:@"autoplayOnCellular"];
    
    if ([autoplayOnWifi isEqual:@1]) {
        if ([[ME_ConfigManager singleton] isConnectedOverWiFi]) {
            [self startAutoStopTimer];
            [self playNextVideo];
            return;
        }
    }
    
    if ([autoplayOnCellular isEqual:@1]) {
        if ([[ME_ConfigManager singleton] isConnectedOverCellular]) {
            [self playNextVideo];
            [self startAutoStopTimer];
            return;
        }
    }

    // if auto play is disabled, show watch next after one video ends. if already showing, do nothing.
    if (self.moreView.hidden) {
        [self toggleCollectionView];
    }
}

- (void)startAutoStopTimer {
    if (!self.timer) {
        // auto stop after two hours.
        self.timer = [NSTimer scheduledTimerWithTimeInterval:(7200) target:self selector:@selector(stillWatching) userInfo:nil repeats:NO];
    }
}

- (void)stopAutoStopTimer {
    // if user chooses a videos.
    [self.timer invalidate];
    self.timer = nil;
}

- (void)stillWatching {
    [self.playerView pauseVideo];
    self.messageAction = @"stillWatching";
    [self showMessage:@"Are you still watching" title:@"Continue autoplay?" okButtonTitle:@"yes" cancelButtonTitle:@"no"];
}

- (void)messageViewClosedWithCancel {
    [super messageViewClosedWithCancel];
}

- (void)messageViewClosedWithOK {
    if ([self.messageAction isEqualToString:@"stillWatching"]) {
        [self.playerView playVideo];
        [self stopAutoStopTimer];
    }
    [super messageViewClosedWithOK];
}

- (void)playVideo:(NSString *)vid {
    _vid = vid;
    [self.playerView stopVideo];
    self.playerView.hidden = YES;
    
    NSDictionary *ratingsDict = [[ME_PrefsManager singleton] dictionaryPrefsForKey:@"ratingsDict"];
    
    NSString *ratingKey = [NSString stringWithFormat:@"%@", self._id];
    if ([ratingsDict.allKeys containsObject:ratingKey]) {
        // already rated.
        NSNumber *rating = ratingsDict[ratingKey];
        [self showStars:[rating intValue]];
        [self disableStarButtons];
    } else [self enableStarButtons];
    
    
    // clear dicts after 200 views and ratings. enough to deter fake views and ratings.
    if (ratingsDict.allKeys.count > 200) {
        [[ME_PrefsManager singleton] saveDictionaryPrefs:@{} forKey:@"ratingsDict"];
    }
    
    NSDictionary *playerVars = @{
                                 @"playsinline" : @1,
                                 @"rel":@0
                                 };
    [self.playerView loadWithVideoId:vid playerVars:playerVars];
}

- (void)saveViewed {
    if (!self.typeForRating || !self._id || !self.vid) return;
    NSString *vidKey = self.vid;
    NSDictionary *viewsDict = [[ME_PrefsManager singleton] dictionaryPrefsForKey:@"viewsDict"];
    
    if ([viewsDict.allKeys containsObject:self.vid]) {
        // already viewed.
        _vid = nil;
        return;
    }
    _vid = nil;
    if (viewsDict.allKeys.count > 200) {
        [[ME_PrefsManager singleton] saveDictionaryPrefs:@{} forKey:@"viewsDict"];
    }
    
    NSMutableDictionary *mutableViewsDict = [viewsDict mutableCopy];
    [mutableViewsDict setObject:@1 forKey:vidKey];
    [[ME_PrefsManager singleton] saveDictionaryPrefs:mutableViewsDict forKey:@"viewsDict"];
    
    NSString *form = [NSString stringWithFormat:@"type=%@&_id=%@", self.typeForRating, self._id];
    [[FlixPakClient singleton] POSTRoute:@"view" form:form withCompletion:^(NSDictionary *dictionary, NSError *error) {
        // TODO: log errors or success.
        if (dictionary) {
            
        }
    }];
}

- (void)saveRating {
    if (!self.typeForRating || !self._id || !self.starRating) return;
    
    [self disableStarButtons];
    
    NSDictionary *ratingsDict = [[ME_PrefsManager singleton] dictionaryPrefsForKey:@"ratingsDict"];
    NSMutableDictionary *mutableratingsDict = [ratingsDict mutableCopy];
    [mutableratingsDict setObject:self.starRating forKey:[NSString stringWithFormat:@"%@", self._id]];
    [[ME_PrefsManager singleton] saveDictionaryPrefs:mutableratingsDict forKey:@"ratingsDict"];
    
    NSString *form = [NSString stringWithFormat:@"type=%@&_id=%@&rating=%@", self.typeForRating, self._id, self.starRating];
    [[FlixPakClient singleton] POSTRoute:@"rating" form:form withCompletion:^(NSDictionary *dictionary, NSError *error) {
        // TODO: log errors or success.
        self.starRating = @0;
        if (dictionary) {
            
        }
    }];
}

- (void)showAvgRatings {
    // TODO: if user has not rated for this video yet, show avg rating by other users.
    [self resetStarButtons];
    // else show user's own ratings and disable buttons as user already rated and can not rate again.
    [self disableStarButtons];
    
    if ([self.section[@"kind"] isEqualToString:@"App Category"] | [self.section[@"kind"] isEqualToString:@"Browse"]) {
        self.starsView.hidden = NO;
        NSNumber *rat = [[self latestVideos] objectAtIndex:self.playingIndex][@"ratings"];
        int rating = [rat intValue];
        [self showStars:rating];
        [self enableStarButtons];
    } else {
        self.starsView.hidden = YES;
    }
}

- (void)toggleCollectionView {
    if (self.moreView.hidden) {
        CGRect rect = self.moreView.frame;
        rect.origin.x = 0;
        rect.origin.y = self.playerContainerView.frame.size.height - 44;
        rect.size.width = self.playerContainerView.frame.size.width;
        self.moreView.frame = rect;
        
        self.moreView.hidden = NO;
        
        rect.origin.y = self.playerContainerView.bounds.size.height - rect.size.height - 44;
        [UIView animateWithDuration:0.3f animations:^{
            self.moreView.frame = rect;
        }];
    } else {
        CGRect rect = self.moreView.frame;
        rect.origin.y = self.playerContainerView.bounds.size.height - 44;
        
        [UIView animateWithDuration:0.3f animations:^{
            self.moreView.frame = rect;
        } completion:^(BOOL finished) {
            self.moreView.hidden = YES;
        }];
    }
}

#pragma mark - Youtube Player View delegate methods.

- (void)playerViewDidBecomeReady:(YTPlayerView *)playerView {
    [self.spinner stopAnimating];
    self.playerView.hidden = NO;
    [playerView playVideo];
}

- (void)playerView:(nonnull YTPlayerView *)playerView didChangeToState:(YTPlayerState)state {
    if (state == kYTPlayerStateEnded) {
        [self gotoNextVideo];
    } else if (state == kYTPlayerStatePlaying) {
        [self saveViewed];
    }
}

- (UIColor *)playerViewPreferredWebViewBackgroundColor:(YTPlayerView *)playerView {
    return [UIColor blackColor];
}

#pragma mark - Collection View Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == self.episodesCollectionView) {
        return self.allEpisodes.count;
    }
    return self.latestVideos.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cvCell";
    PlayerOverlayVideoCell *cell = (PlayerOverlayVideoCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (collectionView == self.episodesCollectionView) {
        NSDictionary *episode = self.allEpisodes[indexPath.row];
        [cell populateWithTitle:@"" subtitle:[NSString stringWithFormat:@"Episode %@", episode[@"episode"]] rating:@"" thb:@"" vid:episode[@"video_link"]];
        if (indexPath.row == self.episodeIndex) {
            [cell setPlaying:YES];
        } else [cell setPlaying:NO];
        return cell;
    } else {
        NSDictionary *video = self.latestVideos[indexPath.row];
        [cell populateWithTitle:video[@"title"] subtitle:video[@"subtitle"] rating:video[@"ratings"] thb:video[@"thb"] vid:video[@"vid"]];
        if (indexPath.row == self.playingIndex) {
            [cell setPlaying:YES];
        } else [cell setPlaying:NO];
        return cell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.episodesCollectionView) {
        self.episodeIndex = indexPath.row;
        [self playVideoAtEpisodeIndex];
    } else {
        self.playingIndex = indexPath.row;
        [self playVideoAtPlayingIndex];
    }
    [self stopAutoStopTimer];
}

@end
