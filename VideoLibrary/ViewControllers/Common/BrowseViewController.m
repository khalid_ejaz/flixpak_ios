//
//  ChooseFavsViewController.m
//  PakTV
//
//  Created by Malik Khalid Ejaz on 11/16/2013.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import "BrowseViewController.h"
#import "ContentBrowserViewController.h"
#import "CategoryViewCell.h"
#import "ContentViewCell.h"
#import "DataManager.h"
#import "UserManager.h"
#import "Utility.h"
#import "ContentViewerVC.h"
#import "CategoryViewerVC.h"
#import "ChannelContainerVC.h"
#import "HoriScrollView.h"
#import <QuartzCore/QuartzCore.h>
#import "ContainerViewController.h"

#define CATEGORY_CELL_REUSE_ID      @"categoryCellReuseId"
#define CONTENT_CELL_REUSE_ID       @"contentCellReuseId"
#define CHANNEL_CELL_REUSE_ID       @"channelCellReuseId"

#define CATEGORY_CELL_HEIGHT    44
#define CONTENT_CELL_HEIGHT     44

#define SECTION_HEADER_HEIGHT   20

#define TILE_MARGIN             10

@interface BrowseViewController ()


@property (unsafe_unretained, nonatomic) IBOutlet UIView *contentView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *doneButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *headerView;
@property (unsafe_unretained, nonatomic) IBOutlet UITableView *tableView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *backButton;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *categoryNameLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UITextField *searchTextField;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *searchView;

@property (nonatomic, strong) NSArray *subCategories;
@property (nonatomic, strong) NSMutableArray *filteredSubCategories;

@property (nonatomic, strong) NSArray *contents;
@property (nonatomic, strong) NSMutableArray *filteredContents;

@property (nonatomic, strong) NSArray *channels;
@property (nonatomic, strong) NSMutableArray *filteredChannels;

@property (nonatomic, strong) NSArray *userChannels;
@property (nonatomic, strong) NSMutableArray *filteredUserChannels;

@property (nonatomic, strong) PSTCollectionView *collectionView;

@property (nonatomic, strong) HoriScrollView *channelScrolView;
@property (nonatomic, assign) CGSize tileSize;

@property (nonatomic, assign) BOOL contentLoaded;
@property (nonatomic, assign) BOOL channelsLoaded;

@end

@implementation BrowseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.searchTextField addTarget:self action:@selector(searchTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    self.filteredContents = [NSMutableArray array];
    self.filteredSubCategories = [NSMutableArray array];
    self.headerView.backgroundColor = [[UserManager sharedManager] colorForUser];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.firstLoadDone) {
        return;
    } else self.firstLoadDone = YES;
    
    [self setupCollectionView];
    
    self.navigationController.navigationBarHidden = YES;
    if (self.navigationController.viewControllers.count == 1) {
        self.backButton.hidden = YES;
    } else self.backButton.hidden = NO;
    if (self.showingLocalFavs) {
        self.categoryNameLabel.text = @"Your Favorites";
        
        self.subCategories = [[DataManager sharedManager] getSortedArrayForLocalFavCategories:[[UserManager sharedManager] favCategories]];
        self.filteredSubCategories = [self.subCategories mutableCopy];
        
        self.contents = [[DataManager sharedManager] getSortedArrayForLocalFavContents:[[UserManager sharedManager] favContents]];
        self.filteredContents = [self.contents mutableCopy];
        
        self.channels = [[DataManager sharedManager] getSortedArrayForLocalFavChannels:[[UserManager sharedManager] favChannels]];
        self.filteredChannels = [self.channels mutableCopy];
        
        self.userChannels = [[DataManager sharedManager] getSortedArrayForLocalFavUserChannels:[[UserManager sharedManager] favUserChannels]];
        self.filteredUserChannels = [self.userChannels mutableCopy];

        [self.collectionView reloadData];
    } else {
        [self showSpinner];
        [self updateViews];
    }
}

-(void) loadCategoryData {
    if (self.selectedCategoryId) {
        [self readCategoryWithId:self.selectedCategoryId completion:^(PFObject *category, NSError *error) {
            
            if (error) {
                [self showError:error];
            } else {
                if (category) {
                    _category = (ContentCategory *)category;
                }
                [self updateViews];
            }
        }];
    }
}

-(void) setSelectedCategoryId:(NSString *)selectedCategoryId {
    if ([_selectedCategoryId isEqualToString:selectedCategoryId]) {
        return;
    }
    
    _selectedCategoryId = selectedCategoryId;
    [self loadCategoryData];
}

-(void) setCategory:(ContentCategory *)category {
    _category = category;
}

-(void) updateViews {
    
//    if (!self.category) {
//        
//        self.categoryNameLabel.text = @"Browse";
//        BOOL childOnly = [[[UserManager sharedManager] currentUser] childUser];
//        [self readRootCategoriesWithChildOnly:childOnly completion:^(NSArray *objects, NSError *error) {
//            if (error) {
//                [self showError:error];
//            } else {
//                self.subCategories = objects;
//                self.filteredSubCategories = [self.subCategories mutableCopy];
//                [self.collectionView reloadData];
//            }
//            [self hideSpinner];
//        }];
//    } else {
//        
//        self.categoryNameLabel.text = self.category.categoryName;
//        
//        [self readCategoriesWithParentCategoryId:self.category.objectId completion:^(NSArray *objects, NSError *error) {
//            if (error) {
//                [self showError:error];
//            } else {
//                self.subCategories = objects;
//                self.filteredSubCategories = [self.subCategories mutableCopy];
//                [self.collectionView reloadData];
//            }
//            
//            BOOL furtherLoading = NO;
//            if ([self.category.contents integerValue] > 0) {
//                [self readContents];
//                furtherLoading = YES;
//            }
//            
//            if ([self.category.channels integerValue] > 0) {
//                [self readChannels];
//                furtherLoading = YES;
//            }
//            if (!furtherLoading) {
//                self.contentLoaded = YES;
//                self.channelsLoaded = YES;
//                [self showSearchResult];
//                [self hideSpinner];
//            }
//        }];
//    }
}

// when browse opened from categoryViewerVC to search for content.
-(void) showSearchResult {
    if (self.searchText.length > 0 && self.contentLoaded && self.channelsLoaded) {
        self.searchTextField.text = self.searchText;
        [self searchTextFieldDidChange:self.searchTextField];
        [self.searchTextField becomeFirstResponder];
    }
}

-(void) readContents {
//    [self readContentsWithCategoryId:self.category.objectId completion:^(NSArray *objects, NSError *error) {
//        if (error) {
//            [self showError:error];
//        } else {
//            _contents = objects;
//            self.filteredContents = [self.contents mutableCopy];
//            [self.collectionView reloadData];
//        }
//        self.contentLoaded = YES;
//        [self showSearchResult];
//        [self hideSpinner];
//    }];
}

-(void) readChannels {
//    [self readChannelsWithCategoryId:self.category.objectId completion:^(NSArray *objects, NSError *error) {
//        if (error) {
//            [self showError:error];
//        } else {
//            _channels = objects;
//            self.filteredChannels = [self.channels mutableCopy];
//            [self.collectionView reloadData];
//        }
//        self.channelsLoaded = YES;
//        [self showSearchResult];
//        [self hideSpinner];
//    }];
}

-(void) setupChannelsView {
    if (self.channels.count == 0) {
        return;
    }
    
    [self addChannelScrolView];
    [self showChannels];
}

-(void) addChannelScrolView {
    UIView *headerView = self.tableView.tableHeaderView;
    
    self.channelScrolView = [[HoriScrollView alloc] init];
    [self.channelScrolView setShowsHorizontalScrollIndicator:NO];
    [self.channelScrolView setShowsVerticalScrollIndicator:NO];
    
    headerView.frame = CGRectMake(0, 0, self.view.bounds.size.width, 188);
    
    UILabel *channelLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 40, self.view.bounds.size.width, 20)];
    channelLabel.backgroundColor = GRAY_WITH_PERCENT(20);
    channelLabel.textColor = GRAY_WITH_PERCENT(80);
    channelLabel.textAlignment = NSTextAlignmentCenter;
    channelLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
    if (self.showingLocalFavs) {
        channelLabel.text = @"Favourited YouTube channels";
    } else channelLabel.text = @"Popular YouTube channels";
    [channelLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [headerView addSubview:channelLabel];
    
    CGRect channelRect;
    channelRect.origin.x = 0;
    channelRect.origin.y = 60;
    channelRect.size.width = self.view.bounds.size.width;
    channelRect.size.height = 142;
    self.channelScrolView.frame = channelRect;
    
    [headerView addSubview:self.channelScrolView];
    [self.channelScrolView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    self.tableView.tableHeaderView = headerView;
}

-(void) showChannels {
//    [self.channelScrolView removeAllViews];
//    
//    for (YoutubeChannel *channel in self.filteredChannels) {
//        ChannelView *channelView = [[NSBundle mainBundle] loadNibNamed:@"ChannelView" owner:self options:nil][0];
//        if ([channel isKindOfClass:[UserChannel class]]) {
//            [channelView populateWithUserChannel:(UserChannel *)channel delegate:self];
//        } else [channelView populateWithChannel:channel delegate:self];
//        channelView.layer.cornerRadius = 5.0;
//        channelView.layer.masksToBounds = YES;
//        [self.channelScrolView addView:channelView];
//    }
}

#pragma mark - IBActions

- (IBAction)doneAction:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - search related

-(void) searchTextFieldDidChange:(id)sender {
//    NSString *searchString = [self.searchTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//    if ([searchString isEqualToString:@""]) {
//        BOOL reload = FALSE;
//        if (self.subCategories.count != self.filteredSubCategories.count) {
//            self.filteredSubCategories = [self.subCategories mutableCopy];
//            reload = TRUE;
//        }
//        if (self.contents.count != self.filteredContents.count) {
//            self.filteredContents = [self.contents mutableCopy];
//            reload = TRUE;
//        }
//        if (reload) {
//            [self.collectionView reloadData];
//        }
//        if (self.channels.count != self.filteredChannels.count) {
//            self.filteredChannels = [self.channels mutableCopy];
////            [self showChannels];
//        }
//    } else {
//        NSMutableArray *cats = [[DataManager sharedManager] filterArray:self.subCategories SearchString:searchString field:KEY_CATEGORY_NAME];
//        BOOL reload = FALSE;
//        if (cats.count != self.filteredSubCategories.count) {
//            self.filteredSubCategories = cats;
//            reload = TRUE;
//        }
//        
//        NSMutableArray *cont = [[DataManager sharedManager] filterArray:self.contents SearchString:searchString field:KEY_CONTENT_NAME];
//        if (cont.count != self.filteredContents.count) {
//            self.filteredContents = cont;
//            reload = TRUE;
//        }
//        
//        NSMutableArray *chan = [[DataManager sharedManager] filterArray:self.channels SearchString:searchString field:KEY_YOUTUBE_CHANNEL_NAME];
//        if (chan.count != self.filteredChannels.count) {
//            self.filteredChannels = chan;
//            reload = TRUE;
//        }
//        
//        if (reload) {
//            [self.collectionView reloadData];
//        }
//    }
}

#pragma mark - Table view data source

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (self.subCategories.count == 0 && section == 0) {
        return 0;
    }
    
    if (self.contents.count == 0 && section == 1) {
        return 0;
    }
    
    return SECTION_HEADER_HEIGHT;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (self.subCategories.count == 0 && section == 0) {
        return nil;
    }
    
    if (self.contents.count == 0 && section == 1) {
        return nil;
    }
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, SECTION_HEADER_HEIGHT)];
    [headerView setBackgroundColor:GRAY_WITH_PERCENT(20)];
    [headerView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];

    UILabel *label = [[UILabel alloc] initWithFrame:headerView.bounds];
    label.backgroundColor = GRAY_WITH_PERCENT(20);
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14]];
    [label setTextColor:GRAY_WITH_PERCENT(80)];
    [label setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    
    if (section == 0) {
        if (self.showingLocalFavs) {
            [label setText:@"Favourite Categories"];
        } else [label setText:@"Categories"];
    } else  {
        if (self.showingLocalFavs) {
            [label setText:@"Favourite Content"];
        } else [label setText:@"Content"];
    }
    
    [headerView addSubview:label];
    
    return headerView;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return CATEGORY_CELL_HEIGHT;
    } else return [self tileSize].height + TILE_MARGIN;
//    } else return CONTENT_CELL_HEIGHT;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0) {
        return self.filteredSubCategories.count;
    }
    if (section == 1) {
        return [self rowsInSection:section];
        //return self.filteredContents.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        CategoryViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CATEGORY_CELL_REUSE_ID];
        ContentCategory *category = [self.filteredSubCategories objectAtIndex:indexPath.row];
        [cell populateWithCategory:category];
        cell.showingInMenu = NO;
        if (self.showingLocalFavs) {
            [cell hideChevronLabel];
        }
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else if (indexPath.section == 1) {
//        ContentViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CONTENT_CELL_REUSE_ID];
//        cell.delegate = self;
//        Content *content = [self.filteredContents objectAtIndex:indexPath.row];
//        [cell populateWithContent:content];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        return cell;

        // no cell reuse.
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = GRAY_WITH_PERCENT(6);
        
        
        int itemsAllowedInARow = [self numberOfColumnsAllowed];
        int arrayStart = indexPath.row * itemsAllowedInARow;
        int arrayEnd = arrayStart + itemsAllowedInARow;
        
        int len = [self numberOfColumnsAllowed];
        if(arrayEnd > [self.filteredContents count]) {
            len = [self.filteredContents count] - arrayStart;
        }
        
        NSArray *subArray = [self.filteredContents subarrayWithRange:NSMakeRange(arrayStart, len)];
        
        CGFloat x = TILE_MARGIN;
        for (Content *content in subArray) {
            TileShowView *showView = [[NSBundle mainBundle] loadNibNamed:@"TileShowView" owner:self options:nil][0];
            CGRect frame = showView.frame;
            frame.size = [self tileSize];
            frame.origin.x = x;
            frame.origin.y = TILE_MARGIN;
            showView.frame = frame;
            [showView populateWithContent:content];
            [showView setDelegate:self];
            [cell.contentView addSubview:showView];
            x+= frame.size.width + TILE_MARGIN;
        }
        
        return cell;
    }
    
    abort();
}

-(void) calculateTileSize {
    int cols = [self numberOfColumnsAllowed];
    int marginSpace = TILE_MARGIN * (cols + 1);
    int remaingingSpace = self.view.bounds.size.width - marginSpace;
    int spacePerTile = remaingingSpace/cols;
    self.tileSize = CGSizeMake(spacePerTile, spacePerTile/1.66);
}

-(int) rowsInSection:(int)section {
    int totalShows = self.filteredContents.count;
    int cols = [self numberOfColumnsAllowed];
    int rows = totalShows/cols;
    if (totalShows%cols != 0) {
        rows++;
    }
    return rows;
}

-(int) numberOfColumnsAllowed {
    if (IS_IPAD) {    
        return [self isPortrait]?3:4;
    } else if (IS_FOUR_INCH_IPHONE) {
        return [self isPortrait]?2:4;
    } else return [self isPortrait]?2:3;
}

//-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
//    [self calculateTileSize];
//    [self.tableView reloadData];
//}

#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.showingLocalFavs) {
        return;
    }
    
    if (indexPath.section == 0) {
        ContentCategory *category = [self.filteredSubCategories objectAtIndex:indexPath.row];
        BrowseViewController *catsVC = [[BrowseViewController alloc] initWithNibName:@"BrowseViewController" bundle:nil];
        [catsVC setCategory:category];
        [self.navigationController pushViewController:catsVC animated:YES];
    } else if (indexPath.section == 1) {
        // show all episodes of this content.
        
        Content *content = [self.filteredContents objectAtIndex:indexPath.row];
        
        ContentBrowserViewController *contentVC = [[ContentBrowserViewController alloc] initWithNibName:@"ContentBrowserViewController" bundle:nil];
        [contentVC setCategory:self.category];
        [contentVC setContent:content];
        [self.navigationController pushViewController:contentVC animated:YES];
    }
}

#pragma mark - scroll view delegate

-(void) scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([self.searchTextField isFirstResponder]) {
        [self.searchTextField resignFirstResponder];
    }
}

#pragma mark - text field delegate

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

#pragma mark - channel view delegate

-(void) channelView:(id)channelView selectedChannel:(YoutubeChannel *)channel {
    
    ChannelContainerVC *channelsVC = [[ChannelContainerVC alloc] initWithNibName:@"ChannelContainerVC" bundle:nil];
    [channelsVC setChannel:channel];
    [self.navigationController pushViewController:channelsVC animated:YES];
}

-(void) channelView:(id)channelView selectedUserChannel:(UserChannel *)channel {
//    ChannelContainerVC *channelsVC = [[ChannelContainerVC alloc] initWithNibName:@"ChannelContainerVC" bundle:nil];
//    [channelsVC setUserChannel:channel];
//    [self.navigationController pushViewController:channelsVC animated:YES];
}

#pragma mark - Content view cell delegate 

-(void) addParentCategoryAsFavourite {
//    if (self.category) {
//        [self.category saveAsFavourite];
//        NSNotification *notification = [NSNotification notificationWithName:NOTIFICATION_CHANGED_FAVS_FROM_SIDE_MENU object:nil userInfo:@{@"category":self.category,@"selected":@"yes"}];
//        [[NSNotificationCenter defaultCenter] postNotification:notification];
//    }
}

#pragma mark - Tile show view delegate {

-(void) tileViewSelectedContent:(Content *)content {
    ContentBrowserViewController *contentVC = [[ContentBrowserViewController alloc] initWithNibName:@"ContentBrowserViewController" bundle:nil];
    [contentVC setCategory:self.category];
    [contentVC setContent:content];
    [self.navigationController pushViewController:contentVC animated:YES];
}

-(void) tileViewFavdContent:(Content *)content {
//    if (self.category) {
//        [self.category saveAsFavourite];
//        NSNotification *notification = [NSNotification notificationWithName:NOTIFICATION_CHANGED_FAVS_FROM_SIDE_MENU object:nil userInfo:@{@"category":self.category,@"selected":@"yes"}];
//        [[NSNotificationCenter defaultCenter] postNotification:notification];
//        [self showTipForAction:TIP_ACTION_FAV_CONTENT];
//    }
}

#pragma mark --
#pragma mark - PST Collection View implementation
#pragma mark --

-(PSTCollectionViewLayout *) flowLayout {
    PSTCollectionViewFlowLayout *flowLayout =[[PSTCollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:PSTCollectionViewScrollDirectionVertical];
    //    [collectionViewFlowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [flowLayout setItemSize:CGSizeMake(100, 120)];
    //    [flowLayout setHeaderReferenceSize:CGSizeMake(self.view.bounds.size.width, 30)];
    //    [collectionViewFlowLayout setFooterReferenceSize:CGSizeMake(500, 50)];
    [flowLayout setMinimumInteritemSpacing:TILE_MARGIN];
    [flowLayout setMinimumLineSpacing:TILE_MARGIN];
    [flowLayout setSectionInset:UIEdgeInsetsMake(TILE_MARGIN, TILE_MARGIN, TILE_MARGIN, TILE_MARGIN)];
    return flowLayout;
}

-(void) setupCollectionView {

    self.collectionView = [[PSTCollectionView alloc] initWithFrame:self.contentView.bounds collectionViewLayout:[self flowLayout]];
    
    [_collectionView setDelegate:self];
    [_collectionView setDataSource:self];
    [_collectionView setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    [_collectionView setBackgroundColor:GRAY_WITH_PERCENT(15)];
    
    [_collectionView registerNib:[UINib nibWithNibName:@"ChannelView" bundle:nil] forCellWithReuseIdentifier:CHANNEL_CELL_REUSE_ID];
    [_collectionView registerNib:[UINib nibWithNibName:@"CategoryViewCell" bundle:nil] forCellWithReuseIdentifier:CATEGORY_CELL_REUSE_ID];
    [_collectionView registerNib:[UINib nibWithNibName:@"ContentViewCell" bundle:nil] forCellWithReuseIdentifier:CONTENT_CELL_REUSE_ID];
    [_collectionView registerNib:[UINib nibWithNibName:@"TileShowView" bundle:nil] forCellWithReuseIdentifier:SHOW_CELL_ID];
    [_collectionView registerClass:[PSTCollectionReusableView class] forSupplementaryViewOfKind:PSTCollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    
    [self.contentView addSubview:self.collectionView];
}

//-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
//    self.collectionView.collectionViewLayout = [self flowLayout];
//    [self.collectionView reloadData];
//}

#pragma mark - PST Collection view data source

- (UIEdgeInsets)collectionView:(PSTCollectionView *)collectionView
                        layout:(PSTCollectionViewLayout*)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
    
    if (section == 0 && self.filteredUserChannels.count == 0) {
        return UIEdgeInsetsZero;
    }
    
    if (section == 1 && self.filteredChannels.count == 0) {
        return UIEdgeInsetsZero;
    }
    
    if (section == 2 && self.filteredSubCategories.count == 0) {
        return UIEdgeInsetsZero;
    }
    
    if (section == 3 && self.filteredContents.count == 0) {
        return UIEdgeInsetsZero;
    }
    
    return UIEdgeInsetsMake(TILE_MARGIN, TILE_MARGIN, TILE_MARGIN, TILE_MARGIN);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    if (section == 0 && self.filteredUserChannels.count == 0) {
        return CGSizeZero;
    }
    
    if (section == 1 && self.filteredChannels.count == 0) {
        return CGSizeZero;
    }
    
    if (section == 2 && self.filteredSubCategories.count == 0) {
        return CGSizeZero;
    }
    
    if (section == 3 && self.filteredContents.count == 0) {
        return CGSizeZero;
    }
    
    return CGSizeMake(self.view.bounds.size.width, 30);
}

- (PSTCollectionReusableView *)collectionView:(PSTCollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    
    if (kind == PSTCollectionElementKindSectionHeader) {
        
        if (indexPath.section == 0 && self.filteredUserChannels.count == 0) {
            return nil;
        }
        
        if (indexPath.section == 1 && self.filteredChannels.count == 0) {
            return nil;
        }
        
        if (indexPath.section == 2 && self.filteredSubCategories.count == 0) {
            return nil;
        }
        
        if (indexPath.section == 3 && self.filteredContents.count == 0) {
            return nil;
        }
        
        
        PSTCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:PSTCollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        [headerView setFrame:CGRectMake(0, 0, self.view.bounds.size.width, 30)];
        [headerView setBackgroundColor:GRAY_WITH_PERCENT(20)];
        [headerView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
        
        UILabel *label = [[UILabel alloc] initWithFrame:headerView.bounds];
        label.backgroundColor = GRAY_WITH_PERCENT(30);
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14]];
        [label setTextColor:GRAY_WITH_PERCENT(80)];
        [label setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
        
        if (indexPath.section == 0) {
            [label setText:self.showingLocalFavs?@"User Added channels":@"User Channels"];
        } else if (indexPath.section == 1) {
            [label setText:self.showingLocalFavs?@"Favourite Channels":@"YouTube Channels"];
        } else if (indexPath.section == 2) {
            [label setText:self.showingLocalFavs?@"Favourite Categories":@"Categories"];
        } else  {
            [label setText:self.showingLocalFavs?@"Favourite Content":@"Content"];
        }
        
        [headerView addSubview:label];
        return headerView;
        
    }
    
    return nil;
}

- (CGSize)collectionView:(PSTCollectionView *)collectionView layout:(PSTCollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 || indexPath.section == 1) {
        return CGSizeMake(90, 120);
    } else if (indexPath.section == 2) {
        return CGSizeMake(self.view.bounds.size.width - 2*TILE_MARGIN, 44);
    } else return CGSizeMake(IS_IPAD?220:140,IS_IPAD?140:90);
}

- (NSInteger)collectionView:(PSTCollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section == 0) {
        return self.filteredUserChannels.count;
    }
    
    if (section == 1) {
        return self.filteredChannels.count;
    }
    
    if (section == 2) {
        return self.filteredSubCategories.count;
    }
    
    if (section == 3) {
        return self.filteredContents.count;
    }
    
    return 0;
}

-(NSInteger) numberOfSectionsInCollectionView:(PSTCollectionView *)collectionView {
    return 4;
}

- (PSTCollectionViewCell *)collectionView:(PSTCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        ChannelView *channelView = [collectionView dequeueReusableCellWithReuseIdentifier:CHANNEL_CELL_REUSE_ID forIndexPath:indexPath];
        UserChannel *channel = [self.filteredUserChannels objectAtIndex:indexPath.row];
        [channelView populateWithUserChannel:channel delegate:self];
        return channelView;
    } else if (indexPath.section == 1) {
        ChannelView *channelView = [collectionView dequeueReusableCellWithReuseIdentifier:CHANNEL_CELL_REUSE_ID forIndexPath:indexPath];
        YoutubeChannel *channel = [self.filteredChannels objectAtIndex:indexPath.row];
        [channelView populateWithChannel:channel delegate:self];
//        channelView.layer.cornerRadius = 5.0;
//        channelView.layer.masksToBounds = YES;
        return channelView;
    } else if (indexPath.section == 2) {
        CategoryViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CATEGORY_CELL_REUSE_ID forIndexPath:indexPath];
        ContentCategory *category = [self.filteredSubCategories objectAtIndex:indexPath.row];
        [cell populateWithCategory:category];
        cell.showingInMenu = NO;
//        if (self.showingLocalFavs) {
//            [cell hideChevronLabel];
//        }
        //        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else if (indexPath.section == 3) {
        
        TileShowView *showView = [collectionView dequeueReusableCellWithReuseIdentifier:SHOW_CELL_ID forIndexPath:indexPath];
        Content *content = self.filteredContents[indexPath.row];
        [showView populateWithContent:content];
        [showView setDelegate:self];
        showView.layer.shouldRasterize = YES;
        showView.layer.rasterizationScale = [UIScreen mainScreen].scale;
        return showView;
    }
    return nil;
}

#pragma mark - Collection view delegate

-(void) collectionView:(PSTCollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
//    if (self.showingLocalFavs) {
//        return;
//    }
    
    if ([self.searchTextField isFirstResponder]) {
        [self.searchTextField resignFirstResponder];
    }
    
    if (indexPath.section == 0) {
//        ChannelContainerVC *channelsVC = [[ChannelContainerVC alloc] initWithNibName:@"ChannelContainerVC" bundle:nil];
//        UserChannel *channel = [self.filteredUserChannels objectAtIndex:indexPath.row];
//        [channelsVC setUserChannel:channel];
//        [self.navigationController pushViewController:channelsVC animated:YES];
    }
    
    if (indexPath.section == 1) {
//        ChannelContainerVC *channelsVC = [[ChannelContainerVC alloc] initWithNibName:@"ChannelContainerVC" bundle:nil];
//        YoutubeChannel *channel = [self.filteredChannels objectAtIndex:indexPath.row];
//        [channelsVC setChannel:channel];
//        [self.navigationController pushViewController:channelsVC animated:YES];
    }
    
    if (indexPath.section == 2) {
        ContentCategory *category = [self.filteredSubCategories objectAtIndex:indexPath.row];
        BrowseViewController *catsVC = [[BrowseViewController alloc] initWithNibName:@"BrowseViewController" bundle:nil];
        [catsVC setCategory:category];
        [self.navigationController pushViewController:catsVC animated:YES];
    } else if (indexPath.section == 3) {
        // show all episodes of this content.
        
        Content *content = [self.filteredContents objectAtIndex:indexPath.row];
        
        ContentBrowserViewController *contentVC = [[ContentBrowserViewController alloc] initWithNibName:@"ContentBrowserViewController" bundle:nil];
        [contentVC setCategory:self.category];
        [contentVC setContent:content];
        [self.navigationController pushViewController:contentVC animated:YES];
    }
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if ([self.searchTextField isFirstResponder]) {
        [self.searchTextField resignFirstResponder];
    }
}

@end
