//
//  BrowserVC.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-02-04.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "BrowserVC.h"
#import "FlixPakClient.h"
#import "ME_ErrorHandler.h"
#import "ME_Logger.h"
#import "InterestsGridItem.h"
#import "BrowseInterestVC.h"

@interface BrowserVC ()

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSArray *allInterests;
@property (nonatomic, strong) BrowseInterestVC *browseInterestVC;

@end

@implementation BrowserVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
//    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    //    [self.collectionView registerClass:[InterestsGridItem class] forCellWithReuseIdentifier:@"cvCell"];
    UINib *cellNib = [UINib nibWithNibName:@"InterestsGridItem" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"cvCell"];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(280, 200)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [flowLayout setMinimumLineSpacing:20.0f];
    [flowLayout setMinimumInteritemSpacing:20.0f];
    [flowLayout setSectionInset:UIEdgeInsetsMake(20.0f, 20.0f, 20.0f, 20.0f)];
    [self.collectionView setCollectionViewLayout:flowLayout];
    self.collectionView.backgroundColor = self.view.backgroundColor;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.browseInterestVC = nil;
    self.navigationController.navigationBarHidden = NO;
    if (!self.allInterests) {
        [self loadInterests];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)loadInterests {
    [self showSoftWaitIndicatorWithMessage:@"Loading"];
    
    [[FlixPakClient singleton] getInterestsWithCompletion:^(NSDictionary *dictionary, NSError *error) {
        [self hideSoftWaitIndicator];
        if (error) {
            [self showInformativeError:error];
        } else if (!dictionary) {
            [self showInformativeErrorMessage:@"Empty response."];
            [[ME_Logger singleton] log:@"Empty GET Interests response." level:ME_Log_Level_ERROR];
        } else {
            _allInterests = dictionary[@"data"];
            [self reloadCollectionView];
        }
    }];
    // add route to API to send interests.
    // defaults and all interests.
}

- (void)reloadCollectionView {
    // show all interests.
    [self.collectionView reloadData];
}

// TODO: when API Route added to send defaults, update this. till then this button picks all.
- (IBAction)pickDefaultsAction:(id)sender {
    
}

#pragma mark - Collection View Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.allInterests.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cvCell";
    InterestsGridItem *cell = (InterestsGridItem *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    NSDictionary *interest = self.allInterests[indexPath.row];
    [cell setInterestTitle:interest[@"title"] subtitle:interest[@"subtitle"] categoryId:interest[@"_id"]];
    cell.backgroundColor = [UIColor colorWithWhite:0.2f alpha:1.0f];
    [cell setDeSelectedInterest];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
//    InterestsGridItem *selectedItem = [self collectionView:self.collectionView cellForItemAtIndexPath:indexPath];
    NSDictionary *interest = self.allInterests[indexPath.row];
//    if (![self.selectedInterests containsObject:interest]) {
//        [self.selectedInterests addObject:interest];
//        [selectedItem setSelectedInterest];
//    } else {
//        [self.selectedInterests removeObject:interest];
//        [selectedItem setDeSelectedInterest];
//    }
//    [collectionView reloadData];
    // browse this interest.
    self.browseInterestVC = [[BrowseInterestVC alloc] initWithNibName:@"BrowseInterestVC" bundle:nil];
    [self.browseInterestVC setSelectedCategory:interest];
    [self.browseInterestVC.navigationItem setTitle:interest[@"title"]];
    [self.navigationController pushViewController:self.browseInterestVC animated:YES];
}

@end
