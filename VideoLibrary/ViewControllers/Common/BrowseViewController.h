//
//  ChooseFavsViewController.h
//  PakTV
//
//  Created by Malik Khalid Ejaz on 11/16/2013.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import "RootViewController.h"
#import "ContentViewCell.h"
#import "ChannelView.h"
#import "TileShowView.h"
#import <PSTCollectionView.h>

@class ContentCategory;

@interface BrowseViewController : RootViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIScrollViewDelegate, ChannelViewDelegate, ContentViewCellDelegate, TileShowViewDelegate, PSTCollectionViewDataSource, PSTCollectionViewDelegate>

@property (nonatomic, strong) NSString *selectedCategoryId;
@property (nonatomic, strong) ContentCategory *category;

@property (nonatomic, strong) NSString *searchText;

@property (nonatomic, assign) BOOL showingLocalFavs;
@property (nonatomic, assign) BOOL isInSidePanel;

@end
