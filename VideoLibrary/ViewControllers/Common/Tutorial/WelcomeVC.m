//
//  WelcomeVC.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/26/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "WelcomeVC.h"
#import "WelcomeNewCategorisVC.h"
#import "UserManager.h"
#import "DataManager.h"

@interface WelcomeVC ()

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *updatedLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIActivityIndicatorView *busyWheel;

@property (nonatomic, assign) BOOL wasUpdated;

@end

@implementation WelcomeVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self detectUpdateOrNewInstall];
    
    [self checkForInternetConnectionAndProceed];
}

-(void) detectUpdateOrNewInstall {
    // if update
    self.wasUpdated = [[UserManager sharedManager] hasPreviousVersionFavs];
    self.updatedLabel.hidden = !self.wasUpdated;
}

-(void) checkForInternetConnectionAndProceed {
    // if connected
    [self showInternetNeededIfNotAvailable];
    [self markPreviousAppCategoriesAsFavourite];
}

-(void) markPreviousAppCategoriesAsFavourite {
//    [self readCategoriesWithParentCategoryId:@"root" completion:^(NSArray *objects, NSError *error) {
//        if (error) {
//            [self showError:error];
//        } else {
//            if (objects.count == 0) {
//                [self showNetworkError];
//            } else {
//                for (ContentCategory *category in objects) {
//                    if ([category.categoryName isEqualToString:@"Pakistani TV Shows"] || [category.categoryName isEqualToString:@"Pakistani TV Dramas"]) {
//                        
//                        [category saveAsFavourite];
//                    }
//                }
//                if (self.wasUpdated) {
//                    [self saveFavouritesFromPreviousAppSettings];
//                } else {
//                    [self proceedToTutorial];
//                }
//            }
//        }
//    }];
}

-(void) saveFavouritesFromPreviousAppSettings {
    
    [self performSelector:@selector(proceedToTutorial) withObject:nil afterDelay:2];
}

-(void) proceedToTutorial {
    [self.busyWheel stopAnimating];
    
    WelcomeNewCategorisVC *tutorialVC = [[WelcomeNewCategorisVC alloc] initWithNibName:@"WelcomeNewCategorisVC" bundle:nil];
    [self.navigationController pushViewController:tutorialVC animated:YES];
}

@end
