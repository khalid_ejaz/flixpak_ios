//
//  WelcomeNewCategorisVC.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/26/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "WelcomeNewCategorisVC.h"
#import "DataManager.h"
#import "Utility.h"

#define INSTRUCTIONS_PATH @"http://www.anmaple.com/VideoLibrary/tutorial/instructions.plist"

@interface WelcomeNewCategorisVC ()

@property (nonatomic, strong) NSMutableArray *localInstructions;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *label;

@property (unsafe_unretained, nonatomic) IBOutlet UIButton *nextButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *previousButton;
@property (nonatomic, assign) NSInteger index;

@end

@implementation WelcomeNewCategorisVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupLocalInstructions];

    self.index = -1;
    [self nextAction:nil];
}

-(void) setupLocalInstructions {
    self.localInstructions = [NSMutableArray array];
    
    [self.localInstructions addObject:@"All videos are sorted in categories"];
    [self.localInstructions addObject:@"e.g. you will find Pakistani political talk shows in the category 'Pakistani TV Shows'. Easy, right ?"];
    [self.localInstructions addObject:@"If a category interests you, mark it as favourite. A tab will be added to home screen and all updates to this category will appear in that tab."];
    [self.localInstructions addObject:@"You can browse through all categories to see older videos or to choose more favourites."];
    [self.localInstructions addObject:@"While favourited categories appear as tabs on home screen, facvourited content appears under the section 'Your Favourites' on home screen."];
    [self.localInstructions addObject:@"You can go to side menu and choose 'List favorites' to see what you have as favourite"];
    [self.localInstructions addObject:@"And finally, you can tag videos to 'watch later'. Like some thing? tag it and watch later without having to search for it again."];
    [self.localInstructions addObject:@"Proceed to app."];
    
    
}

-(void) resetLabelFrameAtRight {
    CGRect frame = self.label.frame;
    frame.origin.x = self.view.frame.size.width;
    self.label.frame = frame;
}

-(void) resetLabelFrameAtLeft {
    CGRect frame = self.label.frame;
    frame.origin.x = self.view.frame.origin.x - self.label.frame.size.width;
    self.label.frame = frame;
}


-(void) removeLabelWithCompletion:(voidBlock)completion {
    CGRect frame = self.label.frame;
    frame.origin.x = self.view.frame.origin.x - frame.size.width;
    [self setLabelFrame:frame withCompletion:^{
        completion();
    }];
}

-(void) removeLabelToRightWithCompletion:(voidBlock)completion {
    CGRect frame = self.label.frame;
    frame.origin.x = self.view.frame.size.width;
    [self setLabelFrame:frame withCompletion:^{
        completion();
    }];
}

-(void) showLabel {
    CGRect frame = self.label.frame;
    frame.origin.x = 20;
    [self setLabelFrame:frame withCompletion:nil];
}

-(void) setLabelFrame:(CGRect)frame withCompletion:(voidBlock)completion {
    [UIView animateWithDuration:0.3 animations:^{
        self.label.frame = frame;
    } completion:^(BOOL finished) {
        if (completion) {
            completion();
        }
    }];
}

-(void) proceedWithNormalAppFlow {
    NSNotification *notification = [NSNotification notificationWithName:NOTIFICATION_TUTORIAL_FINISHED object:nil];
    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

#pragma mark - IBActions

- (IBAction)skipAction:(id)sender {
    [self proceedWithNormalAppFlow];
}

- (IBAction)nextAction:(id)sender {
    self.index++;
    if (self.index >= self.localInstructions.count) {
        self.index --;
        [self proceedWithNormalAppFlow];
    } else {
        if (self.index > 0) {
            self.previousButton.enabled = YES;
        }
        
        NSString *instruction = [self.localInstructions objectAtIndex:self.index];
        [self removeLabelWithCompletion:^{
            self.label.text = instruction;
            [self resetLabelFrameAtRight];
            [self showLabel];
        }];
    }
}

- (IBAction)previousAction:(id)sender {
    
    if (self.index == 0) {
        return;
    }
    
    self.index--;
    if (self.index < 1) {
        self.previousButton.enabled = NO;
    } else self.previousButton.enabled = YES;
    
    NSString *instruction = [self.localInstructions objectAtIndex:self.index];
    [self removeLabelToRightWithCompletion:^{
        self.label.text = instruction;
        [self resetLabelFrameAtLeft];
        [self showLabel];
    }];
}



@end
