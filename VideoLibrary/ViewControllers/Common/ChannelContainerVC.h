//
//  ChannelContainerVC.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/1/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "RootViewController.h"

@class UserChannel;
@interface ChannelContainerVC : RootViewController

@property (nonatomic, strong) UserChannel *channel;
//@property (nonatomic, strong) UserChannel *userChannel;
@property (nonatomic, strong) NSString *channelId;
@property (nonatomic, strong) NSString *channelTitle;

@end
