//
//  ContainerViewController.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2013-04-20.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "ContainerViewController.h"
#import "DataManager.h"
#import "ShowView.h"
#import "Utility.h"
//#import "ShowsCell.h"

#define SCROLL_EDGE_INSET   20


@interface ContainerViewController ()

@property (nonatomic, assign) BOOL hasLoaded;
@property (nonatomic, strong) NSArray *showsArray;
@property (nonatomic, assign) BOOL favsOnly;
@property (nonatomic, assign) BOOL showingEpisodes;

@end

@implementation ContainerViewController

-(id) init {
    self = [super init];
    if(self) {
        _showViews = [NSMutableArray array];
        
        self.contentTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        self.contentTable.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.contentTable.delegate = self;
        self.contentTable.dataSource = self;
        
        _showViewSize = CGSizeMake(0, 0);
    }
    return self;
}

-(BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return YES;
}

-(BOOL) shouldAutorotate {
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [self addBackgroundImageView];
//    
//    [self.contentTable setFrame:self.view.bounds];
//    [self.view addSubview:self.contentTable];
//    
//    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.contentTable.frame.size.width, [Utility gap])];
//    self.contentTable.tableFooterView = footerView;
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
//    [self.contentTable setFrame:self.view.bounds];
//    if(self.hasLoaded) {
//        [self calculateShowViewDimensions];
//        
////        [self.contentTable reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
//        [self.contentTable reloadData];
//        NSLog(@"reloaded.");
//    }
}

-(void) addBackgroundImageView {
    self.contentTable.backgroundColor = [UIColor clearColor];
}

-(void) setHeaderView:(UIView *)headerView {
    _headerView = headerView;
    self.contentTable.tableHeaderView = headerView;
}

-(void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self.contentTable setFrame:self.view.bounds];
}

-(void) calculateShowViewDimensions {
//    CGFloat cols = [Utility numberOfColumnsForPortrait:[self isPortrait] forSmallerScreen:[self isSmallerScreen]];
//    CGFloat sizeX;
//    CGFloat sizeY;
//    int gap = [Utility gap];
//        
//    CGFloat totalGap = gap * (cols + 1);
//    CGFloat totalWidth = self.view.bounds.size.width;
//    sizeX = (totalWidth - totalGap)/(cols);
//    sizeY = sizeX / ratio;
//    _showViewSize = CGSizeMake((int)sizeX, (int)(sizeY+gap));
}

-(void) setupShowViewsForAllEpisodes:(NSArray *)episodeArray inShow:(NSString *) showName {
    
//    TVShow *show = [[DataManager sharedInstance] getShowWithShowName:showName];
//    if(!show) {
//        NSLog(@"An error occured when reading for show with Name: %@",showName);
//        return;
//    }
//    
//    [self calculateShowViewDimensions];
//    
//    [_showViews removeAllObjects];
//    for(NSString *episode in episodeArray) {
//        ShowView *showView = [[[NSBundle mainBundle] loadNibNamed:@"ShowView" owner:self options:nil] objectAtIndex:0];
//        [showView injectDataFromShow:show forEpisode:episode];
//        [showView setDelegate:self.delegate];
//        [_showViews addObject:showView];
//    }
    
//    [self.contentTable reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
    [self.contentTable reloadData];
    [self setHasLoaded:YES];
}

-(void) setupShowViewsWithNameWise:(BOOL) nameWise FavOnly:(BOOL) favOnly{
    [self calculateShowViewDimensions];
//    NSArray *showsArray = nameWise?[[DataManager sharedManager] nameWiseSortedShows]:[[DataManager sharedManager] dateWiseSortedShows];
//    self.showsArray = showsArray;
//    self.favsOnly = favOnly;
//    [_showViews removeAllObjects];
//    for(NSString *showName in showsArray) {
//        TVShow *show = [[DataManager sharedInstance] getShowWithShowName:showName];
//        if(show) {
//            if(favOnly) {
//                if([[DataManager sharedInstance] isShowFavourite:showName]) {
//                    ShowView *showView = [[[NSBundle mainBundle] loadNibNamed:@"ShowView" owner:self options:nil] objectAtIndex:0];
//                    [showView injectDataFromShow:show];
//                    [showView setDelegate:self.delegate];
//                    [_showViews addObject:showView];
//                }
//            } else {
//                ShowView *showView = [[[NSBundle mainBundle] loadNibNamed:@"ShowView" owner:self options:nil] objectAtIndex:0];
//                [showView injectDataFromShow:show];
//                [showView setDelegate:self.delegate];
//                [_showViews addObject:showView];
//            }
//        }
//    }
//    [self.contentTable reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
    [self.contentTable reloadData];
    [self setHasLoaded:YES];
}

-(void) setupShowViewsWithArray:(NSArray *) showsArray {
//    [self calculateShowViewDimensions];
//
//    [_showViews removeAllObjects];
//    for(NSString *showName in showsArray) {
//        TVShow *show = [[DataManager sharedInstance] getShowWithShowName:showName];
//        if(show) {
//            ShowView *showView = [[[NSBundle mainBundle] loadNibNamed:@"ShowView" owner:self options:nil] objectAtIndex:0];
//            [showView injectDataFromShow:show];
//            [showView setDelegate:self.delegate];
//            [_showViews addObject:showView];
//        }
//    }
    
////    [self.contentTable reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
//    [self.contentTable reloadData];
//    [self setHasLoaded:YES];
}

-(BOOL) isPortrait {
    return UIDeviceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation]);
}

-(BOOL) isSmallerScreen {
    if(self.view.bounds.size.width == 480)
        return YES;
    return NO;
}

-(void) reloadData {
    if(self.contentTable) {
        [self calculateShowViewDimensions];
        
//        [self.contentTable reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
        [self.contentTable reloadData];
    }
}

#pragma mark - TableView Data Source and Delegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    int totalShows = self.showsArray.count; //[_showViews count];
//    int cols = [Utility numberOfColumnsForPortrait:[self isPortrait] forSmallerScreen:[self isSmallerScreen]];
//    int rows = totalShows/cols;
//    if (totalShows%cols != 0) {
//        rows++;
//    }
//    return rows;
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    ShowsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];
//    if (cell == nil) {
//        cell = [[ShowsCell alloc] initWithStyle:UITableViewCellStyleDefault
//                                reuseIdentifier:@"CellIdentifier@"];
//    }
//
//    int itemsAllowedInARow = [Utility numberOfColumnsForPortrait:[self isPortrait] forSmallerScreen:[self isSmallerScreen]];
//    int arrayStart = indexPath.row * itemsAllowedInARow;
//    int arrayEnd = arrayStart + itemsAllowedInARow;
//  
//    int len = [Utility numberOfColumnsForPortrait:[self isPortrait] forSmallerScreen:[self isSmallerScreen]];
//    if(arrayEnd > [self.showsArray count]) {
//        len = [self.showsArray count] - arrayStart;
//    }
//    
//    NSArray *subArray = [self.showsArray subarrayWithRange:NSMakeRange(arrayStart, len)];
//
//    NSMutableArray *showViews = [NSMutableArray array];
//    for (int i=0; i<subArray.count; i++) {
//        NSString *showName = [subArray objectAtIndex:i];
//        TVShow *show = [[DataManager sharedInstance] getShowWithShowName:showName];
//        if(show) {
//            if(self.favsOnly) {
//                if([[DataManager sharedInstance] isShowFavourite:showName]) {
//                    ShowView *showView = [[[NSBundle mainBundle] loadNibNamed:@"ShowView" owner:self options:nil] objectAtIndex:0];
//                    [showView injectDataFromShow:show];
//                    [showView setDelegate:self.delegate];
//                    [showViews addObject:showView];
//                }
//            } else {
//                ShowView *showView = [[[NSBundle mainBundle] loadNibNamed:@"ShowView" owner:self options:nil] objectAtIndex:0];
//                [showView injectDataFromShow:show];
//                [showView setDelegate:self.delegate];
//                [showViews addObject:showView];
//            }
//        }
//    }
//    
//    [cell updateShowViewCellWithShowViewsArray:showViews ShowViewDimensions:_showViewSize];
    
//    TVShow *show = [[DataManager sharedInstance] getShowWithShowName:showName];
//    if(show) {
//        if(favOnly) {
//            if([[DataManager sharedInstance] isShowFavourite:showName]) {
//                ShowView *showView = [[[NSBundle mainBundle] loadNibNamed:@"ShowView" owner:self options:nil] objectAtIndex:0];
//                [showView injectDataFromShow:show];
//                [showView setDelegate:self.delegate];
//                [_showViews addObject:showView];
//            }
//        } else {
//            ShowView *showView = [[[NSBundle mainBundle] loadNibNamed:@"ShowView" owner:self options:nil] objectAtIndex:0];
//            [showView injectDataFromShow:show];
//            [showView setDelegate:self.delegate];
//            [_showViews addObject:showView];
//        }
//    }
    
//    return cell;
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return _showViewSize.height;
}

#pragma mark - Scrollview delegate

-(void) scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([self.delegate respondsToSelector:@selector(scrollViewDidScroll:)]) {
        [self.delegate scrollViewDidScroll:scrollView];
    }
}

@end
