//
//  PublisherCategoryVC.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-02-02.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "AppRootViewController.h"

@interface PublisherCategoryVC : AppRootViewController <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>

- (void)setCode:(NSString *)code videoUrl:(NSString *)url channelId:(NSString *)channelId channelTitle:(NSString *)title;

@end
