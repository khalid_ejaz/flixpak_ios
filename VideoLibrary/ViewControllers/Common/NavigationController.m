//
//  NavigationController.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-18.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "NavigationController.h"
#import "SideMenuCell.h"
#import "ME_ConfigManager.h"
#import "MainVC.h"
#import "InterestsVC.h"
#import "BrowserVC.h"
#import "SettingsVC.h"
#import "AddUserChannelVC.h"
#import "YoutubeSearchVC.h"
#import "ME_AdManager.h"
#import "ME_Logger.h"

typedef enum {
    AppSpecificMenuOptionTVShows,
    AppSpecificMenuOptionTVDramas,
    AppSpecificMenuOptionInterests,
    AppSpecificMenuOptionsTotal
}AppSpecificMenuOption;

#define SECTION_HEADER_HEIGHT           8

#define CELL_HEIGHT                     44
#define CATEGORY_CELL_HEIGHT            40
#define MENU_WIDTH                      240

#define MENU_CELL_ID                    @"menuCellId"
#define CATEGORIES_CELL_ID              @"categoriesCellId"

#define SIDE_MENU_TEXT_SHARE            @"Share"
#define SIDE_MENU_TEXT_RATE_OR_REVIEW   @"Rate / Write a review"
#define SIDE_MENU_TEXT_CONTACT_SUPPORT  @"Contact Support"
#define SIDE_MENU_TEXT_MORE_APPS        @"More Apps"
#define SIDE_MENU_TEXT_ABOUT            @"About"

#define SIDE_MENU_TEXT_HOME             @"Home"
#define SIDE_MENU_TEXT_INTERESTS        @"Choose Interests"
#define SIDE_MENU_TEXT_BROWSE           @"Browse"
#define SIDE_MENU_TEXT_SEARCH_YOUTUBE   @"Search Youtube"
#define SIDE_MENU_TEXT_PUBLISH          @"Promote your channel"
#define SIDE_MENU_TEXT_SHARE_VIDEO      @"Share a video"
#define SIDE_MENU_TEXT_SETTINGS         @"Settings"

// Share sub options.
#define SIDE_MENU_TEXT_SHARE_TELL_A_FRIEND          @"Tell a friend"
#define SIDE_MENU_TEXT_SHARE_SHARE_ON_TWITTER       @"Share on twitter"
#define SIDE_MENU_TEXT_SHARE_SHARE_ON_FACEBOOK      @"Share on facebook"

@interface NavigationController ()

@property (nonatomic, strong) NSArray *sideMenuChoices;
@property (nonatomic, strong) UITableView *menu;

@property (nonatomic, assign) BOOL isMenuOpen;

@property (nonatomic, strong) NSArray *defaultMenuOptions;
@property (nonatomic, strong) NSArray *defaultMenuSymbols;

@property (nonatomic, strong) NSArray *specialMenuOptions;
@property (nonatomic, strong) NSArray *specialMenuSymbols;

@property (nonatomic, strong) InterestsVC *interestsVC;
@property (nonatomic, strong) BrowserVC *browserVC;
@property (nonatomic, strong) SettingsVC *settingsVC;
@property (nonatomic, strong) AddUserChannelVC *addUserChannelVC;

@property (nonatomic, strong) UIButton *closeMenuButton;

@end

@implementation NavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationBar setBarTintColor:[UIColor colorWithRed:46.0f/256.0f green:46.0f/256.0f blue:46.0f/256.0f alpha:1]];
    
    self.menu = [[UITableView alloc] init];
    [self.view addSubview:self.menu];
    [self.menu setHidden:YES];
    [self.menu setDelegate:self];
    [self.menu setDataSource:self];
    
    self.isMenuOpen = FALSE;
    
    self.specialMenuOptions = @[SIDE_MENU_TEXT_HOME, SIDE_MENU_TEXT_BROWSE, SIDE_MENU_TEXT_INTERESTS, SIDE_MENU_TEXT_PUBLISH, SIDE_MENU_TEXT_SEARCH_YOUTUBE, SIDE_MENU_TEXT_SETTINGS];
    
    self.specialMenuSymbols = @[@"⌂", @"📑", @"✓", @"✪", @"🔍", @"🔧"];
    
    // client and iphone app
    self.defaultMenuOptions = @[@{SIDE_MENU_TEXT_SHARE: @[SIDE_MENU_TEXT_SHARE_TELL_A_FRIEND, SIDE_MENU_TEXT_SHARE_SHARE_ON_TWITTER, SIDE_MENU_TEXT_SHARE_SHARE_ON_FACEBOOK]}, SIDE_MENU_TEXT_RATE_OR_REVIEW, SIDE_MENU_TEXT_CONTACT_SUPPORT, SIDE_MENU_TEXT_ABOUT];
    //TODO: add more apps menu.
    
    self.defaultMenuSymbols = @[@"☍", @"✭", @"✉", @"i"];
    self.menu.backgroundColor = [UIColor colorWithWhite:0.2 alpha:1.0f];
    
    [self.menu registerNib:[UINib nibWithNibName:@"SideMenuCell" bundle:nil] forCellReuseIdentifier:MENU_CELL_ID];
    [self.menu registerNib:[UINib nibWithNibName:@"CategoryViewCell" bundle:nil] forCellReuseIdentifier:CATEGORIES_CELL_ID];
//    self.menu.separatorColor = [UIColor darkGrayColor];
    [self.menu setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

-(void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    CGRect sideMenuFrame = self.menu.frame;
    sideMenuFrame.size.height = self.view.bounds.size.height - sideMenuFrame.origin.y - [[ME_AdManager sharedManager] adHeight];
    sideMenuFrame.size.width = MENU_WIDTH;
    if (self.isMenuOpen) {
        sideMenuFrame.origin.x = 0;
    } else sideMenuFrame.origin.x = 0 - MENU_WIDTH;
    sideMenuFrame.origin.y = 20;
    [self.menu setFrame:sideMenuFrame];
    
    if (self.closeMenuButton) {
        self.closeMenuButton.frame = [self closeMenuButtonRect];
    }
}

#pragma mark - Side menu open/close/choose

- (void)toggleMenu {
    if (self.isMenuOpen) [self hideMenuWithCompletion:nil];
    else [self showMenu];
}

- (void)showMenu {
    
    if (self.isMenuOpen) return;
    
    [self.menu reloadData];
    CGRect frame = self.menu.frame;
    frame.origin.x = 0 - MENU_WIDTH;
    self.menu.frame = frame;
    [self.view bringSubviewToFront:self.menu];
    [self.menu setHidden:NO];
    frame.origin.x = 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.menu.frame = frame;
    } completion:^(BOOL finished) {
        self.isMenuOpen = TRUE;
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = [UIColor clearColor];
        [button setTitle:@"" forState:UIControlStateNormal];
        button.frame = [self closeMenuButtonRect];
        [self.view addSubview:button];
        [button addTarget:self action:@selector(hideMenu) forControlEvents:UIControlEventTouchUpInside];
        self.closeMenuButton = button;
        
    }];
//    [self.view bringSubviewToFront:self.overlayView];
}

- (void)hideMenu {
    [self hideMenuWithCompletion:nil];
}

- (CGRect) closeMenuButtonRect {
    return CGRectMake(MENU_WIDTH, 0, self.view.frame.size.width - MENU_WIDTH, self.view.frame.size.height);
}

- (void)hideMenuWithCompletion:(Completion)completion {
    if (!self.isMenuOpen) return;
    self.isMenuOpen = FALSE;
    CGRect frame = self.menu.frame;
    frame.origin.x = 0;
    self.menu.frame = frame;
    
    frame.origin.x = 0 - MENU_WIDTH;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.menu.frame = frame;
    } completion:^(BOOL finished) {
        [self.closeMenuButton removeFromSuperview];
        self.closeMenuButton = nil;
        if (completion) completion();
    }];
//    [self.view sendSubviewToBack:self.overlayView];
}

- (void)sideMenuHomeSelected {
    [self popToRootViewControllerAnimated:YES];
}

- (void)sideMenuMyInterestsSelected {
    [self showChooseInterests];
}

- (void)sideMenuSearchYouTubeSelected {
    YoutubeSearchVC *searchVC = [[YoutubeSearchVC alloc] initWithNibName:@"YoutubeSearchVC" bundle:nil];
    [searchVC.navigationItem setTitle:@"Search YouTube"];
    [self pushViewController:searchVC animated:YES];
}

- (void)sideMenuSettingsSelected {
    self.settingsVC = [[SettingsVC alloc] initWithNibName:@"SettingsVC" bundle:nil];
    [self.settingsVC.navigationItem setTitle:@"Settings"];
    [self pushViewController:self.settingsVC animated:YES];
    UIViewController *vc = self.viewControllers[0];
    if (vc) {
        if ([vc isKindOfClass:[MainVC class]]) {
            [(MainVC *)vc resetLastUpdate];
        }
    }
}

- (void)sideMenuShareSelected:(CGRect)rect {
    NSURL *url = [NSURL URLWithString:[[ME_ConfigManager singleton] appUrl]];
    NSString *text = [NSString stringWithFormat:@"Check this great app: %@", [[ME_ConfigManager singleton] appName]];
    [self shareText:text andImage:nil andUrl:url rect:rect];
}

- (void)sideMenuPublishSelected {
    [[ME_Logger singleton] log:@"MENU - promote your channel selected."];
//    self.publishVC = [[PublishVC alloc] initWithNibName:@"PublishVC" bundle:nil];
//    [self.publishVC.navigationItem setTitle:@"Have a channel?"];
//    [self pushViewController:self.publishVC animated:YES];
    self.addUserChannelVC = [[AddUserChannelVC alloc] initWithNibName:@"AddUserChannelVC" bundle:nil];
    [self.addUserChannelVC.navigationItem setTitle:@"Promote your channel"];
    [self.addUserChannelVC setShowingOnHome:YES];
    [self pushViewController:self.addUserChannelVC animated:YES];
}

- (void)sideMenuShareVideoSelected {
    [[ME_Logger singleton] log:@"MENU - share video selected."];
    //    self.publishVC = [[PublishVC alloc] initWithNibName:@"PublishVC" bundle:nil];
    //    [self.publishVC.navigationItem setTitle:@"Have a channel?"];
    //    [self pushViewController:self.publishVC animated:YES];
    self.addUserChannelVC = [[AddUserChannelVC alloc] initWithNibName:@"AddUserChannelVC" bundle:nil];
    [self.addUserChannelVC.navigationItem setTitle:@"Share a single video"];
    [self.addUserChannelVC setShowingOnHome:YES];
    [self pushViewController:self.addUserChannelVC animated:YES];
}

- (void)sideMenuBrowseSelected {
    self.browserVC = [[BrowserVC alloc] initWithNibName:@"BrowserVC" bundle:nil];
    [self.browserVC.navigationItem setTitle:@"Browse"];
    [self pushViewController:self.browserVC animated:YES];
}

- (void)sideMenuMoreAppsSelected {
    
}

- (void)showChooseInterests {
    self.interestsVC = [[InterestsVC alloc] initWithNibName:@"InterestsVC" bundle:nil];
    [self.interestsVC.navigationItem setTitle:@"What would you like to watch?"];
    [self pushViewController:self.interestsVC animated:YES];
    UIViewController *vc = self.viewControllers[0];
    if (vc) {
        if ([vc isKindOfClass:[MainVC class]]) {
            [(MainVC *)vc resetLastUpdate];
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.menu.bounds.size.width, SECTION_HEADER_HEIGHT)];
    
    header.backgroundColor = [UIColor colorWithWhite:0.1 alpha:1.0];
    return header;
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 0.01f;
    }
    return SECTION_HEADER_HEIGHT;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        return CATEGORY_CELL_HEIGHT;
    }
    return CELL_HEIGHT;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.specialMenuOptions.count;
    } else if (section == 1) {
        return self.defaultMenuOptions.count;
    }
    
    abort();
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        SideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:MENU_CELL_ID];
        NSString *menuText = @"";
        BOOL subOptions = FALSE;
        
        if ([[self.specialMenuOptions objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]) {
            menuText = [self.specialMenuOptions objectAtIndex:indexPath.row];
        } else {
            NSDictionary *menuOptionWithSubMenus = [self.specialMenuOptions objectAtIndex:indexPath.row];
            menuText = [[menuOptionWithSubMenus allKeys] objectAtIndex:0];
            subOptions = TRUE;
        }
        
        UIImage *menuArt = nil;
        
        cell.menuTextLabel.text = menuText;
        
        if (indexPath.row == AppSpecificMenuOptionsTotal) {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        } else {
            cell.menuArtView.image = menuArt;
        }
        cell.symbolLabel.text = [self.specialMenuSymbols objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.backgroundColor = [UIColor colorWithWhite:0.2f alpha:1.0f];
        
        return cell;
    }
    
    if (indexPath.section == 1) {
        SideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:MENU_CELL_ID];
        NSString *menuText = @"";
        BOOL subOptions = FALSE;
        
        if ([[self.defaultMenuOptions objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]) {
            menuText = [self.defaultMenuOptions objectAtIndex:indexPath.row];
        } else {
            NSDictionary *menuOptionWithSubMenus = [self.defaultMenuOptions objectAtIndex:indexPath.row];
            menuText = [[menuOptionWithSubMenus allKeys] objectAtIndex:0];
            subOptions = TRUE;
        }
        UIImage *menuArt = nil;
        
        cell.menuTextLabel.text = menuText;
        
        if (indexPath.row == AppSpecificMenuOptionsTotal) {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        } else {
            cell.menuArtView.image = menuArt;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.backgroundColor = [UIColor colorWithWhite:0.2f alpha:1.0f];
        cell.symbolLabel.text = [self.defaultMenuSymbols objectAtIndex:indexPath.row];
        return cell;
    }
    
    abort();
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            // home.
            [self hideMenuWithCompletion:^{
                [self sideMenuHomeSelected];
            }];
        } else if (indexPath.row == 1) {
            // browse.
            [self hideMenuWithCompletion:^{
                [self sideMenuBrowseSelected];
            }];
        }  else if (indexPath.row == 2) {
            // interests.
            [self hideMenuWithCompletion:^{
               [self sideMenuMyInterestsSelected];
            }];
        } else if (indexPath.row == 3) {
            // publish.
            [self hideMenuWithCompletion:^{
                [self sideMenuPublishSelected];
            }];
        }
//        else if (indexPath.row == 4) {
//            // share a video.
//            [self hideMenuWithCompletion:^{
//                [self sideMenuShareVideoSelected];
//            }];
//        }
        else if (indexPath.row == 4) {
            // search youtube.
          [self hideMenuWithCompletion:^{
              [self sideMenuSearchYouTubeSelected];
          }];
        } else if (indexPath.row == 5) {
            // settings.
            [self hideMenuWithCompletion:^{
               [self sideMenuSettingsSelected];
            }];
        }
    } else if (indexPath.section == 1) {
        // default menu options.
        if (indexPath.row == 0) {
            // share.
            [self hideMenuWithCompletion:^{
                CGRect rectOfCellInTableView = [tableView rectForRowAtIndexPath:indexPath];
                CGRect rectOfCellInSuperview = [tableView convertRect:rectOfCellInTableView toView:[[tableView superview] superview]];
                [self sideMenuShareSelected:rectOfCellInSuperview];
            }];
        } else if (indexPath.row == 1) {
            // rate the app.
            [self hideMenuWithCompletion:^{
                NSString* url = @"itms-apps://itunes.apple.com/app/id485585283";
                [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
            }];
        } else if (indexPath.row == 2) {
            [self hideMenuWithCompletion:^{
                //contact support.
                
                if ([MFMailComposeViewController canSendMail]) {
                    // Show the composer
                    // Email Subject
                    NSString *emailTitle = [[ME_ConfigManager singleton] appName];
                    // Email Content
                    NSString *messageBody = [NSString stringWithFormat:@"<br><br><br><br>Sent from <a href=%@>%@</a>", [[ME_ConfigManager singleton] appUrl] , [[ME_ConfigManager singleton] appName]];
                    // To address
                    NSArray *toRecipents = [NSArray arrayWithObject:[[ME_ConfigManager singleton] supportEmail]];
                    
                    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
                    mc.mailComposeDelegate = self;
                    [mc setSubject:emailTitle];
                    [mc setMessageBody:messageBody isHTML:YES];
                    [mc setToRecipients:toRecipents];
                    
                    // Present mail view controller on screen
                    [self presentViewController:mc animated:YES completion:NULL];
                } else {
                    // Handle the error
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Can not send email. No accounts setup." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }];
        }
//        else if (indexPath.row == 3) {
//            [self hideMenuWithCompletion:^{
//                // more apps.
//                [self sideMenuMoreAppsSelected];
//            }];
//        }
        else if (indexPath.row == 3) {
            [self hideMenuWithCompletion:^{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[ME_ConfigManager singleton] aboutApp] message:[NSString stringWithFormat:@"By %@",[[ME_ConfigManager singleton] developerName]] delegate:self cancelButtonTitle:@"great" otherButtonTitles:nil];
                [alert show];
            }];
        }
        return;
    }
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 1) {
        NSString* url;
        //        if (OS_VERSION >= 7) {
        url = @"itms-apps://itunes.apple.com/app/id524728157";
        //        } else {
        //            url = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=524728157";
        //        }
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    //    [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)shareText:(NSString *)text andImage:(UIImage *)image andUrl:(NSURL *)url rect:(CGRect)rect
{
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    if (text) {
        [sharingItems addObject:text];
    }
    if (image) {
        [sharingItems addObject:image];
    }
    if (url) {
        [sharingItems addObject:url];
    }
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    if ( [activityController respondsToSelector:@selector(popoverPresentationController)] ) {
        // iOS8
        activityController.popoverPresentationController.sourceView = self.view;
        activityController.popoverPresentationController.sourceRect = rect;
    }
    [self presentViewController:activityController animated:YES completion:nil];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *aTouch = [touches anyObject];
    CGPoint location = [aTouch locationInView:self.view];
    if (!CGRectContainsPoint(self.menu.frame, location)) {
        [self hideMenuWithCompletion:nil];
    }
}

@end
