//
//  InterestsVC.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-16.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "ME_ParentViewController.h"

@interface InterestsVC : ME_ParentViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@end
