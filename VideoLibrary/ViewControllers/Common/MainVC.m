//
//  MainVC.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-16.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "MainVC.h"
#import "FlixPakClient.h"
#import "ME_Logger.h"
#import "FP_Response.h"
#import "HoriScrollView.h"
#import "ME_PrefsManager.h"
#import "Utility.h"
#import "PlayVC.h"
#import "AddUserChannelVC.h"
#import "NavigationController.h"
#import "UserChannel.h"
#import "ChannelContainerVC.h"
#import "YoutubeVideo.h"

@interface MainVC ()

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) FP_Response *mainResponse;
@property (nonatomic, strong) IBOutlet HoriScrollView *horiScrollView;
@property (nonatomic, strong) NSMutableArray *tabs;
@property (nonatomic, strong) NSArray *interests;
@property (nonatomic, strong) NSNumber *selectedCategoryId;
@property (nonatomic, strong) SectionsTableViewController *sectionsVC;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (nonatomic, strong) PlayVC *playerVC;
@property (weak, nonatomic) IBOutlet UIView *tabSeparatorView;
@property (weak, nonatomic) IBOutlet UIView *shareInviteView;
@property (nonatomic, strong) NSArray *favChannels;

@property (nonatomic, strong) NSDate *lastUpdate;
@end

@implementation MainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self resetViews];
    self.lastUpdate = nil;
//    self.tabSeparatorView.backgroundColor = [UIColor colorWithWhite:0.5f alpha:1.0f];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTabs) name:@"reload-tabs" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadIfNeeded) name:@"reload-if-needed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playVideo:) name:@"NOTIFICATION_PLAY_VIDEO" object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.playerVC = nil;
    [self reloadIfNeeded];
}

- (void)reloadIfNeeded {
    if (self.lastUpdate) {
        NSDate *now = [NSDate date];
        NSTimeInterval interval = [now timeIntervalSinceDate:self.lastUpdate];
        if (interval > 300) {
            [self reloadData];
        }
    } else {
        [self reloadData];
    }
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self layout];
}

- (void)layout {
    CGRect frame = self.contentView.frame;
    frame.size.height = self.view.frame.size.height - frame.origin.y - [[ME_AdManager sharedManager] adHeight];
    frame.size.width = self.view.frame.size.width;
    self.contentView.frame = frame;
    [self.sectionsVC.view setFrame:self.contentView.bounds];
}

- (NSString *)screenName {
    return @"main";
}

- (void)resetLastUpdate {
    self.lastUpdate = nil;
}

- (void)resetViews {
    // setup tab view.
    self.shareInviteView.hidden = YES;
    _selectedCategoryId = nil;
    self.tabs = nil;
    self.tabs = [NSMutableArray array];
    [self.horiScrollView removeAllViews];
    self.horiScrollView.showsVerticalScrollIndicator = NO;
    self.horiScrollView.clipsToBounds = YES;
    self.horiScrollView.contentOffset = CGPointMake(0, 0);
    
    // setup grid view.
    UINib *cellNib = [UINib nibWithNibName:@"NibCell" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"cvCell"];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(200, 200)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.collectionView setCollectionViewLayout:flowLayout];
}

- (IBAction)shareAction:(id)sender {
    AddUserChannelVC *addUserChannelVC = [[AddUserChannelVC alloc] initWithNibName:@"AddUserChannelVC" bundle:nil];
    [addUserChannelVC.navigationItem setTitle:@"Promote your channel"];
    [addUserChannelVC setShowingOnHome:YES];
    [self.navigationController pushViewController:addUserChannelVC animated:YES];
}

- (IBAction)toggleMenu:(id)sender {
    NavigationController *navC = (NavigationController *)self.navigationController;
    [navC toggleMenu];
}

- (void)reloadTabs {
    // show tabs.
    self.tabs = [NSMutableArray array];
    [self.horiScrollView removeAllViews];
    NSNumber *lastTabId = [[ME_PrefsManager singleton] numberPrefForKey:@"lastOpenedTabId"];
    BOOL foundLastSelectedCategory = FALSE;
    
    for (int i=0; i<self.interests.count; i++) {
        NSDictionary *interest = self.interests[i];
        TabView *tabView = [[NSBundle mainBundle] loadNibNamed:@"TabView" owner:nil options:nil][0];
        [tabView setFrame:CGRectMake(0, 0, 160, 40)];
        [tabView setTitle:interest[@"title"] categoryId:interest[@"_id"]];
        [tabView setDelegate:self];
        [self.tabs addObject:tabView];
        [self.horiScrollView addView:tabView];
        [tabView setDeselected];
        
        // found last selected tab.
        NSNumber *interestId = interest[@"_id"];
        if ([interestId isEqualToNumber:lastTabId]) {
            foundLastSelectedCategory = TRUE;
            _selectedCategoryId = interestId;
            [self showSelectedTab:tabView];
        }
    }
    
    self.favChannels = [[ME_PrefsManager singleton] arrayPrefsForKey:@"fav-channels"];
    for (NSDictionary *channel in self.favChannels) {
        TabView *tabView = [[NSBundle mainBundle] loadNibNamed:@"TabView" owner:nil options:nil][0];
        [tabView setFrame:CGRectMake(0, 0, 160, 40)];
        [tabView setTitle:channel[@"channelName"] categoryId:channel[@"channelId"]];
        [tabView setDelegate:self];
        [self.tabs addObject:tabView];
        [self.horiScrollView addView:tabView];
        [tabView setDeselected];
    }
    if (!foundLastSelectedCategory) {
        _selectedCategoryId = self.interests[0][@"_id"];
        [(TabView *)self.tabs[0] setSelected];
    }
}

- (void)reloadData {
    self.shareInviteView.hidden = YES;
    _interests = [[ME_PrefsManager singleton] arrayPrefsForKey:@"fav-interests"];
    if (!self.interests) {
        // ask for interests.
        [self askForInterests];
    } else if (self.interests.count == 0) {
        [self askForInterests];
    } else {
        [self reloadTabs];
        
        // load first tab category.
        [self loadCategory:self.selectedCategoryId];
        self.lastUpdate = [NSDate date];
    }
}

- (void)askForInterests {
    NavigationController *navC = (NavigationController *)self.navigationController;
    [navC showChooseInterests];
}

- (void)loadCategory:(NSNumber *)categoryId {
    [self showBlockingWaitIndicatorWithMessage:@"Loading"];
    self.shareInviteView.hidden = YES;
    [[FlixPakClient singleton] getBrowseWithQueryParams:@{@"category_id":categoryId} completion:^(NSDictionary *dictionary, NSError *error) {
        if (dictionary) {
            [[ME_Logger singleton] log:[dictionary description]];
            [self showResponse:dictionary];
        } else if (error) {
            [[ME_Logger singleton] logError:error];
        } else [[ME_Logger singleton] log:@"Empty response for GET main." level:ME_Log_Level_WARN];
        [self hideBlockingWaitIndicator];
    }];
}

- (void)showResponse:(NSDictionary *)response {
    [self removeSectionVC];
//    [self layoutTableFrame];
//    [self addChildViewController:self.sectionsVC];
    NSDictionary *dict = response[@"data"];
    NSArray *sections = dict[@"sections"];
    if (sections && (sections.count > 0)) {
        self.sectionsVC = [[SectionsTableViewController alloc] initWithStyle:UITableViewStylePlain];
        self.sectionsVC.delegate = self;
        [self.contentView addSubview:self.sectionsVC.view];
        [self.sectionsVC.view setFrame:self.contentView.bounds];
        [self.sectionsVC setResponse:response];
    } else {
        self.shareInviteView.hidden = NO;
    }
}

- (void)removeSectionVC {
    [self.sectionsVC.view removeFromSuperview];
    [self.sectionsVC removeFromParentViewController];
    self.sectionsVC = nil;
}

#pragma mark - Tab View Delegate

- (void)tabView:(TabView *)tabView selectedCategory:(NSNumber *)categoryId {
    
    if ([categoryId isKindOfClass:[NSString class]]) {
        // user pinned youtube channel is opened.
        
        // find channel id and name.
        NSArray *favs = [[ME_PrefsManager singleton] arrayPrefsForKey:@"fav-channels"];
        for (NSDictionary *dict in favs) {
            if ([dict[@"channelId"] isEqualToString:(NSString *)categoryId]) {
                UserChannel *channel = [[UserChannel alloc] init];
                channel.channelId = dict[@"channelId"];
                channel.channelName = dict[@"channelName"];
                ChannelContainerVC *channelsVC = [[ChannelContainerVC alloc] initWithNibName:@"ChannelContainerVC" bundle:nil];
                [channelsVC setChannel:channel];
                [channelsVC.navigationItem setTitle:channel.channelName];
                [self.navigationController pushViewController:channelsVC animated:YES];
                return;
            }
        }
        
        // if not found, show error.
        [self showMessage:@"Could not open tab." title:@"Error"];
        
    } else {
        // app category is opened.
        _selectedCategoryId = categoryId;
        [[ME_PrefsManager singleton] saveNumberPrefs:categoryId forKey:@"lastOpenedTabId"];
        [self loadCategory:self.selectedCategoryId];
        
        for (TabView *tab in self.tabs) {
            if (tab == tabView) {
                [self showSelectedTab:tab];
            } else [tab setDeselected];
        }
    }
}

- (void)showSelectedTab:(TabView *)tab {
    [tab setSelected];
    [self.horiScrollView focusView:tab];
}

#pragma mark - Section VC delegate

- (void)sectionViewSelectedVideoAtIndex:(NSInteger)videoIndex section:(NSDictionary *)section {
    self.playerVC = [[PlayVC alloc] initWithNibName:@"PlayVC" bundle:nil];
    self.playerVC.view.frame = self.navigationController.view.bounds;
    [self.navigationController pushViewController:self.playerVC animated:YES];
    [self.playerVC playVideosInSection:section startingIndex:videoIndex];
}

- (void)playVideo:(NSNotification *)notif {
    YoutubeVideo *video = notif.userInfo[@"video"];
    PlayVC *player = [[PlayVC alloc] initWithNibName:@"PlayVC" bundle:nil];
    NSMutableDictionary *section = [NSMutableDictionary dictionary];
    [section setObject:@1 forKey:@"fromChannel"];
    [section setObject:@"heading" forKey:video.channelTitle];
    NSMutableDictionary *videosDict = [NSMutableDictionary dictionary];
    [videosDict setObject:video.title forKey:@"title"];
    [videosDict setObject:video.channelTitle forKey:@"subtitle"];
    [videosDict setObject:video.videoId forKey:@"vid"];
    NSArray *videos = @[videosDict];
    [section setObject:videos forKey:@"videos"];
    [player playVideosInSection:section startingIndex:0];
    [self.navigationController pushViewController:player animated:YES];
}

@end
