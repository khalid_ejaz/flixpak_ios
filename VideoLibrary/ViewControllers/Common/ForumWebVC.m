//
//  ForumWebVC.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/14/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "ForumWebVC.h"
#import "UserManager.h"
#import "Utility.h"

@interface ForumWebVC ()
@property (unsafe_unretained, nonatomic) IBOutlet UIView *headerView;
@property (unsafe_unretained, nonatomic) IBOutlet UIWebView *webView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *backButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIActivityIndicatorView *busyWheel;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;

@property (nonatomic, assign) NSInteger activeRequests;

@end

@implementation ForumWebVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self showInternetNeededIfNotAvailable];
    self.headerView.backgroundColor = [[UserManager sharedManager] colorForUser];
    self.activeRequests = 0;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    if (self.firstLoadDone) {
//        return;
//    } else self.firstLoadDone = YES;
//    
//    if (self.forum) {
//        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.forum.forumURL]]];
//    } else {
//        if (self.forumURL) {
//            self.backButton.hidden = YES;
//            if (OS_VERSION >= 7) {
//                
//                CGRect headerRect = self.headerView.frame;
//                headerRect.size.height -= 20;
//                self.headerView.frame = headerRect;
//                
//                CGRect webRect = self.webView.frame;
//                webRect.origin.y = self.headerView.bounds.size.height;
//                webRect.size.height = self.view.bounds.size.height - self.headerView.bounds.size.height;
//                self.webView.frame = webRect;
//            }
//            self.headerView.backgroundColor = [[UserManager sharedManager] darkerColorForUser];
//            [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.forumURL]]];
//        } else {
//            [self showGeneralError];
//        }
//    }
}

-(void) setActiveRequests:(NSInteger)activeRequests {
//    _activeRequests = activeRequests;
//    if (activeRequests > 0) {
//        [self.busyWheel startAnimating];
//    } else if (activeRequests == 0) {
//        [self.busyWheel stopAnimating];
//    } else {
//        NSLog(@"Less than zero active requests.");
//    }
//    NSLog(@"Active requests: %d", self.activeRequests);
}

#pragma mark - IBActions

-(void) showAddressField {
    CGRect rect = self.addressTextField.frame;
    rect.origin.y = 0 - self.addressTextField.bounds.size.height;
    self.addressTextField.frame = rect;
    
    rect.origin.y = self.headerView.bounds.size.height + 6;
    [UIView animateWithDuration:0.3 animations:^{
        self.addressTextField.frame = rect;
    } completion:^(BOOL finished) {
        [self.addressTextField becomeFirstResponder];
    }];
}

-(void) hideAddressField {
    CGRect rect = self.addressTextField.frame;
    rect.origin.y = 0 - self.addressTextField.bounds.size.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.addressTextField.frame = rect;
    }];
    
    [self.addressTextField resignFirstResponder];
}

- (IBAction)bookmarkAction:(id)sender {
    
}

- (IBAction)addAction:(id)sender {
    if (self.addressTextField.frame.origin.y == self.headerView.bounds.size.height + 6) {
        [self hideAddressField];
    } else {
        [self showAddressField];
    }
}

- (IBAction)reload:(id)sender {
    [self.webView reload];
}

- (IBAction)goBack:(id)sender {
    if ([self.webView canGoBack]) {
        [self.webView goBack];
    }
}

- (IBAction)goForward:(id)sender {
    if ([self.webView canGoForward]) {
        [self.webView goForward];
    }
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - web view delegate

-(void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    self.activeRequests--;
}

-(void) webViewDidStartLoad:(UIWebView *)webView {
    self.activeRequests++;
}

-(void) webViewDidFinishLoad:(UIWebView *)webView {
    self.activeRequests--;
}

#pragma mark - text field delegate

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    NSString *searchText = [Utility stringByRemovingWhiteSpaces:textField.text];
    if (searchText.length == 0) {
        return NO;
    }
    
    NSString *url;
    if ([searchText rangeOfString:@"."].location == NSNotFound) {
        url = [NSString stringWithFormat:@"http://www.google.com?q=%@",[searchText stringByReplacingOccurrencesOfString:@" " withString:@"+"]];
    } else url = [searchText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    return NO;
}

@end
