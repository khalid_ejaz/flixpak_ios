//
//  SettingsVC.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-18.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "SettingsVC.h"
#import "ME_PrefsManager.h"

@interface SettingsVC ()

@property (weak, nonatomic) IBOutlet UISwitch *wifiSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *cellularSwitch;

@end

@implementation SettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    // Do any additional setup after loading the view from its nib.
    [self updateViews];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)updateViews {
    NSNumber *autoPlayOnWiFi = [[ME_PrefsManager singleton] numberPrefForKey:@"autoplayOnWiFi"];
    NSNumber *autoPlayOnCelluar = [[ME_PrefsManager singleton] numberPrefForKey:@"autoplayOnCellular"];
    if ([autoPlayOnWiFi isEqual:@1]) {
        [self.wifiSwitch setOn:YES];
    } else [self.wifiSwitch setOn:NO];
    if ([autoPlayOnCelluar isEqual:@1]) {
        [self.cellularSwitch setOn:YES];
    } else [self.cellularSwitch setOn:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (IBAction)wifiSwitchChanged:(id)sender {
    NSNumber *num = @0;
    if (self.wifiSwitch.isOn) {
        num = @1;
    }
    [[ME_PrefsManager singleton] saveNumberPrefs:num forKey:@"autoplayOnWiFi"];
}

- (IBAction)cellularSwitchChanged:(id)sender {
    NSNumber *num = @0;
    if (self.cellularSwitch.isOn) {
        num = @1;
    }
    [[ME_PrefsManager singleton] saveNumberPrefs:num forKey:@"autoplayOnCellular"];
}

- (IBAction)resetSettingsAction:(id)sender {
    self.messageAction = @"reset";
    [self showMessage:@"This will delete all preferences including interests." title:@"Are you sure?" okButtonTitle:@"yes" cancelButtonTitle:@"cancel"];
}

- (void)messageViewClosedWithOK {
    if ([self.messageAction isEqualToString:@"reset"]) {
        [[ME_PrefsManager singleton] resetDefaults];
        [self updateViews];
    }
    [super messageViewClosedWithCancel];
}

- (void)messageViewClosedWithCancel {
    [super messageViewClosedWithCancel];
}

@end
