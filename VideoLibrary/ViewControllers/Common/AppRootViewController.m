//
//  AppRootViewController.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-18.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "AppRootViewController.h"
#import "NavigationController.h"
#import "ME_ConfigManager.h"

@interface AppRootViewController ()

@end

@implementation AppRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([self respondsToSelector:@selector(screenName)]) {
        NSString *screenName = [self screenName];
        if ([[ME_ConfigManager singleton] isAdsEnabled]) {
            if ([[ME_ConfigManager singleton] adsAllowedOnScreen:screenName]) {
                [[ME_AdManager sharedManager] showAdOnViewController:self atPosition:AD_POSITION_BOTTOM];
            }
        }
    }
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [[ME_AdManager sharedManager] layoutAdView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[ME_AdManager sharedManager] removeAds];
}

@end
