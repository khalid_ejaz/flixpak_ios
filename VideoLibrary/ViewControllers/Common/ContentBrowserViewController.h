//
//  ContentBrowserViewController.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/27/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "RootViewController.h"

@class Content, ContentCategory;

@interface ContentBrowserViewController : RootViewController

@property (nonatomic, strong) Content *content;
@property (nonatomic, strong) NSString *contentId;
@property (nonatomic, strong) ContentCategory *category;

@end
