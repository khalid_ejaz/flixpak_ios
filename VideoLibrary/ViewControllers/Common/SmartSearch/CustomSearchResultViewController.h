//
//  CustomSearchResultViewController.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2013-08-26.
//
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"

@interface CustomSearchResultViewController : RootViewController <UIWebViewDelegate>

@property (nonatomic, strong) NSString *searchString;
@property (nonatomic, strong) NSArray *recommendedLinks;
@property (nonatomic, strong) NSString *interestTitle;

@end
