//
//  CustomSearchResultViewController.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2013-08-26.
//
//

#import "CustomSearchResultViewController.h"
#import "ME_AdManager.h"
#import "DataManager.h"
#import "UserManager.h"

@interface CustomSearchResultViewController ()

@property (unsafe_unretained, nonatomic) IBOutlet UIView *headerView;
@property (unsafe_unretained, nonatomic) IBOutlet UIWebView *webView;
@property (unsafe_unretained, nonatomic) IBOutlet UISegmentedControl *searchSourceSwitcher;

@property (nonatomic, assign) int activeRequests;
@end

@implementation CustomSearchResultViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self loadWebView];
    
    self.headerView.backgroundColor = [[UserManager sharedManager] colorForUser];
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)viewDidUnload {
    [self setWebView:nil];
    [self setSearchSourceSwitcher:nil];
    [super viewDidUnload];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[ME_AdManager sharedManager] showAdOnViewController:self atPosition:AD_POSITION_BOTTOM];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[ME_AdManager sharedManager] removeAds];
}

-(void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [[ME_AdManager sharedManager] layoutAdView];
    
    CGRect webRect = self.webView.frame;
    webRect.size.height = self.view.bounds.size.height - self.headerView.bounds.size.height - [[ME_AdManager sharedManager] adHeight];
    [self.webView setFrame:webRect];
}

#pragma mark - show results.

-(void) loadWebView {
    NSString *urlString = [self youtubeSearchString];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)reloadAction:(id)sender {
    [self.webView reload];
}

- (IBAction)goForwardAction:(id)sender {
    if ([self.webView canGoForward]) {
        [self.webView goForward];
    }
}

- (IBAction)goBackAction:(id)sender {
    if ([self.webView canGoBack]) {
        [self.webView goBack];
    }
}

#pragma mark - web view delegate

-(void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
//    NSLog(@"Failed load: Error: %@",error);
//    if (error.code == -1009) {
//        [self showNetworkError];
//    }
    
    [self showInternetNeededIfNotAvailable];
}

# pragma mark - Autorotation related

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(BOOL)shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

#pragma mark - helper methods

-(NSString *) youtubeSearchString {
    NSString *searchText = [self.searchString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    return [NSString stringWithFormat:@"https://m.youtube.com/results?q=%@",searchText];
}

-(NSString *) dailymotionSearchString {
    NSString *searchText = [self.searchString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    return [NSString stringWithFormat:@"https://www.bing.com/search?q=dailymotion.com+%@",searchText];
}

@end
