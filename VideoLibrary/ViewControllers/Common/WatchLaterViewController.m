//
//  WatchLaterViewController.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/27/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "WatchLaterViewController.h"
#import "WatchLaterVC.h"

@interface WatchLaterViewController ()

@property (unsafe_unretained, nonatomic) IBOutlet UIView *contentView;
@property (nonatomic, strong) WatchLaterVC *watchLaterVC;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *headerView;
@end

@implementation WatchLaterViewController

-(void) viewDidLoad {
    [super viewDidLoad];
    self.headerView.backgroundColor = [[UserManager sharedManager] colorForUser];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.watchLaterVC = [[WatchLaterVC alloc] initWithNibName:@"ViewerVC" bundle:nil];
    [self.watchLaterVC.view setFrame:self.contentView.bounds];
    [self.contentView addSubview:self.watchLaterVC.view];
    [self addChildViewController:self.watchLaterVC];
    [self.watchLaterVC loadData];
}

#pragma mark - IBAction

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
