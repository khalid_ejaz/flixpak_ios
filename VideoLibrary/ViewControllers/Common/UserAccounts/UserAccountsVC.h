//
//  UserAccountsVC.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/29/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "RootViewController.h"
#import "UserCell.h"

@interface UserAccountsVC : RootViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIScrollViewDelegate, UserCellDelegate>

@property (nonatomic, assign) BOOL mustCreateUserName;

@end
