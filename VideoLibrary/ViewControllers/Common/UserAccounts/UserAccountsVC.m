//
//  UserAccountsVC.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/29/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "UserAccountsVC.h"
#import "UserManager.h"
#import <QuartzCore/QuartzCore.h>
#import "Utility.h"
#import "UserCell.h"

#define ENABLE_CHILD_ACCOUNTS   0

#define CELL_ID @"cellId"

#define ACTION_TYPE_LOGIN       @"login"
#define ACTION_TYPE_DELETE      @"delete"

@interface UserAccountsVC ()
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *selectedColorLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *but1;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *but2;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *but3;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *but4;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *but5;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *but6;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *but7;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *but8;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *but9;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *pinLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *childAccountCheckButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIScrollView *scrollView;

@property (unsafe_unretained, nonatomic) IBOutlet UIView *headerView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *addAccountButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *cancelButton;
@property (unsafe_unretained, nonatomic) IBOutlet UITextField *nameTextField;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *saveButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *backButton;
@property (unsafe_unretained, nonatomic) IBOutlet UITableView *usersTableView;
@property (unsafe_unretained, nonatomic) IBOutlet UITextField *pinTextField;
@property (unsafe_unretained, nonatomic) IBOutlet UITextField *repinTextField;

@property (unsafe_unretained, nonatomic) IBOutlet UIView *addAccountView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *accountsView;

// child account outlets.
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *childLabel1;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *childLabel2;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *childLabel3;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *childLabel4;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *childLabel5;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *childLabel6;


@property (nonatomic, strong) NSArray *colorButtons;
@property (nonatomic, strong) NSString *action;
@property (nonatomic, strong) NSMutableArray *namesForWhichColorChanged;

@property (nonatomic, strong) User *userToDelete;

@end

@implementation UserAccountsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    if (ENABLE_CHILD_ACCOUNTS) {
//        self.childAccountCheckButton.hidden = self.childLabel1.hidden = self.childLabel2.hidden = self.childLabel3.hidden = self.childLabel4.hidden = self.childLabel5.hidden = self.childLabel6.hidden = NO;
//    } else {
//        self.childAccountCheckButton.hidden = self.childLabel1.hidden = self.childLabel2.hidden = self.childLabel3.hidden = self.childLabel4.hidden = self.childLabel5.hidden = self.childLabel6.hidden = YES;
//    }
//    
//    self.action = nil;
//    
//    [self applyStyle];
//    
//    self.accountsView.layer.cornerRadius = 10.0f;
//    self.scrollView.layer.cornerRadius = 10.0f;
//    self.addAccountView.layer.cornerRadius = 10.0f;
//    
//    [self.scrollView setContentSize:self.addAccountView.bounds.size];
//    self.scrollView.delegate = self;
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLogOut) name:NOTIFICATION_USER_LOG_OUT object:nil];
//    
//    [self.usersTableView registerNib:[UINib nibWithNibName:@"UserCell" bundle:nil] forCellReuseIdentifier:CELL_ID];
//    
//    [self.nameTextField addTarget:self action:@selector(searchTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
//    
//    self.headerView.backgroundColor = [[UserManager sharedManager] colorForUser];
//    
//    self.colorButtons = @[self.but1,self.but2,self.but3,self.but4,self.but5,self.but6,self.but7,self.but8,self.but9];
//    for (UIButton *button in self.colorButtons) {
//        UIColor *color = [[UserManager sharedManager] colorForNumber:button.tag];
//        [button setTitleColor:color forState:UIControlStateNormal];
////        [button setTitleColor:color forState:UIControlStateHighlighted];
////        [button setTitleColor:color forState:UIControlStateSelected];
////        [button setTitleColor:color forState:UIControlStateDisabled];
//
//        [button addTarget:self action:@selector(colorSelected:) forControlEvents:UIControlEventTouchUpInside];
//    }
}

-(void) colorSelected:(id)sender {
    UIButton *button =  (UIButton *)sender;
    button.selected = NO;
    int number = [button tag];
    self.selectedColorLabel.textColor = [[UserManager sharedManager] colorForNumber:number];
    [self changeHeaderToColor:self.selectedColorLabel.textColor];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.action = nil;
    self.namesForWhichColorChanged = [NSMutableArray array];
    [[UserManager sharedManager] logoutCurrentUserIfNeedsPinAccess];
    
    if (self.mustCreateUserName) {
        self.backButton.hidden = YES;
        self.cancelButton.hidden = YES;
    }
    if ([[[UserManager sharedManager] users] count] > 0) {
        [self loadAccounts];
    } else {
        [self showAddAccountIfNoUsers];
    }
}

-(void) loadAccounts {
    [self hideAddAccountView];
    [self.usersTableView reloadData];
}

-(void) hideAddAccountView {
    self.addAccountView.hidden = YES;
    self.scrollView.hidden = YES;
    self.accountsView.alpha = 0.0f;
    self.accountsView.hidden = NO;
    [UIView animateWithDuration:0.3 animations:^{
        self.accountsView.alpha = 1.0f;
    }];
    
    self.addAccountButton.hidden = NO;
    [self hideKeyboard];
}

-(void) showAddAccountIfNoUsers {
    if ([[UserManager sharedManager] users].count == 0) {
        
        self.backButton.hidden = YES;
        self.cancelButton.hidden = YES;
        self.mustCreateUserName = YES;
        [self addAccountAction:nil];
    }
}

#pragma mark - text field delegate and call backs

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    int maxAllowed = 4;
    if (textField == self.nameTextField) {
        NSCharacterSet *blockedCharacters = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
        if ([string rangeOfCharacterFromSet:blockedCharacters].location != NSNotFound) {
            return NO;
        }
        maxAllowed = 20;
    }
    return (newLength > maxAllowed) ? NO : YES;
}

-(void) searchTextFieldDidChange:(UITextField *)textField {
    if (textField == self.nameTextField) {
        NSString *name = [self.nameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if ([name isEqualToString:@""]) {
            self.saveButton.enabled = NO;
        } else self.saveButton.enabled = YES;
    } else if (textField == self.pinTextField) {
        NSString *pin = self.pinTextField.text;
        if (pin.length == 4) {
            [self.pinTextField resignFirstResponder];
            [self.repinTextField becomeFirstResponder];
        }
    } else if (textField == self.repinTextField) {
        NSString *repin = self.repinTextField.text;
        if (repin.length == 4) {
            [self hideKeyboard];
        }
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.nameTextField) {
        [self.pinTextField becomeFirstResponder];
    } else if (textField == self.pinTextField) {
        [self.repinTextField becomeFirstResponder];
    } else {
        [self hideKeyboard];
    }
    return NO;
}

#pragma mark - IBActions

- (IBAction)cancelAction:(id)sender {
    [self hideAddAccountView];
    [self resetHeaderColor];
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

// create account related.

- (IBAction)toggleChildButton:(id)sender {
    self.childAccountCheckButton.selected = !self.childAccountCheckButton.selected;
    if (self.childAccountCheckButton.selected) {
        self.pinLabel.text = @"PIN";
        if (self.pinTextField.text.length < 4) {
            [self.pinTextField becomeFirstResponder];
        }
    } else {
        self.pinLabel.text = @"PIN (optional)";
    }
}


- (IBAction)saveAction:(id)sender {
    NSString *name = [self.nameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (name == nil) {
        [self showAlertWithTitle:@"Error" message:@"Account name is needed."];
        return;
    }
    
    NSString *pin = self.pinTextField.text;
    NSString *repin = self.repinTextField.text;
    
    if ([[UserManager sharedManager] userAccountNameExists:name]) {
        [self showAlertWithTitle:@"Error" message:[NSString stringWithFormat:@"Account name '%@' already exists.", name]];
        return;
    }
    
    BOOL needPin = NO;
    
    if (pin.length > 0 || self.childAccountCheckButton.selected) {
        needPin = YES;
    }
    
    if (needPin) {
        if (pin.length == 0 && self.childAccountCheckButton.selected) {
            [self showAlertWithTitle:@"Enter a PIN" message:@"Accounts for children must have a PIN."];
            return;
        } else if (pin.length < 4 || repin.length < 4 || ![pin isEqualToString:repin]) {
            [self showAlertWithTitle:@"Invalid PIN" message:@"PIN must be 4 digits and match with PIN again"];
            return;
        }
    }
    
    self.addAccountButton.hidden = NO;
    [[UserManager sharedManager] addUserAccount:name color:self.selectedColorLabel.textColor pin:pin childAccount:self.childAccountCheckButton.selected];
    [self loadAccounts];
    self.nameTextField.text = @"";
    
    // no other accounts exist. auto select the created account.
    if (self.mustCreateUserName) {
        [self showSpinner];
        [[UserManager sharedManager] setCurrentUser:[[UserManager sharedManager] users][0]];
        [self markPreviousAppCategoriesAsFavouriteWithCompletion:^{
            [self hideSpinner];
            self.action = ACTION_TYPE_LOGIN;
            [self allowAccessToUser:[[UserManager sharedManager] users][0]];
        }];
    } else {
        [self resetHeaderColor];
    }
}

- (IBAction)addAccountAction:(id)sender {
    
    // new users have default color selected.
    self.selectedColorLabel.textColor = [UIColor colorWithRed:162.0/255.0 green:21.0/255.0 blue:34.0/255.0 alpha:1.0];
    [self changeHeaderToColor:self.selectedColorLabel.textColor];
    
    self.addAccountButton.hidden = YES;
    [UIView animateWithDuration:0.3 animations:^{
        self.accountsView.alpha = 0.0f;
    } completion:^(BOOL finished) {
        
        self.accountsView.hidden = YES;
        self.accountsView.alpha = 1.0;
        
        self.addAccountView.alpha = 0.0f;
        self.scrollView.hidden = NO;
        self.addAccountView.hidden = NO;
        [UIView animateWithDuration:0.3 animations:^{
            
            self.addAccountView.alpha = 1.0f;
        } completion:^(BOOL finished) {
            [self.nameTextField becomeFirstResponder];
        }];
    }];
}

#pragma mark - table view data source

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 78;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[UserManager sharedManager] users].count;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UserCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_ID];
    cell.delegate = self;
    User *user = [[[UserManager sharedManager] users] objectAtIndex:indexPath.row];
//    [cell setupWithName:user.name color:user.color];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        //add code here for when you hit delete
//        
//        self.action = ACTION_TYPE_DELETE;
//        User *user = [[[UserManager sharedManager] users] objectAtIndex:indexPath.row];
//        if (user.pin.length == 4) {
//            [self askPinWithUser:user];
//        } else {
//            [self confirmDeleteUser:user];
//        }
//    }
}

#pragma mark - table view delegate

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    User *user = [[[UserManager sharedManager] users] objectAtIndex:indexPath.row];
//    
//    if ([[UserManager sharedManager] currentUser]) {
//        // if tapped on logged in user again. no need to reload stuff.
//        if ([user.name isEqualToString:[[UserManager sharedManager] currentUser].name]) {
//            
//            // need to apply new color, if the user changed color other wise just close.
//            if (![self.namesForWhichColorChanged containsObject:user.name]) {
//                [self.navigationController popViewControllerAnimated:YES];
//                return;
//            }
//        }
//    }
//    
//    self.action = ACTION_TYPE_LOGIN;
//    if (user.pin.length == 4 && !user.childUser) {
//        [self askPinWithUser:user];
//    } else {
//        [self allowAccessToUser:user];
//    }
}

-(void) deleteUserAccount:(User *)user {
//    NSString *name = user.name;
//    [[UserManager sharedManager] removeUserAccount:name];
//    [self.usersTableView reloadData];
//    [self showAddAccountIfNoUsers];
//    self.action = nil;
}

-(void) allowAccessToUser:(User *)user {
    if ([self.action isEqualToString:ACTION_TYPE_LOGIN]) {
        [[UserManager sharedManager] setCurrentUser:user];
        
        [self.view setUserInteractionEnabled:NO];
        [UIView animateWithDuration:0.75 animations:^{
            self.headerView.backgroundColor = [[UserManager sharedManager] colorForUser];
        } completion:^(BOOL finished) {
            [self.view setUserInteractionEnabled:YES];
            NSNotification *notif = [NSNotification notificationWithName:NOTIFICATION_USER_ACCOUNT_SELECTED object:nil];
            [[NSNotificationCenter defaultCenter] postNotification:notif];
        }];
    } else if ([self.action isEqualToString:ACTION_TYPE_DELETE]) {
        [self confirmDeleteUser:user];
        return;
    }
    
    self.action = nil;
}

-(void) confirmDeleteUser:(User *)user {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Are you sure ?" message:@"All your favourites will be deleted permanently. Continue?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alert show];
    self.userToDelete = user;
}

-(void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if ([self.action isEqualToString:ACTION_TYPE_DELETE]) {
        self.action = nil;
        if (buttonIndex == 1) {
            [self deleteUserAccount:self.userToDelete];
        }
    }
    self.userToDelete = nil;
}

#pragma mark -pin view delegate

-(void) pinViewAllowedAccessToUser:(User *)user {
    [self removePinView];
    [self allowAccessToUser:user];
}

-(void) pinViewdeniedAccessToUser:(User *)user {
    [self removePinView];
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self hideKeyboard];
}

-(void) hideKeyboard {
    if ([self.nameTextField isFirstResponder]) {
        [self.nameTextField resignFirstResponder];
    } else if ([self.pinTextField isFirstResponder]) {
        [self.pinTextField resignFirstResponder];
    } else if ([self.repinTextField isFirstResponder]) {
        [self.repinTextField resignFirstResponder];
    }
}

-(void) changeHeaderToColor:(UIColor *)color {
    [UIView animateWithDuration:0.5 animations:^{
        self.headerView.backgroundColor = color;
    } completion:^(BOOL finished) {
        [self.view setUserInteractionEnabled:YES];
    }];
    [self applyStyle];
}

#pragma mark - user cell delegate

-(void) userCell:(id)cell requestedColorChangeToColor:(UIColor *)color {
    UserCell *userCell = (UserCell *)cell;
    [self.namesForWhichColorChanged addObject:userCell.userNameLabel.text];
    [self.view setUserInteractionEnabled:NO];
    [self changeHeaderToColor:color];
}

-(void) userCellRequestedResetHeaderColor:(id)cell {
    [self resetHeaderColor];
}

#pragma mark - user log out related.

-(void) handleLogOut {
    self.backButton.hidden = YES;
    [self resetHeaderColor];
}

-(void) resetHeaderColor {
    [UIView animateWithDuration:0.5 animations:^{
        self.headerView.backgroundColor = [[UserManager sharedManager] colorForUser];
    }];
}

#pragma mark - scroll view delegate

-(void) scrollViewDidScroll:(UIScrollView *)scrollView {
    [self hideKeyboard];
}

#pragma mark - style related

-(void) applyStyle {
    [[StyleManager sharedManager] styleButtons:@[self.cancelButton,     self.saveButton]];
//    [[StyleManager sharedManager] styleHeaderButtons:@[self.addAccountButton, self.backButton]];
}

#pragma mark - mark previous categories as favs related.

-(void) markPreviousAppCategoriesAsFavouriteWithCompletion:(voidBlock)completion {
    [self readClassicCategoriesWithCompletion:^(NSArray *objects, NSError *error) {
        if (!error && objects.count > 0) {
            for (ContentCategory *category in objects) {
//                if ([category.categoryName isEqualToString:@"Pakistani TV Shows"] || [category.categoryName isEqualToString:@"Pakistani TV Dramas"]) {
//                    [category saveAsFavourite];
//                }
            }
        }
        completion();
    }];
}

@end
