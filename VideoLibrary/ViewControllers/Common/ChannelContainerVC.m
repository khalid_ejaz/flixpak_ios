//
//  ChannelContainerVC.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/1/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "ChannelContainerVC.h"
#import "YoutubeVideosVC.h"
#import "ME_PrefsManager.h"
#import "UserChannel.h"
#import "YoutubeVideo.h"
#import "PlayVC.h"

@interface ChannelContainerVC ()

@property (unsafe_unretained, nonatomic) IBOutlet UIView *headerView;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *channelNameLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *contentView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *favButton;

@property (nonatomic, strong) YoutubeVideosVC *videosVC;

@end

@implementation ChannelContainerVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupVideosVC];
    self.headerView.backgroundColor = [[UserManager sharedManager] colorForUser];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = YES;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    
    if (self.firstLoadDone) {
        return;
    } else self.firstLoadDone = YES;
    
    if (!self.channel) {
//        if (self.userChannel) {
//            self.channelNameLabel.text = [NSString stringWithFormat:@"Channel: %@", self.userChannel.channelName];
//            [self.videosVC setChannelId:self.userChannel.channelId];
//            [self.videosVC loadData];
//            self.favButton.selected = [UserManager isChannelFavouriteAlready:self.userChannel.channelId];
//            return;
//        } else {
            [self showGeneralError];
            return;
//        }
    }
    
    self.channelNameLabel.text = [NSString stringWithFormat:@"Channel: %@", self.channel.channelName];
    [self.videosVC setChannelId:self.channel.channelId];
    self.videosVC.showingChannel = YES;
    [self.videosVC loadData];
    
    self.favButton.selected = NO;
    NSArray *favChannels = [[ME_PrefsManager singleton] arrayPrefsForKey:@"fav-channels"];
    for (NSDictionary *dict in favChannels) {
        if ([dict[@"channelId"] isEqualToString:self.channel.channelId]) {
            self.favButton.selected = YES;
        }
    }
}

-(void) setupVideosVC {
    self.videosVC = [[YoutubeVideosVC alloc] initWithNibName:@"ViewerVC" bundle:nil];
    self.videosVC.view.frame = self.contentView.bounds;
    [self.contentView addSubview:self.videosVC.view];
    [self addChildViewController:self.videosVC];
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)toggleFavAction:(id)sender {
    NSMutableArray *favs = [[[ME_PrefsManager singleton] arrayPrefsForKey:@"fav-channels"] mutableCopy];
    for (NSDictionary *dict in favs) {
        if ([dict[@"channelId"] isEqualToString:self.channel.channelId]) {
            [favs removeObject:dict];
            self.favButton.selected = NO;
            [[ME_PrefsManager singleton] saveArrayPrefs:favs forKey:@"fav-channels"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"reload-tabs" object:nil];
            return;
        }
    }
    
    // not already added. add it now.
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.channel.channelId forKey:@"channelId"];
    [dict setObject:self.channel.channelName forKey:@"channelName"];
    [favs addObject:dict];
    [[ME_PrefsManager singleton] saveArrayPrefs:favs forKey:@"fav-channels"];
    self.favButton.selected = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reload-tabs" object:nil];
    
//    if (self.userChannel) {
//        [[UserManager sharedManager] setUserChannel:self.userChannel asFavourite:self.favButton.selected];
    
//    } else if (self.channel) {
//        [[UserManager sharedManager] setChannel:self.channel asFavourite:self.favButton.selected];
//    }
}

@end
