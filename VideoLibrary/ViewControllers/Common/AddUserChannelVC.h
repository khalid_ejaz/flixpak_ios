//
//  AddUserChannelVC.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-02-03.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "AppRootViewController.h"

@interface AddUserChannelVC : AppRootViewController <UITextFieldDelegate>

- (void)setShowingOnHome:(BOOL)onHome;
- (void)setupForASingleVideo;

@end
