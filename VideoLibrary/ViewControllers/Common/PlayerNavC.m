//
//  PlayerNavC.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/10/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "PlayerNavC.h"
#import "Utility.h"

@interface PlayerNavC ()

@end

@implementation PlayerNavC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL) shouldAutorotate {
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD) {
        return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
    }
    return YES;
}

//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation // iOS 6 autorotation fix
//{
//    return UIInterfaceOrientationLandscapeRight;
//}

-(NSUInteger)supportedInterfaceOrientations
{
    if (IS_IPAD) {
        return UIInterfaceOrientationMaskLandscape;
    }
    return UIInterfaceOrientationMaskAll;
}



@end
