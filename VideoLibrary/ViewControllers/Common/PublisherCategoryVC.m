//
//  PublisherCategoryVC.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-02-02.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "PublisherCategoryVC.h"
#import "FlixPakClient.h"
#import "DataManager.h"

@interface PublisherCategoryVC ()

@property (weak, nonatomic) IBOutlet UITextField *categoryTextField;

@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *channelId;
@property (nonatomic, strong) NSString *channelTitle;
@property (nonatomic, strong) NSString *videoUrl;

@end

@implementation PublisherCategoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)setCode:(NSString *)code videoUrl:(NSString *)url channelId:(NSString *)channelId channelTitle:(NSString *)title {
    _code = code;
    _videoUrl = url;
    _channelId = channelId;
    _channelTitle = title;
}

- (IBAction)finishAction:(id)sender {
    // go back to home.
    NSString *interest = self.categoryTextField.text;
    if ([interest isEqualToString:@""]) {
        interest = @"unsure";
    }
    [self showBlockingWaitIndicatorWithMessage:@"Please wait..."];
//    [[DataManager sharedManager] findUploadsIdByChannelId:self.channelId completion:^(NSArray *objects, NSError *error) {
//        NSString *uploads_id = @"";
//        if (!error) {
//            
//        } else {
//            uploads_id = @"failed";
//        }
//        
//        uploads_id = objects[0];
        NSString *form = [NSString stringWithFormat:@"code=%@&video_url=%@&channel_id=%@&channel_title=%@&interest=%@", self.code, self.videoUrl, self.channelId, self.channelTitle, interest];
        [[FlixPakClient singleton] POSTRoute:@"savechannelrequest" form:form withCompletion:^(NSDictionary *dictionary, NSError *error) {
            BOOL success = NO;
            [self hideBlockingWaitIndicator];
            if (dictionary) {
                success = YES;
                if ([dictionary.allKeys containsObject:@"data"]) {
                    NSDictionary *data = dictionary[@"data"];
                    if ([data.allKeys containsObject:@"message"]) {
                        NSString *message = data[@"message"];
                        [self showCompletionMessage:message];
                        return;
                    }
                }
            }
            if (!success) {
                [self showMessage:@"Failed to send request. Please contact support to add your channel." title:@"Error"];
                return;
            } else {
                [self showCompletionMessage:@"We have received your request. After passing review, your channel will be promoted at FlixPak."];
            }
        }];
//    }];
}

- (void)showCompletionMessage:(NSString *)message {
    self.messageAction = @"complete";
    [self showMessage:message title:@""];
}

- (void)messageViewClosedWithOK {
    if ([self.messageAction isEqualToString:@"complete"]) {
        [self backToHome];
    }
    [super messageViewClosedWithOK];
}

- (void)backToHome {
    self.messageAction = @"done";
    [self showMessage:@"We have received your request. After passing review, your channel be promoted at FlixPak." title:@"Done"];
    self.navigationController.navigationBarHidden = YES;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self finishAction:nil];
    return NO;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

@end
