//
//  PlayerVC.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/29/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "RootViewController.h"
#import <YTPlayerView.h>

@class YoutubeVideo, Episode;
@interface PlayerVC : RootViewController <UIWebViewDelegate, YTPlayerViewDelegate, UIActionSheetDelegate>

@property (nonatomic, strong) Episode *episode;
@property (nonatomic, strong) YoutubeVideo *video;

@end
