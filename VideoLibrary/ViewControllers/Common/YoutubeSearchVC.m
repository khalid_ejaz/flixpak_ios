//
//  YoutubeSearchVC.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/7/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "YoutubeSearchVC.h"
#import "SwitchControl.h"
#import "DataManager.h"
#import "YCSearchResultItem.h"
#import "UserManager.h"
#import "SwitchOption.h"
#import "YCSeachVideoResult.h"
#import "ChannelContainerVC.h"
#import "SwitchOption.h"
#import "UserChannel.h"

@interface YoutubeSearchVC ()

@property (nonatomic, strong) YCSearchResultView *channelResultView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *contentView;
@property (unsafe_unretained, nonatomic) IBOutlet UITextField *searchTextField;
@property (nonatomic, strong) UINavigationController *resultsNavC;
@property (nonatomic, strong) UIActivityIndicatorView *busyWheel;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *headerView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *switchContainerView;
@property (nonatomic, assign) BOOL searchChannels;
@property (weak, nonatomic) IBOutlet UIButton *sortButton;
@property (nonatomic, strong) NSString *searchOrder;

@end

@implementation YoutubeSearchVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    [self setupResultNavC];
    [self setupSwitchControl];
    self.headerView.backgroundColor = [[UserManager sharedManager] colorForUser];
    self.searchOrder = @"relevance";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showChannel:) name:@"NOTIFICATION_SHOW_CHANNEL" object:nil];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = YES;
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!self.firstLoadDone) {
        [self.searchTextField becomeFirstResponder];
    }
    self.firstLoadDone = YES;
}

#pragma mark - IBActions

- (IBAction)backAction:(id)sender {
    if (self.resultsNavC) {
        if (self.resultsNavC.viewControllers.count > 1) {
            [self.resultsNavC popViewControllerAnimated:YES];
            return;
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)closeAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - setup channel search result related

-(void) setupSwitchControl {
    SwitchOption *opt1 = [[SwitchOption alloc] initWithDisplayText:@"Videos" value:@"videos" ascending:NO];
    SwitchOption *opt2 = [[SwitchOption alloc] initWithDisplayText:@"Channels" value:@"channels" ascending:NO];
    
    SwitchControl *control = [[NSBundle mainBundle] loadNibNamed:@"SwitchControl" owner:self options:nil][0];
    [control populateWithSwitchOptions:@[opt1, opt2] controlBackColor:GRAY_WITH_PERCENT(6) optionBackColor:GRAY_WITH_PERCENT(10) textColor:GRAY_WITH_PERCENT(60) showingSort:NO];
    control.delegate = self;
    control.frame = self.switchContainerView.bounds;
    control.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.switchContainerView addSubview:control];
}

-(void) setupResultNavC {
    UIViewController *vc = [[UIViewController alloc] init];
    [vc.view setFrame:self.contentView.bounds];
    vc.view.backgroundColor = self.contentView.backgroundColor;
    self.resultsNavC = [[UINavigationController alloc] initWithRootViewController:vc];
    [self.resultsNavC.view setFrame:self.contentView.bounds];
    [self.contentView addSubview:self.resultsNavC.view];
    [self.resultsNavC setNavigationBarHidden:YES];
    [self addChildViewController:self.resultsNavC];
    
    self.busyWheel = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.busyWheel.center = vc.view.center;
    [self.busyWheel hidesWhenStopped];
    [self.busyWheel stopAnimating];
    [self.busyWheel setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin];
    [vc.view addSubview:self.busyWheel];
}

-(void) removeResultsView {
    if (self.channelResultView) {
        [self.channelResultView removeFromSuperview];
        self.channelResultView = nil;
    }
}

-(void) setupResultView {
    [self removeResultsView];
    self.channelResultView = [[NSBundle mainBundle] loadNibNamed:@"YCSearchResultView" owner:self options:nil][0];
    [self.channelResultView setDelegate:self];
    [self.channelResultView setFrame:self.contentView.bounds];
    [self.channelResultView setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    UIViewController *rootVC = [self.resultsNavC.viewControllers objectAtIndex:0];
    [rootVC.view addSubview:self.channelResultView];
    [self.busyWheel stopAnimating];
}

- (IBAction)findAction:(id)sender {
    NSString *searchText = [[Utility stringByRemovingWhiteSpaces:self.searchTextField.text] stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    if (searchText.length < 3) {
        return;
    }
    
    [self removeResultsView];
    
    [self.resultsNavC popToRootViewControllerAnimated:NO];

    if (self.searchChannels) {
        [self.busyWheel startAnimating];
        [[DataManager sharedManager] findChannelsByName:searchText order:self.searchOrder pageToken:nil completion:^(YCSearchResult *result, NSError *error) {
            if (error) {
                [self showError:error];
            } else {
                [self setupResultView];
                [self.channelResultView setSearchResult:result];
            }
        }];
    } else {
        YoutubeVideosVC *videoVC = [[YoutubeVideosVC alloc] initWithNibName:@"ViewerVC" bundle:nil];
        [videoVC setSearchText:searchText];
        [videoVC setPresetSortOrder:self.searchOrder];
        [videoVC.view setFrame:self.resultsNavC.view.bounds];
        [self.resultsNavC pushViewController:videoVC animated:YES];
//        [self.resultsNavC addChildViewController:videoVC];
        videoVC.delegate = self;
        [videoVC loadData];
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    [self findAction:nil];
    [textField resignFirstResponder];
    return NO;
}

-(void) hideKeyboard {
    if ([self.searchTextField isFirstResponder]) {
        [self.searchTextField resignFirstResponder];
    }
}

#pragma mark - Youtube Channel Result view delegate

-(void) selectedResultItem:(YCSearchResultItem *)resultItem {
    
    UserChannel *channel = [[UserChannel alloc] init];
    channel.channelId = resultItem.channelId;
    channel.channelName = resultItem.channelTitle;
    channel.channelDesc = resultItem.channelDescription;
    channel.channelThumbnail = resultItem.thumbnail;

    ChannelContainerVC *channelsVC = [[ChannelContainerVC alloc] initWithNibName:@"ChannelContainerVC" bundle:nil];
    [channelsVC setChannel:channel];
    [channelsVC.navigationItem setTitle:channel.channelName];
    [self.navigationController pushViewController:channelsVC animated:YES];
}

#pragma mark - Switch control delegate

-(void) switchControl:(id)control selectedOption:(SwitchOption *)option {
    if ([option.value isEqualToString:@"videos"]) {
        self.searchChannels = NO;
    } else self.searchChannels = YES;
    
    [self findAction:nil];
    [self hideKeyboard];
}

#pragma mark - notification selectors

-(void) showChannel:(NSNotification *)notification {
    NSString *channelId = notification.userInfo[@"channelId"];
    NSString *channelTitle = notification.userInfo[@"channelTitle"];
    
    if (channelId) {
        ChannelContainerVC *channelsVC = [[ChannelContainerVC alloc] initWithNibName:@"ChannelContainerVC" bundle:nil];
        [channelsVC setChannelId:channelId];
        [channelsVC setChannelTitle:channelTitle];
        [self.navigationController pushViewController:channelsVC animated:YES];
    }
}

#pragma mark - sort related.

#define SEARCH_ORDER_DATE           @"date"
#define SEARCH_ORDER_RATING         @"rating"
#define SEARCH_ORDER_RELEVANCE      @"relevance"
#define SEARCH_ORDER_TITLE          @"title"
#define SEARCH_ORDER_VIEW_COUNT     @"viewCount"

-(NSArray *) sortOptions {
    SwitchOption *opt2 = [[SwitchOption alloc] initWithDisplayText:@"Relevance" value:SEARCH_ORDER_RELEVANCE ascending:YES];
    SwitchOption *opt1 = [[SwitchOption alloc] initWithDisplayText:@"Date" value:SEARCH_ORDER_DATE ascending:YES];
    SwitchOption *opt3 = [[SwitchOption alloc] initWithDisplayText:@"Rating" value:SEARCH_ORDER_RATING ascending:YES];
    SwitchOption *opt4 = [[SwitchOption alloc] initWithDisplayText:@"View Count" value:SEARCH_ORDER_VIEW_COUNT ascending:YES];
    return @[opt2, opt1, opt3, opt4];
}

- (IBAction)sortAction:(id)sender {
    if(![self sortOptions]) {
        return;
    }
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Sort:"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                          destructiveButtonTitle:nil
                                               otherButtonTitles:[(SwitchOption *)(self.sortOptions[0]) displayText], [(SwitchOption *)(self.sortOptions[1]) displayText], [(SwitchOption *)(self.sortOptions[2]) displayText], (self.sortOptions.count==4)? [(SwitchOption *)(self.sortOptions[3]) displayText]:nil, nil];
    
    [action  showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex < self.sortOptions.count) {
        SwitchOption *option = self.sortOptions[buttonIndex];
//        [self sortWithColumn:option.value ascending:option.ascending];
        self.searchOrder = option.value;
        [self findAction:nil];
    }
}

@end
