//
//  ContentBrowserViewController.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/27/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "ContentBrowserViewController.h"
#import "ContentViewerVC.h"
#import "DataManager.h"
#import <UIImageView+AFNetworking.h>
#import "Utility.h"
#import "StyleManager.h"

@interface ContentBrowserViewController ()

@property (unsafe_unretained, nonatomic) IBOutlet UIView *headerView;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *contentNameLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *backButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *contentImageView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *contentView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *favButton;

@property (nonatomic, strong) ContentViewerVC *contentVC;

@end

@implementation ContentBrowserViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.headerView.backgroundColor = [[UserManager sharedManager] colorForUser];
//    [[StyleManager sharedManager] styleHeaderButton:self.favButton];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    if (!self.contentVC) {
        [self showSpinner];
        self.favButton.hidden = YES;
        self.contentNameLabel.hidden = YES;
        if (!self.content) {
            [self readContentWithId:self.contentId completion:^(PFObject *object, NSError *error) {
                if (error) {
                    [self showNetworkError];
                } else {
                    if (!object) {
                        [self showGeneralError];
                    } else {
                        _content = (Content *)object;
                        [self loadData];
                    }
                }
            }];
        } else {
            [self loadData];
        }
    }
}

-(void) loadData {
    
//    [self hideSpinner];
//    if (!self.content) {
//        [self showGeneralError];
//        return;
//    }
//    
//    if ([self.category.categoryType isEqualToString:CATEGORY_TYPE_TITLE]) {
//        [self.contentImageView removeFromSuperview];
//        
//        CGRect headerRect = CGRectMake(0, 0, self.view.bounds.size.width, OS_VERSION>=7?52:32);
//        self.headerView.frame = headerRect;
//        
//        CGRect buttonFrame = self.backButton.frame;
//        buttonFrame.origin.y = self.headerView.bounds.size.height/2 - buttonFrame.size.height/2;
//        if (OS_VERSION >= 7) {
//            buttonFrame.origin.y += 10;
//        }
//        self.backButton.frame = buttonFrame;
//        
//        CGRect contentFrame = self.contentView.frame;
//        contentFrame.origin.y = self.headerView.bounds.size.height;
//        contentFrame.size.height = self.view.bounds.size.height - self.headerView.bounds.size.height;
//        self.contentView.frame = contentFrame;
//        
//        CGRect contentNameLabelRect = self.contentNameLabel.frame;
//        contentNameLabelRect.size.width = self.view.bounds.size.width - 60 - buttonFrame.size.width;
//        contentNameLabelRect.size.height = buttonFrame.size.height;
//        contentNameLabelRect.origin.y = buttonFrame.origin.y;
//        self.contentNameLabel.frame = contentNameLabelRect;
//        
//    } else {
//        if (OS_VERSION >= 7) {
//            CGRect headerFrame = self.headerView.frame;
//            headerFrame.size.height += 20;
//            self.headerView.frame = headerFrame;
//        }
//        NSURL *imageURL = [[DataManager sharedManager] imageURLWithCategoryId:self.content.categoryId contentId:self.content.objectId];
//        [self.contentImageView setImageWithURL:imageURL placeholderImage:[Utility placeHolderImage]];
//    }
//    
//    self.contentNameLabel.hidden = NO;
//    self.contentNameLabel.text = self.content.name;
//    self.favButton.selected = [self.content isFavourite];
//    self.favButton.hidden = NO;
//    self.contentVC = [[ContentViewerVC alloc] initWithNibName:@"ViewerVC" bundle:nil];
//    [self.contentVC.view setFrame:self.contentView.bounds];
//    [self.contentView addSubview:self.contentVC.view];
//    [self addChildViewController:self.contentVC];
//    [self.contentVC setCategoryType:self.category.categoryType];
//    [self.contentVC setContent:self.content];
//    [self.contentVC setShowContentImageInCell:NO];
//    [self.contentVC loadData];
}

#pragma mark - IBActions

- (IBAction)toggleFavAction:(id)sender {
//    self.favButton.selected = !self.favButton.selected;
//    if (self.favButton.selected) {
//        [self.content saveAsFavourite];
//    } else [self.content saveAsNotFavourite];
//    
//    if (self.favButton.selected) {
//        if (self.category) {
//            [self.category saveAsFavourite];
//        } else {
//            if (self.content) {
//                
//                [[UserManager sharedManager] addCategoryAsFavouriteById:self.content.categoryId];
//            } 
//        }
//    }
//    NSNotification *notification = [NSNotification notificationWithName:NOTIFICATION_CHANGED_PERSONALISED_CONTENT object:nil];
//    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

-(IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
