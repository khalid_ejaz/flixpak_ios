//
//  HowSharingWorksVC.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-02-04.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "HowSharingWorksVC.h"

@interface HowSharingWorksVC ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation HowSharingWorksVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, 600)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
