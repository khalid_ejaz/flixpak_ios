//
//  NavigationController.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-18.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

typedef void(^Completion)();

@interface NavigationController : UINavigationController <MFMailComposeViewControllerDelegate, UIAlertViewDelegate, UITableViewDelegate, UITableViewDataSource>

- (void)toggleMenu;
- (void)showMenu;
- (void)hideMenuWithCompletion:(Completion)completion;

- (void)showChooseInterests;
- (void)showWatchLater;

@end
