//
//  Utility.m
//  ContentManager
//
//  Created by Malik Khalid Ejaz on 2013-10-05.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import "Utility.h"

@implementation Utility

#pragma mark - string utilities

+(NSString *) stringByRemovingWhiteSpaces:(NSString *)string {
    return [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

#pragma mark - color from string, darker/lighter color related.

+(UIColor *) colorFromHexString:(NSString *)hexString {
//    unsigned rgbValue = 0;
//    NSScanner *scanner = [NSScanner scannerWithString:hexString];
//    [scanner setScanLocation:1]; // bypass '#' character
//    [scanner scanHexInt:&rgbValue];
//    return [UIColor colorWithRed:((rgbValue & 0xFF000000) >> 16)/255.0 green:((rgbValue & 0x00FF0000) >> 8)/255.0 blue:(rgbValue & 0x0000FF00)/255.0 alpha:(rgbValue & 0x000000FF)/255.0];
    
    NSArray *components = [hexString componentsSeparatedByString:@"_"];
    if (components.count == 4) {
        CGFloat r = [components[0] integerValue]/255.0f;
        CGFloat g = [components[1] integerValue]/255.0f;
        CGFloat b = [components[2] integerValue]/255.0f;
        CGFloat a = [components[3] integerValue]/255.0f;
        return [UIColor colorWithRed:r green:g blue:b alpha:a];
    }
    return [UIColor darkGrayColor];
}

+(NSString *) stringForColor:(UIColor *)color {
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    int r = components[0] * 255.0f;
    int g = components[1] * 255.0f;
    int b = components[2] * 255.0f;
    int a = components[3] * 255.0f;
    NSString *colorString = [NSString stringWithFormat:@"%d_%d_%d_%d", r, g, b, a];
//    NSString *hexString=[NSString stringWithFormat:@"%.02X%.02X%.02X%.02X", (int)(r * 255), (int)(g * 255), (int)(b * 255), (int)(a*255)];
    return colorString;
}

+(UIColor *) darkerColorForColor:(UIColor *)color {
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    int r = components[0] * 255.0f - 10;
    int g = components[1] * 255.0f - 10;
    int b = components[2] * 255.0f - 10;
    int a = components[3] * 255.0f - 10;
    return [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a/255.0f];
}

+(UIColor *) lighterColorForColor:(UIColor *)color {
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    int r = components[0] * 255.0f + 80;
    int g = components[1] * 255.0f + 80;
    int b = components[2] * 255.0f + 80;
    int a = components[3] * 255.0f + 10;
    return [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a/255.0f];
}

#pragma mark - image crop, compress, scale related.

+(void) compressImage:(UIImage *)image withCompletion:(UtilityURLAndErrorBlock)completion {
    // use tingPNG.org to compress image.
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://www.tinypng.org/api/shrink"]];
    [request setHTTPMethod:@"POST"];
    
    [request setValue:@"image/png" forHTTPHeaderField:@"Content-Type"];
    NSData *imageData = UIImagePNGRepresentation(image);
    
    [request setHTTPBody:imageData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        int responseStatusCode = [httpResponse statusCode];
        if (responseStatusCode == 200) {
            NSLog(@"Response: %@", response);
            NSError *parseError;
            NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
            if (!parseError) {
                NSDictionary *output = dictionary[@"output"];
                completion(output[@"url"], nil);
            } else completion(nil, parseError);
        } else {
            completion(nil, connectionError);
        }
        
    }];
}

+(UIImage*) scaleImage:(UIImage *)image toSize:(CGSize)size
{
    // Create a bitmap graphics context
    // This will also set it as the current context
    UIGraphicsBeginImageContext(size);
    
    // Draw the scaled image in the current context
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    
    // Create a new image from current context
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // Pop the current context from the stack
    UIGraphicsEndImageContext();
    
    // Return our new scaled image
    return scaledImage;
}

+(UIImage *) placeHolderImage {
    return [UIImage imageNamed:@"placeholder"];
}

#pragma mark - show general messages

+(void) showDone {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Done" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}

+(void) showError:(NSError *)error {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Something went wrong. Please try later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}

#pragma mark - Date Parse

+(NSString *) friendlyDateStringFromDate:(NSDate *)date
{
    //NSDate *date = [self.datePicker date];// [NSDate date];
    NSString *dateString;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    [dateFormatter setTimeZone: [NSTimeZone timeZoneWithAbbreviation:@"EST"]];
    dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

+(NSString *) dateStringFromDate:(NSDate *)date
{
    //NSDate *date = [self.datePicker date];// [NSDate date];
    NSString *dateString;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM_dd_yy"];
    [dateFormatter setTimeZone: [NSTimeZone timeZoneWithAbbreviation:@"EST"]];
    dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

+(NSString *) mySQLCompatibleDateStringFromDate:(NSDate *)date
{
    //NSDate *date = [self.datePicker date];// [NSDate date];
    NSString *dateString;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setTimeZone: [NSTimeZone timeZoneWithAbbreviation:@"EST"]];
    dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

+(NSDate *) dateFromDateString:(NSString *)dateString {
    dateString = [dateString stringByReplacingOccurrencesOfString:@"_" withString:@""];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMddyy"];
    [dateFormatter setTimeZone: [NSTimeZone timeZoneWithAbbreviation:@"EST"]];
    NSDate *date = [dateFormatter dateFromString:dateString];
    return date;
}

+(NSString *) publishedAtStringFromString:(NSString *)dateString {
    if (dateString.length > 10) {
        dateString = [dateString substringToIndex:10];
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:dateString];
    
    return [Utility friendlyDateStringFromDate:date];
}

+(NSDate *) dateFromUNIXDateString:(NSString *)unixDateString {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[unixDateString doubleValue]];
    return date;
}

+(NSDate *) dateDaysAgo:(NSInteger)daysAgo {
    NSTimeInterval interval = -86400 * daysAgo;
    NSDate *today = [NSDate date];
    
    // All intervals taken from Google
    NSDate *dateOfDay = [today dateByAddingTimeInterval: interval];
    return dateOfDay;
}

+(NSString *) durationStringFromYoutubeDuration:(NSString *)duration {
    if (!duration) {
        return @"";
    }
    BOOL hasHour = NO;
    if ([duration rangeOfString:@"H"].location != NSNotFound) {
        hasHour = YES;
    }
    
    duration = [duration stringByReplacingOccurrencesOfString:@"PT" withString:@""];
    NSArray *array = [duration componentsSeparatedByString:@"M"];
    if (array.count == 2) {
        NSString *min = array[0];
        
        NSString *hourString = @"";
        if (hasHour) {
            NSArray *arr = [min componentsSeparatedByString:@"H"];
            if (arr.count == 2) {
                hourString = arr[0];
                min = arr[1];
            }
        }
        
        if (min.length == 0) {
            min = @"00";
        } else if (min.length == 1) {
            min = [NSString stringWithFormat:@"0%@",min];
        }
        NSString *seconds = array[1];
        seconds = [seconds stringByReplacingOccurrencesOfString:@"S" withString:@""];
        if (seconds.length == 0) {
            seconds = @"00";
        } else if (seconds.length == 1) {
            seconds = [NSString stringWithFormat:@"0%@",seconds];
        }
        if (hasHour) {
            return [NSString stringWithFormat:@"%@:%@:%@", hourString,min, seconds];
        } else return [NSString stringWithFormat:@"%@:%@",min, seconds];
    }
    
    return @"";
}

+(NSString *) durationSinceTime:(NSString *)time {
    NSDate *dateNow = [NSDate date];
    NSDate *date = [Utility dateFromDateString:time];
    NSTimeInterval duration = [date timeIntervalSinceDate:dateNow];
    if (duration < 0) duration = duration * -1;
    
    if (duration < 60) {
        return [NSString stringWithFormat:@"%f secs ago.", duration];
    }
    if (duration < 3600) {
        return [NSString stringWithFormat:@"%f mins ago.", duration/60];
    }
    if (duration < 86400) {
        return [NSString stringWithFormat:@"%f hours ago.", duration/3600];
    }
    if (duration < 604800) {
        return [NSString stringWithFormat:@"%f days ago.", duration/86400];
    }
    else return @"> 1 week ago.";
}

+(NSString *) ratingStarsStringForRatings:(int) rating {
    
    if (rating == 0) {
        return @"☆☆☆☆☆";
    } else if (rating == 1) {
        return @"★☆☆☆☆";
    } else if (rating == 2) {
        return @"★★☆☆☆";
    } else if (rating == 3) {
        return @"★★★☆☆";
    } else if (rating == 4) {
        return @"★★★★☆";
    } else if (rating == 5) {
        return @"★★★★★";
    }
    return nil;
}

#pragma mark - file paths

+(NSString *) documentsDirPathForFile:(NSString *)fileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDirectory = [paths objectAtIndex:0];
    return [docDirectory stringByAppendingPathComponent:fileName];
}

+(NSString *) cachesDirPathForFile:(NSString *)fileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *docDirectory = [paths objectAtIndex:0];
    return [docDirectory stringByAppendingPathComponent:fileName];
}

@end
