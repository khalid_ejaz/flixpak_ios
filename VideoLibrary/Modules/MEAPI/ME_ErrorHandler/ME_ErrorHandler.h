//
//  MEErrorHandler.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-14.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//
//  Generic error handler.
//  ----------------------

@interface ME_ErrorHandler : NSObject

+ (id)singleton;

- (void)handleError:(NSError *)error;
- (NSError *)errorWithDescription:(NSString *)description;

@end
