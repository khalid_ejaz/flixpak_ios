//
//  MEErrorHandler.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-14.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "ME_ErrorHandler.h"
#import "ME_Logger.h"
#import "ME_ConfigManager.h"

@implementation ME_ErrorHandler

+ (id)singleton {
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    // returns the same object each time
    return _sharedObject;
}

- (NSError *)errorWithDescription:(NSString *)description {
    NSMutableDictionary* details = [NSMutableDictionary dictionary];
    [details setValue:description forKey:NSLocalizedDescriptionKey];
    // populate the error object with the details
    return [NSError errorWithDomain:[[ME_ConfigManager singleton] shortAppId] code:001 userInfo:details];
}

@end
