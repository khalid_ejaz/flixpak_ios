//
//  ME_UserManager.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-14.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ME_UserManager : NSObject
+ (id)singleton;
// adds a default user to each app.
// apps can override to provide multiuser experience and for online accounts.
// all settings and preferences are stored for this user.
// on reset, all these settings are lost.

- (NSString *)prefixForCurrentUser;

@end
