//
//  ME_ParentViewController.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-14.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageView.h"

@interface ME_ParentViewController : UIViewController <MessageViewDelegate>

@property (nonatomic, strong) NSString *messageAction;

// add functionalilty like common loaders. soft and blocking.
// add functionality like show a message etc.
// having it in one place, its possible to make themese in one spot and reuse in the entire app.
// ME_ParentViewController provides a basic implementation. Apps can override the implementation to add more fancy implementations and to match with app colors and design.

- (void)showSoftWaitIndicatorWithMessage:(NSString *)msg;
- (void)hideSoftWaitIndicator;
- (void)showBlockingWaitIndicatorWithMessage:(NSString *)msg;
- (void)hideBlockingWaitIndicator;

- (void)showMessage:(NSString *)message title:(NSString *)title okButtonTitle:(NSString *)okButtonTitle cancelButtonTitle:(NSString *)cancelButtonTitle;
- (void)showMessage:(NSString *)message title:(NSString *)title;
- (void)showNoInternetMessage;

- (void)showInformativeErrorMessage:(NSString *)message;
- (void)showInformativeError:(NSError *)error;
- (void)showGeneralNetworkError;
- (void)showUnExpectedError;

// used to see if ads are to be shown on this screen. Must override.
// also used to log analytics and to cache field values. 
- (NSString *)screenName;

@end
