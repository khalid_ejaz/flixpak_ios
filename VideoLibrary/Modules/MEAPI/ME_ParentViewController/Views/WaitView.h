//
//  waitView.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-15.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WaitView : UIView

- (void)showWithMessage:(NSString *)msg;
- (void)hide;

@end
