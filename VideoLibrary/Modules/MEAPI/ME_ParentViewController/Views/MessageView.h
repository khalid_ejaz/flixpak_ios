//
//  MessageView.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-15.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MessageViewDelegate <NSObject>

- (void)messageViewClosedWithOK;
- (void)messageViewClosedWithCancel;

@end

@interface MessageView : UIView

@property (nonatomic, weak) id<MessageViewDelegate> delegate;

- (void)showWithTitle:(NSString *)title message:(NSString *)msg okButtonTitle:(NSString *)okButtonTitle cancelButtonTitle:(NSString *)cancelButtonTitle delegate:(id<MessageViewDelegate>)delegate;
- (void)hide;

@end
