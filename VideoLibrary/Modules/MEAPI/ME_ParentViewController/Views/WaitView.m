//
//  waitView.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-15.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "WaitView.h"

@interface WaitView()

@property (weak, nonatomic) IBOutlet UILabel *waitTextLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@end

@implementation WaitView

- (void)hide {
    [self removeFromSuperview];
    [self clearText];
}

- (void)clearText {
    self.waitTextLabel.text = @"";
    self.spinner.hidden = YES;
}

- (void)showWithMessage:(NSString *)msg {
    [self clearText];
    self.spinner.hidden = NO;
    self.waitTextLabel.text = msg;
}

@end
