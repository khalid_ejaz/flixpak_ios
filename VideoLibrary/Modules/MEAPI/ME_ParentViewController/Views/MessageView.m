//
//  MessageView.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-15.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "MessageView.h"
#import "ME_Logger.h"

@interface MessageView()

@property (weak, nonatomic) IBOutlet UILabel *messageTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;


@end

@implementation MessageView

- (void)hide {
    [self removeFromSuperview];
    [self clearText];
}

- (void)clearText {
    self.messageLabel.text = @"";
    self.messageTitleLabel.text = @"";
    [self.okButton setTitle:@"" forState:UIControlStateNormal];
    [self.cancelButton setTitle:@"" forState:UIControlStateNormal];
    self.okButton.hidden = self.cancelButton.hidden = YES;
}

- (void)showWithTitle:(NSString *)title message:(NSString *)msg okButtonTitle:(NSString *)okButtonTitle cancelButtonTitle:(NSString *)cancelButtonTitle delegate:(id<MessageViewDelegate>)delegate {
    _delegate = delegate;
    [self clearText];
    self.messageLabel.text = msg;
    self.messageTitleLabel.text = title;
    if (okButtonTitle) {
        self.okButton.hidden = NO;
        [self.okButton setTitle:okButtonTitle forState:UIControlStateNormal];
    } else self.okButton.hidden = YES;
    
    if (cancelButtonTitle) {
        [self.cancelButton setTitle:cancelButtonTitle forState:UIControlStateNormal];
        self.cancelButton.hidden = NO;
    } else self.cancelButton.hidden = YES;
}

- (IBAction)closedWithOk:(id)sender {
    if (_delegate) [self.delegate messageViewClosedWithOK];
    else [[ME_Logger singleton] log:@"No delegate for message view." level:ME_Log_Level_WARN];
}
- (IBAction)closedWithCancel:(id)sender {
    if (_delegate) [self.delegate messageViewClosedWithCancel];
    else [[ME_Logger singleton] log:@"No delegate for message view." level:ME_Log_Level_WARN];
}
@end
