//
//  ME_ParentViewController.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-14.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "ME_ParentViewController.h"
#import "ME_Logger.h"
#import "WaitView.h"

@interface ME_ParentViewController ()

@property (nonatomic, strong) WaitView *waitView;
@property (nonatomic, strong) MessageView *messageView;
@property (nonatomic, strong) UIImageView *blockingOverlay;

@end

@implementation ME_ParentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _waitView = [[NSBundle mainBundle] loadNibNamed:@"WaitView" owner:nil options:nil][0];
    _waitView.layer.cornerRadius = 10.0f;
    _waitView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.waitView.layer.borderWidth = 2.0f;
    self.waitView.clipsToBounds = YES;
    
    _messageView = [[NSBundle mainBundle] loadNibNamed:@"MessageView" owner:nil options:nil][0];
    _messageView.layer.cornerRadius = 10.0f;
    _messageView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.messageView.layer.borderWidth = 2.0f;
    self.messageView.clipsToBounds = YES;
    
    self.blockingOverlay = [[UIImageView alloc] initWithFrame:self.view.bounds];
    self.blockingOverlay.image = [UIImage imageNamed:@"blocking_overlay"];
    self.blockingOverlay.backgroundColor = [UIColor darkGrayColor];
    self.blockingOverlay.alpha = 0.5f;
    self.blockingOverlay.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    _messageAction = nil;
}

// cancellable when shown as a dialog.
- (void)showSoftWaitIndicatorWithMessage:(NSString *)msg {
    _waitView.center = self.view.center;
    [self.view addSubview:self.waitView];
    [self.waitView showWithMessage:msg];
}

- (void)hideSoftWaitIndicator {
    [self.waitView hide];
}

// not cancellable. disables UI.
- (void)showBlockingWaitIndicatorWithMessage:(NSString *)msg {
    // show blocking overlay.
    self.blockingOverlay.frame = self.view.bounds;
    [self.view addSubview:self.blockingOverlay];
    
    [self showSoftWaitIndicatorWithMessage:msg];
}

- (void)hideBlockingWaitIndicator {
    [self.blockingOverlay removeFromSuperview];
    [self.waitView hide];
}

- (void)showMessageView {
    self.messageView.center = self.view.center;
    [self.view addSubview:self.messageView];
}

- (void)showMessage:(NSString *)message title:(NSString *)title {
    [self showMessage:message title:title okButtonTitle:@"ok" cancelButtonTitle:nil];
}

- (void)showMessage:(NSString *)message title:(NSString *)title okButtonTitle:(NSString *)okButtonTitle cancelButtonTitle:(NSString *)cancelButtonTitle {
    [self showMessageView];
    [self.messageView showWithTitle:title message:message okButtonTitle:okButtonTitle cancelButtonTitle:cancelButtonTitle delegate:self];
}

- (void)showNoInternetMessage {
    [self showMessageView];
    [self.messageView showWithTitle:@"Error" message:@"Not connected to internet. Please check connection and try again." okButtonTitle:@"ok" cancelButtonTitle:nil delegate:self];
}

- (void)showInformativeError:(NSError *)error {
    [self showMessageView];
    BOOL incorrectlyFormedErrorObject = NO;
    if (!error.userInfo) incorrectlyFormedErrorObject = YES;
    else {
        if (![error.userInfo.allKeys containsObject:NSLocalizedDescriptionKey]) {
            incorrectlyFormedErrorObject = YES;
        }
    }
    if (incorrectlyFormedErrorObject) {
        [[ME_Logger singleton] log:@"Incorrectly formed error object. no message key in userinfo dict." level:ME_Log_Level_ERROR];
        [self.messageView showWithTitle:@"Error" message:@"unknown error." okButtonTitle:@"ok" cancelButtonTitle:nil delegate:self];
    } else {
        [self.messageView showWithTitle:@"Error" message:error.userInfo[NSLocalizedDescriptionKey] okButtonTitle:@"ok" cancelButtonTitle:nil delegate:self];
    }
}

- (void)showInformativeErrorMessage:(NSString *)message {
    [self showMessageView];
    [self.messageView showWithTitle:@"Error" message:message okButtonTitle:@"ok" cancelButtonTitle:nil delegate:self];
}

- (void)showGeneralNetworkError {
    [self.messageView showWithTitle:@"Error" message:@"Network error" okButtonTitle:@"ok" cancelButtonTitle:nil delegate:self];
}

- (void)showUnExpectedError {
    [self.messageView showWithTitle:@"Error" message:@"unexpected error." okButtonTitle:@"ok" cancelButtonTitle:nil delegate:self];
}

#pragma mark - Message view delegate

- (void)messageViewClosedWithOK {
    [self.messageView hide];
    self.messageAction = nil;
}

- (void)messageViewClosedWithCancel {
    [self.messageView hide];
    self.messageAction = nil;
}

@end
