//
//  types.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-16.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#ifndef types_h
#define types_h

typedef void (^BoolAndErrorBlock) (BOOL success, NSError *error);

typedef void (^ArrayAndErrorBlock) (NSArray *objects, NSError *error);
typedef void (^DictionaryAndErrorBlock) (NSDictionary *dictionary, NSError *error);

typedef void (^MutableArrayAndErrorBlock) (NSMutableArray *array, NSError *error);
typedef void (^MutableDictionaryAndErrorBlock) (NSMutableDictionary *dictionary, NSError *error);

typedef enum {
    ME_CACHE_IS_ENOUGH,         // if cached version is not expired, its enough.
    ME_CACHE_THEN_CURRENT,      // first return cached version then update with version from net.
    ME_CACHE_IGNORE             // do not look in cache. need most recent version.
} ME_Cache_Policy;

typedef enum {
    ME_Log_Level_INFO,          // just an FYI.
    ME_Log_Level_WARN,          // reporting unrecommended behavior.
    ME_Log_Level_DEBUG,         // development help.
    ME_Log_Level_ERROR,
    ME_Log_Level_FATAL,         // crash or app unusable.
    ME_Log_Level_TRACE,         // to find or lookup places in code.
    ME_Log_Level_ANALYTICS      // app usage and user experience analysis related.
} ME_Log_Level;

#endif /* types_h */
