//
//  ME_Launcher.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-14.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "ME_Launcher.h"
#import "ME_Logger.h"
#import "ME_SQLiteManager.h"
#import "ME_ConfigManager.h"
#import "ME_PrefsManager.h"

#import "ME_Launcher_ViewController.h"

@interface ME_Launcher()

@property (nonatomic, retain) ME_Launcher_ViewController *launcherViewController;

@end

@implementation ME_Launcher

+ (id)singleton {
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    // returns the same object each time
    return _sharedObject;
}

- (void)showLauncherWithAppName:(NSString *)appName shortId:(NSString *)shortId appUrl:(NSString *)appUrl defaultPrefs:(NSDictionary *)defaultPrefs rootViewController:(UIViewController *)rootViewController withCompletion:(LauncherCompletionBlock)completion {
    self.launcherViewController = [[ME_Launcher_ViewController alloc] initWithNibName:@"ME_Launcher_ViewController" bundle:nil];
    
    if (!rootViewController) {
        [[ME_Logger singleton] log:@"App Delegate did not provide rootViewController." level:ME_Log_Level_FATAL];
        return;
    }
    [self.launcherViewController.view setFrame:rootViewController.view.bounds];
    [rootViewController presentViewController:self.launcherViewController animated:NO completion:nil];
    
    [self.launcherViewController showLauncherWithAppName:appName shortId:shortId appUrl:appUrl defaultPrefs:defaultPrefs rootViewController:rootViewController withCompletion:completion];
}

@end
