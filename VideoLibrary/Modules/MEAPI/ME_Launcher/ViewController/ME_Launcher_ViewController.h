//
//  ME_Launcher_ViewController.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-14.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "ME_ParentViewController.h"
#import "ME_Launcher.h"

@interface ME_Launcher_ViewController : ME_ParentViewController

- (void)setAppName:(NSString *)appName;
- (void)setLaunchStatus:(NSString *)status;

- (void)showLauncherWithAppName:(NSString *)appName shortId:(NSString *)shortId appUrl:(NSString *)appUrl defaultPrefs:(NSDictionary *)defaultPrefs rootViewController:(UIViewController *)rootViewController withCompletion:(LauncherCompletionBlock)completion;

@end
