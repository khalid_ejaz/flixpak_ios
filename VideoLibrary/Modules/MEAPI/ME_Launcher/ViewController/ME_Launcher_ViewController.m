//
//  ME_Launcher_ViewController.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-14.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "ME_Launcher_ViewController.h"
#import "ME_Logger.h"
#import "ME_ConfigManager.h"
#import "ME_PrefsManager.h"
#import "ME_SQLiteManager.h"

@interface ME_Launcher_ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *appNameLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingSpinner;
@property (weak, nonatomic) IBOutlet UILabel *launcherStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *launchFailReasonLabel;
@property (weak, nonatomic) IBOutlet UIButton *retryButton;
@property (nonatomic, strong) LauncherCompletionBlock completion;

@property (nonatomic, strong) NSString *appName;
@property (nonatomic, strong) NSString *shortId;
@property (nonatomic, strong) NSString *appUrl;
@property (nonatomic, strong) NSDictionary *defaultPrefs;
@property (nonatomic, strong) UIViewController *rootViewController;

@end

@implementation ME_Launcher_ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

//    [self showBlockingWaitIndicatorWithMessage:@"Loading"];
//    [self showSoftWaitIndicatorWithMessage:@"Loading settings"];
//    [self showMessage:@"this is the message" title:@"Message title"];
}
- (IBAction)retryAction:(id)sender {
    [self showLauncherWithAppName:self.appName shortId:self.shortId appUrl:self.appUrl defaultPrefs:self.defaultPrefs rootViewController:self.rootViewController withCompletion:self.completion];
}

- (void)setAppName:(NSString *)appName {
    self.appNameLabel.text = appName;
}

- (void)setLaunchStatus:(NSString *)status {
    self.launcherStatusLabel.text = status;
}

- (void)showLauncherWithAppName:(NSString *)appName shortId:(NSString *)shortId appUrl:(NSString *)appUrl defaultPrefs:(NSDictionary *)defaultPrefs rootViewController:(UIViewController *)rootViewController withCompletion:(LauncherCompletionBlock)completion {
    _appName = appName;
    _shortId = shortId;
    _appUrl = appUrl;
    _defaultPrefs = defaultPrefs;
    _rootViewController = rootViewController;
    _completion = completion;
    
    
    self.retryButton.hidden = YES;
    [self.loadingSpinner startAnimating];
    [self setAppName:appName];
    [self setLaunchStatus:@"Searching mothership..."];
    
    //
    // initialize modules.
    [[ME_SQLiteManager singleton] initialize];
    [[ME_Logger singleton] initializeWithShortId:shortId];
    [[ME_PrefsManager singleton] initializeWithDefaults:defaultPrefs];
    
    //
    // always do this last in this method.
    [[ME_ConfigManager singleton] initializeWithAppName:appName shortId:shortId appUrl:(NSString *)appUrl withCompletion:^(BOOL success) {
        [self.loadingSpinner stopAnimating];
        if (success) {
            // app can continue.
            // check for update and messages and show if available.
            [self setLaunchStatus:@"Mothership connected."];
            self.messageAction = nil;
            if ([[ME_ConfigManager singleton] isUpdateAvailable]) {
//                NSString *latestVersion = [[ME_ConfigManager singleton] latestVersion];
                NSString *latestVersionLink = [[ME_ConfigManager singleton] latestVersionLink];
                self.messageAction = latestVersionLink;
                [self showMessage:@"A new app version is available. Please update." title:@"Update available" okButtonTitle:@"update" cancelButtonTitle:@"later"];
            } else if ([[ME_ConfigManager singleton] isThereMessageForUser]) {
                NSDictionary *message = [[ME_ConfigManager singleton] messageForUser];
                if (message) {
                    NSString *msg = message[@"text"];
                    NSString *btnTitle = message[@"btn_title"];
                    if (!btnTitle) btnTitle = @"ok";
                    NSString *btnLink = message[@"btn_link"];
                    NSString *lastMsg = [[ME_PrefsManager singleton] stringPrefForKey:@"lastMsg"];
                    if (msg && !([msg isEqualToString:lastMsg])) {
                        if (btnLink) {
                            self.messageAction = btnLink;
                            [self showMessage:msg title:@"Message" okButtonTitle:btnTitle cancelButtonTitle:nil];
                        } else {
                            [self showMessage:msg title:@"Message" okButtonTitle:btnTitle cancelButtonTitle:nil];
                        }
                        [[ME_PrefsManager singleton] saveStringPrefs:msg forKey:@"lastMsg"];
                    } else {
                        [self performSelector:@selector(continueToAppWithCompletion:) withObject:completion afterDelay:0.3f];
                    }
                }
            } else {
                [self performSelector:@selector(continueToAppWithCompletion:) withObject:completion afterDelay:0.3f];
            }
        } else {
            // app not supported.
            if ([[ME_ConfigManager singleton] isUpdateAvailable]) {
//                NSString *latestVersion = [[ME_ConfigManager singleton] latestVersion];
                NSString *latestVersionLink = [[ME_ConfigManager singleton] latestVersionLink];
                self.messageAction = latestVersionLink;
                [self showMessage:@"Please update the app to continue using the app." title:@"Unsupported app version" okButtonTitle:@"update" cancelButtonTitle:nil];
                [self setLaunchStatus:@"Mothership refused access :("];
            } else {
                self.retryButton.hidden = NO;
                self.messageAction = @"no internet";
                [self showMessage:@"Check internet connection. If the problem persists, please check for app update." title:@"Failed to connect"];
                [self setLaunchStatus:@"Mothership connection failed :("];
            }
        }
    }];
}

- (void)continueToAppWithCompletion:(LauncherCompletionBlock)completion {
    [self hideBlockingWaitIndicator];
    if (completion) {
        [self dismissViewControllerAnimated:NO completion:nil];
        completion();
    } else {
        //debug log and log missing completion and avoid crash.
        [self setLaunchStatus:@"App launch failed."];
        [[ME_Logger singleton] log:@"App Delegate sent empty completion to launcher." level:ME_Log_Level_DEBUG];
    }
}

- (void)messageViewClosedWithCancel {
    if (self.completion) {
        [self continueToAppWithCompletion:self.completion];
    }
    [super messageViewClosedWithCancel];
}

- (void)messageViewClosedWithOK {
    if (self.messageAction) {
        if ([self.messageAction isEqualToString:@"no internet"]) {
            [super messageViewClosedWithOK];
            return;
        } else {
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString: self.messageAction]];
            if ([[ME_ConfigManager singleton] isAppSupported]) {
                if (self.completion) {
                    [self continueToAppWithCompletion:self.completion];
                }
            }
        }
    } else {
        if (self.completion) {
            [self continueToAppWithCompletion:self.completion];
        }
    }
    [super messageViewClosedWithOK];
}

@end
