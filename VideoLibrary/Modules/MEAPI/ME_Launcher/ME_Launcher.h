//
//  ME_Launcher.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-14.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^LauncherCompletionBlock) ();

@interface ME_Launcher : NSObject

+ (id)singleton;

- (void)showLauncherWithAppName:(NSString *)appName shortId:(NSString *)shortId appUrl:(NSString *)appUrl defaultPrefs:(NSDictionary *)defaultPrefs rootViewController:(UIViewController *)rootViewController withCompletion:(LauncherCompletionBlock)completion;

// App Delegate calls the launcher to initialize the app.
// Launcher will updateAndVerifyApp and execute the completion block provided by AppDelegate.
// if updateAndVerifyApp fails, that is app is no longer supported, launcher takes care of it and all other possible scenarios. hence only adding the launcher to an app will take care of launch event.
// launcher needs to be provided with the app name, version and completion block.
// launcher also will take care of showing the loading window and will close the window before handing off the control to the App delegate so the app can be started.
// Adding this launcher in app, adds
//  1. a launch screen with a loading indicator, app can provide an image to show as background, loading animation and app display name.
//  2. version check ability and check if app is supported.
//  3. a unified way to update common app preferences. (for app specific preferences, app will need to make use of the PrefsManager to ask for data dictionary for a particular key).
//  4. save and update common app preferences in PrefsManager.
//  5. make available app specific configurations by sending those over to ConfigManager to be stored with a key.
//  6. show update meessages if updates are available.
//  7. block app usage if app is no longer supported.
//  8. show service and info messages.
//  9. ask users to rate the app if not already rated.
//  10. initializes logger and logs app start with device, os and app version details.
//  11. performs any app specific update tasks that can be programmed by inheriting the PrefsManager class and overriding the executeTasksOnFirstLaunch function. This function will execute the programmed tasks once for a particular version and mark as tasks performed. This is usefull in scenarios where data migration or modification is needed for a new version to be compatible with settings from a previous version. An Empty implementation is called on every app launch.
//  12. starts user manager and creates a default user to save preferences.
@end
