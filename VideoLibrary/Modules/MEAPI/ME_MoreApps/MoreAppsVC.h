//
//  MoreAppsVC.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/10/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "RootViewController.h"
#import "AppDetailCell.h"
#import "types.h"

@interface MoreAppsVC : RootViewController <UITableViewDataSource, UITableViewDelegate, AppDetailDelegate>

@end
