//
//  AppDetailCell.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/10/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "AppDetailCell.h"
#import "AppDetail.h"
#import <UIImageView+AFNetworking.h>
#import "Utility.h"
#import <QuartzCore/QuartzCore.h>

@interface AppDetailCell()

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *appNameLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *appThumbnailImageView;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *appPriceLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *appArtistNameLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *appDescriptionLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *appVersionLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *shareButton;

@property (nonatomic, strong) AppDetail *appDetail;

@end

@implementation AppDetailCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) populateWithAppDetail:(AppDetail *)appDetail {
    _appDetail = appDetail;
    
    self.appThumbnailImageView.layer.cornerRadius = 12.0f;
    self.appThumbnailImageView.layer.masksToBounds = YES;
    
    self.appNameLabel.text = appDetail.appName;
    self.appArtistNameLabel.text = [NSString stringWithFormat:@"By: %@", appDetail.artistName];
    self.appDescriptionLabel.text = appDetail.appDescription;
    self.appPriceLabel.text = appDetail.price;
    self.appVersionLabel.text = [NSString stringWithFormat:@"Version: %@",appDetail.version];
    [self.appThumbnailImageView setImageWithURL:appDetail.thumbnailURL placeholderImage:[self placeHolderImage]];
}

#pragma mark - IBActions

- (IBAction)shareAction:(id)sender {
//    NSString *appUrl = [self.appDetail.appStoreURL absoluteString];
    
//    NSString *titleString = [NSString stringWithFormat:@"Check this app: %@", self.appDetail.appName];
        // TODO: share implementation
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(selectedAppDetail:AppIconImage:)]) {
            [self.delegate selectedAppDetail:self.appDetail AppIconImage:self.appThumbnailImageView.image];
        }
    }
}

- (IBAction)downloadAction:(id)sender {
    
    // Send the dimensions to Parse along with the 'search' event
    if (self.appDetail.appName && self.appDetail.appId) {
        NSString *stringAppId = [NSString stringWithFormat:@"%@",self.appDetail.appId];
//        [PFAnalytics trackEvent:@"moreApps" dimensions:@{@"event":@"download", @"appName":self.appDetail.appName, @"appId":stringAppId}];
    }
    
    [[UIApplication sharedApplication] openURL:self.appDetail.appStoreURL];
}

- (IBAction)showScreenshotsAction:(id)sender {
    
    NSArray *screenshotsArray = self.appDetail.screenShots;
    
    if (IS_IPAD) {
        screenshotsArray = self.appDetail.ipadScreenShots;
    }
    
    if (screenshotsArray.count == 0) {
        // if on ipad and iphone screenshots available, use those. and vice versa.
        if (IS_IPAD) {
            if (self.appDetail.screenShots.count == 0) {
                return;
            } else screenshotsArray = self.appDetail.screenShots;
        } else {
            if (self.appDetail.ipadScreenShots.count == 0) {
                return;
            } else screenshotsArray = self.appDetail.ipadScreenShots;
        }
    }
    
    NSURL *url1 = [NSURL URLWithString: screenshotsArray[0]];
    NSURL *url2 = [NSURL URLWithString: screenshotsArray[1]];
    NSURL *url3 = [NSURL URLWithString: screenshotsArray[2]];
    
    NSArray *array = @[url1, url2, url3];
    
    NSDictionary *userInfo = @{@"urls":array, @"title":self.appDetail.appName};
    
    if (userInfo) {
        NSNotification *notif = [NSNotification notificationWithName:NOTIFICATION_SHOW_PREVIEW_IMAGES object:nil userInfo:userInfo];
        [[NSNotificationCenter defaultCenter] postNotification:notif];
    }
    
}

- (IBAction)showDeveloperPageAction:(id)sender {
    [[UIApplication sharedApplication] openURL:self.appDetail.artistViewURL];
}

#pragma mark - helper methods

-(UIImage *) placeHolderImage {
    return [UIImage imageNamed:@"placeholder.png"];
}

@end
