//
//  AppDetailCell.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/10/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AppDetail;

@protocol AppDetailDelegate <NSObject>

-(void)selectedAppDetail:(AppDetail *)appDetail AppIconImage:(UIImage *)image;

@end

@interface AppDetailCell : UITableViewCell

@property (nonatomic, weak) id<AppDetailDelegate> delegate;

-(void) populateWithAppDetail:(AppDetail *)appDetail;

@end
