//
//  AppDetail.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/10/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "AppDetail.h"

@implementation AppDetail

-(id) initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        
        _appName = dictionary[@"trackName"];
        _appId = [NSString stringWithFormat:@"%@", dictionary[@"trackId"]];
        _artistName = dictionary[@"artistName"];
        
        NSString *appStoreURLString = dictionary[@"trackViewUrl"];
        if (appStoreURLString) {
            _appStoreURL = [NSURL URLWithString:appStoreURLString];
        }
        
        NSString *thumbnailUrlString = dictionary[@"artworkUrl60"];
        if (thumbnailUrlString) {
            _thumbnailURL = [NSURL URLWithString:thumbnailUrlString];
        } else _thumbnailURL = nil;
        
        _appDescription = dictionary[@"description"];
        
        _screenShots = dictionary[@"screenshotUrls"];
        _ipadScreenShots = dictionary[@"ipadScreenshotUrls"];
        
        _supportedDevices = dictionary[@"supportedDevices"];
        _version = dictionary[@"version"];
        _price = dictionary[@"formattedPrice"];

        NSString *artistViewString = dictionary[@"artistViewUrl"];
        if (artistViewString) {
            _artistViewURL = [NSURL URLWithString:artistViewString];
        } else _artistViewURL = nil;
        
        _avgRating = dictionary[@"averageUserRating"];
        _avgRatingForCurrentVersion = dictionary[@"averageUserRatingForCurrentVersion"];
    }
    return self;
}

@end
