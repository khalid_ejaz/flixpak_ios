//
//  AppDetail.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/10/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppDetail : NSObject

@property (nonatomic, strong) NSString *appName;
@property (nonatomic, strong) NSString *appId;
@property (nonatomic, strong) NSURL *appStoreURL;
@property (nonatomic, strong) NSURL *thumbnailURL;
@property (nonatomic, strong) NSString *appDescription;
@property (nonatomic, strong) NSArray *screenShots;
@property (nonatomic, strong) NSArray *ipadScreenShots;
@property (nonatomic, strong) NSArray *supportedDevices;
@property (nonatomic, strong) NSString *artistName;
@property (nonatomic, strong) NSString *version;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSURL *artistViewURL;
@property (nonatomic, strong) NSNumber *avgRating;
@property (nonatomic, strong) NSNumber *avgRatingForCurrentVersion;

-(id) initWithDictionary:(NSDictionary *)dictionary;

@end
