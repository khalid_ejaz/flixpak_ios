//
//  MoreAppsVC.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/10/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "MoreAppsVC.h"
#import "AppDetail.h"
#import "UserManager.h"

#define MORE_APP_IDS_URL                @"http://www.anmaple.com/appIds.plist"
#define ITUNES_STORE_BASE_LOOKUP_URL    @"https://itunes.apple.com/lookup?id="
#define APP_DETAIL_CELL_ID              @"appDetailCellId"
@interface MoreAppsVC ()

@property (unsafe_unretained, nonatomic) IBOutlet UITableView *tableView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *headerView;

@property (nonatomic, strong) NSOperationQueue *remoteDataLoadQueue;
@property (nonatomic, strong) NSMutableArray *appIds;
@property (nonatomic, strong) NSMutableArray *appsData;

@end

@implementation MoreAppsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setRemoteDataLoadQueue:[[NSOperationQueue alloc] init]];
    self.headerView.backgroundColor = [[UserManager sharedManager] colorForUser];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"AppDetailCell" bundle:nil] forCellReuseIdentifier:APP_DETAIL_CELL_ID];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self showSpinner];
    [self loadAppIds];
}

#pragma mark - data load.

-(void) loadAppIds {
    self.appIds = [NSMutableArray array];
    self.appsData = [NSMutableArray array];
    
    [self readArraywithUrl:MORE_APP_IDS_URL completion:^(NSArray *objects, NSError *error) {
        if (error) {
            [self showNetworkError];
        } else {
            self.appIds = [objects mutableCopy];
            [self loadDataDictionaries];
        }
    }];
}

-(void) loadDataDictionaries {
    
    if (self.appIds.count == 0) {
        [self showGeneralError];
        return;
    }
    
    NSString *commaSeparatedAppIds = [self.appIds componentsJoinedByString:@","];
    NSString *url = [NSString stringWithFormat:@"%@%@&entity=software", ITUNES_STORE_BASE_LOOKUP_URL, commaSeparatedAppIds];
    [self readDictionarywithUrl:url completion:^(NSMutableDictionary *dictionary, NSError *error) {
        if (error) {
            [self showNetworkError];
        } else {
            NSArray *dicts = dictionary[@"results"];
            self.appsData = [NSMutableArray array];
            for (NSDictionary *appDict in dicts) {
                
                AppDetail *detail = [[AppDetail alloc] initWithDictionary:appDict];
                [self.appsData addObject:detail];
            }
            [self.tableView reloadData];
        }
        [self hideSpinner];
    }];
}

#pragma mark - IBActions

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - table view data source.

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.appsData.count;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AppDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:APP_DETAIL_CELL_ID];
    [cell populateWithAppDetail:[self.appsData objectAtIndex:indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.delegate = self;
    return cell;
}

#pragma mark - table view delegate.

#pragma mark - helper methods.

-(void) readArraywithUrl:(NSString *)url completion:(ArrayAndErrorBlock)completion {
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:self.remoteDataLoadQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([data length] > 0 && error == nil)
                // downloaded as NSData.
                [self executeCompletion:completion withData:data];
            else if ([data length] == 0 && error == nil)
                // empty reply.
                completion(nil, nil);
            else if (error != nil && error.code == NSURLErrorTimedOut)
                // times out.
                completion(nil, nil);
            else if (error != nil)
                // download error.
                completion(nil, error);
        });
    }];
}

-(void) executeCompletion:(ArrayAndErrorBlock)completion withData:(NSData *)data {
    NSError *error;
    NSPropertyListFormat plistFormat;
    NSArray *temp = [NSPropertyListSerialization propertyListWithData:data options:NSPropertyListImmutable format:&plistFormat error:&error];
    
    completion(temp, nil);
}

-(NSMutableDictionary *)dictionaryFromJSONData:(NSData *)data {
    NSError *parseError;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
    if (dict) {
        return [dict mutableCopy];
    }
    return nil;
}

-(void) readDictionarywithUrl:(NSString *)url completion:(MutableDictionaryAndErrorBlock)completion {
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:self.remoteDataLoadQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([data length] > 0 && error == nil) {
                // downloaded as NSData.
                NSDictionary *resultDictionary = [self dictionaryFromJSONData:data];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (resultDictionary) {
                        completion([resultDictionary mutableCopy], nil);
                    } else completion(nil, error);
                });
            }
            else if ([data length] == 0 && error == nil)
                // empty reply.
                completion(nil, nil);
            else if (error != nil && error.code == NSURLErrorTimedOut)
                // times out.
                completion(nil, nil);
            else if (error != nil)
                // download error.
                completion(nil, error);
        });
    }];
}

#pragma mark - App detail cell delegate

-(void) selectedAppDetail:(AppDetail *)appDetail AppIconImage:(UIImage *)image {
    NSString *titleString = [NSString stringWithFormat:@"I saw this app '%@' and thought you might like it. Check it out! ", appDetail.appName];
    NSURL *url = appDetail.appStoreURL;
    [self shareText:titleString andImage:image andUrl:url];
}

- (void)shareText:(NSString *)text andImage:(UIImage *)image andUrl:(NSURL *)url
{
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    if (text) {
        [sharingItems addObject:text];
    }
    if (image) {
        [sharingItems addObject:image];
    }
    if (url) {
        [sharingItems addObject:url];
    }
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:nil];
}


@end
