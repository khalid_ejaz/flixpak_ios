//
//  ConfigManager.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-15.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

//
//  Stores common app configurations that are received from malikejaz.com.
//  ----------------------------------------------------------------------

#import <Foundation/Foundation.h>

typedef void (^ConfigLoadCompletionBlock) (BOOL success);

@interface ME_ConfigManager : NSObject

+ (id)singleton;
- (void)initializeWithAppName:(NSString *)appName shortId:(NSString *)shortId appUrl:(NSString *)appUrl withCompletion:(ConfigLoadCompletionBlock)completion;
- (BOOL)isAppSupported;
- (BOOL)isUpdateAvailable;
- (BOOL)isThereMessageForUser;
- (BOOL)isAdsEnabled;
- (BOOL)adsAllowedOnScreen:(NSString *)screenName;

- (NSString *)latestVersion;
- (NSString *)latestVersionLink;

- (BOOL)isConnectedOverWiFi;
- (BOOL)isConnectedOverCellular;

- (NSString *)appName;
- (NSString *)shortAppId;
- (NSString *)appUrl;
- (NSDictionary *)messageForUser;
- (NSDictionary *)config;
- (NSString *)supportEmail;
- (NSString *)aboutApp;
- (NSString *)developerName;

- (NSString *) appVersion;
- (NSString *) appBuildNumber;

- (NSString *)deviceName;
- (NSString *)deviceOSVersion;
- (NSString *)deviceCountry;


@end
