//
//  ConfigManager.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-15.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "ME_ConfigManager.h"
#import "ME_Logger.h"
#import "ME_HTTPClient.h"
#import <sys/sysctl.h>
#import <Reachability.h>

@interface ME_ConfigManager()

@property (nonatomic, strong) NSString *appName;
@property (nonatomic, strong) NSString *shortAppId;
@property (nonatomic, strong) NSString *appUrl;

@property (nonatomic, assign) BOOL badConfigSyntax;
@property (nonatomic, assign) BOOL supportedVersion;
@property (nonatomic, assign) BOOL updateAvailable;
@property (nonatomic, assign) BOOL haveMessageForUser;
@property (nonatomic, assign) BOOL adsEnabled;
@property (nonatomic, strong) NSString *linkForLatestVersion;

@property (nonatomic, strong) NSDictionary *config;
@property (nonatomic, strong) NSDictionary *message;
@property (nonatomic, strong) NSDictionary *ads;
@property (nonatomic, strong) NSString *latestVersion;
@property (nonatomic, strong) NSString *latestVersionLink;

@property (nonatomic, strong) NSString *country;

@property (nonatomic, strong) Reachability *reachability;

@end

@implementation ME_ConfigManager

+ (id)singleton {
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    // returns the same object each time
    return _sharedObject;
}

- (void)initializeWithAppName:(NSString *)appName shortId:(NSString *)shortId appUrl:(NSString *)appUrl withCompletion:(ConfigLoadCompletionBlock)completion {
    _country = [self deviceLocaleCountry];
    _appName = appName;
    _shortAppId = shortId;
    _appUrl = appUrl;
    [[ME_Logger singleton] log:@"Config Manager init."];
    [self setupReachability];
    
    // read configs from malikejaz.com and update.
    // TODO: use cached API client to use cached version of GET and do network call only when cache not available or expired.
    NSString *path = [NSString stringWithFormat:@"https://www.malikejaz.com/%@/ios/config.json", shortId];
    [[ME_HTTPClient singleton] GETRoute:path headers:nil withCompletion:^(NSDictionary *dictionary, NSError *error) {
        if (error) {
            // disable app usage.
            // TODO: Instead of disabling app usage, use cached version if available. cache expiry can be set to one day. So if the server is down, i will have one day to get it back up before existing apps will become unusable.
            if (completion)completion(false);
        } else {
            // process config.json
            [self processConfig:dictionary];
            if (completion) {
                if (self.badConfigSyntax) {
                    completion(NO);
                } else {
                    [[ME_Logger singleton] log:@"Config Manager initialized." level:ME_Log_Level_INFO];
                    if (self.supportedVersion) {
                        completion(YES);
                    } else completion(NO);
                }
            } else {
                [[ME_Logger singleton] log:@"Completion not sent to config initialize." level:ME_Log_Level_DEBUG];
            }
        }
    }];
}

- (NSString *)appName {
    return _appName;
}

- (BOOL)isAppSupported {
    if (self.badConfigSyntax) return NO;
    return self.supportedVersion;
}

- (BOOL)isUpdateAvailable {
    return self.updateAvailable;
}

- (BOOL)isThereMessageForUser {
    return self.haveMessageForUser;
}

- (BOOL)isAdsEnabled {
    return self.adsEnabled;
}

- (BOOL)isConnectedOverWiFi {
    if ([self.reachability isReachableViaWiFi]) {
        return YES;
    } else return NO;
}

- (BOOL)isConnectedOverCellular {
    if ([self.reachability isReachableViaWWAN]) {
        return YES;
    } else return NO;
}

- (NSDictionary *)messageForUser {
    return self.message;
}

- (NSDictionary *)config {
    return _config;
}

- (NSString *)latestVersionLink {
    return _linkForLatestVersion;
}

#pragma mark - internal methods.

-(void) setupReachability {
    self.reachability = [Reachability reachabilityWithHostname:@"www.google.com"];
    self.reachability.reachableBlock = ^(Reachability *reachability) {
        [[ME_Logger singleton] log:@"Internet connected."];
    };
    self.reachability.unreachableBlock = ^(Reachability *reachability) {
        [[ME_Logger singleton] log:@"Internet disconnected."];
    };
    // Start Monitoring
    [self.reachability startNotifier];
}

// ME_AdManager will check for ad config on its own by requesting the config dictionary and checking for ads dictionary inside it.
// this is because not all apps will have ads so this functionality will be in AdManager to be added to app if needed.
- (void)processConfig:(NSDictionary *)config {
    
    _config = config;
    
    // check access control.
    if ([config.allKeys containsObject:@"access_control"]) {
    
        // check supported app version.
        NSDictionary *accessControl = config[@"access_control"];
        if (![accessControl.allKeys containsObject:@"latest_version"]) {
            return [self saveAsBadConfig];
        }
        if (![accessControl.allKeys containsObject:@"last_supported_version"]) {
            return [self saveAsBadConfig];
        }
        if (![accessControl.allKeys containsObject:@"latest_version_link"]) {
            return [self saveAsBadConfig];
        }
        NSString *bld = [self appBuildNumber];
        int build = [bld intValue];
        int lastSupportedBuildVersion = [accessControl[@"last_supported_version"] intValue];
        int latestBuildVersion = [accessControl[@"latest_version"] intValue];
        NSString *linkForLatest = accessControl[@"latest_version_link"];
        _linkForLatestVersion = linkForLatest;
        
        if (build == latestBuildVersion) {
            self.supportedVersion = YES;
            self.updateAvailable = NO;
        } else if (build < lastSupportedBuildVersion) {
            self.supportedVersion = NO;
            self.updateAvailable = YES;
        } else {
            self.supportedVersion = YES;
            self.updateAvailable = NO;
        }
        
    } else return [self saveAsBadConfig];
    
    // check for ads.
    if ([config.allKeys containsObject:@"ads"]) {
        NSDictionary *ads = config[@"ads"];
        
        if (![ads.allKeys containsObject:@"app"]) {
            _adsEnabled = NO;
        } else {
            BOOL adsAllowedAppWide = [ads[@"app"] boolValue];
            _adsEnabled = adsAllowedAppWide;
            if (adsAllowedAppWide) {
                if ([ads.allKeys containsObject:[self appBuildNumber]]) {
                    _ads = ads[[self appBuildNumber]];
                } else return [self saveAsBadConfig];
            }
        }
    }
    
    // check info message.
    if ([config.allKeys containsObject:@"message"]) {
        
        // check if message for all versions or for this version.
        NSDictionary *messageDict = config[@"message"];
        if ([messageDict.allKeys containsObject:@"app"]) {
            // app-wide message.
            _message = messageDict[@"app"];
            _haveMessageForUser = TRUE;
            
        } else {
            // check for version specific message.
            if ([messageDict.allKeys containsObject:[self appBuildNumber]]) {
                // message is for this app version.
                _message = messageDict[[self appBuildNumber]];
                _haveMessageForUser = TRUE;
            }
        }
    }
}

- (void)saveAsBadConfig {
    self.supportedVersion = FALSE;
    self.badConfigSyntax = TRUE;
    NSString *logMsg = @"Bad configuration from mothership. All access blocked.";
    [[ME_Logger singleton] log:logMsg level:ME_Log_Level_FATAL];
}

- (BOOL)adsAllowedOnScreen:(NSString *)screenName {
    if (_ads) {
        if ([self.ads.allKeys containsObject:screenName]) {
            NSNumber *allowed = self.ads[screenName];
            if ([allowed  isEqual: @1]) return YES;
            return NO;
        }
        // have to allow on each screen individually.
        return NO;
    }
    return NO;
}

- (NSString *)shortAppId {
    return _shortAppId;
}

- (NSString *) appBuildNumber {
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
}

- (NSString *) appVersion {
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
}

- (NSString *)appUrl {
    return _appUrl;
}

- (NSString *)supportEmail {
    return @"malik@malikejaz.com";
}

- (NSString *)developerName {
    return @"FlixPak";
}

- (NSString *)aboutApp {
    return [NSString stringWithFormat:@"%@, version: %@.", [self appName], [self appVersion]];
}

#pragma mark - Device related

- (NSString *)deviceName {
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
//    NSLog(@"iPhone Device%@",[self platformType:platform]);
    free(machine);
    return platform;
    
}

- (NSString *)deviceOSVersion {
    return [[UIDevice currentDevice] systemVersion];
}

- (NSString *)deviceCountry {
    return _country;
}

- (NSString *)deviceLocaleCountry {
    NSLocale *locale = [NSLocale currentLocale];
    NSString *country = [locale displayNameForKey:NSLocaleIdentifier value:[locale localeIdentifier]];
    return country;
}

//func platformType(platform : NSString) -> NSString{
//    if platform.isEqualToString("iPhone1,1")  {
//        return "iPhone 1G"
//    }
//    else if platform.isEqualToString("iPhone1,2"){
//        return "iPhone 3G"
//    }
//    else if platform.isEqualToString("iPhone2,1"){
//        return "iPhone 3GS"
//    }
//    else if platform.isEqualToString("iPhone3,1"){
//        return "iPhone 4"
//    }
//    else if platform.isEqualToString("iPhone3,3"){
//        return "Verizon iPhone 4"
//    }
//    else if platform.isEqualToString("iPhone4,1"){
//        return "iPhone 4S"
//    }
//    else if platform.isEqualToString("iPhone5,1"){
//        return "iPhone 5 (GSM)"
//    }
//    else if platform.isEqualToString("iPhone5,2"){
//        return "iPhone 5 (GSM+CDMA)"
//    }
//    else if platform.isEqualToString("iPhone5,3"){
//        return "iPhone 5c (GSM)"
//    }
//    else if platform.isEqualToString("iPhone5,4"){
//        return "iPhone 5c (GSM+CDMA)"
//    }
//    else if platform.isEqualToString("iPhone6,1"){
//        return "iPhone 5s (GSM)"
//    }
//    else if platform.isEqualToString("iPhone6,2"){
//        return "iPhone 5s (GSM+CDMA)"
//    }
//    else if platform.isEqualToString("iPhone7,2"){
//        return "iPhone 6"
//    }
//    else if platform.isEqualToString("iPhone7,1"){
//        return "iPhone 6 Plus"
//    }
//    else if platform.isEqualToString("iPhone8,1"){
//        return "iPhone 6s"
//    }
//    else if platform.isEqualToString("iPhone8,2"){
//        return "iPhone 6s Plus"
//    }
//    else if platform.isEqualToString("iPhone8,4"){
//        return "iPhone SE"
//    }
//    else if platform.isEqualToString("iPhone9,1"){
//        return "iPhone 7"
//    }
//    else if platform.isEqualToString("iPhone9,2"){
//        return "iPhone 7 Plus"
//    }
//    else if platform.isEqualToString("iPod1,1"){
//        return "iPod Touch 1G"
//    }
//    else if platform.isEqualToString("iPod2,1"){
//        return "iPod Touch 2G"
//    }
//    else if platform.isEqualToString("iPod3,1"){
//        return "iPod Touch 3G"
//    }
//    else if platform.isEqualToString("iPod4,1"){
//        return "iPod Touch 4G"
//    }
//    else if platform.isEqualToString("iPod5,1"){
//        return "iPod Touch 5G"
//    }
//    else if platform.isEqualToString("iPad1,1"){
//        return "iPad"
//    }
//    else if platform.isEqualToString("iPad2,1"){
//        return "iPad 2 (WiFi)"
//    }
//    else if platform.isEqualToString("iPad2,2"){
//        return "iPad 2 (GSM)"
//    }
//    else if platform.isEqualToString("iPad2,3"){
//        return "iPad 2 (CDMA)"
//    }
//    else if platform.isEqualToString("iPad2,4"){
//        return "iPad 2 (WiFi)"
//    }
//    else if platform.isEqualToString("iPad2,5"){
//        return "iPad Mini (WiFi)"
//    }
//    else if platform.isEqualToString("iPad2,6"){
//        return "iPad Mini (GSM)"
//    }
//    else if platform.isEqualToString("iPad2,7"){
//        return "iPad Mini (GSM+CDMA)"
//    }
//    else if platform.isEqualToString("iPad3,1"){
//        return "iPad 3 (WiFi)"
//    }
//    else if platform.isEqualToString("iPad3,2"){
//        return "iPad 3 (GSM+CDMA)"
//    }
//    else if platform.isEqualToString("iPad3,3"){
//        return "iPad 3 (GSM)"
//    }
//    else if platform.isEqualToString("iPad3,4"){
//        return "iPad 4 (WiFi)"
//    }
//    else if platform.isEqualToString("iPad3,5"){
//        return "iPad 4 (GSM)"
//    }
//    else if platform.isEqualToString("iPad3,6"){
//        return "iPad 4 (GSM+CDMA)"
//    }
//    else if platform.isEqualToString("iPad4,1"){
//        return "iPad Air (WiFi)"
//    }
//    else if platform.isEqualToString("iPad4,2"){
//        return "iPad Air (Cellular)"
//    }
//    else if platform.isEqualToString("iPad4,3"){
//        return "iPad Air"
//    }
//    else if platform.isEqualToString("iPad4,4"){
//        return "iPad Mini 2G (WiFi)"
//    }
//    else if platform.isEqualToString("iPad4,5"){
//        return "iPad Mini 2G (Cellular)";}
//    else if platform.isEqualToString("iPad4,6"){
//        return "iPad Mini 2G";
//    }
//    else if platform.isEqualToString("iPad4,7"){
//        return "iPad Mini 3 (WiFi)"
//    }
//    else if platform.isEqualToString("iPad4,8"){
//        return "iPad Mini 3 (Cellular)";}
//    else if platform.isEqualToString("iPad4,9"){
//        return "iPad Mini 3 (China)"
//    }
//    else if platform.isEqualToString("iPad5,3"){
//        return "iPad Air 2 (WiFi)"
//    }
//    else if platform.isEqualToString("iPad5,4"){
//        return "iPad Air 2 (Cellular)"
//    }
//    else if platform.isEqualToString("AppleTV2,1"){
//        return "Apple TV 2G"
//    }
//    else if platform.isEqualToString("AppleTV3,1"){
//        return "Apple TV 3"
//    }
//    else if platform.isEqualToString("AppleTV3,2"){
//        return "Apple TV 3 (2013)"
//    }
//    else if platform.isEqualToString("i386"){
//        return "Simulator"
//    }
//    else if platform.isEqualToString("x86_64"){
//        return "Simulator"
//    }
//    return platform
//}

@end
