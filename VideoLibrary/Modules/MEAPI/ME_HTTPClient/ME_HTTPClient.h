//
//  MEAPIClient.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-14.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//
//  Generic HTTP Client.
//  --------------------

#import <Foundation/Foundation.h>
#import "types.h"

@interface ME_HTTPClient : NSObject

+ (id)singleton;
- (void)initialize;

- (void)GETRoute:(NSString *)route headers:(NSDictionary *)headers withCompletion:(DictionaryAndErrorBlock)completion;
- (void)POSTRoute:(NSString *)route headers:(NSDictionary *)headers form:(NSString *)form withCompletion:(DictionaryAndErrorBlock)completion;
- (void)PUTRoute:(NSString *)route headers:(NSDictionary *)headers form:(NSString *)form withCompletion:(DictionaryAndErrorBlock)completion;
- (void)DELETERoute:(NSString *)route headers:(NSDictionary *)headers form:(NSString *)form withCompletion:(DictionaryAndErrorBlock)completion;

@end
