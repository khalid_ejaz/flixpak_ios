//
//  MEAPIClient.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-14.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "ME_HTTPClient.h"
#import "ME_Logger.h"

@implementation ME_HTTPClient

+ (id)singleton {
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    // returns the same object each time
    return _sharedObject;
}

- (void)initialize {
    
}

#pragma mark - interface methods

- (void)GETRoute:(NSString *)route headers:(NSDictionary *)headers withCompletion:(DictionaryAndErrorBlock)completion {
    NSMutableURLRequest *request = [self URLRequestWithPath:route headers:headers];
    [request setHTTPMethod:@"GET"];
    [self doRequest:request completion:completion];
}

- (void)POSTRoute:(NSString *)route headers:(NSDictionary *)headers form:(NSString *)form withCompletion:(DictionaryAndErrorBlock)completion {
    NSMutableURLRequest *request = [self URLRequestWithPath:route headers:headers];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[form dataUsingEncoding:NSUTF8StringEncoding]];
    [self doRequest:request completion:completion];
}

- (void)PUTRoute:(NSString *)route headers:(NSDictionary *)headers form:(NSString *)form withCompletion:(DictionaryAndErrorBlock)completion {
    NSMutableURLRequest *request = [self URLRequestWithPath:route headers:headers];
    [request setHTTPMethod:@"PUT"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[form dataUsingEncoding:NSUTF8StringEncoding]];
    [self doRequest:request completion:completion];
}

- (void)DELETERoute:(NSString *)route headers:(NSDictionary *)headers form:(NSString *)form withCompletion:(DictionaryAndErrorBlock)completion {
    NSMutableURLRequest *request = [self URLRequestWithPath:route headers:headers];
    [request setHTTPMethod:@"DELETE"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[form dataUsingEncoding:NSUTF8StringEncoding]];
    [self doRequest:request completion:completion];
}

#pragma mark - internal methods

- (NSMutableURLRequest *)URLRequestWithPath:(NSString *)path headers:(NSDictionary *)headers {
//    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    NSString *completeURL = [NSString stringWithFormat:@"%@/%@/%@",API_ROOT_URL,API_VER,path];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:path] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30.0];
    
    // Add header fields if provided.
    if (headers) {
        for (NSString *key in headers.allKeys) {
            [urlRequest addValue:headers[key] forHTTPHeaderField:key];
        }
    }
    
//    [urlRequest addValue:CLIENT_KEY forHTTPHeaderField:CLIENT_KEY_FIELD];
//    [urlRequest addValue:@"iOS" forHTTPHeaderField:@"ahos"];
//    [urlRequest addValue:@"10.02" forHTTPHeaderField:@"ahosversion"];
//    [urlRequest addValue:@"Malik's iPhone" forHTTPHeaderField:@"ahdevice"];
//    [urlRequest addValue:@"mgmt_v1_0" forHTTPHeaderField:@"ahappversion"];
    
    return urlRequest;
}

// TODO: Cleanup here.
- (void)doRequest:(NSURLRequest *)request completion:(DictionaryAndErrorBlock)completion {
    if (!completion) {
        [[ME_Logger singleton] log:@"No completion block for request." level:ME_Log_Level_DEBUG];
    }
    NSString *log = [NSString stringWithFormat:@"%@: %@", request.HTTPMethod, [[request URL] absoluteString]];
    [[ME_Logger singleton] log:log];
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
//                [self.delegate apiErrorOccured:error];
                [[ME_Logger singleton] logError:error];
                if (completion) completion(nil, error);
            } else {
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                NSMutableDictionary *dict = [self dictionaryFromJSONData:data];
                if (httpResponse.statusCode == 200) {
                    // success response.
                    if (completion) completion(dict, nil);
                } else {
                    // response is not 200 and no error.
                    NSDictionary *dict = [self dictionaryFromJSONData:data];
                    NSError *apiError = [NSError errorWithDomain:@"ME_PV_MGMT_API" code:httpResponse.statusCode userInfo:dict[@"debug"][@"error"]];
//                    [self.delegate apiErrorOccured:apiError];
                    if (completion) completion(nil, apiError);
                }
            }
        });
    }] resume];
}

- (NSMutableDictionary *)dictionaryFromJSONData:(NSData *)data {
    NSError *parseError;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
    if (dict) {
        return [dict mutableCopy];
    }
    return nil;
}

@end
