//
//  ME_WebAPIClient.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-16.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "ME_WebAPIClient.h"
#import "ME_Logger.h"

@implementation ME_WebAPIClient

+ (id)singleton {
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    // returns the same object each time
    return _sharedObject;
}

- (void)GETRoute:(NSString *)route rootURL:(NSString *)rootURL queryParams:(NSDictionary *)queryParams headers:(NSDictionary *)headers cachePolicy:(ME_Cache_Policy)cachePolicy withCompletion:(DictionaryAndErrorBlock)completion {
    NSString *path = [rootURL stringByAppendingPathComponent:route];
    
    if (queryParams) {
        if (queryParams.allKeys.count > 0) {
            for (int i=0; i<queryParams.allKeys.count; i++) {
                if (i == 0) {
                    path = [path stringByAppendingString:@"?"];
                } else {
                    path = [path stringByAppendingString:@"&"];
                }
                NSString *key = queryParams.allKeys[i];
                key = [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSString *value = [NSString stringWithFormat:@"%@", queryParams[key]];
                value = [value stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                path = [path stringByAppendingFormat:@"%@=%@", key, value];
            }
        }
    }
    
    // TODO: check if cached version available.
    if (cachePolicy == ME_CACHE_IGNORE) {
        [[ME_HTTPClient singleton] GETRoute:path headers:headers withCompletion:completion];
    } else if (cachePolicy == ME_CACHE_IS_ENOUGH) {
        // check in cache, if unexpired version available, return that.
        [[ME_Logger singleton] log:@"ME_CACHE_IS_ENOUGH policy not supported yet. reverting to ME_CACHE_IGNORE" level:ME_Log_Level_DEBUG];
        [[ME_HTTPClient singleton] GETRoute:path headers:headers withCompletion:completion];
    } else if (cachePolicy == ME_CACHE_THEN_CURRENT) {
        // return cached version, then update with current version. execute completion twice.
        [[ME_Logger singleton] log:@"ME_CACHE_THEN_CURRENT policy not supported yet. reverting to ME_CACHE_IGNORE" level:ME_Log_Level_DEBUG];
        [[ME_HTTPClient singleton] GETRoute:path headers:headers withCompletion:completion];
    } else {
        [[ME_Logger singleton] log:@"Unsupported cache policy asked. reverting to ME_CACHE_IGNORE" level:ME_Log_Level_DEBUG];
        [[ME_HTTPClient singleton] GETRoute:path headers:headers withCompletion:completion];
    }
}

- (void)POSTRoute:(NSString *)route rootURL:(NSString *)rootURL headers:(NSDictionary *)headers form:(NSString *)form withCompletion:(DictionaryAndErrorBlock)completion {
    NSString *path = [rootURL stringByAppendingPathComponent:route];
    [[ME_HTTPClient singleton] POSTRoute:path headers:headers form:form withCompletion:completion];
}

- (void)PUTRoute:(NSString *)route rootURL:(NSString *)rootURL headers:(NSDictionary *)headers form:(NSString *)form withCompletion:(DictionaryAndErrorBlock)completion {
    NSString *path = [rootURL stringByAppendingPathComponent:route];
    [[ME_HTTPClient singleton] PUTRoute:path headers:headers form:form withCompletion:completion];
}

- (void)DELETERoute:(NSString *)route rootURL:(NSString *)rootURL headers:(NSDictionary *)headers form:(NSString *)form withCompletion:(DictionaryAndErrorBlock)completion {
    NSString *path = [rootURL stringByAppendingPathComponent:route];
    [[ME_HTTPClient singleton] DELETERoute:path headers:headers form:form withCompletion:completion];
}

@end
