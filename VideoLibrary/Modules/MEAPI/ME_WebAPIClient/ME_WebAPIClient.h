//
//  ME_WebAPIClient.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-16.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//
//  Provides caching of GET requests.
//  Generates full route strings for HTTP Client.
//

#import <Foundation/Foundation.h>
#import "ME_HTTPClient.h"
#import "ME_CacheManager.h"

@interface ME_WebAPIClient : NSObject

+ (id)singleton;

- (void)GETRoute:(NSString *)route rootURL:(NSString *)rootURL queryParams:(NSDictionary *)queryParams headers:(NSDictionary *)headers cachePolicy:(ME_Cache_Policy)cachePolicy withCompletion:(DictionaryAndErrorBlock)completion;
- (void)POSTRoute:(NSString *)route rootURL:(NSString *)rootURL headers:(NSDictionary *)headers form:(NSString *)form withCompletion:(DictionaryAndErrorBlock)completion;
- (void)PUTRoute:(NSString *)route rootURL:(NSString *)rootURL headers:(NSDictionary *)headers form:(NSString *)form withCompletion:(DictionaryAndErrorBlock)completion;
- (void)DELETERoute:(NSString *)route rootURL:(NSString *)rootURL headers:(NSDictionary *)headers form:(NSString *)form withCompletion:(DictionaryAndErrorBlock)completion;

@end
