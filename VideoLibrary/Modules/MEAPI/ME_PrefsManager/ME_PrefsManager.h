//
//  ME_PrefsManager.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-14.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//
//  User Preferences (per user by making use of user prefix from UserManager) for different options provided by the app.
//  ---------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>

@interface ME_PrefsManager : NSObject

+ (id)singleton;
- (void)initializeWithDefaults:(NSDictionary *)defaults;
- (void)resetDefaults;

- (void)executeTasksOnFirstLaunch;

- (NSArray *)arrayPrefsForKey:(NSString *)key;
- (NSDictionary *)dictionaryPrefsForKey:(NSString *)key;
- (NSString *)stringPrefForKey:(NSString *)key;
- (int)intPrefForKey:(NSString *)key;
- (NSNumber *)numberPrefForKey:(NSString *)key;
- (BOOL)boolPrefForKey:(NSString *)key;

- (void)saveArrayPrefs:(NSArray *)array forKey:(NSString *)key;
- (void)saveDictionaryPrefs:(NSDictionary *)dict forKey:(NSString *)key;
- (void)saveStringPrefs:(NSString *)string forKey:(NSString *)key;
- (void)saveNumberPrefs:(NSNumber *)number forKey:(NSString *)key;
- (void)saveIntPrefs:(int)number forKey:(NSString *)key;
- (void)saveBOOLPrefs:(BOOL)flag forKey:(NSString *)key;

- (void)setAppLanguages:(NSArray *)langs;
- (void)setPrefferedLanguage:(NSString *)lang;
- (NSString *)preferredLanguage;

@end
