//
//  ME_PrefsManager.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-14.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "ME_PrefsManager.h"
#import "ME_UserManager.h"

#define KEY_SUPPORTED_APP_LANGUAGES @"supported-app-languages"
#define KEY_PREFFERED_APP_LANGUAGE  @"preffered-app-language"
#define KEY_Defaults    @"kKey_Defaults"

@implementation ME_PrefsManager

+ (id)singleton {
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    // returns the same object each time
    return _sharedObject;
}

- (void)initializeWithDefaults:(NSDictionary *)defaults {
    if (!defaults) return;
    
    [self savePrefsObject:defaults forKey:KEY_Defaults];
    for (NSString *key in defaults.allKeys) {
        if (![self havePrefsForKey:key]) {
            NSObject *obj = defaults[key];
            [self savePrefsObject:obj forKey:key];
        }
    }
}

- (BOOL)havePrefsForKey:(NSString *)key {
    return [[[NSUserDefaults standardUserDefaults] dictionaryRepresentation].allKeys containsObject:[self keyByAddingUserPrefix:key]];
}

- (void)resetDefaults {
    NSDictionary *defaults = [[self prefsObjectForKey:KEY_Defaults] copy];
    if (!defaults) return;
    // override with defaults
    for (NSString *key in defaults.allKeys) {
        NSObject *obj = defaults[key];
        [self savePrefsObject:obj forKey:key];
    }
}

#pragma mark - higher level interface methods

- (void)setSupportedAppLanguages:(NSArray *)langs {
    [self savePrefsObject:langs forKey:KEY_SUPPORTED_APP_LANGUAGES];
}

- (NSArray *)supportedAppLanguages {
    return (NSArray *)[self prefsObjectForKey:KEY_SUPPORTED_APP_LANGUAGES];
}

- (void)setPrefferedLanguage:(NSString *)lang {
    [self savePrefsObject:lang forKey:KEY_PREFFERED_APP_LANGUAGE];
}

- (NSString *)preferredLanguage {
    return (NSString *)[self prefsObjectForKey:KEY_PREFFERED_APP_LANGUAGE];
}

#pragma mark - primitive interface methods

- (NSString *)stringPrefForKey:(NSString *)key {
    return (NSString *)[self prefsObjectForKey:key];
}

- (NSDictionary *)dictionaryPrefsForKey:(NSString *)key {
    return (NSDictionary *)[self prefsObjectForKey:key];
}

- (NSArray *)arrayPrefsForKey:(NSString *)key {
    return (NSArray *)[self prefsObjectForKey:key];
}

- (int)intPrefForKey:(NSString *)key {
    return [self intForKey:key];
}

- (NSNumber *)numberPrefForKey:(NSString *)key {
    return (NSNumber *)[self prefsObjectForKey:key];
}

- (BOOL)boolPrefForKey:(NSString *)key {
    return [self boolForKey:key];
}

- (void)saveDictionaryPrefs:(NSDictionary *)dict forKey:(NSString *)key {
    [self savePrefsObject:dict forKey:key];
}

- (void)saveArrayPrefs:(NSArray *)array forKey:(NSString *)key {
    [self savePrefsObject:array forKey:key];
}

- (void)saveStringPrefs:(NSString *)string forKey:(NSString *)key {
    [self savePrefsObject:string forKey:key];
}

- (void)saveNumberPrefs:(NSNumber *)number forKey:(NSString *)key {
    [self savePrefsObject:number forKey:key];
}

- (void)saveIntPrefs:(int)number forKey:(NSString *)key {
    [self setInt:number forKey:key];
}

- (void)saveBOOLPrefs:(BOOL)flag forKey:(NSString *)key {
    [self setBOOL:flag forKey:key];
}

#pragma mark - internal methods

- (NSString *)keyByAddingUserPrefix:(NSString *)key {
    return [NSString stringWithFormat:@"%@%@", [[ME_UserManager singleton] prefixForCurrentUser], key];
}

- (void)savePrefsObject:(NSObject *)object forKey:(NSString *)key {
    key = [self keyByAddingUserPrefix:key];
    [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSObject *)prefsObjectForKey:(NSString *)key {
    key = [self keyByAddingUserPrefix:key];
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

- (void)setInt:(int)num forKey:(NSString *)key {
    key = [self keyByAddingUserPrefix:key];
    [[NSUserDefaults standardUserDefaults] setInteger:num forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (int)intForKey:(NSString *)key {
    key = [self keyByAddingUserPrefix:key];
    NSInteger val = [[NSUserDefaults standardUserDefaults] integerForKey:key];
    return (int)val;
}

- (void)setBOOL:(BOOL)flag forKey:(NSString *)key {
    key = [self keyByAddingUserPrefix:key];
    [[NSUserDefaults standardUserDefaults] setBool:flag forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)boolForKey:(NSString *)key {
    key = [self keyByAddingUserPrefix:key];
    return [[NSUserDefaults standardUserDefaults] boolForKey:key];
}

@end
