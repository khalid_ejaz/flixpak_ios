//
//  MESQLiteManager.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-14.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//
//  Adds data.db, log.db, debug.db and cache.db for storing local app data and cached responses for GET network calls.
//  Provides interfaces to deal with sqlite databases.
//  --------------------------------------------------

#import <Foundation/Foundation.h>

@interface ME_SQLiteManager : NSObject

+ (id)singleton;

- (void)initialize;
- (void)createDB:(NSString *)db;
- (NSArray *)querySQL:(NSString *)sql onDB:(NSString *)db;
- (BOOL)executeSQL:(NSString *)sql onDB:(NSString *)db;

@end
