//
//  MESQLiteManager.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-14.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "ME_SQLiteManager.h"
#import "ME_Logger.h"
#import <sqlite3.h>

@implementation ME_SQLiteManager

// Not App specific.
// Will deal with local data saves.
// App will use interfaces to connect and perform actions on db.
+ (id)singleton {
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    // returns the same object each time
    return _sharedObject;
}

- (void)initialize {
    // check if DBs exist. if not, create DBs.
    NSArray *defaultDBs = @[
                            @{
                                @"name": @"cache",
                                @"tables": @[
                                        @{
                                            @"name":@"fields",
                                            @"sql":@""
                                            },
                                        @{
                                            @"name":@"responses",
                                            @"sql":@""
                                            }
                                        ]
                                },
                            @{
                                @"name": @"data",
                                @"tables": @[
                                        @{
                                            @"name":@"localdata",
                                            @"sql":@""
                                            }
                                        ]
                                },
                            @{
                                @"name": @"debug",
                                @"tables": @[
                                        @{
                                            @"name":@"errors",
                                            @"sql":@""
                                            },
                                        @{
                                            @"name":@"logs",
                                            @"sql":@""
                                            },
                                        @{
                                            @"name":@"diags",
                                            @"sql":@""
                                            }
                                        ]
                                },
                            @{
                                @"name": @"logs",
                                @"tables": @[
                                        @{
                                            @"name":@"pending",
                                            @"sql":@""
                                            },
                                        @{
                                            @"name":@"recent",
                                            @"sql":@""
                                            }
                                        ]
                                }
                            ];
    
    for (NSDictionary *dict in defaultDBs) {
        NSString *dbName = dict[@"name"];
        NSArray *tables = dict[@"tables"];
        
        [self checkAndCreateDB:dbName];
        
        for (NSDictionary *tableSchemaDict in tables) {
            NSString *tableName = tableSchemaDict[@"name"];
            NSString *query = tableSchemaDict[@"sql"];
            
            [self checkAndCreateTable:tableName inDB:dbName withQuery:query];
        }
    }
}

- (void)createDB:(NSString *)db {
    
}

- (NSArray *)querySQL:(NSString *)sql onDB:(NSString *)db {
    return nil;
}

- (BOOL)executeSQL:(NSString *)sql onDB:(NSString *)db {
    return NO;
}

#pragma mark - internal methods

// check and create tables.
- (void)checkAndCreateTable:(NSString *)table inDB:(NSString *)db withQuery:(NSString *)query {
    if (![self doesTable:table existInDB:db]) {
        [self createTable:table inDB:db withQuery:query];
    }
}

- (BOOL)doesTable:(NSString* )table existInDB:(NSString *)db {
    return NO;
}

- (void)createTable:(NSString* )table inDB:(NSString *)db withQuery:(NSString *)query {
    
}

// check and create databases.
- (void)checkAndCreateDB:(NSString *)dbName {
    
    [[ME_Logger singleton] log:[NSString stringWithFormat: @"Creating DB File: %@",[self fileNameForDB:dbName]]];
    
    NSString *dbPath= [self filePathForDB:dbName];
    [[ME_Logger singleton] log:dbPath];
    sqlite3 *db;
    
    NSFileManager *fm=[NSFileManager new];
    
    if([fm fileExistsAtPath:dbPath isDirectory:nil])
    {
        [[ME_Logger singleton] log:@"Database already exists.."];
        return;
    }
    
    if (sqlite3_open([dbPath UTF8String],&db)==SQLITE_OK)
    {
//        const char *query="create table user (userName VARCHAR(20),userAdd VARCHAR(20), userPass VARCHAR(20))";
//        
//        if (sqlite3_exec(db, query, NULL, NULL, NULL)==SQLITE_OK)
//        {
//            NSLog(@"User table created successfully..");
//        }else
//        {
//            NSLog(@"User table creation failed..");
//        }
        
    }else
    {
        NSLog(@"Open table failed..");
    }
    sqlite3_close(db);
}
     
- (NSString *)fileNameForDB:(NSString *)dbName {
 return dbName;
}

-(NSString *) filePathForDB:(NSString *)db {
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory=[paths objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.db",db]];
}

@end


//{
//    "dbs": [
//            {
//                "name": "cache",
//                "tables": [
//                           {
//                               "name": "fields",
//                               "sql": "create table fields on cache"
//                           },
//                           {
//                               "name": "responses",
//                               "sql": ""
//                           }
//                           ]
//            },
//            {
//                "name": "data",
//                "tables": [
//                           {
//                               "name": "localdata",
//                               "sql": "create table for local app data"
//                           }
//                           ]
//            },
//            {
//                "name": "debug",
//                "tables": [
//                           {
//                               "name": "errors",
//                               "sql": "create table for debugging errors"
//                           },
//                           {
//                               "name": "logs",
//                               "sql": "create table for debugging logs"
//                           },
//                           {
//                               "name": "diag",
//                               "sql": "create table for diagnostics"
//                           }
//                           ]
//            },
//            {
//                "name": "logs",
//                "tables": [
//                           {
//                               "name": "pending",
//                               "sql": "create table for logs not sent to server yet."
//                           },
//                           {
//                               "name": "recent",
//                               "sql": "create table for recent logs"
//                           }
//                           ]
//            }
//            ]
//}
