//
//  ANAdManager.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2013-06-10.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//
//  Easily ad Google Admob ads to any viewController.
//  -------------------------------------------------

#import <Foundation/Foundation.h>
#import <GoogleMobileAds/GoogleMobileAds.h>

typedef enum {
    AD_POSITION_BOTTOM,
    AD_POSITION_TOP
}AD_POSITION;

@protocol MEAdManagerDelegate <NSObject>

-(void) adWillShow;
-(void) adWillHide;

@end

@interface ME_AdManager : NSObject <GADBannerViewDelegate> {
    
}

@property (nonatomic, assign) id delegate;

+(ME_AdManager *) sharedManager;

-(void) showAdOnViewController:(UIViewController *) viewController atPosition:(AD_POSITION) adPosition;
-(void) removeAds;
-(void) layoutAdView;
-(CGFloat) adHeight;

@end
