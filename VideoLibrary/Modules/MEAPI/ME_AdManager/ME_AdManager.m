//
//  ANAdManager.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2013-06-10.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//
//

#import "ME_AdManager.h"

#define IS_IPAD              (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
#define IS_PORTRAIT          UIDeviceOrientationIsPortrait((UIDeviceOrientation)[UIApplication sharedApplication].statusBarOrientation)

@interface ME_AdManager ()

@property (nonatomic, strong) GADBannerView *adView;
@property (nonatomic, assign) AD_POSITION adPosition;
@property (nonatomic, unsafe_unretained) UIViewController *viewControllerForAd;
@end

@implementation ME_AdManager

# pragma mark - setup of objects

+(ME_AdManager *) sharedManager {
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
        [_sharedObject setupAdObjects];
    });
    
    // returns the same object each time
    return _sharedObject;
}

-(void) setupAdObjects {
    self.adView = [[GADBannerView alloc] init];
    if(IS_IPAD)
    {
        self.adView.adUnitID = @"a151043822d16f0";
    }
    else
    {
        self.adView.adUnitID = @"a151043c5d24681";
    }
        
    [self.adView setDelegate:self];
    [self.adView setRootViewController:self.viewControllerForAd];
    
    [self.adView setHidden:YES];
    
    [self logText:@"Ad Banner objects created."];
}

-(void) requestAdMobAd
{
    GADRequest *request = [GADRequest request];
    request.testDevices = @[ kGADSimulatorID ];
    [self.adView loadRequest:request];
    request = nil;
}

- (CGFloat)predefinedAdHeight {
    if (IS_IPAD) {
        return 90.0f;
    } else {
        if (IS_PORTRAIT) {
            return 50.0f;
        } else return 32.0f;
    }
}

# pragma mark - interface methods related

-(void) showAdOnViewController:(UIViewController *) viewController atPosition:(AD_POSITION) adPosition {
    
    _adPosition = adPosition;
    [self.adView removeFromSuperview];
    [self setViewControllerForAd:viewController];
    [self layoutAdView];
    
    [self.adView setRootViewController:viewController];
    
    [self.adView setFrame:[self frameForHiddenAd]];
    [self.viewControllerForAd.view addSubview:self.adView];
    [self requestAdMobAd];
}

-(void) removeAds {
    [self.adView setHidden:YES];
    [self.adView removeFromSuperview];
    [self.adView setRootViewController:nil];
    [self setViewControllerForAd:nil];
}

-(void) layoutAdView {
    
    if (!self.viewControllerForAd) {
        return;
    }
    if (self.adView.hidden) {
        [self.adView setFrame:[self frameForHiddenAd]];
    } else [self.adView setFrame:[self frameForVisibleAd]];
}

-(CGFloat) adHeight {
    if (self.adView.hidden) return 0.0f;
    return [self predefinedAdHeight];
}

#pragma mark - internal methods

- (CGRect)frameForHiddenAd {
    if (self.adPosition == AD_POSITION_BOTTOM) {
        return CGRectMake(0, self.viewControllerForAd.view.bounds.size.height, self.viewControllerForAd.view.frame.size.width, [self predefinedAdHeight]);
    } else {
        return CGRectMake(0, 0 - [self predefinedAdHeight], self.viewControllerForAd.view.frame.size.width, [self predefinedAdHeight]);
    }
}

- (CGRect)frameForVisibleAd {
    if (self.adPosition == AD_POSITION_BOTTOM) {
        return CGRectMake(0, self.viewControllerForAd.view.bounds.size.height - [self predefinedAdHeight], self.viewControllerForAd.view.frame.size.width, [self predefinedAdHeight]);
    } else {
        return CGRectMake(0, 0, self.viewControllerForAd.view.frame.size.width, [self predefinedAdHeight]);
    }
}

#pragma mark - Utility functions.

-(void) logText:(NSString *) string {
    // TODO: use logger.
    NSLog(@"Ad Manager: %@",string);
}

# pragma mark - Show/hide ads from Ad states



-(void) restoreAds
{
    if (self.adView.hidden) {
        [self requestAdMobAd];
    }
}

# pragma mark - Ad show/hide animations

-(void) showAdView {
    if (!self.viewControllerForAd) {
        return;
    }
    
    self.adView.frame = [self frameForHiddenAd];
    self.adView.hidden = NO;
    [self.viewControllerForAd.view bringSubviewToFront:self.adView];
    [UIView animateWithDuration:0.3f animations:^{
        self.adView.frame = [self frameForVisibleAd];
    } completion:^(BOOL finished) {
        [self.viewControllerForAd.view setNeedsLayout];
    }];
}

-(void) HideAdView
{
    // if not already hidden. hide ad.
    if (!self.adView.hidden) {
        if ([self.delegate respondsToSelector:@selector(adWillHide)]) {
            [self.delegate adWillHide];
        }
        [UIView animateWithDuration:0.3f animations:^{
            self.adView.frame = [self frameForHiddenAd];
        } completion:^(BOOL finished) {
            self.adView.hidden = YES;
        }];
    }
}

# pragma mark - Ad delegates

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView
{
    [self showAdView];
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error
{
    [self HideAdView];
}

@end
