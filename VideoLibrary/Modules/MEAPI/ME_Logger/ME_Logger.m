//
//  MELogger.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-14.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "ME_Logger.h"

@implementation ME_Logger

// Not App Specific.
// Will deal with Logging data to log server.
// App will provide link to log server and use interfaces to save logs.

+ (id)singleton {
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    // returns the same object each time
    return _sharedObject;
}

- (void)initializeWithShortId:(NSString *)shortId {
    // TODO: prepare app launch analytic data and send to log server.
    [self sendLogs];
}

- (void)sendLogs {
    //TODO: on app launch, send any logs that are not sent yet.
    
}

- (void)log:(NSString *)text {
    [self log:text level:ME_Log_Level_INFO];
}

- (void)log:(NSString *)text level:(ME_Log_Level)level {
    NSLog(@"ME_Logger {%@}--- %@", [self textForLogLevel:level], text);
    // TODO: log in dbs and transmit to log server.
}

- (void)logError:(NSError *)error {
    [self log:[error description] level:ME_Log_Level_ERROR];
}

- (NSString *)textForLogLevel:(ME_Log_Level)level {
    switch (level) {
        case ME_Log_Level_INFO:
            return @"INFO";
            break;
        case ME_Log_Level_WARN:
            return @"WARN";
            break;
        case ME_Log_Level_DEBUG:
            return @"DEBUG";
            break;
        case ME_Log_Level_ERROR:
            return @"ERROR";
            break;
        case ME_Log_Level_FATAL:
            return @"FATAL";
            break;
        case ME_Log_Level_TRACE:
            return @"TRACE";
            break;
            
        default:
            return @"";
            break;
    }
}

@end
