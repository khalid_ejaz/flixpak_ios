//
//  MELogger.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-14.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//
//  Connects to the log server and sends app usage analytics and other reports.
//  ---------------------------------------------------------------------------
//  Saves log in a log.db using sqlitedb manager. When connected to internet, sends all logs to log server.

#import <Foundation/Foundation.h>
#import "types.h"

@interface ME_Logger : NSObject

+ (id)singleton;
- (void)initializeWithShortId:(NSString *)shortId;
- (void)log:(NSString *)text;
- (void)log:(NSString *)text level:(ME_Log_Level)level;
- (void)logError:(NSError *)error;
@end


/*
 Log Levels: 
 ----------
 
 TRACE - Only when I would be "tracing" the code and trying to find one part of a function specifically.
 DEBUG - Information that is diagnostically helpful to people more than just developers (IT, sysadmins, etc.).
 INFO - Generally useful information to log (service start/stop, configuration assumptions, etc). Info I want to always have available but usually don't care about under normal circumstances. This is my out-of-the-box config level.
 WARN - Anything that can potentially cause application oddities, but for which I am automatically recovering. (Such as switching from a primary to backup server, retrying an operation, missing secondary data, etc.)
 ERROR - Any error which is fatal to the operation, but not the service or application (can't open a required file, missing data, etc.). These errors will force user (administrator, or direct user) intervention. These are usually reserved (in my apps) for incorrect connection strings, missing services, etc.
 FATAL - Any error that is forcing a shutdown of the service or application to prevent data loss (or further data loss). I reserve these only for the most heinous errors and situations where there is guaranteed to have been data corruption or loss.
*/
