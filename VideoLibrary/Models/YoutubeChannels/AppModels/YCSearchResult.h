//
//  YCSearchResult.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/30/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import <Foundation/Foundation.h>

#define YC_SEARCH_RESULT_PAGE_INFO_KEY          @"pageInfo"
#define YC_SEARCH_RESULT_COUNT                  @"totalResults"
#define YC_SEARCH_RESULTS_PER_PAGE              @"resultsPerPage"

@interface YCSearchResult : NSObject

@property (nonatomic, strong) NSString *searchQuery;
@property (nonatomic, strong) NSMutableArray *resultItems;
@property (nonatomic, strong) NSNumber *resultsCount;
@property (nonatomic, strong) NSNumber *resultsPerPage;
@property (nonatomic, strong) NSString *nextPageToken;
@property (nonatomic, strong) NSString *prevPageToken;

-(id) initWithResultItems:(NSArray *)items count:(NSNumber *)count resultsPerPage:(NSNumber *)perPageCount nextPageToken:(NSString *)nextPageToken prevPageToken:(NSString *)prevPageToken searchQuery:(NSString *)searchQuery;

@end
