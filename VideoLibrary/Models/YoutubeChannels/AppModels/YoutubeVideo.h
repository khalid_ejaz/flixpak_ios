//
//  YoutubeVideo.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/1/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import <Foundation/Foundation.h>

#define LOCAL_FAV_VIDEOS_FILE_NAME        @"favVideos.plist"

#define KEY_VIDEO_SAVED_AT                  @"savedAt"

@interface YoutubeVideo : NSObject

@property (nonatomic, strong) NSString *videoId;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *channelId;
@property (nonatomic, strong) NSString *channelTitle;
@property (nonatomic, strong) NSString *videoDescription;
@property (nonatomic, strong) NSString *duration;
@property (nonatomic, strong) NSString *publishedAt;
@property (nonatomic, strong) NSString *embedURL;
@property (nonatomic, strong) NSURL *hqThumbnailURL;
@property (nonatomic, strong) NSURL *medThumbnailURL;
@property (nonatomic, strong) NSURL *thumbnail1;
@property (nonatomic, strong) NSURL *thumbnail2;
@property (nonatomic, strong) NSURL *thumbnail3;

@property (nonatomic, strong) NSDate *savedAt;

-(id) initWithVideoID:(NSString *)videoId;
-(id) initWithDictionary:(NSDictionary *)dictionary;
-(void) populateVideoDetail:(NSDictionary *)dictionary;

-(NSMutableDictionary *)dictionary;


-(BOOL) isFavourite;
-(void) saveAsFavourite;
-(void) saveAsNotFavourite;

@end
