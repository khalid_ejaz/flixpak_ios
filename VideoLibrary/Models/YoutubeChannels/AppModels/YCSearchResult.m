//
//  YCSearchResult.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/30/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "YCSearchResult.h"

@implementation YCSearchResult

-(id) initWithResultItems:(NSArray *)items count:(NSNumber *)count resultsPerPage:(NSNumber *)perPageCount nextPageToken:(NSString *)nextPageToken prevPageToken:(NSString *)prevPageToken searchQuery:(NSString *)searchQuery {
    self = [super init];
    if (self) {
        _searchQuery = searchQuery;
        _resultItems = items;
        _resultsCount = count;
        _resultsPerPage = perPageCount;
        _nextPageToken = nextPageToken;
        _prevPageToken = prevPageToken;
        
    }
    
    return self;
}

@end
