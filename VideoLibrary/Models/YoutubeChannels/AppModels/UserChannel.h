//
//  UserChannel.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-02-06.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserChannel : NSObject

@property (nonatomic, strong) NSString *channelId;
@property (nonatomic, strong) NSString *channelName;
@property (nonatomic, strong) NSString *channelDesc;
@property (nonatomic, strong) NSString *channelThumbnail;

@end
