//
//  YCSearchResult.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/30/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "YCSearchResultItem.h"

#define KEY_ID                      @"id"
#define KEY_SNIPPET                 @"snippet"

#define KEY_CHANNEL_ID              @"channelId"
#define KEY_CHANNEL_TITLE           @"title"
#define KEY_CHANNEL_DESCRIPTION     @"description"
#define KEY_THUMBNAILS_DICT         @"thumbnails"
#define KEY_THUMBNAIL_DEFAULT       @"default"
#define KEY_THUMBNAIL_MEDIUM        @"medium"
#define KEY_PUBLISHED_AT            @"publishedAt"

@implementation YCSearchResultItem

-(id) initWithItemDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        
        if (dictionary) {
            NSDictionary *snippetDictionary = dictionary[KEY_SNIPPET];
            if (snippetDictionary) {
                _channelId = snippetDictionary[KEY_CHANNEL_ID];
                _channelTitle = snippetDictionary[KEY_CHANNEL_TITLE];
                _channelDescription = snippetDictionary[KEY_CHANNEL_DESCRIPTION];
                
                NSDictionary *thumbnails = snippetDictionary[KEY_THUMBNAILS_DICT];
                if (thumbnails) {
                    NSDictionary *medium = thumbnails[KEY_THUMBNAIL_DEFAULT];
                    _thumbnail = medium[@"url"];
                }
                
                _publishedAt = snippetDictionary[KEY_PUBLISHED_AT];
            }
        }
    }
    return self;
}

@end
