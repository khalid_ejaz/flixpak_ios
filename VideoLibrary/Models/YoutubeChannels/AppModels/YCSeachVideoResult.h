//
//  YCSeachVideoResult.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/30/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YCSeachVideoResult : NSObject

@property (nonatomic, strong) NSNumber *totalResults;
@property (nonatomic, strong) NSNumber *resultsPerPage;
@property (nonatomic, strong) NSMutableArray *youtubeVideos;
@property (nonatomic, strong) NSString *nextPageToken;
@property (nonatomic, strong) NSString *prevPageToken;

-(id) initWithDictionary:(NSDictionary *)dictioanry;

@property (nonatomic, strong) NSMutableArray *videoIds;
-(NSArray *) videoIdsThatHaveNoDetail;

@end

