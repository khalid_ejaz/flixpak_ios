//
//  YCSearchResult.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/30/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import <Foundation/Foundation.h>

// json parse keys.
#define YC_SEARCH_RESULT_ITEM_ROOT_KEY                          @"items"

@interface YCSearchResultItem : NSObject

@property (nonatomic, strong) NSString *channelId;
@property (nonatomic, strong) NSString *channelTitle;
@property (nonatomic, strong) NSString *channelDescription;
@property (nonatomic, strong) NSString *thumbnail;
@property (nonatomic, strong) NSString *publishedAt;
@property (nonatomic, strong) NSString *nextPageToken;
@property (nonatomic, strong) NSString *prevPageToken;

-(id) initWithItemDictionary:(NSDictionary *)dictionary;

@end
