//
//  YoutubeVideo.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/1/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "YoutubeVideo.h"
#import "DataManager.h"
#import "Utility.h"
#import "UserManager.h"

@implementation YoutubeVideo

-(id) initWithVideoID:(NSString *)videoId {
    self = [super init];
    if (self) {
        _videoId = videoId;
        
        _embedURL = [[DataManager sharedManager] getYoutubeEmbedLinkFromVID:videoId];
        
        _hqThumbnailURL = [[DataManager sharedManager] getPreviewImageURLOfYoutubeVideo:_embedURL number:0];
        
        _medThumbnailURL = [[DataManager sharedManager] getPreviewMediumQualityImageURLOfYoutubeVideoId:videoId];
        
        _thumbnail1 = [[DataManager sharedManager] getPreviewImageURLOfYoutubeVideo:_embedURL number:1];
        _thumbnail2 = [[DataManager sharedManager] getPreviewImageURLOfYoutubeVideo:_embedURL number:2];
        _thumbnail3 = [[DataManager sharedManager] getPreviewImageURLOfYoutubeVideo:_embedURL number:3];
        
    }
    return self;
}

-(id) initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        _videoId = dictionary[@"videoId"];
        _title = dictionary[@"title"];
        _videoDescription = dictionary[@"videoDescription"];
        _duration = dictionary[@"duration"];
        _publishedAt = dictionary[@"publishedAt"];
        _embedURL = dictionary[@"embedURL"];
        _hqThumbnailURL = [NSURL URLWithString:dictionary[@"hqThumbnailURL"]];
        _medThumbnailURL = [NSURL URLWithString:dictionary[@"medThumbnailURL"]];
        _thumbnail1 = [NSURL URLWithString:dictionary[@"thumbnail1"]];
        _thumbnail2 = [NSURL URLWithString:dictionary[@"thumbnail2"]];
        _thumbnail3 = [NSURL URLWithString:dictionary[@"thumbnail3"]];
        
        if ([dictionary.allKeys containsObject:KEY_VIDEO_SAVED_AT]) {
            _savedAt = dictionary[KEY_VIDEO_SAVED_AT];
        }
    }
    return self;
}

-(NSMutableDictionary *)dictionary {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    [dictionary setObject:self.videoId forKey:@"videoId"];
    [dictionary setObject:self.title forKey:@"title"];
    [dictionary setObject:self.videoDescription forKey:@"videoDescription"];
    [dictionary setObject:self.duration forKey:@"duration"];
    [dictionary setObject:self.publishedAt forKey:@"publishedAt"];
    [dictionary setObject:self.embedURL forKey:@"embedURL"];
    [dictionary setObject:[self.hqThumbnailURL absoluteString] forKey:@"hqThumbnailURL"];
    [dictionary setObject:[self.medThumbnailURL absoluteString] forKey:@"medThumbnailURL"];
    [dictionary setObject:[self.thumbnail1 absoluteString] forKey:@"thumbnail1"];
    [dictionary setObject:[self.thumbnail2 absoluteString] forKey:@"thumbnail2"];
    [dictionary setObject:[self.thumbnail3 absoluteString] forKey:@"thumbnail3"];
    
    return dictionary;
}

-(void) populateVideoDetail:(NSDictionary *)dictionary {
    if (!dictionary) {
        return;
    }
    
    NSDictionary *snippetDict = dictionary[@"snippet"];
    if (snippetDict) {
        _publishedAt = snippetDict[@"publishedAt"];
        if (_publishedAt) {
            _publishedAt = [Utility publishedAtStringFromString:_publishedAt];
        }
        _videoDescription = snippetDict[@"description"];
        _title = snippetDict[@"title"];
        _channelId = snippetDict[@"channelId"];
        _channelTitle = snippetDict[@"channelTitle"];
    }
    
    NSDictionary *contentDetails = dictionary[@"contentDetails"];
    if (contentDetails) {
        NSString *durationString = contentDetails[@"duration"];
        _duration = [Utility durationStringFromYoutubeDuration:durationString];
//        NSLog(@"Duration String: %@ - converted duration: %@", durationString, self.duration);
    }
}

// favourite related.

-(BOOL) isFavourite {
    NSDictionary *favVideos = [[UserManager sharedManager] favVideos];
    if ([favVideos.allKeys containsObject:self.videoId]) {
        return YES;
    }
    return NO;
}

-(void) saveAsFavourite {
    if ([self isFavourite]) {
        return;
    } else {
        NSMutableDictionary *favVideos = [[UserManager sharedManager] favVideos];
        NSMutableDictionary *dict = [self dictionary];
        [dict setObject:[NSDate date] forKey:KEY_VIDEO_SAVED_AT];
        [favVideos setObject:dict forKey:self.videoId];
    }
    [[UserManager sharedManager] saveFavouriteVideos];
}

-(void) saveAsNotFavourite {
    if (![self isFavourite]) {
        return;
    } else {
        NSMutableDictionary *favVideos = [[UserManager sharedManager] favVideos];
        [favVideos removeObjectForKey:self.videoId];
    }
    [[UserManager sharedManager] saveFavouriteVideos];
}

@end
