//
//  UserChannel.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-02-06.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "UserChannel.h"

@implementation UserChannel

- (id)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        _channelId = dict[@"channelId"];
        _channelName = dict[@"channelTitle"];
        _channelDesc = dict[@"channelDescription"];
        _channelThumbnail = dict[@"thumbnail"];
    }
    return self;
}

@end
