//
//  YCSeachVideoResult.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/30/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "YCSeachVideoResult.h"
#import "YoutubeVideo.h"

@interface YCSeachVideoResult()


@end

@implementation YCSeachVideoResult

-(id) initWithDictionary:(NSDictionary *)dictioanry {
    self = [super init];
    if (self) {
        NSDictionary *pageInfo = dictioanry[@"pageInfo"];
        if (pageInfo) {
            _resultsPerPage = pageInfo[@"resultsPerPage"];
            _totalResults = pageInfo[@"totalResults"];
        }
        
        _nextPageToken = dictioanry[@"nextPageToken"];
        _prevPageToken = dictioanry[@"prevPageToken"];
        
        _youtubeVideos = [NSMutableArray array];
        _videoIds = [NSMutableArray array];
        NSArray *items = dictioanry[@"items"];
        for (NSDictionary *dict in items) {
            NSDictionary *contentDetails = dict[@"contentDetails"];
            if (contentDetails) {
                NSString *videoId = contentDetails[@"videoId"];
                if (videoId) {
                    [_videoIds addObject:videoId];
                    YoutubeVideo *video = [[YoutubeVideo alloc] initWithVideoID:videoId];
                    [_youtubeVideos addObject:video];
                }
            } else {
                NSDictionary *idDict = dict[@"id"];
                if (idDict) {
                    NSString *videoId = idDict[@"videoId"];
                    if (videoId) {
                        [_videoIds addObject:videoId];
                        YoutubeVideo *video = [[YoutubeVideo alloc] initWithVideoID:videoId];
                        [_youtubeVideos addObject:video];
                    }
                }
            }
        }
    }
    return self;
}

-(NSArray *) videoIdsThatHaveNoDetail {
    NSMutableArray *noDetailIds = [NSMutableArray array];
    for (YoutubeVideo *video in self.youtubeVideos) {
        if (!video.title) {
            [noDetailIds addObject:video.videoId];
        }
    }
    return noDetailIds;
}

@end
