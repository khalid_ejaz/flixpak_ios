//
//  FlixPakModel.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-11.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlixPakModel : NSObject

@property (nonatomic, strong) NSNumber *_id;

- (id)initWithDictionary:(NSDictionary *)dict;

@end
