//
//  FP_DramaSeasonEpisodes.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-16.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "FlixPakModel.h"

@interface FP_DramaSeasonEpisodes : FlixPakModel

@property (nonatomic, strong) NSNumber *seasonId;
@property (nonatomic, strong) NSNumber *episodeNumber;
@property (nonatomic, strong) NSString *vid;

- (id)initWithDictionary:(NSDictionary *)dict;

@end
