//
//  SwitchOption.h
//  PakTV
//
//  Created by Malik Khalid Ejaz on 11/15/2013.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SwitchOption : NSObject

#define KEY_SWITCH_OPTION_DISPLAY_TEXT  @"displayText"
#define KEY_SWITCH_OPTION_VALUE         @"value"
#define KEY_SWITCH_OPTION_ASCENDING     @"ascending"

@property (nonatomic, strong) NSString *displayText;
@property (nonatomic, strong) NSString *value;
@property (nonatomic, assign) BOOL ascending;

-(id) initWithDisplayText:(NSString *)displayText value:(NSString *)value ascending:(BOOL)ascending;



@end
