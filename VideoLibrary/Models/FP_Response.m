//
//  FP_Response.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-12.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "FP_Response.h"

@implementation FP_Response

- (id)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        __responseDict = dict;
    }
    return self;
}

@end
