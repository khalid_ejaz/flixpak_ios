//
//  FP_Drama.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-11.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "FlixPakModel.h"

@interface FP_Drama : FlixPakModel

@property (nonatomic, strong) NSString *title;

@end
