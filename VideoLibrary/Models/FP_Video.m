//
//  FP_Video.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-16.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "FP_Video.h"
#import "Utility.h"

@implementation FP_Video

- (id)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        _title = dict[@"title"];
        _subtitle = dict[@"subtitle"];
        _vid = dict[@"vid"];
        _thb = dict[@"thb"];
        _addedOn = dict[@"added_on"];
        _whenAdded = [Utility durationSinceTime:_addedOn];
    }
    return self;
}

/*
 "title": "Dunya Kamran Khan Kay Sath - 16 January 2017 - Dunya News",
 "subtitle": "Dunya News",
 "vid": "zmtz7BTR2m4",
 "fromChannel": 1,
 "thb": "https://i.ytimg.com/vi/zmtz7BTR2m4/mqdefault.jpg",
 "added_on": "2017-01-16T20:57:07.000Z"
 */


@end
