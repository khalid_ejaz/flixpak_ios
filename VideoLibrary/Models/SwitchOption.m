//
//  SwitchOption.m
//  PakTV
//
//  Created by Malik Khalid Ejaz on 11/15/2013.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import "SwitchOption.h"

@implementation SwitchOption

-(id) initWithDisplayText:(NSString *)displayText value:(NSString *)value ascending:(BOOL)ascending {
    
    self = [super init];
    if (self) {
        _displayText = displayText;
        _value = value;
        _ascending = ascending;
    }
    return self;
}

@end
