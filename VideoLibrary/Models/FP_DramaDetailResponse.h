//
//  FP_DramaDetailResponse.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-16.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "FP_Response.h"

@interface FP_DramaDetailResponse : FP_Response

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSNumber *drama_id;
@property (nonatomic, strong) NSArray *dramaEpisodes;

@end
