//
//  FP_SectionResponse.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-12.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "FP_Response.h"

@interface FP_SectionResponse : FP_Response

@property (nonatomic, strong) NSString *heading;
@property (nonatomic, strong) NSString *kind;
@property (nonatomic, strong) NSArray *videos;

- (id)initWithDictionary:(NSDictionary *)dict;

@end
