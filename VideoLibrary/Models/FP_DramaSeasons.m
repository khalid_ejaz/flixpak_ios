//
//  FP_DramaSeasons.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-16.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "FP_DramaSeasons.h"

@implementation FP_DramaSeasons

- (id)initWithDictionary:(NSDictionary *)dict {
    self = [super initWithDictionary:dict];
    if (self) {
        _dramaId = dict[@"drama_id"];
        if ([dict.allKeys containsObject:@"title"]) {
            _title = dict[@"title"];
        }
        if ([dict.allKeys containsObject:@"season"]) {
            _seasonNumber = dict[@"season"];
        }
    }
    return self;
}
@end
