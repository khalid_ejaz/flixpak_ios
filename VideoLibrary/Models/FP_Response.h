//
//  FP_Response.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-12.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FP_Response : NSObject

@property (nonatomic, retain) NSDictionary *_responseDict;

- (id)initWithDictionary:(NSDictionary *)dict;

@end
