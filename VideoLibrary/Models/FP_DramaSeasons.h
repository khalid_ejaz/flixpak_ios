//
//  FP_DramaSeasons.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-16.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "FlixPakModel.h"

@interface FP_DramaSeasons : FlixPakModel

@property (nonatomic, strong) NSNumber *dramaId;
@property (nonatomic, strong) NSNumber *seasonNumber;
@property (nonatomic, strong) NSString *title;

@end
