//
//  FP_Video.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-16.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "FlixPakModel.h"

@interface FP_Video : FlixPakModel

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *subtitle;
@property (nonatomic, strong) NSString *vid;
@property (nonatomic, strong) NSString *thb;
@property (nonatomic, strong) NSString *addedOn;
@property (nonatomic, strong) NSString *whenAdded;  // like 3 mins ago. derived from addedOn.

- (id)initWithDictionary:(NSDictionary *)dict;

@end
