//
//  FP_DramaSeasonEpisodes.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-16.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "FP_DramaSeasonEpisodes.h"

@implementation FP_DramaSeasonEpisodes

- (id)initWithDictionary:(NSDictionary *)dict {
    self = [super initWithDictionary:dict];
    if (self) {
        _vid = dict[@"video_link"];
        if ([dict.allKeys containsObject:@"season_id"]) {
            _seasonId = dict[@"season_id"];
        }
    }
    return self;
}
@end
