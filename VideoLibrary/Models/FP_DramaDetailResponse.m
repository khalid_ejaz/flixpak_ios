//
//  FP_DramaDetailResponse.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-16.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "FP_DramaDetailResponse.h"

@implementation FP_DramaDetailResponse

- (id)initWithDictionary:(NSDictionary *)dict {
    self = [super initWithDictionary:dict];
    if (self ) {
        _title = dict[@"title"];
        
    }
    return self;
}

/*
 {
 "data": {
 "title": "Aaasmano Pe Likha",
 "id": 2,
 "sections": [
 {
 "id": 2,
 "title": "Season 1",
 "num_of_episodes": 25,
 "episodes": [
 {
 "id": 7,
 "episode": 1,
 "video_link": "rc8MMrKDWl0"
 },
 {
 "id": 8,
 "episode": 2,
 "video_link": "cPaK-4oEiCI"
 }
 ]
 }
 ]
 }
 }
 */

@end
