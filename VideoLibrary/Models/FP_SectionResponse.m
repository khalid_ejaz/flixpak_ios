//
//  FP_SectionResponse.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-12.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "FP_SectionResponse.h"

@implementation FP_SectionResponse

- (id)initWithDictionary:(NSDictionary *)dict {
    self = [super initWithDictionary:dict];
    if (self) {
        _kind = dict[@"kind"];
        _heading = dict[@"heading"];
    }
    return self;
}

/*
 "heading": "News Feed",
 "kind": "Youtube Channel",
 */

@end
