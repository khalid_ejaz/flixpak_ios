//
//  DataManager.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/18/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "types.h"

#define LOCAL_USER_SETTINGS_FILE_NAME   @"settings.plist"
#define LOCAL_FAV_CATS_FILE_NAME        @"favCats.plist"
#define LOCAL_FAV_EPISODES_FILE_NAME    @"favEpisode.plist"
#define LOCAL_FAV_CONTS_FILE_NAME       @"favConts.plist"

#define VIEWED_EPISODES_FILE_NAME       @"views.plist"
#define RATED_EPISODES_FILE_NAME        @"ratings.plist"
#define RATED_CHANNELS_FILE_NAME        @"channelRatings.plist"
#define LOCAL_VERSIONS_FILE_NAME        @"versions.plist"
#define LOCAL_USER_ACCOUNTS_FILE_NAME    @"users.plist"

@class ContentCategory, Content, Episode, Source, Update, PFObject, YCSearchResult, YCSeachVideoResult;
typedef void (^YCSearchVideoResultAndErrorBlock) (YCSeachVideoResult *result, NSError *error);
typedef void (^YCSearchResultAndErrorBlock) (YCSearchResult *result, NSError *error);
typedef void (^voidBlock) (void);
typedef void (^ParseObjectAndErrorBlock) (PFObject *object, NSError *error);
typedef void (^CategoryAndErrorBlock) (ContentCategory *category, NSError *error);
typedef void (^StringAndErrorBlock) (NSString *string, NSError *error);
typedef void (^DataAndErrorBlock) (NSData *data, NSError *error);

@interface DataManager : NSObject

+(id) sharedManager;

// user defauls.
-(NSString *) selectedCategoryId;
-(void) setSelectedCategoryId:(NSString *)selectedCategoryId;

// auto detect content form web page title.
-(BOOL) contentName:(NSString *)name matchesTitle:(NSString *)title;

// detect video content from webpages related.
-(void)detectLinksFromDailyMotionFromURL:(NSURL *) url withCompletion:(ArrayAndErrorBlock)completion;
-(void) detectYoutubeVideosFromURL:(NSURL *)url withCompletion:(ArrayAndErrorBlock)completion;
-(NSURL *) getPreviewImageURLOfYoutubeVideo:(NSString *)vid number:(int)number;
-(NSURL *) getPreviewMediumQualityImageURLOfYoutubeVideoId:(NSString *)vid;
-(NSString *) getLengthOfYoutubeVideo:(NSString *)video;
-(NSMutableArray *) shortLinks:(NSArray *)links;
-(NSMutableArray *) longLinks:(NSArray *)links;
-(NSString *) getYoutubeEmbedLinkFromVID:(NSString *)vid;

// date parsing.
+(NSString *) dateStringFromDate:(NSDate *)date;
-(NSString *) friendlyDateFromString:(NSString *)dateString;
-(NSString *) shortfriendlyDateFromString:(NSString *)dateString;

// arrays and dictionaries read/write
// local
-(NSMutableDictionary *) readLocalDictionaryWithName:(NSString *)fileName;
-(NSMutableArray *) readLocalArrayWithName:(NSString *)fileName;

-(void) saveDictionary:(NSDictionary *)dict AsFileName:(NSString *)fileName;
-(void) saveArray:(NSArray *)array AsFileName:(NSString *)fileName;

-(void) deleteUserFile:(NSString *)fileName;
-(NSString *) pathForLocalResource:(NSString *)resourceName;

// remote
-(void) readDictionarywithUrl:(NSString *)url completion:(MutableDictionaryAndErrorBlock)completion;
-(void) readArraywithUrl:(NSString *)url completion:(ArrayAndErrorBlock)completion;
-(NSURL *) imageURLWithCategoryId:(NSString *)categoryId contentId:(NSString *)contentId;
-(NSString *) imageUrlFromClassicContentName:(NSString *)name path:(NSString *)path;
-(void) downloadImagewithUrl:(NSString *)url completion:(DataAndErrorBlock)completion;
-(void) uploadImage:(NSData *)data withFileName:(NSString *)fileName completion:(BoolAndErrorBlock)completion;

// methods related to re-sorting parse responses locally.

-(NSArray *) sortStringArray:(NSArray *) objects;
-(NSArray *) sortObjects:(NSArray *) objects orderBy:(NSString *)field ascending:(BOOL) asc;
-(NSArray *) sortCaseInsensitiveObjects:(NSArray *) objects orderBy:(NSString *)field ascending:(BOOL) asc;
-(NSArray *) uniqueObjectsFromArray:(NSArray *)objects withKeyColumn:(NSString *)column;

-(void) sortArraysInsideDictionary:(NSMutableDictionary *)dictionary orderBy:(NSString *)field ascending:(BOOL) asc completion:(MutableDictionaryAndErrorBlock)completion;

-(NSArray *) getSortedArrayForLocalFavCategories:(NSDictionary *)catDict;
-(NSArray *) getSortedArrayForLocalFavContents:(NSDictionary *)contentDict;
-(NSArray *) getSortedArrayForLocalFavEpisodes:(NSDictionary *)episodesDict;
-(NSArray *) getSortedArrayForLocalFavChannels:(NSDictionary *)contentDict;
-(NSArray *) getSortedArrayForLocalFavUserChannels:(NSDictionary *)userChannels;
-(NSArray *) getSortedArrayForLocalFavVideos:(NSDictionary *)episodesDict;

-(void) filterArraysInsideDictionary:(NSMutableDictionary *)dictionary searchString:(NSString *)searchString field:(NSString *)field completion:(DictionaryAndErrorBlock)completion;
-(NSMutableArray *) filterArray:(NSArray *)array SearchString:(NSString *)string field:(NSString *)field;

// user accounts related

-(NSString *)lastLoggedInUserName;
-(void) saveLastLoggedInUserName:(NSString *)name;

- (void) createDirectoryForUserName:(NSString *)name;

// Youtube Channels related
-(void) findChannelsByName:(NSString *)name completion:(YCSearchResultAndErrorBlock)completion;
-(void) findChannelsByName:(NSString *)name order:(NSString *)order pageToken:(NSString *)pageToken completion:(YCSearchResultAndErrorBlock)completion;

-(void) findUploadsIdByChannelId:(NSString *)channelId completion:(ArrayAndErrorBlock)completion;

// TODO: include sort filters.
-(void) findVideosByUploadId:(NSString *)uploadId pageToken:(NSString *)pageToken completion:(YCSearchVideoResultAndErrorBlock)completion;
-(void) findVideosByChannelId:(NSString *)channelId searchString:(NSString *)searchString order:(NSString *)order pageToken:(NSString *)pageToken completion:(YCSearchVideoResultAndErrorBlock)completion;

-(void) findVideoDetailByVideoIds:(NSArray *)videoIds completion:(ArrayAndErrorBlock)completion;

// previous version favs related.

-(BOOL) hasPreviousVersionShowFavs;
-(BOOL) hasPreviousVersionDramaFavs;
-(NSArray *) previousVersionShowFavs;
-(NSArray *) previousVersionDramaFavs;

// find youtube video direct url to play in custom player.
- (void)findURLForVideoID:(NSString *)vid completion:(StringAndErrorBlock)completion;

// app url, api keys and name related.
-(NSString *)appName;
- (NSString*)appURL;

#pragma mark - Pak Videos Node API related method declarations.

// NAPI - Node API.
// POST /category
-(void) NAPICreateCategoryWithParentCategoryId:(NSString *)parentCategoryId name:(NSString *)name completion:(BoolAndErrorBlock)completion;

// POST /content
-(void) NAPICreateContentWithCategoryId:(NSString *)categoryId name:(NSString *)name completion:(BoolAndErrorBlock)completion;

// POST /video
-(void) NAPICreateVideoWithContentId:(NSString *)contentId url:(NSString *)videoURL title:(NSString *)title completion:(BoolAndErrorBlock)completion;

// PUT /video
-(void) NAPIUpdateVideoWithVideoId:(NSString *)videoId url:(NSString *)videoURL completion:(BoolAndErrorBlock)completion;

// GET /video
-(void) NAPIReadVideoWithVideoId:(NSString *)videoId completion:(DictionaryAndErrorBlock)completion;
-(void) NAPIReadVideoWithContentId:(NSString *)contentId limit:(int)limit skip:(int)skip completion:(DictionaryAndErrorBlock)completion;

// POST /rating
-(void) NAPICreateRatingWithContentId:(NSString *)contentId videoId:(NSString *)videoId profileId:(NSString *)profileId rating:(int)rating completion:(BoolAndErrorBlock)completion;

// POST /view
-(void) NAPICreateViewWithContentId:(NSString *)contentId videoId:(NSString *)videoId profileId:(NSString *)profileId completion:(BoolAndErrorBlock)completion;

// GET /updates
-(void) NAPIReadUpdateWithCategoryId:(NSString *)categoryId completion:(DictionaryAndErrorBlock)completion;
-(void) NAPIReadUpdateWithCategoryId:(NSString *)categoryId date:(NSString *)date completion:(DictionaryAndErrorBlock)completion;

// GET /update_dates
-(void) NAPIReadUpdateDatesWithCategoryId:(NSString *)categoryId limit:(int)limit skip:(int)skip completion:(DictionaryAndErrorBlock)completion;

// POST /source
-(void) NAPICreateSourceWithCategoryId:(NSString *)categoryId name:(NSString *)name url:(NSString *)url completion:(BoolAndErrorBlock)completion;

// GET /source
-(void) NAPIReadSourceWithCategoryId:(NSString *)categoryId completion:(DictionaryAndErrorBlock)completion;

// GET /category
-(void) NAPIReadCategoryWithCategoryId:(NSString *)categoryId completion:(DictionaryAndErrorBlock)completion;
-(void) NAPIReadCategoryWithParentCategoryId:(NSString *)parentCategoryId completion:(DictionaryAndErrorBlock)completion;

#pragma mark - Migration from MongoDB to MySQL

-(NSDictionary *) executeSql:(NSString *)sql read:(BOOL)read;

#pragma mark -

@end
