//
//  UserManager.m
//  PakTV
//
//  Created by Malik Khalid Ejaz on 11/16/2013.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import "UserManager.h"
#import "DataManager.h"
#import "SwitchOption.h"
#import "YoutubeVideo.h"
#import "Utility.h"

#define LOCAL_SEEK_TIMES_FILE_NAME      @"seekTimes.plist"

#define TIMEOUT_TO_RECHECK_UPDATE_FOR_CATEGORY      300

#define KEY_LAST_FAV_TAB        @"KEY_LAST_FAV_TAB"

#define KEY_FIRST_LAUCH         @"KEY_FIRST_LAUCH"
#define KEY_HAS_SELECTED_FAVS   @"KEY_HAS_SELECTED_FAVS"
#define KEY_HAS_RATED_APP       @"KEY_HAS_RATED_APP"
#define KEY_USAGE_COUNT         @"KEY_USAGE_COUNT"

#define KEY_INCORRECT_LOGIN_ATTEMPTS    @"KEY_INCORRECT_LOGIN"
#define KEY_INCORRECT_ATTEMPT_COUNT     @"ATTEMPT_COUT"
#define KEY_INCORRECT_ATTEMPT_DATE      @"ATTEMPT_DATE"

@implementation UserManager

+(UserManager *) sharedManager {
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
        [_sharedObject loadUsers];
        [_sharedObject loadFavourites];
    });
    
    // returns the same object each time
    return _sharedObject;
}

#pragma mark - previous version compatibility

-(BOOL) hasPreviousVersionFavs {
    if ([[DataManager sharedManager] hasPreviousVersionShowFavs] || [[DataManager sharedManager] hasPreviousVersionDramaFavs]) {
        return YES;
    }
    return NO;
}

#pragma mark - status checks

-(BOOL) isFirstLaunch {
    BOOL firstLaunch = ![[NSUserDefaults standardUserDefaults] boolForKey:KEY_FIRST_LAUCH];
    if (firstLaunch) {
        [self saveFirstLaunchDone];
    }
    return firstLaunch;
}

-(void) saveFirstLaunchDone {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:KEY_FIRST_LAUCH];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(BOOL) hasSelectedFavs {
    return [[NSUserDefaults standardUserDefaults] boolForKey:KEY_HAS_SELECTED_FAVS];
}

-(BOOL) isLoggedIn {
    return NO;
}

-(BOOL) hasRatedApp {
    return [[NSUserDefaults standardUserDefaults] boolForKey:KEY_HAS_RATED_APP];
}

-(NSInteger) usageCount {
    return [[NSUserDefaults standardUserDefaults] integerForKey:KEY_USAGE_COUNT];
}

-(NSInteger) incUsageCount {
    NSInteger usage = [[NSUserDefaults standardUserDefaults] integerForKey:KEY_USAGE_COUNT];
    usage++;
    [[NSUserDefaults standardUserDefaults] setInteger:usage forKey:KEY_USAGE_COUNT];
    [[NSUserDefaults standardUserDefaults] synchronize];
    return usage;
}

#pragma mark - Favourites management

-(BOOL) hasFavContentForCategoryId:(NSString *)categoryId {
//    for (NSDictionary *dict in self.favContents) {
//        if ([dict[KEY_CONTENT_CATEGORY_ID] isEqualToString:categoryId]) {
//            return YES;
//        }
//    }
    return NO;
}

+(BOOL) isChannelFavouriteAlready:(NSString *)channelId {
//    if ([UserChannel isFavouriteByChannelId:channelId]) {
//        return YES;
//    }
//    
//    if (([YoutubeChannel isFavouriteByChannelId:channelId])) {
//        return YES;
//    }
//    
    return NO;
}

-(void) setUserChannel:(UserChannel *) userChannel asFavourite:(BOOL)fav {
//    if (fav) {
//        if ([UserManager isChannelFavouriteAlready:userChannel.channelId]) {
//            return;
//        }
//        
////        [self.favUserChannels setObject:userChannel forKey:userChannel.channelId];
//        
//        NSMutableDictionary *favUserChannels = [[UserManager sharedManager] favUserChannels];
//        
//        [userChannel saveOnParseIfNeededWithCompletion:^(PFObject *object, NSError *error) {
//            if (!error) {
//                if (object) {
//                    UserChannel *channel = (UserChannel *)object;
//                    [favUserChannels setObject:[channel dictionary] forKey:channel.channelId];
//                    [[UserManager sharedManager] saveFavouriteUserChannels];
//                }
//            }
//        }];
//        
//    } else {
//        
//        if ([UserChannel isFavouriteByChannelId:userChannel.channelId]) {
//            [self.favUserChannels removeObjectForKey:userChannel.channelId];
//            [self saveFavouriteUserChannels];
//        } else if ([YoutubeChannel isFavouriteByChannelId:userChannel.channelId]) {
//            [self.favChannels removeObjectForKey:userChannel.channelId];
//            [self saveFavouriteChannels];
//        }
//    }
//    
//    NSNotification *notification = [NSNotification notificationWithName:NOTIFICATION_TOGGLE_FAV_ON_CHANNEL object:nil userInfo:@{@"channel":userChannel,@"selected":fav?@"yes":@"no"}];
//    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

-(void) setChannel:(YoutubeChannel *)channel asFavourite:(BOOL)fav {
//    if (fav) {
//        if ([UserManager isChannelFavouriteAlready:channel.channelId]) {
//            return;
//        }
//        
//        [self.favChannels setObject:[channel dictionary] forKey:channel.channelId];
//        [self saveFavouriteChannels];
//        
//    } else {
//        if ([YoutubeChannel isFavouriteByChannelId:channel.channelId]) {
//            [self.favChannels removeObjectForKey:channel.channelId];
//            [self saveFavouriteChannels];
//        } else if ([UserChannel isFavouriteByChannelId:channel.channelId]) {
//            [self.favUserChannels removeObjectForKey:channel.channelId];
//            [self saveFavouriteUserChannels];
//        }
//    }
//    
//    NSNotification *notification = [NSNotification notificationWithName:NOTIFICATION_TOGGLE_FAV_ON_CHANNEL object:nil userInfo:@{@"channel":channel,@"selected":fav?@"yes":@"no"}];
//    [[NSNotificationCenter defaultCenter] postNotification:notification];
}


-(void) saveVersions {
    [[DataManager sharedManager] saveDictionary:self.versionsDict AsFileName:LOCAL_VERSIONS_FILE_NAME];
}

-(void) saveFavouriteUserChannels {
//    [[DataManager sharedManager] saveDictionary:self.favUserChannels AsFileName:[self fileNameByAppendingUserName:LOCAL_FAV_USER_CHANNELS_FILE_NAME]];
}

-(void) saveFavouriteChannels {
//    [[DataManager sharedManager] saveDictionary:self.favChannels AsFileName:[self fileNameByAppendingUserName:LOCAL_FAV_CHANNELS_FILE_NAME]];
}

-(void) saveFavouriteVideos {
    [[DataManager sharedManager] saveDictionary:self.favVideos AsFileName:[self fileNameByAppendingUserName:LOCAL_FAV_VIDEOS_FILE_NAME]];
}

-(void) saveFavouriteCategories {
    [[DataManager sharedManager] saveDictionary:self.favCategories AsFileName:[self fileNameByAppendingUserName:LOCAL_FAV_CATS_FILE_NAME]];
}

-(void) saveFavouriteContents {
    [self setFavouritesChanged:YES];
    [[DataManager sharedManager] saveDictionary:self.favContents AsFileName:[self fileNameByAppendingUserName:LOCAL_FAV_CONTS_FILE_NAME]];
}

-(void) saveFavouriteEpisode {
    [[DataManager sharedManager] saveDictionary:self.favEpisodes AsFileName:[self fileNameByAppendingUserName:LOCAL_FAV_EPISODES_FILE_NAME]];
}

-(void) saveUserSettings {
    [[DataManager sharedManager] saveDictionary:self.userSettings AsFileName:LOCAL_USER_SETTINGS_FILE_NAME];
}

-(void) loadFavourites {
    
//    if (!self.currentUser) {
//        self.favForums = [NSMutableDictionary dictionary];
//        self.favEpisodes = [NSMutableDictionary dictionary];
//        self.favContents = [NSMutableDictionary dictionary];
//        self.favCategories = [NSMutableDictionary dictionary];
//        self.ratings = [NSMutableDictionary dictionary];
//        self.favChannels = [NSMutableDictionary dictionary];
//        self.favUserChannels = [NSMutableDictionary dictionary];
//        self.favVideos = [NSMutableDictionary dictionary];
//        self.views = [NSMutableArray array];
//        self.seekTimes = [NSMutableDictionary dictionary];
//        return;
//    }
//    
//    [self setFavouritesChanged:YES];
//    
//    self.seekTimes = [[DataManager sharedManager] readLocalDictionaryWithName:[self fileNameByAppendingUserName:LOCAL_SEEK_TIMES_FILE_NAME]];
//    if (!self.seekTimes) {
//        self.seekTimes = [NSMutableDictionary dictionary];
//    }
    
//    self.favForums = [[DataManager sharedManager] readLocalDictionaryWithName:[self fileNameByAppendingUserName:LOCAL_FAV_FORUMS_FILE_NAME]];
//    if (!self.favForums) {
//        self.favForums = [NSMutableDictionary dictionary];
//    }
    
//    self.favCategories = [[DataManager sharedManager] readLocalDictionaryWithName:[self fileNameByAppendingUserName:LOCAL_FAV_CATS_FILE_NAME]];
//    if (!self.favCategories) {
//        self.favCategories = [NSMutableDictionary dictionary];
//    }
//    
//    self.favContents = [[DataManager sharedManager] readLocalDictionaryWithName:[self fileNameByAppendingUserName:LOCAL_FAV_CONTS_FILE_NAME]];
//    if (!self.favContents) {
//        self.favContents = [NSMutableDictionary dictionary];
//    }
//    
//    self.favEpisodes = [[DataManager sharedManager] readLocalDictionaryWithName:[self fileNameByAppendingUserName:LOCAL_FAV_EPISODES_FILE_NAME]];
//    if (!self.favEpisodes) {
//        self.favEpisodes = [NSMutableDictionary dictionary];
//    }
//    
//    self.ratings = [[DataManager sharedManager] readLocalDictionaryWithName:[self fileNameByAppendingUserName:RATED_EPISODES_FILE_NAME]];
//    if (!self.ratings) {
//        self.ratings = [NSMutableDictionary dictionary];
//    }
//
//    self.views = [[DataManager sharedManager] readLocalArrayWithName:[self fileNameByAppendingUserName:VIEWED_EPISODES_FILE_NAME]];
//    if (!self.views) {
//        self.views = [NSMutableArray array];
//    }
    
//    self.favChannels = [[DataManager sharedManager] readLocalDictionaryWithName:[self fileNameByAppendingUserName:LOCAL_FAV_CHANNELS_FILE_NAME]];
//    if (!self.favChannels) {
//        self.favChannels = [NSMutableDictionary dictionary];
//    }
    
//    self.favUserChannels = [[DataManager sharedManager] readLocalDictionaryWithName:[self fileNameByAppendingUserName:LOCAL_FAV_USER_CHANNELS_FILE_NAME]];
//    if (!self.favUserChannels) {
//        self.favUserChannels = [NSMutableDictionary dictionary];
//    }
//    
//    self.favVideos = [[DataManager sharedManager] readLocalDictionaryWithName:[self fileNameByAppendingUserName:LOCAL_FAV_VIDEOS_FILE_NAME]];
//    if (!self.favVideos) {
//        self.favVideos = [NSMutableDictionary dictionary];
//    }
}

-(NSArray *) getCategoryIdsFromFavContent {
//    NSMutableArray *array = [NSMutableArray array];
//    for (NSString *key in self.favContents.allKeys) {
//        Content *aContent = [[Content alloc] initWithDictionary:self.favContents[key]];
//        if ([array containsObject:aContent.categoryId]) {
//            continue;
//        }
//        [array addObject:aContent.categoryId];
//    }
//    
//    return array;
    return nil;
}

-(NSArray *) getContentIdsFromFavContent {
//    return self.favContents.allKeys;
    return nil;
}

// get all favourited contents in this category.
-(NSArray *) getFavContentIdsForCategoryId:(NSString *)catId {
//    NSMutableArray *array = [NSMutableArray array];
//    for (NSString *key in self.favContents.allKeys) {
//        Content *aContent = [[Content alloc] initWithDictionary:self.favContents[key]];
//        if ([aContent.categoryId isEqualToString:catId]) {
//            [array addObject:aContent.objectId];
//        }
//    }
//    
//    return array;
    return nil;
}

-(BOOL) favouritesChanged {
//    BOOL changed = _favouritesChanged;
//    _favouritesChanged = NO;
//    return changed;
    return NO;
}

-(void) addContentAsFavouriteById:(NSString *)contentId {
//    [Content readContentWithId:contentId completion:^(PFObject *object, NSError *error) {
//        if (error) {
//            return ;
//        } else if (object) {
//            Content *content = (Content *)object;
//            [content saveAsFavourite];
//        }
//    }];
}

-(void) addCategoryAsFavouriteById:(NSString *)categoryId {
    
//    [ContentCategory readCategoryWithId:categoryId completion:^(PFObject *object, NSError *error) {
//        if (error) {
//            return ;
//        } else {
//            if (object) {
//                ContentCategory *category = (ContentCategory *)object;
//                NSNotification *notification = [NSNotification notificationWithName:NOTIFICATION_CHANGED_FAVS_FROM_SIDE_MENU object:nil userInfo:@{@"category":category,@"selected":@"yes"}];
//                [[NSNotificationCenter defaultCenter] postNotification:notification];
//            }
//        }
//    }];
}

#pragma mark - switch options related

-(NSArray *)switchOptionsFromFavCategories {
//    NSMutableArray *options = [NSMutableArray array];
//    if (!self.favCategories) {
//        self.favCategories = [[DataManager sharedManager] readLocalDictionaryWithName:[self fileNameByAppendingUserName:LOCAL_FAV_CATS_FILE_NAME]];
//    }
//    for (NSString *key in self.favCategories.allKeys) {
//        NSDictionary *dict = [self.favCategories objectForKey:key];
//        NSString *catName = dict[KEY_CATEGORY_NAME];
//        SwitchOption *opt = [[SwitchOption alloc] initWithDisplayText:catName value:key ascending:YES];
//        [options addObject:opt];
//    }
//    
////    if (self.favContents.count > 0) {
////        SwitchOption *opt = [[SwitchOption alloc] initWithDisplayText:HEART_SYMBOL value:SWITCH_VALUE_FAV ascending:YES];
////        [options addObject:opt];
////    }
//    
//    if (self.favChannels.allKeys.count > 0) {
//        for (NSString *key in self.favChannels.allKeys) {
//            NSDictionary *dict = [self.favChannels objectForKey:key];
//            SwitchOption *opt = [[SwitchOption alloc] initWithDisplayText:[self switchDisplayTextForYoutubeChannelName:dict[KEY_YOUTUBE_CHANNEL_NAME]] value:[self switchValueForYoutubeChannelId:dict[KEY_YOUTUBE_CHANNEL_ID]] ascending:YES];
//            [options addObject:opt];
//        }
//    }
//    
//    if (self.favUserChannels.allKeys.count > 0) {
//        for (NSString *key in self.favUserChannels.allKeys) {
//            NSDictionary *dict = [self.favUserChannels objectForKey:key];
//            SwitchOption *opt = [[SwitchOption alloc] initWithDisplayText:[self switchDisplayTextForYoutubeChannelName:dict[KEY_YOUTUBE_CHANNEL_NAME]] value:[self switchValueForYoutubeChannelId:dict[KEY_YOUTUBE_CHANNEL_ID]] ascending:YES];
//            [options addObject:opt];
//        }
//    }
//    
//    if (self.favForums.allKeys.count > 0) {
//        for (NSString *key in self.favForums.allKeys) {
//            NSDictionary *dict = [self.favForums objectForKey:key];
//            SwitchOption *opt = [[SwitchOption alloc] initWithDisplayText:dict[KEY_FORUM_NAME] value:[self switchValueForForumURL:dict[KEY_FORUM_URL]] ascending:YES];
//            [options addObject:opt];
//        }
//    }
//    
////    options = [[[DataManager sharedManager] sortObjects:options orderBy:KEY_SWITCH_OPTION_DISPLAY_TEXT ascending:YES] mutableCopy];
//    
//    options = [[[DataManager sharedManager] sortCaseInsensitiveObjects:options orderBy:KEY_SWITCH_OPTION_DISPLAY_TEXT ascending:YES] mutableCopy];
//    
//    return options;
    return nil;
}

-(NSString *)switchDisplayTextForYoutubeChannelName:(NSString *)channelName {
    NSString *displayText = channelName;
    if (displayText.length > 20) {
        displayText = [displayText substringToIndex:20];
    }
    return displayText;
}

-(NSString *) switchValueForYoutubeChannelId:(NSString *)channelId {
    return [NSString stringWithFormat:@"*YT_%@",channelId];
}

-(NSString *) switchValueForForumURL:(NSString *)forumURL {
    return [NSString stringWithFormat:@"*web_%@",forumURL];
}

#pragma mark - user accounts related

-(void) setCurrentUser:(User *)currentUser {
//    _currentUser = currentUser;
//    [[DataManager sharedManager] saveLastLoggedInUserName:self.currentUser.name];
//    [self loadFavourites];
}

-(void) loadUsers {
    
//    self.userSettings = [[DataManager sharedManager] readLocalDictionaryWithName:LOCAL_USER_SETTINGS_FILE_NAME];
//    if (!self.userSettings) {
//        self.userSettings = [NSMutableDictionary dictionary];
//    }
//    
//    self.versionsDict = [[DataManager sharedManager] readLocalDictionaryWithName:LOCAL_VERSIONS_FILE_NAME];
//    if (!self.versionsDict) {
//        self.versionsDict = [NSMutableDictionary dictionary];
//        [[DataManager sharedManager] saveDictionary:self.versionsDict AsFileName:LOCAL_VERSIONS_FILE_NAME];
//    }
//    
//    NSArray *usersArray = [[DataManager sharedManager] readLocalArrayWithName:LOCAL_USER_ACCOUNTS_FILE_NAME];
//    if (usersArray.count == 0) {
//        self.users = [NSMutableArray array];
//    } else {
//        self.users = [NSMutableArray array];
//        for (NSDictionary *userDict in usersArray) {
//            User *user = [[User alloc] initWithDictionary:userDict];
//            [self.users addObject:user];
//        }
//    }
//    NSString *lastUserName = [[DataManager sharedManager] lastLoggedInUserName];
//    self.currentUser = [self userWithName:lastUserName];
}

-(BOOL) userAccountNameExists:(NSString *)name {
//    for (User *user in self.users) {
//        if ([[user.name lowercaseString] isEqualToString:[name lowercaseString]]) {
//            return YES;
//        }
//    }
//    return NO;
    return nil;
}

-(BOOL) haveUserAccounts {
    if (self.users.count > 0) {
        return YES;
    } else return NO;
}

-(void) changeColorForUser:(NSString *)name toColor:(UIColor *)color {
    
//    for (int i=0; i<self.users.count; i++) {
//        User *user = self.users[i];
//        if ([user.name isEqualToString:name]) {
//            user.color = color;
//            [self.users replaceObjectAtIndex:i withObject:user];
//            [[DataManager sharedManager] saveArray:[self usersArrayToWrite] AsFileName:LOCAL_USER_ACCOUNTS_FILE_NAME];
//            return;
//        }
//    }
}

-(NSArray *) usersArrayToWrite {
//    NSMutableArray *array = [NSMutableArray array];
//    for (User *user in self.users) {
//        [array addObject:[user dictionary]];
//    }
//    return array;
return nil;
}

-(NSString *) addUserAccount:(NSString *)name color:(UIColor *)color pin:(NSString *)pin childAccount:(BOOL)childAccount {
//    if ([self userAccountNameExists:name]) {
//        return @"User account already exists.";
//    }
//    
//    User *user = [[User alloc] initWithDictionary:@{KEY_USER_NAME:name, KEY_USER_PIN:pin, KEY_USER_COLOR:[Utility stringForColor:color], KEY_USER_CHILD_USER:[NSNumber numberWithBool:childAccount]}];
//    
//    [self.users addObject:user];
//    [[DataManager sharedManager] saveArray:[self usersArrayToWrite] AsFileName:LOCAL_USER_ACCOUNTS_FILE_NAME];
//    
//    return nil;
    return nil;
}

-(void) removeUserAccount:(NSString *)name {
//    if (![self userAccountNameExists:name]) {
//        return;
//    }
//    
//    int index = -1;
//    for (int i=0;i <self.users.count; i++) {
//        User *user = self.users[i];
//        if ([[user.name lowercaseString] isEqualToString:[name lowercaseString]]) {
//            index = i;
//            break;
//        }
//    }
//    if (index != -1) {
//        [self.users removeObjectAtIndex:index];
//        
//        // delete all user content.
//        [[DataManager sharedManager] deleteUserFile:[self fileNameByAppendingUserName:LOCAL_FAV_FORUMS_FILE_NAME]];
//        [[DataManager sharedManager] deleteUserFile:[self fileNameForFile:LOCAL_FAV_CATS_FILE_NAME byAppendingUserName:name]];
//        [[DataManager sharedManager] deleteUserFile:[self fileNameForFile:LOCAL_FAV_CONTS_FILE_NAME byAppendingUserName:name]];
//        [[DataManager sharedManager] deleteUserFile:[self fileNameForFile:LOCAL_FAV_EPISODES_FILE_NAME byAppendingUserName:name]];
//        [[DataManager sharedManager] deleteUserFile:[self fileNameForFile:LOCAL_FAV_CHANNELS_FILE_NAME byAppendingUserName:name]];
//        [[DataManager sharedManager] deleteUserFile:[self fileNameForFile:LOCAL_FAV_USER_CHANNELS_FILE_NAME byAppendingUserName:name]];
//        [[DataManager sharedManager] deleteUserFile:[self fileNameForFile:LOCAL_FAV_VIDEOS_FILE_NAME byAppendingUserName:name]];
//        [[DataManager sharedManager] deleteUserFile:[self fileNameForFile:VIEWED_EPISODES_FILE_NAME byAppendingUserName:name]];
//        [[DataManager sharedManager] deleteUserFile:[self fileNameForFile:RATED_EPISODES_FILE_NAME byAppendingUserName:name]];
//        [[DataManager sharedManager] deleteUserFile:[self fileNameForFile:RATED_CHANNELS_FILE_NAME byAppendingUserName:name]];
//        [[DataManager sharedManager] deleteUserFile:LOCAL_USER_SETTINGS_FILE_NAME];
//        
//        // save updated users array.
//        [[DataManager sharedManager] saveArray:[self usersArrayToWrite] AsFileName:LOCAL_USER_ACCOUNTS_FILE_NAME];
//        
//        [self logoutUserName:name];
//    }
}

-(BOOL) logoutCurrentUserIfNeedsPinAccess {
//    if (!self.currentUser.childUser && self.currentUser.pin.length == 4) {
//        [[UserManager sharedManager] logoutUser];
//        return YES;
//    }
    return NO;
}

-(void) logoutUserName:(NSString *)name {
    
//    if ([self.currentUser.name isEqualToString:name]) {
//        self.currentUser = nil;
//        
//        NSNotification *noti = [NSNotification notificationWithName:NOTIFICATION_USER_LOG_OUT object:nil];
//        [[NSNotificationCenter defaultCenter] postNotification:noti];
//    }
}

-(void) logoutUser {
//    if (self.currentUser) {
//        [self logoutUserName:self.currentUser.name];
//    }
}

-(User *) userWithName:(NSString *)name {
//    for (User *user in self.users) {
//        if ([[user.name lowercaseString] isEqualToString:[name lowercaseString]]) {
//            return user;
//        }
//    }
    return nil;
}

-(UIColor *)colorForNumber:(int)number {
    number = number % 9;
    switch(number) {
        case 0:
            return [UIColor colorWithRed:162.0/255.0 green:21.0/255.0 blue:34.0/255.0 alpha:1.0];
            break;
        case 1:
//            return [UIColor colorWithRed:22.0/255.0 green:106.0/255.0 blue:237.0/255.0 alpha:1.0];
            return [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:100.0/255.0 alpha:1.0];
            break;
        case 2:
            return [UIColor colorWithRed:40.0/255.0 green:160.0/255.0 blue:160.0/255.0 alpha:1.0];
//            return [UIColor colorWithRed:200.0/255.0 green:150.0/255.0 blue:20.0/255.0 alpha:1.0];
            break;
        case 3:
            return [UIColor colorWithRed:46.0/255.0 green:100.0/255.0 blue:50.0/255.0 alpha:1.0];
            break;
        case 4:
            return [UIColor colorWithRed:50.0/255.0 green:100.0/255.0 blue:200.0/255.0 alpha:1.0];
//            return [UIColor colorWithRed:130.0/255.0 green:130.0/255.0 blue:130.0/255.0 alpha:1.0];
            break;
        case 5:
            return [UIColor colorWithRed:175.0/255.0 green:97.0/255.0 blue:148.0/255.0 alpha:1.0];
            break;
        case 6:
            return [UIColor colorWithRed:52.0/255.0 green:52.0/255.0 blue:52.0/255.0 alpha:1.0];
            break;
        case 7:
//            return [UIColor colorWithRed:53.0/255.0 green:153.0/255.0 blue:220.0/255.0 alpha:1.0];
            return [UIColor colorWithRed:50.0/255.0 green:104.0/255.0 blue:140.0/255.0 alpha:1.0];
            break;
        case 8:
            return [UIColor colorWithRed:223.0/255.0 green:105.0/255.0 blue:66.0/255.0 alpha:1.0];
            break;
        default:
            break;
    }
    // red color is default.
    return [UIColor colorWithRed:162.0/255.0 green:21.0/255.0 blue:34.0/255.0 alpha:1.0];
}

-(UIColor *) darkerColorForUser {
    return [Utility darkerColorForColor:[self colorForUser]];
}

-(UIColor *) lighterColorForUser {
    return [Utility lighterColorForColor:[self colorForUser]];
}

-(UIColor *)colorForUser {
//    if (!self.currentUser.name) {
        return [UIColor colorWithRed:162.0/255.0 green:21.0/255.0 blue:34.0/255.0 alpha:1.0];
//    }
    
//    return self.currentUser.color;
}

-(UIColor *)colorForUserName:(NSString *)name {
//    if (!name) {
        return [UIColor colorWithRed:162.0/255.0 green:21.0/255.0 blue:34.0/255.0 alpha:1.0];
//    }
//    
//    User *user = [self userWithName:name];
//    return user.color;
}

#pragma mark - ratings related.

// returns integer amount with which to increment ratings for this episode.
-(void) saveRatings:(int)rating forEpisode:(Episode *)episode {
    
//    if (self.ratings.allKeys.count >= 100) {
//        [self.ratings removeObjectForKey:[self.ratings.allKeys firstObject]];
//    }
//    
//    [self.ratings setObject:[NSNumber numberWithInteger:rating] forKey:episode.objectId];
//    [[DataManager sharedManager] saveDictionary:self.ratings AsFileName:[self fileNameByAppendingUserName:RATED_EPISODES_FILE_NAME]];
}

-(int) userRatingsForEpisode:(Episode *)episode {
//    if ([self.ratings.allKeys containsObject:episode.objectId]) {
//        return [self.ratings[episode.objectId] integerValue];
//    }
    return 0;
}

#pragma mark - views related

-(BOOL) alreadyMakedEpisodeAsViewed:(Episode *)episode {
//    if ([self.views containsObject:episode.objectId]) {
//        return YES;
//    }
    return NO;
}

-(void) saveViewedEpisode:(Episode *)episode {
//    [self.views addObject:episode.objectId];
//    
//    if (self.views.count >= 100) {
//        [self.views removeObjectAtIndex:0];
//    }
//    
//    [[DataManager sharedManager] saveArray:self.views AsFileName:[self fileNameByAppendingUserName:VIEWED_EPISODES_FILE_NAME]];
}

#pragma mark - file name related

-(NSString *)fileNameForFile:(NSString *)file byAppendingUserName:(NSString *)userName {
//    return [NSString stringWithFormat:@"%@_%@",[userName stringByReplacingOccurrencesOfString:@" " withString:@"_"], file];
    return nil;
}

-(NSString *)fileNameByAppendingUserName:(NSString *)fileName {
//    return [NSString stringWithFormat:@"%@_%@",[self.currentUser.name stringByReplacingOccurrencesOfString:@" " withString:@"_"], fileName];
    return nil;
}

#pragma mark - user log in related

-(NSString *) attemptsDictionaryKeyForUser:(User *)user {
//    return [NSString stringWithFormat:@"%@_%@",KEY_INCORRECT_LOGIN_ATTEMPTS, user.name];
    return nil;
}

-(void) saveIncorrectAttemptForUser:(User *)user {
    
    // increments attempts made today. or start from 1 and set last attempt date to today.
    
//    NSString *key = [self attemptsDictionaryKeyForUser:user];
//    
//    NSMutableDictionary *attemptDict = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:key] mutableCopy];
//
//    if (!attemptDict) {
//        
//        attemptDict = [NSMutableDictionary dictionary];
//        
//        [attemptDict setObject:[NSNumber numberWithInteger:1] forKey:KEY_INCORRECT_ATTEMPT_COUNT];
//        [attemptDict setObject:[NSDate date] forKey:KEY_INCORRECT_ATTEMPT_DATE];
//        
//        [[NSUserDefaults standardUserDefaults] setObject:attemptDict forKey:key];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        
//    } else {
//        NSNumber *attempts = attemptDict[KEY_INCORRECT_ATTEMPT_COUNT];
//        
//        NSDate *attemptDate = attemptDict[KEY_INCORRECT_ATTEMPT_DATE];
//        NSString *todayDateString = [Utility dateStringFromDate:[NSDate date]];
//        NSString *dateString = [Utility dateStringFromDate:attemptDate];
//        
//        if ([todayDateString isEqualToString:dateString]) {
//            NSNumber *newAttemptsCount = [NSNumber numberWithInteger:[attempts integerValue] + 1];
//            [attemptDict setObject:newAttemptsCount forKey:KEY_INCORRECT_ATTEMPT_COUNT];
//        } else {
//            NSNumber *newAttemptsCount = [NSNumber numberWithInteger:1];
//            [attemptDict setObject:[NSDate date] forKey:KEY_INCORRECT_ATTEMPT_DATE];
//            [attemptDict setObject:newAttemptsCount forKey:KEY_INCORRECT_ATTEMPT_COUNT];
//        }
//        
//        [[NSUserDefaults standardUserDefaults] setObject:attemptDict forKey:key];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//    }
}

-(void) removeIncorrectAttemptsForUser:(User *)user {
//    NSString *key = [self attemptsDictionaryKeyForUser:user];
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
//    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(int) incorrectAttemptsForUser:(User *)user {
    // if last attempt was more than five minutes old. failed attempt count becomes zero.
//    NSString *key = [self attemptsDictionaryKeyForUser:user];
//    
//    NSMutableDictionary *attemptDict = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:key] mutableCopy];
//    
//    if (!attemptDict) {
//        attemptDict = [NSMutableDictionary dictionary];
//        
//        [attemptDict setObject:[NSNumber numberWithInteger:0] forKey:KEY_INCORRECT_ATTEMPT_COUNT];
//        
//        [[NSUserDefaults standardUserDefaults] setObject:attemptDict forKey:key];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        
//        return 0;
//    } else {
//        NSNumber *attempts = attemptDict[KEY_INCORRECT_ATTEMPT_COUNT];
//        
//        if ([attempts integerValue] == 0) {
//            return 0;
//        } else {
//            NSDate *attemptDate = attemptDict[KEY_INCORRECT_ATTEMPT_DATE];
//            NSDate *todayDate = [NSDate date];
//            double attemptTime = [attemptDate timeIntervalSince1970];
//            double nowTime = [todayDate timeIntervalSince1970];
//            
//            if (nowTime - attemptTime > 300) {
//                // last attempt was old.
//                [self removeIncorrectAttemptsForUser:user];
//                return 0;
//            } else {
//                int seconds = 300 - (nowTime - attemptTime);
//                if (seconds < 60) {
//                    self.timeToReAttemptAccess = [NSString stringWithFormat:@"%d seconds", seconds];
//                } else {
//                    int min = seconds/60;
//                    seconds = seconds%60;
//                    self.timeToReAttemptAccess = [NSString stringWithFormat:@"%d min %d seconds",min, seconds];
//                }
//                return [attempts integerValue];
//            }
//        }
//    }
    
    return 0;
}

#pragma mark - tip/help on first user action.

-(NSString *) TipForAction:(NSString *)action {

    if ([self.userSettings.allKeys containsObject:action]) {
        return nil;
    } else {
        [self.userSettings setObject:@"done" forKey:action];
        [self saveUserSettings];
    }
    
    if ([action isEqualToString:TIP_ACTION_APP_LAUNCH]) {
        return @"Welcome! This update brings many new features. There is a slide out side menu to get you around quickly. You can use 'Browse' and 'Search on YouTube' menu options to mark stuff you like as favourite. Favourited stuff appears on Home screen. Don't forget to checkout 'Forums' in menu.";
    } else if ([action isEqualToString:TIP_ACTION_CREATE_ACCOUNT]) {
        return @"Enter a name for your account to get started. Later, You can also create multiple accounts for same user to keep different kinds of content in different accounts. You can lock access to your account with a PIN code.";
    } else if ([action isEqualToString:TIP_ACTION_BROWSE]) {
        return @"Everything is categorized in categories, subcategories, channels and contents. Here you can go browse and choose what you like. Tap ☐ on channels and categories you like and ♡ to mark favourite content.";
    } else if ([action isEqualToString:TIP_ACTION_SEARCH_YOUTUBE]) {
        return @"Here, search directly on youtube. Hit ⌚ to tag videos to watch later. Tap ☐ on channel results to show as tab on Home screen.";
    } else if ([action isEqualToString:TIP_ACTION_FAVS]) {
        return @"Everything you marked as favourite is listed here. You can also remove favourites here.";
    } else if ([action isEqualToString:TIP_ACTION_WATCH_LATER]) {
        return @"This video has been added to your watch later list. You can find 'Watch Later' in side menu.";
    } else if ([action isEqualToString:TIP_ACTION_TAG_CATEGORY]) {
        return @"A tab for this category has been added to Home screen.";
    } else if ([action isEqualToString:TIP_ACTION_TAG_CHANNEL]) {
        return @"A tab for this channel has been added to Home screen. Any new videos added in this channel will also appear there.";
    } else if ([action isEqualToString:TIP_ACTION_FAV_CONTENT]) {
        return @"Selecting favourite content allows you to filter videos on Home screen categories to only show updates to what you are more interested in.";
    } else if ([action isEqualToString:TIP_ACTION_PLAY_VIDEO]) {
        return @"Here you can watch video, tag video to save in watch later & rate content. If this video is broken or not what you were looking for, hit 'Find on YouTube' in bottom right to get alternate links on YouTube.";
    } else if ([action isEqualToString:TIP_ACTION_TAG_FORUM]) {
        return @"A tab for this forum has been added to Home screen.";
    }
    
    return nil;
}

// last tab related.

-(void) saveLastSelectedHomeTab:(NSString *)optionString {
//    NSString *key = [NSString stringWithFormat:@"%@_%@",self.currentUser.name,KEY_LAST_FAV_TAB];
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setValue:optionString forKey:key];
//    [defaults synchronize];
}

-(NSString *) getLastSelectedHomeTab {
//    NSString *key = [NSString stringWithFormat:@"%@_%@",self.currentUser.name,KEY_LAST_FAV_TAB];
//    NSString *val = [[NSUserDefaults standardUserDefaults] valueForKey:key];
//    return val;
    return nil;
}

// resue playback related.
- (CGFloat)lastPlaybackSeekTimeForVID:(NSString *)vid {
    NSNumber *number = self.seekTimes[vid];
    if (number) {
        return [number floatValue];
    }
    return 0;
}

- (void)savePlayBackSeekTimeForVID:(NSString *)vid seekTime:(CGFloat)time {
    [self.seekTimes setObject:[NSNumber numberWithFloat:time] forKey:vid];
    [[DataManager sharedManager] saveDictionary:self.favChannels AsFileName:[self fileNameByAppendingUserName:LOCAL_SEEK_TIMES_FILE_NAME]];
}

- (void)removePlayBackSeekTimeForVID:(NSString *)vid {
    [self.seekTimes removeObjectForKey:vid];
    [[DataManager sharedManager] saveDictionary:self.favChannels AsFileName:[self fileNameByAppendingUserName:LOCAL_SEEK_TIMES_FILE_NAME]];
}

@end
