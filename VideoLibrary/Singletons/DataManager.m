//
//  DataManager.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/18/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "DataManager.h"
#import "Utility.h"

//#import <AFNetworking/AFHTTPRequestOperation.h>
//#import <AFNetworking/AFHTTPClient.h>
//#import <AFNetworking/AFHTTPRequestOperationManager.h>

#import "SwitchOption.h"
#import "YoutubeVideo.h"

// youtube channels related.
#import "YCSearchResult.h"
#import "YCSearchResultItem.h"
#import "YCSeachVideoResult.h"

#define KEY_SELECTED_CATEGORY               @"key_SelectedCategory"
#define KEY_LAST_LOGGED_IN_USER             @"key_Last_User"

#define SERVER_ROOT_PATH                    @"http://www.anmaple.com/"

#define SERVER_IMAGES_FOLDER_PATH           @"http://www.anmaple.com/VideoLibrary/images/"

//#define YOUTUBE_API_KEY         @"AIzaSyAn_0Wt4iu0n3h5teFlEc3U7c1bjCLV_ww"    // iOS key
#define YOUTUBE_API_KEY             @"AIzaSyDr7xiS3-hTqmQFx05f15q--v4Ahw145S0"
#define CHANNEL_LIST_MAX_RESULTS    @"10"
#define VIDEO_LIST_MAX_RESULTS      @"10"

// previous version fav file names.
#define PREV_VER_FAVS_SHOWS_FILE_NAME                            @"showfavs.plist"
#define PREV_VER_FAVS_DRAMAS_FILE_NAME                           @"dramasfavs.plist"

@interface DataManager()

@property (nonatomic, strong) NSOperationQueue *remoteDataLoadQueue;

@end

@implementation DataManager

+(id) sharedManager {
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
        [_sharedObject setRemoteDataLoadQueue:[[NSOperationQueue alloc] init]];
    });
    
    // returns the same object each time
    return _sharedObject;
}

-(NSString *) serverImageUploadPHPPath {
    return @"image_to_library.php";
}

#pragma mark - User defauls

#pragma mark - save selected/default categoryId.

-(NSString *) selectedCategoryId {
    return [[NSUserDefaults standardUserDefaults] valueForKey:KEY_SELECTED_CATEGORY];
}

-(void) setSelectedCategoryId:(NSString *)selectedCategoryId {
    [[NSUserDefaults standardUserDefaults] setObject:selectedCategoryId forKey:KEY_SELECTED_CATEGORY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - read parse objects

-(void) readCategoryWithId:(NSString *)categoryId completion:(CategoryAndErrorBlock)completion {
//    PFQuery *query = [PFQuery queryWithClassName:[ContentCategory parseClassName]];
//    if ([categoryId isEqualToString:@"root"]) {
//        [query whereKey:@"categoryName" equalTo:categoryId];
//    } else [query whereKey:@"objectId" equalTo:categoryId];
//    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//        if (error) {
//            completion(nil, error);
//        } else {
//            if (objects.count > 0) {
//                ContentCategory *category = [objects objectAtIndex:0];
//                completion(category, nil);
//            } else completion(nil, error);
//        }
//    }];
}

-(void) readCategoriesWithParentCategoryId:(NSString *)categoryId completion:(ArrayAndErrorBlock)completion {
//    PFQuery *query = [PFQuery queryWithClassName:[ContentCategory parseClassName]];
//    [query whereKey:@"parentCategoryId" equalTo:categoryId];
//    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//        if (error) {
//            completion(nil, error);
//        } else {
//            completion(objects, error);
//        }
//    }];
}

#pragma mark - detect video content form webpages

#pragma mark - detect dailymotion link from web pages

-(void)detectLinksFromDailyMotionFromURL:(NSURL *) url withCompletion:(ArrayAndErrorBlock)completion {
    NSMutableArray *foundParts = [NSMutableArray array];
    NSError *error;
    
    NSString *txt = [NSString stringWithFormat:@""];
    
    NSString *page = [NSString stringWithContentsOfURL:url encoding:NSASCIIStringEncoding error:&error];
    
    if(error)
    {
        //        jak5i u4oz,nzm//NSLog(@"Error: %@",error);
    }
    ////NSLog(@"URL: %@",currentURL);
    page = [page stringByReplacingOccurrencesOfString:@"/swf/" withString:@"/embed/video/"];
    page = [page stringByReplacingOccurrencesOfString:@"&" withString:@"'"];
    page = [page stringByReplacingOccurrencesOfString:@"\"" withString:@"'"];
    NSArray *parts = [page componentsSeparatedByString:@"http://www.dailymotion.com/embed/video/"];
    int videos = [parts count] - 1;
    //NSLog(@"Parts: %d", videos);
    if(videos > 0)
    {
        for(int i=1;i<=videos;i++)
        {
            NSArray *p = [[parts objectAtIndex:i] componentsSeparatedByString:@"'"];
            if([p count] > 1)
            {
                NSString *tmp1 = [NSString stringWithFormat:@"%@",[p objectAtIndex:0]];
                NSArray *art = [tmp1 componentsSeparatedByString:@"\""];
                if([art count] > 1)
                {
                    if(![foundParts containsObject:[art objectAtIndex:0]])
                    {
                        [foundParts addObject:[art objectAtIndex:0]];
                    }
                }
                else
                {
                    if(![foundParts containsObject:tmp1])
                    {
                        [foundParts addObject:tmp1];
                    }
                }
                art = nil;
            }
            else
            {
                p = [[parts objectAtIndex:i] componentsSeparatedByString:@"\""];
                if([p count] > 1)
                {
                    NSString *tmp1 = [NSString stringWithFormat:@"%@",[p objectAtIndex:0]];
                    if(![foundParts containsObject:tmp1])
                    {
                        [foundParts addObject:tmp1];
                    }
                }
            }
        }
    }
    if([foundParts count] == 0)
    {
        parts = [page componentsSeparatedByString:@"https://www.dailymotion.com/embed/video/"];
        videos = [parts count] - 1;
        //NSLog(@"Parts: %d", videos);
        if(videos > 0)
        {
            for(int i=1;i<=videos;i++)
            {
                NSArray *p = [[parts objectAtIndex:i] componentsSeparatedByString:@"'"];
                if([p count] > 1)
                {
                    NSString *tmp1 = [NSString stringWithFormat:@"%@",[p objectAtIndex:0]];
                    if(![foundParts containsObject:tmp1])
                    {
                        [foundParts addObject:tmp1];
                    }
                }
                else
                {
                    p = [[parts objectAtIndex:i] componentsSeparatedByString:@"\""];
                    if([p count] > 1)
                    {
                        NSString *tmp1 = [NSString stringWithFormat:@"%@",[p objectAtIndex:0]];
                        if(![foundParts containsObject:tmp1])
                        {
                            [foundParts addObject:tmp1];
                        }
                    }
                }
            }
        }
    }
    
    NSString *tmp;
    
    if([foundParts count] == 0)
    {
        NSLog(@"No video found. trying with link id.");
        NSString *stt = [url absoluteString];
        NSArray *stA = [stt componentsSeparatedByString:@"?id="];
        if([stA count] > 1)
        {
            if(![foundParts containsObject:[stA objectAtIndex:1]])
                [foundParts addObject:[stA objectAtIndex:1]];
        }
    }
    
    
    for(int i=0;i<[foundParts count];i++)
    {
        tmp = [NSString stringWithFormat:@"http://www.dailymotion.com/embed/video/%@",[foundParts objectAtIndex:i]];
        [foundParts replaceObjectAtIndex:i withObject:tmp];
        //NSLog(@"Part %d: %@",(i+1),tmp);
        if(i == 0)
            txt = [NSString stringWithFormat:@"%@",tmp];
        else txt = [NSString stringWithFormat:@"%@\n%@",txt,tmp];
    }
    NSLog(@"New Array : %@",txt);
    completion(foundParts, nil);
}

#pragma mark - Detect videos from URLs

-(void) detectYoutubeVideosFromURL:(NSURL *)url withCompletion:(ArrayAndErrorBlock)completion {

//    NSString *stringUrl = [url absoluteString];
//    if ([stringUrl rangeOfString:@"http://www.youtube.com/watch?v="].location != NSNotFound) {
//        stringUrl = [stringUrl stringByReplacingOccurrencesOfString:@"http://www.youtube.com/watch?v=" withString:@""];
//        completion(@[stringUrl],nil);
//        return;
//    }
    
    NSError *error;
    NSMutableArray *foundParts = [NSMutableArray array];
    
    NSString *txt = [NSString stringWithFormat:@""];
    
    NSString *page = [NSString stringWithContentsOfURL:url encoding:NSASCIIStringEncoding error:&error];
    if(error)
    {
        //        return nil;
    }
    
    page = [page stringByReplacingOccurrencesOfString:@"," withString:@"\""];
    page = [page stringByReplacingOccurrencesOfString:@"&" withString:@"\""];
    page = [page stringByReplacingOccurrencesOfString:@"?" withString:@"\""];
    NSArray *parts = [page componentsSeparatedByString:@"www.youtube.com/embed/"];
    int videos = [parts count] - 1;
    
    if(videos > 0)
    {
        for(int i=1;i<=videos;i++)
        {
            NSArray *p = [[parts objectAtIndex:i] componentsSeparatedByString:@"\""];
            NSString *tmp1 = [NSString stringWithFormat:@"%@",[p objectAtIndex:0]];
            NSArray *p1 = [tmp1 componentsSeparatedByString:@"?"];
            if([p1 count] > 1)
            {
                NSString *ts1 = [p1 objectAtIndex:0];
                NSArray *p2 = [ts1 componentsSeparatedByString:@"&"];
                if([p2 count] > 1)
                    [foundParts addObject:[p2 objectAtIndex:0]];
                [foundParts addObject:[p1 objectAtIndex:0]];
            }
            else
            {
                NSString *ts1 = [p objectAtIndex:0];
                NSArray *p2 = [ts1 componentsSeparatedByString:@"&"];
                if([p2 count] > 1)
                    [foundParts addObject:[p2 objectAtIndex:0]];
                [foundParts addObject:[p objectAtIndex:0]];
            }
        }
    }
    // if no video link found. try finding link with youtu.be
    if([foundParts count] == 0)
    {
        //NSLog(@"Trying to find link with youtu.be");
        parts = [page componentsSeparatedByString:@"http://youtu.be/"];
        videos = [parts count] - 1;
        if(videos > 0)
        {
            for(int i=1;i<=videos;i++)
            {
                //            //NSLog(@"Part %d: %@",i,[parts objectAtIndex:i]);
                
                NSArray *p = [[parts objectAtIndex:i] componentsSeparatedByString:@"\""];
                ////NSLog(@"Part %d: %@\n",i,[p objectAtIndex:0]);
                NSString *tmp1 = [NSString stringWithFormat:@"%@",[p objectAtIndex:0]];
                //NSLog(@"%@",tmp1);
                NSArray *p1 = [tmp1 componentsSeparatedByString:@"?"];
                ////NSLog(@"%@",p1);
                if([p1 count] > 1)
                {
                    NSString *ts1 = [p1 objectAtIndex:0];
                    NSArray *p2 = [ts1 componentsSeparatedByString:@"&"];
                    if([p2 count] > 1)
                        [foundParts addObject:[p2 objectAtIndex:0]];
                    [foundParts addObject:[p1 objectAtIndex:0]];
                }
                else
                {
                    NSString *ts1 = [p objectAtIndex:0];
                    NSArray *p2 = [ts1 componentsSeparatedByString:@"&"];
                    if([p2 count] > 1)
                        [foundParts addObject:[p2 objectAtIndex:0]];
                    [foundParts addObject:[p objectAtIndex:0]];
                }
            }
        }
        
    }
    if([foundParts count] == 0)
    {
        //NSLog(@"Trying to find links with http://www.youtube.com/v/");
        parts = [page componentsSeparatedByString:@"www.youtube.com/v/"];
        videos = [parts count] - 1;
        if(videos > 0)
        {
            for(int i=1;i<=videos;i++)
            {
                //            //NSLog(@"Part %d: %@",i,[parts objectAtIndex:i]);
                
                NSArray *p = [[parts objectAtIndex:i] componentsSeparatedByString:@"\""];
                ////NSLog(@"Part %d: %@\n",i,[p objectAtIndex:0]);
                NSString *tmp1 = [NSString stringWithFormat:@"%@",[p objectAtIndex:0]];
                //NSLog(@"%@",tmp1);
                NSArray *p1 = [tmp1 componentsSeparatedByString:@"?"];
                ////NSLog(@"%@",p1);
                NSString *toAdd = @"";
                if([p1 count] > 1)
                {
                    // if not already added same link.
                    NSString *ts1 = [p1 objectAtIndex:0];
                    NSArray *p2 = [ts1 componentsSeparatedByString:@"&"];
                    if([p2 count] > 1)
                        toAdd = [NSString stringWithFormat:@"%@",[p2 objectAtIndex:0]];
                    else toAdd = [NSString stringWithFormat:@"%@",[p1 objectAtIndex:0]];
                }
                else
                {
                    NSString *ts1 = [p objectAtIndex:0];
                    NSArray *p2 = [ts1 componentsSeparatedByString:@"&"];
                    if([p2 count] > 1)
                        toAdd = [NSString stringWithFormat:@"%@",[p2 objectAtIndex:0]];
                    else toAdd = [NSString stringWithFormat:@"%@",[p objectAtIndex:0]];
                }
                
                int repeat = 0;
                for(int j=0;j<[foundParts count]; j++)
                {
                    NSString *sFound = [foundParts objectAtIndex:j];
                    if([toAdd compare:sFound] == NSOrderedSame)
                    {
                        repeat = 1;
                    }
                }
                
                if(repeat)
                {
                    NSLog(@"Already detected same link.");
                }
                else [foundParts addObject:toAdd];
            }
        }
    }
    
    if(foundParts.count > 0) {
        [[DataManager sharedManager] findVideoDetailByVideoIds:foundParts completion:^(NSArray *objects, NSError *error) {
            NSLog(@"%@",objects);
        }];
    }
    
    NSString *tmp;
    for(int i=0;i<[foundParts count];i++)
    {
        tmp = [NSString stringWithFormat:@"http://www.youtube.com/embed/%@",[foundParts objectAtIndex:i]];
        [foundParts replaceObjectAtIndex:i withObject:tmp];
        if(i == 0)
            txt = [NSString stringWithFormat:@"%@",tmp];
        else txt = [NSString stringWithFormat:@"%@\n%@",txt,tmp];
    }
    completion(foundParts, nil);
}

-(NSURL *) getPreviewMediumQualityImageURLOfYoutubeVideoId:(NSString *)vid {
    if([vid rangeOfString:@"dailymotion"].location != NSNotFound) {
        NSString *imglink = [vid stringByReplacingOccurrencesOfString:@"/embed/" withString:@"/thumbnail/"];
        return [NSURL URLWithString:imglink];
    } else {
        NSString *previewURL = [NSString stringWithFormat:@"https://i1.ytimg.com/vi/%@/mqdefault.jpg",vid];
        return [NSURL URLWithString:previewURL];
    }
}

-(NSURL *) getPreviewImageURLOfYoutubeVideo:(NSString *)vid number:(int)number {
    if([vid rangeOfString:@"dailymotion"].location != NSNotFound) {
        vid = [vid stringByReplacingOccurrencesOfString:@"https://www.youtube.com/embed/" withString:@""];
        NSString *imglink = [vid stringByReplacingOccurrencesOfString:@"/embed/" withString:@"/thumbnail/"];
        return [NSURL URLWithString:imglink];
    } else {
        vid = [vid stringByReplacingOccurrencesOfString:@"http:" withString:@"https:"];
        NSString *previewURL = [vid stringByReplacingOccurrencesOfString:@"https://www.youtube.com/embed/" withString:@"https://img.youtube.com/vi/"];
        previewURL = [NSString stringWithFormat:@"%@/%d.jpg",previewURL, number];
        return [NSURL URLWithString:previewURL];
    }
}

-(NSString *) getLengthOfYoutubeVideo:(NSString *)video
{
    NSString *vid = [video stringByReplacingOccurrencesOfString:@"http://www.youtube.com/embed/" withString:@""];
    NSString *ur = [NSString stringWithFormat:@"https://eol.gdata.youtube.com/feeds/api/videos/%@?v=2&alt=json",vid];
    NSString *lst = [NSString stringWithContentsOfURL:[NSURL URLWithString:ur] encoding:NSUTF8StringEncoding error:nil];
    NSArray *parts = [lst componentsSeparatedByString:@"\"duration\":"];
    NSString *duration;
    if(parts)
    {
        if([parts count] > 1)
        {
            duration = [parts objectAtIndex:1];
            NSArray *p = [duration componentsSeparatedByString:@","];
            if([p count] > 1)
            {
                duration = [p objectAtIndex:0];
                float dur = [duration floatValue];
                dur = dur/60.0f;
                duration = [NSString stringWithFormat:@"%.02f",dur];
                NSLog(@"Video Duration: %.02f",dur);
                return duration;
            }
        }
    }
    return @"";
}

-(NSMutableArray *) shortLinks:(NSArray *)links {
    NSMutableArray *array = [NSMutableArray array];
    for (NSString *link in links) {
        [array addObject:[link stringByReplacingOccurrencesOfString:@"http://www.youtube.com/embed/" withString:@""]];
    }
    return array;
}

-(NSMutableArray *) longLinks:(NSArray *)links {
    NSMutableArray *array = [NSMutableArray array];
    for (NSString *link in links) {
        if ([link rangeOfString:@"dailymotion"].location != NSNotFound) {
            [array addObject:link];
        } else if ([link rangeOfString:@"youtube"].location == NSNotFound) {
            [array addObject:[NSString stringWithFormat:@"%@%@", @"https://www.youtube.com/embed/", link]];
        }
    }
    return array;
}

-(NSString *) getYoutubeEmbedLinkFromVID:(NSString *)vid {
    return [NSString stringWithFormat:@"https://www.youtube.com/embed/%@", vid];
}

#pragma mark - auto detect content name related

-(BOOL) contentName:(NSString *)name matchesTitle:(NSString *)title {
    name = [name lowercaseString];
    title = [title lowercaseString];
    
    if (name.length == 0 || title.length == 0) {
        return NO;
    }
    
    // if whole name matches in title.
    if ([title rangeOfString:name].location != NSNotFound) {
        return YES;
    }
    
    // if any x number of characters match in sequence.
    int charactersToMatch = 4;
    
    // if less than x characters in name, whole name should have matched.
    if (name.length < charactersToMatch) {
        return NO;
    }
    
    int pointer = 0;
    while (pointer + charactersToMatch < name.length) {
        NSString *subString = [name substringWithRange:NSMakeRange(pointer, charactersToMatch)];
        //        NSLog(@"Substring to match: %@", subString);
        if ([title rangeOfString:subString].location != NSNotFound) {
            return YES;
        }
        pointer ++;
    }
    
    return NO;
}

#pragma mark - date parsing

+(NSString *) dateStringFromDate:(NSDate *)date
{
    //NSDate *date = [self.datePicker date];// [NSDate date];
    NSString *dateString;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM_dd_yy"];
    [dateFormatter setTimeZone: [NSTimeZone timeZoneWithAbbreviation:@"EST"]];
    dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

-(NSString *) shortfriendlyDateFromString:(NSString *)dateString {
    NSArray *dateComponents = [dateString componentsSeparatedByString:@"_"];
    if (dateComponents.count != 3) {
        return dateString;
    }
    
    return [NSString stringWithFormat:@"%@ %@ %@", [dateComponents objectAtIndex:1], [self shortMonthStringFromNumberString:[dateComponents objectAtIndex:0]], [dateComponents objectAtIndex:2]];
}

-(NSString *) friendlyDateFromString:(NSString *)dateString {
//    NSArray *array = [dateString componentsSeparatedByString:@"-"];
//    if (array.count != 2) {
//        return dateString;
//    }
//    
//    NSString *parsedDateString = [array objectAtIndex:1];
    NSArray *dateComponents = [dateString componentsSeparatedByString:@"_"];
    if (dateComponents.count != 3) {
        return dateString;
    }
    
    return [NSString stringWithFormat:@"%@ %@ 20%@", [dateComponents objectAtIndex:1], [self monthStringFromNumberString:[dateComponents objectAtIndex:0]], [dateComponents objectAtIndex:2]];
}

-(NSString *) monthStringFromNumberString:(NSString *)numberString {
    switch ([numberString integerValue]) {
        case 1:
            return @"January";
            break;
        case 2:
            return @"February";
            break;
        case 3:
            return @"March";
            break;
        case 4:
            return @"April";
            break;
        case 5:
            return @"May";
            break;
        case 6:
            return @"June";
            break;
        case 7:
            return @"July";
            break;
        case 8:
            return @"August";
            break;
        case 9:
            return @"September";
            break;
        case 10:
            return @"October";
            break;
        case 11:
            return @"November";
            break;
        case 12:
            return @"December";
            break;
        default:
            break;
    }
    return numberString;
}

-(NSString *) shortMonthStringFromNumberString:(NSString *)numberString {
    switch ([numberString integerValue]) {
        case 1:
            return @"Jan";
            break;
        case 2:
            return @"Feb";
            break;
        case 3:
            return @"Mar";
            break;
        case 4:
            return @"Apr";
            break;
        case 5:
            return @"May";
            break;
        case 6:
            return @"Jun";
            break;
        case 7:
            return @"Jul";
            break;
        case 8:
            return @"Aug";
            break;
        case 9:
            return @"Sep";
            break;
        case 10:
            return @"Oct";
            break;
        case 11:
            return @"Nov";
            break;
        case 12:
            return @"Dec";
            break;
        default:
            break;
    }
    return numberString;
}

#pragma mark - remote data read helpers

-(NSURL *) imageURLWithCategoryId:(NSString *)categoryId contentId:(NSString *)contentId {
    NSString *url = [NSString stringWithFormat:@"%@%@_%@_img.png", SERVER_IMAGES_FOLDER_PATH, categoryId, contentId];
    if (url) {
        return [NSURL URLWithString:url];
    }
    return nil;
}

-(NSString *) imageUrlFromClassicContentName:(NSString *)name path:(NSString *)path {
    NSString *url = [NSString stringWithFormat:@"http://www.anmaple.com/%@/%@_img.png",path, [name stringByReplacingOccurrencesOfString:@" " withString:@"_"]];
    return url;
}

-(void) downloadImagewithUrl:(NSString *)url completion:(DataAndErrorBlock)completion {
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30.0];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:self.remoteDataLoadQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([data length] > 0 && error == nil)
                // downloaded as NSData.
                completion(data, nil);
            else if ([data length] == 0 && error == nil)
                // empty reply.
                completion(nil, nil);
            else if (error != nil && error.code == NSURLErrorTimedOut)
                // times out.
                completion(nil, nil);
            else if (error != nil)
                // download error.
                completion(nil, error);
        });
    }];
}

-(void) uploadImage:(NSData *)data withFileName:(NSString *)fileName completion:(BoolAndErrorBlock)completion {

//    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:SERVER_ROOT_PATH]];
//    
//    NSMutableURLRequest *request = [client multipartFormRequestWithMethod:@"POST" path:[self serverImageUploadPHPPath] parameters:nil constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
//        [formData appendPartWithFileData: data name:@"uploadedfile" fileName:fileName mimeType:@"image/jpeg"];
//    }];
    
//    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
//    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSString *response = [operation responseString];
//        NSLog(@"response: [%@]",response);
//        completion(YES, nil);
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        if([operation.response statusCode] == 403){
//            NSLog(@"Upload Failed");
//            return;
//        }
//        NSLog(@"error: %@", [operation error]);
//        completion(NO, error);
//    }];
//    
//    [operation start];
}

//-(void) uploadImage:(UIImage *)image withFileName:(NSString *)fileName completion:(BoolAndErrorCompletionBlock)completion {
//    NSData *imageData = UIImagePNGRepresentation(image);
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    [manager POST:[self serverImageUploadPHPPath] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//        [formData appendPartWithFileData:imageData name:@"uploadedfile" fileName:fileName mimeType:@"image/png"];
//    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        completion(YES, nil);
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        completion(YES, nil);
//    }];
//}

-(void) readDictionarywithUrl:(NSString *)url completion:(DictionaryAndErrorBlock)completion {
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30.0f];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:self.remoteDataLoadQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([data length] > 0 && error == nil) {
                // downloaded as NSData.
                NSDictionary *resultDictionary = [self dictionaryFromJSONData:data];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (resultDictionary) {
                        completion([resultDictionary mutableCopy], nil);
                    } else completion(nil, error);
                });
            }
            else if ([data length] == 0 && error == nil)
                // empty reply.
                completion(nil, nil);
            else if (error != nil && error.code == NSURLErrorTimedOut)
                // times out.
                completion(nil, nil);
            else if (error != nil)
                // download error.
                completion(nil, error);
        });
    }];
}

-(void) readArraywithUrl:(NSString *)url completion:(ArrayAndErrorBlock)completion {
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30.0];

    [NSURLConnection sendAsynchronousRequest:urlRequest queue:self.remoteDataLoadQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([data length] > 0 && error == nil)
                // downloaded as NSData.
                [self executeCompletion:completion withData:data];
            else if ([data length] == 0 && error == nil)
                // empty reply.
                completion(nil, nil);
            else if (error != nil && error.code == NSURLErrorTimedOut)
                // times out.
                completion(nil, nil);
            else if (error != nil)
                // download error.
                completion(nil, error);
        });
    }];
}

-(void) executeCompletion:(ArrayAndErrorBlock)completion withData:(NSData *)data {
    NSError *error;
    NSPropertyListFormat plistFormat;
    NSArray *temp = [NSPropertyListSerialization propertyListWithData:data options:NSPropertyListImmutable format:&plistFormat error:&error];
    
    completion(temp, nil);
}

#pragma mark - local data read helpers

-(NSString *) rootFolderForPlistsWithUserName:(NSString *)name {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    basePath = [basePath stringByAppendingPathComponent:name];
    return basePath;
}

- (void) createDirectoryForUserName:(NSString *)name {
//    NSString *dirPath = [self rootFolderForPlistsWithUserName:name];
//    if ([[NSFileManager defaultManager] fileExistsAtPath:dirPath]) {
//        // clear previous user settings.
//        NSString *filePath = [self rootFolderForPlistsWithUserName:name];
//        
//        NSError *error;
//        if ([[NSFileManager defaultManager] removeItemAtPath:filePath error:&error] != YES) {
//            NSLog(@"Unable to clear old settings: %@", [error localizedDescription]);
//        }
//        return;
//    }
//    
//    NSError* error;
//    if(  [[NSFileManager defaultManager] createDirectoryAtPath:dirPath withIntermediateDirectories:NO attributes:nil error:&error])
//    {
//        NSLog(@"Folder for data store created - %@",dirPath);
//    }
//    else
//    {
//        NSLog(@"ERROR - attempting to create directory %@ failed.", dirPath);
//    }
}

-(NSString *) pathForLocalResource:(NSString *)resourceName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDirectory = [paths objectAtIndex:0];
    return [docDirectory stringByAppendingPathComponent:resourceName];
}

-(NSMutableArray *) readLocalArrayWithName:(NSString *)fileName {
    NSMutableArray *array = [[NSMutableArray arrayWithContentsOfFile:[self pathForLocalResource:fileName]] mutableCopy];
    return array;
}

-(void) deleteUserFile:(NSString *)fileName {
    NSString *path = [self pathForLocalResource:fileName];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isMyFileThere = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if(isMyFileThere){
        
        [fileManager removeItemAtPath:path error:NULL];
    }
    else{
        //file dont exists
    }
}
-(void) saveArray:(NSArray *)array AsFileName:(NSString *)fileName {
    NSString *path = [self pathForLocalResource:fileName];
    NSLog(@"Array saved to: %@", path);
    [array writeToFile:path atomically:YES];
}

-(NSMutableDictionary *) readLocalDictionaryWithName:(NSString *)fileName {
    NSMutableDictionary *dict = [[NSMutableDictionary dictionaryWithContentsOfFile:[self pathForLocalResource:fileName]] mutableCopy];
    return dict;
}

-(void) saveDictionary:(NSDictionary *)dict AsFileName:(NSString *)fileName {
    [dict writeToFile:[self pathForLocalResource:fileName] atomically:YES];
}

#pragma mark - sort related.

-(NSArray *) getSortedArrayForLocalFavCategories:(NSDictionary *)catDict {
    NSMutableArray *array = [NSMutableArray array];
//    for (NSString *key in catDict.allKeys) {
//        NSDictionary *dict = catDict[key];
//        ContentCategory *category = [[ContentCategory alloc] initWithDictionary:dict];
//        [array addObject:category];
//    }
//    
//    if (array.count > 0) {
//        return [self sortObjects:array orderBy:KEY_CATEGORY_NAME ascending:YES];
//    }
    return nil;
}

-(NSArray *) getSortedArrayForLocalFavContents:(NSDictionary *)contentDict {
    NSMutableArray *array = [NSMutableArray array];
//    for (NSString *key in contentDict.allKeys) {
//        NSDictionary *dict = contentDict[key];
//        Content *content = [[Content alloc] initWithDictionary:dict];
//        [array addObject:content];
//    }
//    
//    if (array.count > 0) {
//        return [self sortObjects:array orderBy:KEY_CONTENT_NAME ascending:YES];
//    }
    return nil;
}

-(NSArray *) getSortedArrayForLocalFavChannels:(NSDictionary *)contentDict {
//    NSMutableArray *array = [NSMutableArray array];
//    for (NSString *key in contentDict.allKeys) {
//        NSDictionary *dict = contentDict[key];
//        YoutubeChannel *channel = [[YoutubeChannel alloc] initWithDictionary:dict];
//        [array addObject:channel];
//    }
//    
//    if (array.count > 0) {
//        return [self sortObjects:array orderBy:KEY_YOUTUBE_CHANNEL_NAME ascending:YES];
//    }
    return nil;
}

-(NSArray *) getSortedArrayForLocalFavUserChannels:(NSDictionary *)userChannels {
//    NSMutableArray *array = [NSMutableArray array];
//    
//    for (NSString *key in userChannels.allKeys) {
//        NSDictionary *dict = userChannels[key];
//        UserChannel *channel = [[UserChannel alloc] initWithDictionary:dict];
//        [array addObject:channel];
//    }
//    
//    if (array.count > 0) {
//        return [self sortObjects:array orderBy:KEY_YOUTUBE_CHANNEL_NAME ascending:YES];
//    }
    return nil;
}

// watch later episodes.
-(NSArray *) getSortedArrayForLocalFavEpisodes:(NSDictionary *)episodesDict {
//    NSMutableArray *array = [NSMutableArray array];
//    for (NSString *key in episodesDict.allKeys) {
//        NSDictionary *dict = episodesDict[key];
//        Episode *episode = [[Episode alloc] initWithDictionary:dict];
//        [array addObject:episode];
//    }
//    
//    if (array.count > 0) {
//        return [self sortObjects:array orderBy:KEY_EPISODE_SAVED_AT ascending:YES];
//    }
    return nil;
}

// watch later videos.
-(NSArray *) getSortedArrayForLocalFavVideos:(NSDictionary *)episodesDict {
    NSMutableArray *array = [NSMutableArray array];
    for (NSString *key in episodesDict.allKeys) {
        NSDictionary *dict = episodesDict[key];
        YoutubeVideo *video = [[YoutubeVideo alloc] initWithDictionary:dict];
        [array addObject:video];
    }
    
    if (array.count > 0) {
        return [self sortObjects:array orderBy:KEY_VIDEO_SAVED_AT ascending:YES];
    }
    return nil;
}

-(NSArray *) uniqueObjectsFromArray:(NSArray *)objects withKeyColumn:(NSString *)column {
    if (objects.count <= 1) {
        return objects;
    }
    
    NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
    NSMutableArray *array = [NSMutableArray array];
    
    for (int i=0; i<objects.count; i++) {
        NSObject *object = [objects objectAtIndex:i];
        NSObject *val = [object valueForKey:column];
        if ([array containsObject:val]) {
            continue;
        } else {
            [array addObject:val];
            [indexSet addIndex:i];
        }
    }
    
    array = [NSMutableArray array];
    [indexSet enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        [array addObject:[objects objectAtIndex:idx]];
    }];
    return array;
}

-(NSArray *) sortObjects:(NSArray *) objects orderBy:(NSString *)field ascending:(BOOL) asc {
    
    if (!objects || objects.count < 2) {
        return objects;
    }
    
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:field ascending:asc];
    NSArray *sorted = [objects sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
    
    return sorted;
}

-(NSArray *) sortCaseInsensitiveObjects:(NSArray *) objects orderBy:(NSString *)field ascending:(BOOL) asc {
    
    if (!objects || objects.count < 2) {
        return objects;
    }
    
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:field ascending:asc selector:@selector(caseInsensitiveCompare:)];
    NSArray *sorted = [objects sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
    
    return sorted;
}

-(NSArray *) sortStringArray:(NSArray *) objects {
    
    if (!objects || objects.count < 2) {
        return objects;
    }
    
    NSArray *sorted = [objects sortedArrayUsingSelector:@selector(compare:)];
    
    return sorted;
}

-(void) sortArraysInsideDictionary:(NSMutableDictionary *)dictionary orderBy:(NSString *)field ascending:(BOOL) asc completion:(DictionaryAndErrorBlock)completion {
    
    if (dictionary.allKeys.count < 2) {
        completion(dictionary, nil);
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (NSString *key in dictionary.allKeys) {
            NSArray *array = dictionary[key];
            if (![array isKindOfClass:[NSArray class]]) {
                NSLog(@"Invalid dictionary. not containing all arrays.");
                continue;
            }
            
            [dictionary setObject:[self sortObjects:array orderBy:field ascending:asc] forKey:key];
        }
        if (completion) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(dictionary, nil);
            });
        }
    });
}

#pragma mark - search related.

-(void) filterArraysInsideDictionary:(NSMutableDictionary *)dictionary searchString:(NSString *)searchString field:(NSString *)field completion:(DictionaryAndErrorBlock)completion {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSMutableDictionary *filteredDictionary = [NSMutableDictionary dictionary];
        for (NSString *key in dictionary.allKeys) {
            NSArray *array = dictionary[key];
            NSArray *filteredArray = [self filterArray:array SearchString:searchString field:field];
            [filteredDictionary setObject:filteredArray forKey:key];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(filteredDictionary, nil);
        });
    });
}


-(NSMutableArray *) filterArray:(NSMutableArray *)array SearchString:(NSString *)string field:(NSString *)field {
    
    string = [string lowercaseString];
    NSMutableArray *filteredArray = [NSMutableArray array];
//    
//    for (id object in array) {
//        if ([object isKindOfClass:[NSDictionary class]]) {
//            NSString *value = [[(NSDictionary *)object valueForKey:field] lowercaseString];
//            if ([value rangeOfString:string].location == NSNotFound) {
//                continue;
//            } else {
//                [filteredArray addObject:object];
//            }
//        } else if ([object isKindOfClass:[PFObject class]]) {
//            NSString *value = [[(PFObject *)object objectForKey:field] lowercaseString];
//            if ([value rangeOfString:string].location == NSNotFound) {
//                continue;
//            } else {
//                [filteredArray addObject:object];
//            }
//        }
//    }
    
    return filteredArray;
    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%@ CONTAINS[cd] %@", field, string];
//    NSArray *filtered = [array filteredArrayUsingPredicate:predicate];
//    return [filtered mutableCopy];
}

// user accounts related

-(NSString *)lastLoggedInUserName {
    return [[NSUserDefaults standardUserDefaults] valueForKey:KEY_LAST_LOGGED_IN_USER];
}

-(void) saveLastLoggedInUserName:(NSString *)name {
    [[NSUserDefaults standardUserDefaults] setValue:name forKey:KEY_LAST_LOGGED_IN_USER];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Youtube channel related

-(NSString *)urlForFindChannelByName:(NSString *)name {
    return [NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?part=snippet&q=%@&type=channel&maxResults=%@&fields=items(id,snippet),nextPageToken,pageInfo,prevPageToken,tokenPagination&key=%@",name, CHANNEL_LIST_MAX_RESULTS, YOUTUBE_API_KEY];
}

-(NSString *)urlForFindChannelByName:(NSString *)name order:(NSString *)order pageToken:(NSString *)pageToken {
    if (!pageToken) {
        return [NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?part=snippet&q=%@&type=channel&order=%@&maxResults=%@&fields=items(id,snippet),nextPageToken,pageInfo,prevPageToken,tokenPagination&key=%@",name, order, CHANNEL_LIST_MAX_RESULTS, YOUTUBE_API_KEY];
    } else {
        return [NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?part=snippet&q=%@&type=channel&order=%@&maxResults=%@&pageToken=%@&fields=items(id,snippet),nextPageToken,pageInfo,prevPageToken,tokenPagination&key=%@",name, order, CHANNEL_LIST_MAX_RESULTS, pageToken, YOUTUBE_API_KEY];
    }
}

-(NSString *)urlForFindUploadsIdByChannelId:(NSString *)channelId {
    return [NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/channels?part=contentDetails&id=%@&fields=items/contentDetails&key=%@", channelId, YOUTUBE_API_KEY];
}

-(NSString *) urlForFindVideosByChannelId:(NSString *)channelId searchString:(NSString *)searchString order:(NSString *)order pageToken:(NSString *)pageToken {
    searchString = [searchString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    if(channelId){
        channelId = [NSString stringWithFormat:@"&channelId=%@",channelId];
    } else {
        channelId = @"";
    }
    if (!pageToken) {
        return [NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?part=id%@&videoEmbeddable=true&type=video&videoSyndicated=true&order=%@&q=%@&maxResults=%@&fields=items/id,nextPageToken,prevPageToken&key=%@", channelId, order, searchString, VIDEO_LIST_MAX_RESULTS, YOUTUBE_API_KEY];
    } else {
        return [NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?part=id%@&videoEmbeddable=true&type=video&videoSyndicated=true&order=%@&q=%@&maxResults=%@&pageToken=%@&fields=items/id,nextPageToken,prevPageToken&key=%@", channelId, order, searchString, VIDEO_LIST_MAX_RESULTS, pageToken, YOUTUBE_API_KEY];
    }
    return nil;
}

-(NSString *)urlForFindVideosByUploadsPlaylistId:(NSString *)uploadId pageToken:(NSString *)pageToken {
    if(uploadId){
        uploadId = [NSString stringWithFormat:@"&playlistId=%@",uploadId];
    } else {
        uploadId = @"";
    }
    if (!pageToken) {
        return [NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/playlistItems?part=contentDetails&videoEmbeddable=true&type=video&videoSyndicated=true%@&maxResults=%@&fields=items/contentDetails,nextPageToken,pageInfo,prevPageToken,tokenPagination&key=%@",uploadId, VIDEO_LIST_MAX_RESULTS, YOUTUBE_API_KEY];
    } else return [NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/playlistItems?part=contentDetails&videoEmbeddable=true&type=video&videoSyndicated=true%@&maxResults=%@&pageToken=%@&fields=items/contentDetails,nextPageToken,pageInfo,prevPageToken,tokenPagination&key=%@",uploadId, VIDEO_LIST_MAX_RESULTS, pageToken, YOUTUBE_API_KEY];
}

-(NSString *) urlForVideoDetailsByVideoIDs:(NSArray *)videoIds {
    NSString *commaSeparatedVideoIds = [videoIds componentsJoinedByString:@","];
    return [NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/videos?part=id,+snippet,+contentDetails&id=%@&maxResults=%@&fields=items(contentDetails(duration),snippet(publishedAt,title,description, channelId,channelTitle))&key=%@",commaSeparatedVideoIds, VIDEO_LIST_MAX_RESULTS, YOUTUBE_API_KEY];
}

-(NSMutableURLRequest *)urlRequestForURL:(NSString *)url {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    request.timeoutInterval = 5.0;
    return request;
}

-(void) findVideoDetailByVideoIds:(NSArray *)videoIds completion:(ArrayAndErrorBlock)completion {
    NSMutableURLRequest *urlRequest = [self urlRequestForURL:[self urlForVideoDetailsByVideoIDs:videoIds]];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:self.remoteDataLoadQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([data length] > 0 && error == nil) {
                // downloaded as NSData.
                NSDictionary *resultDictionary = [self dictionaryFromJSONData:data];
                if (resultDictionary) {
                    NSArray *items = resultDictionary[@"items"];
                    completion(items, nil);
                    NSLog(@"%@",resultDictionary);
                } else completion(nil, nil);
            }
            else if ([data length] == 0 && error == nil)
                // empty reply.
                completion(nil, nil);
            else if (error != nil && error.code == NSURLErrorTimedOut)
                // times out.
                completion(nil, nil);
            else if (error != nil)
                // download error.
                completion(nil, error);
        });
    }];
}

-(void) findVideosByUploadId:(NSString *)uploadId pageToken:(NSString *)pageToken completion:(YCSearchVideoResultAndErrorBlock)completion {
    NSMutableURLRequest *urlRequest = [self urlRequestForURL:[self urlForFindVideosByUploadsPlaylistId:uploadId pageToken:pageToken]];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:self.remoteDataLoadQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([data length] > 0 && error == nil) {
                // downloaded as NSData.
                NSDictionary *resultDictionary = [self dictionaryFromJSONData:data];
                if (resultDictionary) {
                    YCSeachVideoResult *videoSearchResult = [[YCSeachVideoResult alloc] initWithDictionary:resultDictionary];
                    completion(videoSearchResult, nil);
                    
                } else completion(nil, nil);
            }
            else if ([data length] == 0 && error == nil)
                // empty reply.
                completion(nil, nil);
            else if (error != nil && error.code == NSURLErrorTimedOut)
                // times out.
                completion(nil, nil);
            else if (error != nil)
                // download error.
                completion(nil, error);
        });
    }];
}

-(void) findVideosByChannelId:(NSString *)channelId searchString:(NSString *)searchString order:(NSString *)order pageToken:(NSString *)pageToken completion:(YCSearchVideoResultAndErrorBlock)completion {
    NSMutableURLRequest *urlRequest = [self urlRequestForURL:[self urlForFindVideosByChannelId:channelId searchString:searchString order:order pageToken:pageToken]];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:self.remoteDataLoadQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([data length] > 0 && error == nil) {
                // downloaded as NSData.
                NSDictionary *resultDictionary = [self dictionaryFromJSONData:data];
                if (resultDictionary) {
                    YCSeachVideoResult *videoSearchResult = [[YCSeachVideoResult alloc] initWithDictionary:resultDictionary];
                    completion(videoSearchResult, nil);
                    
                } else completion(nil, nil);
            }
            else if ([data length] == 0 && error == nil)
                // empty reply.
                completion(nil, nil);
            else if (error != nil && error.code == NSURLErrorTimedOut)
                // times out.
                completion(nil, nil);
            else if (error != nil)
                // download error.
                completion(nil, error);
        });
    }];
}

-(void) findUploadsIdByChannelId:(NSString *)channelId completion:(ArrayAndErrorBlock)completion {
    NSMutableURLRequest *urlRequest = [self urlRequestForURL:[self urlForFindUploadsIdByChannelId:channelId]];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:self.remoteDataLoadQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([data length] > 0 && error == nil) {
                // downloaded as NSData.
                NSDictionary *resultDictionary = [self dictionaryFromJSONData:data];
                if (resultDictionary) {
                    NSMutableArray *uploadIds = [NSMutableArray array];
                    
                    NSArray *items = resultDictionary[@"items"];
                    for (NSDictionary *itemDict in items) {
                        NSDictionary *contentDetailsDict = itemDict[@"contentDetails"];
                        if (contentDetailsDict) {
                            NSDictionary *relatedPlaylistsDictionary = contentDetailsDict[@"relatedPlaylists"];
                            if (relatedPlaylistsDictionary) {
                                NSString *uploadPlaylistId = relatedPlaylistsDictionary[@"uploads"];
                                [uploadIds addObject:uploadPlaylistId];
                            }
                        }
                        
                    }
                    completion(uploadIds, nil);
                    
                } else completion(nil, nil);
            }
            else if ([data length] == 0 && error == nil)
                // empty reply.
                completion(nil, nil);
            else if (error != nil && error.code == NSURLErrorTimedOut)
                // times out.
                completion(nil, nil);
            else if (error != nil)
                // download error.
                completion(nil, error);
        });
    }];
}

-(void) executeChannelSearchCompletion:(YCSearchResultAndErrorBlock)completion withName:(NSString *)name data:(NSData *)data {
    NSDictionary *resultDictionary = [self dictionaryFromJSONData:data];
    if (resultDictionary) {
        NSArray *channelItems = [self channelSearchResultItemsFromDictionary:resultDictionary];
        if (channelItems) {
            
            NSDictionary *infoDict = resultDictionary[YC_SEARCH_RESULT_PAGE_INFO_KEY];
            NSNumber *resultsPerPage = infoDict[YC_SEARCH_RESULTS_PER_PAGE];
            NSNumber *totalResults = infoDict[YC_SEARCH_RESULT_COUNT];
            
            NSString *nextPageToken = resultDictionary[@"nextPageToken"];
            NSString *prevPageToken = resultDictionary[@"prevPageToken"];
            
            YCSearchResult *result = [[YCSearchResult alloc] initWithResultItems:channelItems count:totalResults resultsPerPage:resultsPerPage nextPageToken:nextPageToken prevPageToken:prevPageToken searchQuery:name];
            completion(result, nil);
            
        } else completion(nil, nil);
    } else completion(nil, nil);
}

-(void) findChannelsByName:(NSString *)name order:(NSString *)order pageToken:(NSString *)pageToken completion:(YCSearchResultAndErrorBlock)completion {
    NSMutableURLRequest *urlRequest = [self urlRequestForURL:[self urlForFindChannelByName:name order:order pageToken:pageToken]];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:self.remoteDataLoadQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([data length] > 0 && error == nil) {
                // downloaded as NSData.
                [self executeChannelSearchCompletion:completion withName:name data:data];
            }
            else if ([data length] == 0 && error == nil)
                // empty reply.
                completion(nil, nil);
            else if (error != nil && error.code == NSURLErrorTimedOut)
                // times out.
                completion(nil, nil);
            else if (error != nil)
                // download error.
                completion(nil, error);
        });
    }];
}

-(void) findChannelsByName:(NSString *)name completion:(YCSearchResultAndErrorBlock)completion {
    [self findChannelsByName:name order:@"relevance" pageToken:nil completion:completion];
}

-(NSMutableDictionary *)dictionaryFromJSONData:(NSData *)data {
    NSError *parseError;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
    if (dict) {
        return [dict mutableCopy];
    }
    return nil;
}

// youtube api response parse to objects.

-(NSArray *) channelSearchResultItemsFromDictionary:(NSDictionary *)dictionary {
    NSMutableArray *resultItems = [NSMutableArray array];
    
    if (dictionary.allKeys.count > 0) {
        NSArray *items = dictionary[YC_SEARCH_RESULT_ITEM_ROOT_KEY];
        for (NSDictionary *item in items) {
            
            YCSearchResultItem *resultItem = [[YCSearchResultItem alloc] initWithItemDictionary:item];
            [resultItems addObject:resultItem];
            
        }
    }
    
    return resultItems;
}

#pragma mark - previous version favs related.

-(BOOL) hasPreviousVersionDramaFavs {
    NSArray *array = [self previousVersionDramaFavs];
    if (array.count > 0) {
        return YES;
    }
    return NO;
}

-(BOOL) hasPreviousVersionShowFavs {
    NSArray *array = [self previousVersionShowFavs];
    if (array.count > 0) {
        return YES;
    }
    return NO;
}

-(NSArray *) previousVersionShowFavs {
    NSArray *array = [self readLocalArrayWithName:PREV_VER_FAVS_SHOWS_FILE_NAME];
    return array;
}

-(NSArray *) previousVersionDramaFavs {
    NSArray *array = [self readLocalArrayWithName:PREV_VER_FAVS_DRAMAS_FILE_NAME];
    return array;
}

#pragma mark - detect url to play in custom player.

- (void)findURLForVideoID:(NSString *)vid completion:(StringAndErrorBlock)completion {
    NSString *reqUrl = [NSString stringWithFormat:@"http://eol.gdata.youtube.com/feeds/api/videos?vq=%@",vid];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:reqUrl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10.0];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:self.remoteDataLoadQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([data length] > 0 && error == nil)
                // downloaded as NSData.
                completion([self urlFromYouTubeFeedJSONData:data], nil);
            else if ([data length] == 0 && error == nil)
                // empty reply.
                completion(nil, nil);
            else if (error != nil && error.code == NSURLErrorTimedOut)
                // times out.
                completion(nil, nil);
            else if (error != nil)
                // download error.
                completion(nil, error);
        });
    }];
}

- (NSString *)urlFromYouTubeFeedJSONData:(NSData *)data {
    NSDictionary *dict = [self dictionaryFromJSONData:data];
    
    return @"";
}

#pragma mark - app url and api keys related.

-(NSString *)youtubeAppId {
    return YOUTUBE_API_KEY;
}

-(NSString *)facebookAppId {
    return @"197190493800275";
}

-(NSString *)appName {
    return @"Pak TV Shows Dramas n More";
}

- (NSString*)appURL {
    NSString* url;
//    if (OS_VERSION >= 7) {
        url = @"itms-apps://itunes.apple.com/app/id485585283";
//    } else {
//        url = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=485585283";
//    }
    return url;
}

#pragma mark - Pak Videos Node API related method declarations.

#define NAPI_BASE_URL           @"http://localhost:3000"
//#define NAPI_BASE_URL         @"http://192.168.0.101:3000"

#define NAPI_API_VERSION        @"v0.1_dev_0"
#define NAPI_API_KEY            @"81c60456-d370-11e5-ab30-625662870761"
#define NAPI_API_KEY_FIELD       @"api_key"

#define NAPI_PATH_CATEGORY          @"category"
#define NAPI_PATH_SOURCE            @"source"
#define NAPI_PATH_CONTENT           @"content"
#define NAPI_PATH_UPDATES           @"updates"
#define NAPI_PATH_UPDATE_DATES      @"update_dates"
#define NAPI_PATH_VIEW              @"view"
#define NAPI_PATH_RATING            @"rating"
#define NAPI_PATH_VIDEO             @"video"

// NAPI - Node API.
// POST /category
-(void) NAPICreateCategoryWithParentCategoryId:(NSString *)parentCategoryId name:(NSString *)name completion:(BoolAndErrorBlock)completion {
    NSString *postBody = [NSString stringWithFormat:@"parent_category_id=%@&name=%@",parentCategoryId,name];
    [self POSTNAPIRequest:[self NAPIPOSTRequestWithHTTPMethod:@"POST" path:NAPI_PATH_CATEGORY form:postBody] completion:completion];
}

// POST /content
-(void) NAPICreateContentWithCategoryId:(NSString *)categoryId name:(NSString *)name completion:(BoolAndErrorBlock)completion {
    NSString *postBody = [NSString stringWithFormat:@"category_id=%@&name=%@",categoryId,name];
    [self POSTNAPIRequest:[self NAPIPOSTRequestWithHTTPMethod:@"POST" path:NAPI_PATH_CONTENT form:postBody] completion:completion];
}

// POST /video
-(void) NAPICreateVideoWithContentId:(NSString *)contentId url:(NSString *)videoURL title:(NSString *)title completion:(BoolAndErrorBlock)completion {
    NSString *postBody = [NSString stringWithFormat:@"content_id=%@&video_url=%@&title=%@",contentId, videoURL, title];
    [self POSTNAPIRequest:[self NAPIPOSTRequestWithHTTPMethod:@"POST" path:NAPI_PATH_VIDEO form:postBody] completion:completion];
}

// PUT /video
-(void) NAPIUpdateVideoWithVideoId:(NSString *)videoId url:(NSString *)videoURL completion:(BoolAndErrorBlock)completion {
    NSString *postBody = [NSString stringWithFormat:@"video_id=%@&video_url=%@",videoId,videoURL];
    [self POSTNAPIRequest:[self NAPIPOSTRequestWithHTTPMethod:@"PUT" path:NAPI_PATH_VIDEO form:postBody] completion:completion];
}

// GET /video
-(void) NAPIReadVideoWithVideoId:(NSString *)videoId completion:(DictionaryAndErrorBlock)completion {
    NSString *query = [NSString stringWithFormat:@"video_id=%@", videoId];
    [self GETNAPIRequest:[self NAPIGetRequestWithPath:NAPI_PATH_VIDEO query:query] completion:completion];
}

-(void) NAPIReadVideoWithContentId:(NSString *)contentId limit:(int)limit skip:(int)skip completion:(DictionaryAndErrorBlock)completion {
    NSString *query = [NSString stringWithFormat:@"content_id=%@&limit=%d&skip=%d", contentId, limit, skip];
    [self GETNAPIRequest:[self NAPIGetRequestWithPath:NAPI_PATH_VIDEO query:query] completion:completion];
    
}

// POST /rating
-(void) NAPICreateRatingWithContentId:(NSString *)contentId videoId:(NSString *)videoId profileId:(NSString *)profileId rating:(int)rating completion:(BoolAndErrorBlock)completion {
    NSString *postBody = [NSString stringWithFormat:@"content_id=%@&video_id=%@&profile_id=%@&rating=%d",contentId,videoId, profileId, rating];
    [self POSTNAPIRequest:[self NAPIPOSTRequestWithHTTPMethod:@"POST" path:NAPI_PATH_RATING form:postBody] completion:completion];
}

// POST /view
-(void) NAPICreateViewWithContentId:(NSString *)contentId videoId:(NSString *)videoId profileId:(NSString *)profileId completion:(BoolAndErrorBlock)completion {
    NSString *postBody = [NSString stringWithFormat:@"content_id=%@&video_id=%@&profile_id=%@",contentId,videoId, profileId];
    [self POSTNAPIRequest:[self NAPIPOSTRequestWithHTTPMethod:@"POST" path:NAPI_PATH_VIEW form:postBody] completion:completion];
    
}

// GET /updates
-(void) NAPIReadUpdateWithCategoryId:(NSString *)categoryId completion:(DictionaryAndErrorBlock)completion {
    NSString *query = [NSString stringWithFormat:@"category_id=%@", categoryId];
    [self GETNAPIRequest:[self NAPIGetRequestWithPath:NAPI_PATH_UPDATES query:query] completion:completion];
}

-(void) NAPIReadUpdateWithCategoryId:(NSString *)categoryId date:(NSString *)date completion:(DictionaryAndErrorBlock)completion {
    NSString *query = [NSString stringWithFormat:@"category_id=%@&date='%@'", categoryId,date];
    [self GETNAPIRequest:[self NAPIGetRequestWithPath:NAPI_PATH_UPDATES query:query] completion:completion];
}

// GET /update_dates
-(void) NAPIReadUpdateDatesWithCategoryId:(NSString *)categoryId limit:(int)limit skip:(int)skip completion:(DictionaryAndErrorBlock)completion {
    NSString *query = [NSString stringWithFormat:@"category_id=%@&limit=%d&skip=%d", categoryId,limit, skip];
    [self GETNAPIRequest:[self NAPIGetRequestWithPath:NAPI_PATH_UPDATE_DATES query:query] completion:completion];
}

// POST /source
-(void) NAPICreateSourceWithCategoryId:(NSString *)categoryId name:(NSString *)name url:(NSString *)url completion:(BoolAndErrorBlock)completion  {
    NSString *postBody = [NSString stringWithFormat:@"category_id=%@&name=%@&url=%@",categoryId,name, url];
    [self POSTNAPIRequest:[self NAPIPOSTRequestWithHTTPMethod:@"POST" path:NAPI_PATH_SOURCE form:postBody] completion:completion];
}

// GET /source
-(void) NAPIReadSourceWithCategoryId:(NSString *)categoryId completion:(DictionaryAndErrorBlock)completion {
    NSString *query = [NSString stringWithFormat:@"category_id=%@", categoryId];
    [self GETNAPIRequest:[self NAPIGetRequestWithPath:NAPI_PATH_SOURCE query:query] completion:completion];
}

// GET /category
-(void) NAPIReadCategoryWithCategoryId:(NSString *)categoryId completion:(DictionaryAndErrorBlock)completion {
    NSString *query = [NSString stringWithFormat:@"category_id=%@", categoryId];
    [self GETNAPIRequest:[self NAPIGetRequestWithPath:NAPI_PATH_CATEGORY query:query] completion:completion];
}

-(void) NAPIReadCategoryWithParentCategoryId:(NSString *)parentCategoryId completion:(DictionaryAndErrorBlock)completion {
    NSString *query = [NSString stringWithFormat:@"parent_category_id=%@", parentCategoryId];
    [self GETNAPIRequest:[self NAPIGetRequestWithPath:NAPI_PATH_CATEGORY query:query] completion:completion];
}

#pragma mark - NAPI requests helper methods

-(NSMutableURLRequest *) NAPIURLRequestWithURL:(NSString *)url {
    NSString *completeURL = [NSString stringWithFormat:@"%@/%@/%@",NAPI_BASE_URL,NAPI_API_VERSION,url];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:completeURL] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30.0];
    
    // Add API Key in header.
    [urlRequest addValue:NAPI_API_KEY forHTTPHeaderField:NAPI_API_KEY_FIELD];
    return urlRequest;
}

-(NSURLRequest *) NAPIGetRequestWithPath:(NSString *)path query:(NSString *)query {
    
    // create query string to append to path object.
//    NSString *queryString = @"?";
//    NSString *condition = @"";
//    for (NSString *key in query.allKeys) {
//        condition = [NSString stringWithFormat:@"%@=%@&",key, query[key]];
//        queryString = [queryString stringByAppendingString:condition];
//    }
//    queryString = [queryString substringToIndex:[queryString length] - 1];  // remove last & sign.
    path = [NSString stringWithFormat:@"%@?%@",path,query];
//    path = [path stringByAppendingString:queryString];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [self NAPIURLRequestWithURL:path];
    [request setHTTPMethod:@"GET"];
    
    return request;
}

// method can be POST or PUT.
-(NSURLRequest *) NAPIPOSTRequestWithHTTPMethod:(NSString *)method path:(NSString *)path form:(NSString *)form {
    
//    NSString *postString = @"";
//    NSString *condition = @"";
//    for (NSString *key in form.allKeys) {
//        condition = [NSString stringWithFormat:@"%@=%@&",key, form[key]];
//        postString = [postString stringByAppendingString:condition];
//    }
//    postString = [postString substringToIndex:[postString length] - 1];     // remove last & sign.
    
    NSMutableURLRequest *request = [self NAPIURLRequestWithURL:path];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:method];
    [request setHTTPBody:[form dataUsingEncoding:NSUTF8StringEncoding]];
    
    return request;
}

// for POST & PUT
-(void) POSTNAPIRequest:(NSURLRequest *)request completion:(BoolAndErrorBlock)completion {
    [NSURLConnection sendAsynchronousRequest:request queue:self.remoteDataLoadQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            if (httpResponse.statusCode == 200) {
                // success response.
                NSLog(@"Success response: %@",[self dictionaryFromJSONData:data]);
                completion(TRUE, nil);
            } else if (error != nil) {
                // error occured.
                completion(FALSE, error);
            } else {
                // response is not 200 and no error.
                NSDictionary *dict = [self dictionaryFromJSONData:data];
                NSError *apiError = [NSError errorWithDomain:@"NAPI" code:httpResponse.statusCode userInfo:dict];
                completion(FALSE, apiError);
            }
        });
    }];
}

-(void) GETNAPIRequest:(NSURLRequest *)request completion:(DictionaryAndErrorBlock)completion {
    [NSURLConnection sendAsynchronousRequest:request queue:self.remoteDataLoadQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            if (httpResponse.statusCode == 200) {
                // success response.
                NSMutableDictionary *dict = [self dictionaryFromJSONData:data];
                completion(dict, nil);
            } else if (error != nil) {
                // error occured.
                completion(nil, error);
            } else {
                // response is not 200 and no error.
                NSDictionary *dict = [self dictionaryFromJSONData:data];
                NSError *apiError = [NSError errorWithDomain:@"NAPI" code:httpResponse.statusCode userInfo:dict];
                completion(nil, apiError);
            }
        });
    }];
}

#pragma mark - Data migration from MongoDB to MySQL

-(NSDictionary *) executeSql:(NSString *)sql read:(BOOL)read {
    sql = [NSString stringWithFormat:@"sql_query=%@",sql];
    NSURLResponse *response;
    NSError *error;
    NSURLRequest *req;
    if (read) {
        req = [self NAPIGetRequestWithPath:@"executeQuery" query:sql];
    } else req = [self NAPIPOSTRequestWithHTTPMethod:@"POST" path:@"executeQuery" form:sql];
    NSData *data = [NSURLConnection sendSynchronousRequest:req returningResponse:&response error:&error];
    
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    if (httpResponse.statusCode == 200) {
//        NSDictionary *responseDict = [self dictionaryFromJSONData:data];
        //NSLog(@"Success response: %@",responseDict);
        [dictionary setObject:[self dictionaryFromJSONData:data] forKey:@"data"];
    } else if (error != nil) {
        [dictionary setObject:error forKey:@"error"];
    } else {
        NSDictionary *dict = [self dictionaryFromJSONData:data];
        NSError *apiError = [NSError errorWithDomain:@"NAPI" code:httpResponse.statusCode userInfo:dict];
        [dictionary setObject:apiError forKey:@"error"];
    }
    return dictionary;
}

#pragma mark -

@end
