//
//  StyleManager.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/4/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Utility.h"

@interface StyleManager : NSObject

+(StyleManager *) sharedManager;

-(void) styleButtons:(NSArray *)buttons;
-(void) styleButton:(UIButton *)button;
-(void) styleHeaderButton:(UIButton *)button;
-(void) styleHeaderButtons:(NSArray *)buttons;
@end
