//
//  UserManager.h
//  PakTV
//
//  Created by Malik Khalid Ejaz on 11/16/2013.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

// user settings related.

#define KEY_SETTINGS_LAST_OPEN_HOME_SWITCH_OPTION_DICTIONARY   @"kSLOH_SwitchOption"
#define KEY_SETTINGS_HAS_OPENED_BROWSE  @"kS_HAS_OPENED_BROWSE"

#define TIP_ACTION_APP_LAUNCH       @"appLaunch"
#define TIP_ACTION_CREATE_ACCOUNT   @"createAccount"
#define TIP_ACTION_BROWSE           @"browse"
#define TIP_ACTION_SEARCH_YOUTUBE   @"searchYoutube"
#define TIP_ACTION_FAVS             @"favourites"
#define TIP_ACTION_WATCH_LATER      @"addedToWatchLater"
#define TIP_ACTION_TAG_CATEGORY     @"tagCategory"
#define TIP_ACTION_TAG_CHANNEL      @"tagChannel"
#define TIP_ACTION_TAG_FORUM        @"tagForum"
#define TIP_ACTION_FAV_CONTENT      @"favContent"
#define TIP_ACTION_PLAY_VIDEO       @"playVideo"

// misc user stuff.

#define SWITCH_VALUE_FAV        @"fav"

#define HEART_SYMBOL            @".♥."
#define CHECK_SYMBOL            @"☑"

#define SWITCH_OPTION_BROWSE            @"browse"
#define SWITCH_OPTION_WATCH_LATER       @"watchLater"
#define SWITCH_OPTION_LIST_FAVORITES    @"listFavorites"

@class Episode, Content, User, UserChannel, YoutubeChannel;

@interface UserManager : NSObject

@property (nonatomic, strong) NSMutableDictionary *favEpisodes;
@property (nonatomic, strong) NSMutableDictionary *favContents;
@property (nonatomic, strong) NSMutableDictionary *favCategories;
@property (nonatomic, strong) NSMutableDictionary *favChannels;
@property (nonatomic, strong) NSMutableDictionary *favUserChannels;
@property (nonatomic, strong) NSMutableDictionary *favVideos;
@property (nonatomic, strong) NSMutableDictionary *userSettings;
@property (nonatomic, strong) NSMutableDictionary *favForums;
@property (nonatomic, strong) NSMutableDictionary *seekTimes;

@property (nonatomic, strong) NSMutableDictionary *versionsDict;

@property (nonatomic, strong) NSMutableArray *users;

@property (nonatomic, strong) NSMutableDictionary *ratings;
@property (nonatomic, strong) NSMutableArray *views;

@property (nonatomic, assign) BOOL favouritesChanged;
@property (nonatomic, strong) User *currentUser;
@property (nonatomic, strong) NSString *timeToReAttemptAccess;

+(UserManager *) sharedManager;

// user accounts related.
-(BOOL) haveUserAccounts;
-(BOOL) userAccountNameExists:(NSString *)name;
-(NSString *) addUserAccount:(NSString *)name color:(UIColor *)color pin:(NSString *)pin childAccount:(BOOL)childAccount;
-(void) changeColorForUser:(NSString *)name toColor:(UIColor *)color;
-(void) removeUserAccount:(NSString *)name;
-(UIColor *)colorForNumber:(int)number;
-(UIColor *)colorForUser;
-(UIColor *)colorForUserName:(NSString *)name;
-(UIColor *) darkerColorForUser;
-(UIColor *) lighterColorForUser;

-(NSString *) TipForAction:(NSString *)action;

// login related.
-(int) incorrectAttemptsForUser:(User *)user;
-(void) saveIncorrectAttemptForUser:(User *)user;
-(void) removeIncorrectAttemptsForUser:(User *)user;
-(void) logoutUserName:(NSString *)name;
-(void) logoutUser;
-(BOOL) logoutCurrentUserIfNeedsPinAccess;

// local settings in NSUserDefaults.
-(BOOL) isFirstLaunch;
-(BOOL) hasSelectedFavs;
-(BOOL) hasRatedApp;
-(NSInteger) usageCount;
-(NSInteger) incUsageCount;

// parse related.
-(BOOL) isLoggedIn;

// last tab related.

-(void) saveLastSelectedHomeTab:(NSString *)optionString;
-(NSString *) getLastSelectedHomeTab;

// favourites related.

-(void) setUserChannel:(UserChannel *) userChannel asFavourite:(BOOL)fav;
-(void) setChannel:(YoutubeChannel *)channel asFavourite:(BOOL)fav;
-(BOOL) hasFavContentForCategoryId:(NSString *)categoryId;
+(BOOL) isChannelFavouriteAlready:(NSString *)channelId;
-(void) saveFavouriteCategories;
-(void) saveFavouriteContents;
-(void) saveFavouriteEpisode;
-(void) saveFavouriteChannels;
-(void) saveFavouriteUserChannels;
-(void) saveFavouriteVideos;
-(void) saveVersions;
-(void) saveFavForums;

-(void) addContentAsFavouriteById:(NSString *)contentId;
-(void) addCategoryAsFavouriteById:(NSString *)categoryId;

// ratings related.
-(void) saveRatings:(int)rating forEpisode:(Episode *)episode;
-(int) userRatingsForEpisode:(Episode *)episode;

// views related.
-(void) saveViewedEpisode:(Episode *)episode;
-(BOOL) alreadyMakedEpisodeAsViewed:(Episode *)episode;

// need some mapping for content to match with previous version favs so user does not have to add those favs again.
-(BOOL) hasPreviousVersionFavs;

-(NSArray *) getCategoryIdsFromFavContent;
-(NSArray *) getContentIdsFromFavContent;
-(NSArray *) getFavContentIdsForCategoryId:(NSString *)catId;

// switch options related.
-(NSArray *)switchOptionsFromFavCategories;
-(NSString *)switchDisplayTextForYoutubeChannelName:(NSString *)channelName;
-(NSString *) switchValueForYoutubeChannelId:(NSString *)channelId;
-(NSString *) switchValueForForumURL:(NSString *)forumURL;

// resue playback related.
- (CGFloat)lastPlaybackSeekTimeForVID:(NSString *)vid;
- (void)savePlayBackSeekTimeForVID:(NSString *)vid seekTime:(CGFloat)time;
- (void)removePlayBackSeekTimeForVID:(NSString *)vid;

@end
