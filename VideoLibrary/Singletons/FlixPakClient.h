//
//  FlixPakClient.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-12.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "types.h"

@interface FlixPakClient : NSObject

+ (id)singleton;

- (void)getRoute:(NSString *)route queryParams:(NSDictionary *)queryParams withCompletion:(DictionaryAndErrorBlock)completion;
- (void)getMainWithCompletion:(DictionaryAndErrorBlock)completion;
- (void)getInterestsWithCompletion:(DictionaryAndErrorBlock)completion;
- (void)getBrowseWithQueryParams:(NSDictionary *)queryParams completion:(DictionaryAndErrorBlock)completion;
- (void)POSTRoute:(NSString *)route form:(NSString *)form withCompletion:(DictionaryAndErrorBlock)completion;
@end
