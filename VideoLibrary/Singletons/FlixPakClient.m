//
//  FlixPakClient.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-12.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "FlixPakClient.h"
#import "ME_WebAPIClient.h"
#import "ME_ConfigManager.h"

#define FlixPak_CLIENT_KEY              @"4bae35c8-9102-4d85-9213-5e2b35fbb82b"
#define FlixPakAPIServer                @"https://flixpak.com/api"                // prod
//#define FlixPakAPIServer                @"https://flixpak.com/staging/api"        // staging
//#define FlixPakAPIServer                @"https://khalidejaz.com/dev-hide/pv/api"   // testing
//#define FlixPakAPIServer                @"https://localhost:1828/"   // development
#define FlixPakClientVersion            @"v1_0"
//#define FlixPakMgmtVersion              @"mgmt_v1_0"    // temp hack needed to get interests as client version does not have the interests route built yet.
#define FlixPakClientPlatform           @"iOS"

#define KEY_FP_API                      @"ahapikey"
#define KEY_FP_Device                   @"ahdevice"
#define KEY_FP_OS                       @"ahos"
#define KEY_FP_OS_Version               @"ahosversion"
#define KEY_FP_ClientVersion            @"ahappversion"
#define KEY_FP_AppBuildVersion          @"ahappbuildversion"
#define KEY_FP_AppDisplayVersion        @"ahappdisplayversion"
#define KEY_FP_Country                  @"ahusercountry"

@implementation FlixPakClient

+ (id)singleton {
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    // returns the same object each time
    return _sharedObject;
}

#pragma mark - private methods

- (NSString *)rootURL {
    return [NSString stringWithFormat:@"%@/%@",FlixPakAPIServer, FlixPakClientVersion];
}

// TODO: remove when interests API route is added.
//- (NSString *)managementRootURL {
//    return [NSString stringWithFormat:@"%@/%@",FlixPakAPIServer, FlixPakMgmtVersion];
//}

- (NSDictionary *)headerFields {
    NSMutableDictionary *headers = [NSMutableDictionary dictionary];
    [headers setObject:FlixPak_CLIENT_KEY forKey:KEY_FP_API];
    [headers setObject:FlixPakClientVersion forKey:KEY_FP_ClientVersion];
    [headers setObject:[[ME_ConfigManager singleton] deviceName] forKey:KEY_FP_Device];
    [headers setObject:FlixPakClientPlatform forKey:KEY_FP_OS];
    [headers setObject:[[ME_ConfigManager singleton] deviceOSVersion] forKey:KEY_FP_OS_Version];
    [headers setObject:[[ME_ConfigManager singleton] appBuildNumber] forKey:KEY_FP_AppBuildVersion];
    [headers setObject:[[ME_ConfigManager singleton] appVersion] forKey:KEY_FP_AppDisplayVersion];
    NSString *userCountry = [[ME_ConfigManager singleton] deviceCountry];
    if (!userCountry) userCountry = @"unknown";
    [headers setObject:userCountry forKey:KEY_FP_Country];
    
    return headers;
}

#pragma mark - API interface methods
// TODO: consider sending back prepared models instead of plain dictionaries.

- (void) getRoute:(NSString *)route queryParams:(NSDictionary *)queryParams withCompletion:(DictionaryAndErrorBlock)completion {
    [[ME_WebAPIClient singleton] GETRoute:route rootURL:[self rootURL] queryParams:queryParams headers:[self headerFields] cachePolicy:ME_CACHE_IGNORE withCompletion:^(NSDictionary *dictionary, NSError *error) {
        if (error) {
            completion(nil, error);
        } else {
            if (dictionary) {
                // TODO:prepare models and send back using completion.
                completion(dictionary, nil);
            } else {
                // no response.
                completion(nil, nil);
            }
        }
    }];
}

- (void) getMainWithCompletion:(DictionaryAndErrorBlock)completion {
    [self getRoute:@"main" queryParams:nil withCompletion:completion];
}

- (void) getInterestsWithCompletion:(DictionaryAndErrorBlock)completion {
    NSMutableDictionary *headerDict = [NSMutableDictionary dictionaryWithDictionary:[self headerFields]];
    [headerDict setObject:FlixPakClientVersion forKey:KEY_FP_ClientVersion];
    [[ME_WebAPIClient singleton] GETRoute:@"interests" rootURL:[self rootURL] queryParams:nil headers:headerDict cachePolicy:ME_CACHE_IGNORE withCompletion:^(NSDictionary *dictionary, NSError *error) {
        if (error) {
            completion(nil, error);
        } else {
            if (dictionary) {
                // TODO:prepare models and send back using completion.
                completion(dictionary, nil);
            } else {
                // no response.
                completion(nil, nil);
            }
        }
    }];
}

- (void) getBrowseWithQueryParams:(NSDictionary *)queryParams completion:(DictionaryAndErrorBlock)completion {
    [self getRoute:@"browse-channels" queryParams:queryParams withCompletion:completion];
}

- (void)POSTRoute:(NSString *)route form:(NSString *)form withCompletion:(DictionaryAndErrorBlock)completion {
    [[ME_WebAPIClient singleton] POSTRoute:route rootURL:[self rootURL] headers:[self headerFields] form:form withCompletion:completion];
}

@end
