//
//  StyleManager.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/4/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "StyleManager.h"
#import "UserManager.h"

@implementation StyleManager

+(StyleManager *) sharedManager {
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    // returns the same object each time
    return _sharedObject;
}

-(void) styleButton:(UIButton *)button {
    button.backgroundColor = [[UserManager sharedManager] colorForUser];
    if (!button.backgroundColor) button.backgroundColor = [UIColor lightGrayColor];
    [button setTitleColor:GRAY_WITH_PERCENT(90) forState:UIControlStateNormal];
    [button setTitleColor:[[UserManager sharedManager] lighterColorForUser] forState:UIControlStateHighlighted];
}

-(void) styleButtons:(NSArray *)buttons {
    for (UIButton *button in buttons) {
        [self styleButton:button];
    }
}

-(void) styleHeaderButton:(UIButton *)button {
    button.backgroundColor = GRAY_WITH_PERCENT(90);
    
    [button setTitleColor:[[UserManager sharedManager] colorForUser] forState:UIControlStateNormal];
    [button setTitleColor:[[UserManager sharedManager] lighterColorForUser] forState:UIControlStateHighlighted];
}

-(void) styleHeaderButtons:(NSArray *)buttons {
    for (UIButton *button in buttons) {
        [self styleHeaderButton:button];
    }
}

@end
