//
//  HoriScrollView.h
//  ContentManager
//
//  Created by Malik Khalid Ejaz on 2013-10-11.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HoriScrollView : UIScrollView

-(void) addView:(UIView *) view;
-(void) removeAllViews;
- (void)focusView:(UIView *)view;

@end
