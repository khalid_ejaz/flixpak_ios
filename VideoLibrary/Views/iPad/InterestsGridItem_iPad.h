//
//  InterestsGridItem_iPad.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-16.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InterestsGridItem_iPad : UICollectionViewCell

- (void)setSelectedInterest;
- (void)setDeSelectedInterest;
- (void)setInterestTitle:(NSString *)title categoryId:(NSNumber *)categoryId;

@end
