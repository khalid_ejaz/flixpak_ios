//
//  HoriScrollView.m
//  ContentManager
//
//  Created by Malik Khalid Ejaz on 2013-10-11.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import "HoriScrollView.h"

#define xMargin 5
#define yMargin 0

@interface HoriScrollView()

@property (nonatomic, assign) NSInteger offset;

@end

@implementation HoriScrollView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void) addView:(UIView *) view {
    CGRect frame = view.frame;
    self.offset += xMargin;
    frame.origin.x = self.offset;
    self.offset += view.bounds.size.width;
    frame.origin.y = yMargin;
    view.frame = frame;
    [self addSubview:view];
    [self setContentSize:CGSizeMake(self.offset + xMargin + 1, view.frame.size.height)];
}

-(void) removeAllViews {
    for (UIView *subview in self.subviews) {
        [subview removeFromSuperview];
    }
    [self reset];
}

-(void) reset {
    self.offset = 0;
}

- (void)focusView:(UIView *)view {
    [self scrollRectToVisible:view.frame animated:YES];
}

@end
