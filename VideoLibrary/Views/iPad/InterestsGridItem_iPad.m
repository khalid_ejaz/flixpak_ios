//
//  InterestsGridItem_iPad.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-16.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "InterestsGridItem_iPad.h"

@interface InterestsGridItem_iPad()

@property (weak, nonatomic) IBOutlet UILabel *tickLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) NSNumber *categoryId;

@end
@implementation InterestsGridItem_iPad

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelectedInterest {
    self.tickLabel.hidden = NO;
}

- (void)setDeSelectedInterest {
    self.tickLabel.hidden = YES;
}

- (void)setInterestTitle:(NSString *)title categoryId:(NSNumber *)categoryId {
    self.titleLabel.text = title;
    _categoryId = categoryId;
}

@end
