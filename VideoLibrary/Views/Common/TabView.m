//
//  TabView.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-16.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "TabView.h"
#import "ME_Logger.h"
#import "Utility.h"

@interface TabView()

@property (weak, nonatomic) IBOutlet UIButton *button;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) NSNumber *categoryId;

@end

@implementation TabView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

// when selected, round corners and change background color to that of button background.
// when deselected, remove rounded corners and set background color to white.
- (IBAction)selectTabAction:(id)sender {
    
    if (!self.delegate) {
        [[ME_Logger singleton] log:@"Must set delegate for tab view to work." level:ME_Log_Level_DEBUG];
    } else {
        [self.delegate tabView:self selectedCategory:_categoryId];
    }
    if (![_categoryId isKindOfClass:[NSString class]]) [self setSelected];
}

- (void)setSelected {
    self.button.backgroundColor = [UIColor colorWithWhite:0.72f alpha:1.0f];
//    self.button.backgroundColor = [UIColor lightGrayColor];
//    self.button.tintColor = self.button.backgroundColor;
//    self.backgroundColor = self.button.backgroundColor;
    [self.button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
//    [self.button setTitleShadowColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [self.button.titleLabel setShadowOffset:CGSizeMake(0, 1)];
}

- (void)setDeselected {
    self.button.backgroundColor = [UIColor darkGrayColor];
//    self.button.tintColor = [UIColor clearColor];
//    self.backgroundColor = self.button.backgroundColor;
    [self.button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
//    [self.button setTitleShadowColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [self.button.titleLabel setShadowOffset:CGSizeMake(0, 0)];
}

- (void)setTopCorners:(CGFloat)radius {
//    UIBezierPath *maskPath = [UIBezierPath
//                              bezierPathWithRoundedRect:self.bounds
//                              byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight)
//                              cornerRadii:CGSizeMake(radius, radius)
//                              ];
//    
//    CAShapeLayer *maskLayer = [CAShapeLayer layer];
//    
//    maskLayer.frame = self.bounds;
//    maskLayer.path = maskPath.CGPath;
//    
//    self.layer.mask = maskLayer;
}

- (void)setTitle:(NSString *)title categoryId:(NSNumber *)categoryId {
    _title = title;
    _categoryId = categoryId;
    [self.button setTitle:title forState:UIControlStateNormal];
//    [self setTopCorners:10];
//    self.backgroundColor = self.button.backgroundColor;
}

- (NSNumber *)categoryId {
    return _categoryId;
}
@end
