//
//  ShowView.h
//  PakTV
//
//  Created by Malik Khalid Ejaz on 2013-09-24.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Episode, YoutubeVideo;

@interface ShowView : UITableViewCell <UIScrollViewDelegate>

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *nameLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *detailLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *showImageView;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *previewImageView1;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *previewImageView2;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *previewImageView3;

-(void) populateCellWithShow:(Episode *)episode enableContentButton:(BOOL)enable showContentImage:(BOOL)showContentImage;

@end
