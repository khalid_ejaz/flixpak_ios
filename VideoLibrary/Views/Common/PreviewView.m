//
//  PreviewView.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/30/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "PreviewView.h"
#import <UIImageView+AFNetworking.h>
#import "Utility.h"

@interface PreviewView()
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *titleLabel;

@property (unsafe_unretained, nonatomic) IBOutlet UIScrollView *scrollView;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *imageView1;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *imageView2;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *imageView3;
@property (unsafe_unretained, nonatomic) IBOutlet UIPageControl *pageControl;
@property (unsafe_unretained, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@end

@implementation PreviewView

-(void) popuateWithDictionary:(NSDictionary *)dict {

    NSArray *urls = dict[@"urls"];
    NSString *title = dict[@"title"];
    if ([dict.allKeys containsObject:@"name"]) {
        NSString *name = dict[@"name"];
        
        self.titleLabel.text = [NSString stringWithFormat:@"%@ - %@", name, [title stringByReplacingOccurrencesOfString:@"_" withString:@" "]];
    } else {
        self.titleLabel.text = title;
    }
    
    [self.imageView1 setImageWithURLRequest:[NSURLRequest requestWithURL:urls[0]] placeholderImage:[Utility placeHolderImage] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        if (image) {
            self.imageView1.image = image;
        }
        [self.spinner stopAnimating];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        [self.spinner stopAnimating];
    }];
    
//    [self.imageView1 setImageWithURL:[urls objectAtIndex:0] placeholderImage:[Utility placeHolderImage]];
    [self.imageView2 setImageWithURL:[urls objectAtIndex:1] placeholderImage:[Utility placeHolderImage]];
    [self.imageView3 setImageWithURL:[urls objectAtIndex:2] placeholderImage:[Utility placeHolderImage]];
    
    [self setFrames];
    
    [self.scrollView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    
    [self.pageControl setCurrentPage:0];
}

-(void)layoutSubviews {
    [super layoutSubviews];
    if (self.removing) {
        return;
    }
    [self setFrames];
}

-(void) setFrames {
    
//    CGRect scrolRect = self.scrollView.frame;
    
    CGRect imageFrame = self.imageView1.frame;
    imageFrame.origin.x = imageFrame.size.width;
    self.imageView2.frame = imageFrame;
    
    imageFrame.origin.x += imageFrame.size.width;
    self.imageView3.frame = imageFrame;
    
    int sizeX = imageFrame.origin.x + imageFrame.size.width;
    [self.scrollView setContentSize:CGSizeMake(sizeX, self.imageView1.frame.size.height)];
}

#pragma mark - scroll view delegate

-(void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    if (targetContentOffset->x == 0) {
        [self.pageControl setCurrentPage:0];
    } else if (targetContentOffset->x == self.imageView2.frame.origin.x) {
        [self.pageControl setCurrentPage:1];
    } else if (targetContentOffset->x == self.imageView3.frame.origin.x) {
        [self.pageControl setCurrentPage:2];
    }
}

@end
