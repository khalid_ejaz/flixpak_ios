//
//  TileShowView.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/5/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "TileShowView.h"
#import <UIImageView+AFNetworking.h>
#import "Utility.h"
#import "DataManager.h"
#import "UserManager.h"

@interface TileShowView()

@property (unsafe_unretained, nonatomic) IBOutlet UIButton *favButton;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *contentNameLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *contentImageView;

@property (nonatomic, strong) Content *content;
@end

@implementation TileShowView

-(void) populateWithContent:(Content *)content {
    
    _content = content;
    
//    [content isFavouriteWithCompletion:^(BOOL success, NSError *error) {
//        self.favButton.selected = success;
//    }];
//    self.favButton.selected = [content isFavourite];
//    NSURL *url;
//    if (content.imageURL) {
//        url = [NSURL URLWithString:content.imageURL];
//    } else {
//        url = [[DataManager sharedManager] imageURLWithCategoryId:content.categoryId contentId:content.objectId];
//    }
//    self.contentImageView.image = nil;
//    [self.contentImageView setImageWithURL:url placeholderImage:[Utility placeHolderImage]];
//    self.contentNameLabel.text = content.name;
}

- (IBAction)selectContentAction:(id)sender {
    [self.delegate tileViewSelectedContent:self.content];
}

- (IBAction)favToggleAction:(id)sender {
//    self.favButton.selected = !self.favButton.selected;
//    if (self.favButton.selected) {
//        [self.content saveAsFavourite];
//        [self.delegate tileViewFavdContent:self.content];
//    } else [self.content saveAsNotFavourite];
}

@end
