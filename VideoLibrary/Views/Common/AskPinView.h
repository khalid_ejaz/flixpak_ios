//
//  AskPinView.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/2/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@class User;

@protocol PinViewDelegate <NSObject>

-(void) pinViewAllowedAccessToUser:(User *)user;
-(void) pinViewdeniedAccessToUser:(User *)user;
-(void) pinViewWantsToShowMessage:(NSString *)message;

@end

@interface AskPinView : UIView <UITextFieldDelegate>

@property (nonatomic, unsafe_unretained) id<PinViewDelegate> delegate;

-(void) setUser:(User *)user delegate:(id<PinViewDelegate>)delegate;

@end
