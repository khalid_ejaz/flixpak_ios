//
//  PreviewView.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/30/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreviewView : UIView <UIScrollViewDelegate>

@property (nonatomic, assign) BOOL removing;

-(void) popuateWithDictionary:(NSDictionary *)dict;

@end
