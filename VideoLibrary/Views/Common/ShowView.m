//
//  ShowView.m
//  PakTV
//
//  Created by Malik Khalid Ejaz on 2013-09-24.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import "ShowView.h"
#import "DataManager.h"
#import "Utility.h"
#import <UIImageView+AFNetworking.h>

#define WATCH_LATER_WIDTH   54
#define SANP_MARGIN         45
#define WATCH_LATER_TEXT    @"Watch Later"
#define REMOVE_TAG_TEXT     @"Remove Tag"

#define TIMER_INTERVAL      4

@interface ShowView()

@property (unsafe_unretained, nonatomic) IBOutlet UIButton *selectContentButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIScrollView *imageScroll;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *watchLaterButton;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *watchLaterLabel;
@property (nonatomic, strong) Episode *episode;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *episodeTitleLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *ratingsLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *infoContainerView;

@property (nonatomic, assign) BOOL imageLoaded;
@property (nonatomic, strong) NSTimer *timer;

@end

@implementation ShowView

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void) layoutSubviews {
    [super layoutSubviews];
    if ([self canScroll]) {
        self.imageScroll.contentOffset = CGPointMake(WATCH_LATER_WIDTH, 0);
    }
}

-(BOOL) canScroll {
    if (self.bounds.size.width < self.imageScroll.contentSize.width) {
        return YES;
    }
    return NO;
}

- (IBAction)playEpisodeAction:(id)sender {
    NSNotification *notif = [NSNotification notificationWithName:@"NOTIFICATION_PLAY_EPISODE" object:nil userInfo:@{@"Episode":self.episode}];
    [[NSNotificationCenter defaultCenter] postNotification:notif];
}

- (IBAction)selectContent:(id)sender {
    NSNotification *notif = [NSNotification notificationWithName:@"NOTIFICATION_SHOW_ALL_EPISODES" object:nil userInfo:@{@"Episode":self.episode}];
    [[NSNotificationCenter defaultCenter] postNotification:notif];
}

- (IBAction)watchLaterAction:(id)sender {
    
//    if (self.watchLaterButton.selected) {
//        [self.episode saveAsNotFavourite];
//    } else {
//        [self.episode saveAsFavourite];
//    }
//    
//    self.watchLaterButton.selected = !self.watchLaterButton.selected;
//    if ([self canScroll]) {
//        [self.imageScroll setContentOffset:CGPointMake(WATCH_LATER_WIDTH, 0) animated:YES];
//    }
//    
//    if (self.watchLaterButton.selected) {
//        NSNotification *notif = [NSNotification notificationWithName:NOTIFICATION_FLASH_TAG object:self];
//        [[NSNotificationCenter defaultCenter] postNotification:notif];
//        self.watchLaterLabel.text = REMOVE_TAG_TEXT;
//    } else {
//        if (IS_IPAD) {
//            NSNotification *notif = [NSNotification notificationWithName:NOTIFICATION_FLASH_TAG object:self];
//            [[NSNotificationCenter defaultCenter] postNotification:notif];
//        }
//        self.watchLaterLabel.text = WATCH_LATER_TEXT;
//    }
}

-(void) updateWatchLaterText {
    if(self.watchLaterButton.selected) {
        self.watchLaterLabel.text = REMOVE_TAG_TEXT;
    } else {
        self.watchLaterLabel.text = WATCH_LATER_TEXT;
    }
}

-(void) populateCellWithShow:(Episode *)episode enableContentButton:(BOOL)enable showContentImage:(BOOL)showContentImage {
//    self.selectContentButton.hidden = !enable;
//    [self stopTimer];
//    _episode = episode;
//    
//    self.imageLoaded = YES;
//    
//    if (showContentImage) {
//        NSURL *imageUrl;
//        if (episode.imageURL) {
//            imageUrl = [NSURL URLWithString:episode.imageURL];
//        } else {
//            imageUrl = [[DataManager sharedManager] imageURLWithCategoryId:episode.categoryId contentId:episode.contentId];
//        }
//        [self.showImageView setImageWithURLRequest:[NSURLRequest requestWithURL:imageUrl] placeholderImage:[Utility placeHolderImage] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
//            [self.showImageView setImage:image];
//            self.imageLoaded = YES;
//        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
//            self.imageLoaded = NO;
//            [self locateImages];
//        }];
//    } else {
//        self.imageLoaded = NO;
//    }
//    
//    NSString *youtubeEmbedLink = [[DataManager sharedManager] getYoutubeEmbedLinkFromVID:[episode.ytlinks objectAtIndex:0]];
//    
//    NSURL *url1 = [[DataManager sharedManager] getPreviewImageURLOfYoutubeVideo:youtubeEmbedLink number:1];
//    NSURL *url2 = [[DataManager sharedManager] getPreviewImageURLOfYoutubeVideo:youtubeEmbedLink number:2];
//    NSURL *url3 = [[DataManager sharedManager] getPreviewImageURLOfYoutubeVideo:youtubeEmbedLink number:3];
//    
//    [self.previewImageView1 setImageWithURL:url1 placeholderImage:[Utility placeHolderImage]];
//    [self.previewImageView2 setImageWithURL:url2 placeholderImage:[Utility placeHolderImage]];
//    [self.previewImageView3 setImageWithURL:url3 placeholderImage:[Utility placeHolderImage]];
//    
//    if (episode.detail) {
//        self.detailLabel.text = [NSString stringWithFormat:@"%@: %@",episode.contentName, episode.detail];
//    } else self.detailLabel.text = episode.contentName;
//    
////    self.backgroundColor = [UIColor blackColor];
////    self.infoContainerView.backgroundColor = PLAIN_COLOR_BLACK;
//    if ([self canScroll]) {
//        self.imageScroll.contentOffset = CGPointMake(WATCH_LATER_WIDTH, 0);
//    }
//    self.imageScroll.delegate = self;
//    
//    int avgRatings = 0;
//    if ([self.episode.totalRatings integerValue] > 0) {
//        avgRatings = [self.episode.ratingPoints integerValue]/[self.episode.totalRatings integerValue];
//    }
//    self.ratingsLabel.text = [Utility ratingStarsStringForRatings:avgRatings];
//    self.episodeTitleLabel.text = [[DataManager sharedManager] shortfriendlyDateFromString:self.episode.title];
//    
//    self.watchLaterButton.selected = [self.episode isFavourite];
//    [self updateWatchLaterText];
//    
//    [self locateImages];
}

-(void) locateImages {
    CGFloat x = WATCH_LATER_WIDTH + 100;
    if (self.imageLoaded) {
        x += 154;
        self.showImageView.hidden = NO;
    } else self.showImageView.hidden = YES;
    
    CGRect imageFrame = self.previewImageView1.frame;
    imageFrame.origin.x = x;
    self.previewImageView1.frame = imageFrame;
    
    imageFrame = self.previewImageView2.frame;
    x += 154;
    imageFrame.origin.x = x;
    self.previewImageView2.frame = imageFrame;
    
    imageFrame = self.previewImageView3.frame;
    x += 154;
    imageFrame.origin.x = x;
    self.previewImageView3.frame = imageFrame;
    
    x += 154;
    
    CGSize size = CGSizeMake(x, self.imageScroll.bounds.size.height);
    [self.imageScroll setContentSize:size];
    
    [self setNeedsLayout];
}

- (IBAction)showPreviewImages:(id)sender {
    
//    NSString *youtubeEmbedLink = [[DataManager sharedManager] getYoutubeEmbedLinkFromVID:[self.episode.ytlinks objectAtIndex:0]];
//    
//    NSURL *url1 = [[DataManager sharedManager] getPreviewImageURLOfYoutubeVideo:youtubeEmbedLink number:0];
//    NSURL *url2 = [[DataManager sharedManager] getPreviewImageURLOfYoutubeVideo:youtubeEmbedLink number:1];
//    NSURL *url3 = [[DataManager sharedManager] getPreviewImageURLOfYoutubeVideo:youtubeEmbedLink number:3];
//    
//    NSArray *array = @[url1, url2, url3];
//    
//    NSNotification *notif = [NSNotification notificationWithName:NOTIFICATION_SHOW_PREVIEW_IMAGES object:nil userInfo:@{@"urls":array, @"title":self.episode.title, @"name":self.episode.contentName}];
//    [[NSNotificationCenter defaultCenter] postNotification:notif];
}

-(void) slideBack {
    [self.imageScroll setContentOffset:CGPointMake(WATCH_LATER_WIDTH, 0) animated:YES];
}

-(void) startTimer {
    if (self.timer) {
        [self.timer invalidate];
    }
    self.timer = [NSTimer scheduledTimerWithTimeInterval:TIMER_INTERVAL target:self selector:@selector(slideBack) userInfo:nil repeats:NO];
}

-(void) stopTimer {
    [self.timer invalidate];
}

#pragma mark - Scroll view delegate

-(void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    
    if (![self canScroll]) {
        return;
    }
    if (targetContentOffset->x < WATCH_LATER_WIDTH) {
        [self startTimer];
    }
}

-(void) scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
//    self.watchLaterButton.selected = [self.episode isFavourite];
//    [self updateWatchLaterText];
//    
//    if (![self canScroll]) {
//        return;
//    }
//    
//    [self stopTimer];
    
}

-(void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    if (![self canScroll]) {
        return;
    }
    
    if (abs(scrollView.contentOffset.x - WATCH_LATER_WIDTH) < SANP_MARGIN) {
        [scrollView setContentOffset:CGPointMake(WATCH_LATER_WIDTH, 0) animated:YES];
    } else if ((scrollView.contentOffset.x - WATCH_LATER_WIDTH) < 0) {
        [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
}

@end
