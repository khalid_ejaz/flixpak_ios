//
//  InterestsGridItem.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-16.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InterestsGridItem : UICollectionViewCell

- (void)setSelectedInterest;
- (void)setDeSelectedInterest;
- (void)setInterestTitle:(NSString *)title subtitle:(NSString *)subtitle categoryId:(NSNumber *)categoryId;

@end
