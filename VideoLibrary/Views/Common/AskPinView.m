//
//  AskPinView.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/2/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "AskPinView.h"
#import "UserManager.h"
#import "StyleManager.h"
#import <QuartzCore/QuartzCore.h>

@interface AskPinView()

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) User *user;
@property (unsafe_unretained, nonatomic) IBOutlet UITextField *pinTextField;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *doneButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *goBackButton;

@end

@implementation AskPinView

-(void) setUser:(User *)user delegate:(id<PinViewDelegate>)delegate {
    _user = user;
    _delegate = delegate;
    [[StyleManager sharedManager] styleButtons:@[self.doneButton, self.goBackButton]];
    self.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.layer.borderWidth = 2.0;
    
    self.layer.cornerRadius = 12.0f;
    self.layer.masksToBounds = YES;
//    self.nameLabel.text = [NSString stringWithFormat:@"Enter PIN for '%@'", self.user.name];
    [self.pinTextField becomeFirstResponder];
    
    if ([[UserManager sharedManager] incorrectAttemptsForUser:user] >= 3) {
        [self threeIncorrectAttemptsReached];
    }
}

-(void) threeIncorrectAttemptsReached {
    if (![[[UserManager sharedManager] timeToReAttemptAccess] isEqualToString:@""]) {
        [self.delegate pinViewWantsToShowMessage:[NSString stringWithFormat:@"You made too many incorrect attempts. Try again in %@",[[UserManager sharedManager] timeToReAttemptAccess]]];
    } else {
        [self.delegate pinViewWantsToShowMessage:@"You made too many incorrect attempts. Try again in 5 minutes"];
    }
    [self.delegate pinViewdeniedAccessToUser:self.user];
}

- (IBAction)cancelAction:(id)sender {
    [self.delegate pinViewdeniedAccessToUser:self.user];
}

- (IBAction)doneAction:(id)sender {
    
//    if ([self.pinTextField.text isEqualToString:@""]) {
//        [self.delegate pinViewWantsToShowMessage:@"Enter a pin code"];
//        return;
//    }
    
//    if (![self.pinTextField.text isEqualToString:self.user.pin]) {
//        int failedAttempts = [[UserManager sharedManager] incorrectAttemptsForUser:self.user];
//        if (failedAttempts < 3) {
//            [self.delegate pinViewWantsToShowMessage:@"Incorrect Pin"];
//            self.pinTextField.text = @"";
//            [[UserManager sharedManager] saveIncorrectAttemptForUser:self.user];
//        } else {
//            [self threeIncorrectAttemptsReached];
//        }
//    } else {
//        [[UserManager sharedManager] removeIncorrectAttemptsForUser:self.user];
//        [self.pinTextField resignFirstResponder];
//        self.pinTextField.text = @"";
//        [self.delegate pinViewAllowedAccessToUser:self.user];
//    }
}

#pragma mark - text field delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    int maxAllowed = 4;
    return (newLength > maxAllowed) ? NO : YES;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    [self doneAction:nil];
    return NO;
}


@end
