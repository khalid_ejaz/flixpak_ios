//
//  TabView.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-16.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TabView;
@protocol TabViewDelegate <NSObject>

- (void)tabView:(TabView *)tabView selectedCategory:(NSNumber *)categoryId;

@end

@interface TabView : UIView

@property (nonatomic, weak) id<TabViewDelegate> delegate;

- (void)setSelected;
- (void)setDeselected;

- (void)setTitle:(NSString *)title categoryId:(NSNumber *)categoryId;
- (NSNumber *)categoryId;

@end
