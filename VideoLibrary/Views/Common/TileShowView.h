//
//  TileShowView.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/5/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PSTCollectionView.h>

@class Content;

@protocol TileShowViewDelegate <NSObject>

-(void) tileViewSelectedContent:(Content *)content;
-(void) tileViewFavdContent:(Content *)content;

@end

@interface TileShowView : PSTCollectionViewCell

@property (nonatomic, unsafe_unretained) id<TileShowViewDelegate> delegate;

-(void) populateWithContent:(Content *)content;

@end
