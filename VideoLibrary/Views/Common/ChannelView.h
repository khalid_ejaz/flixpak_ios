//
//  ChannelView.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/1/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PSTCollectionView.h>

@class YoutubeChannel, UserChannel;

@protocol ChannelViewDelegate <NSObject>

-(void) channelView:(id)channelView selectedChannel:(YoutubeChannel *)channel;
-(void) channelView:(id)channelView selectedUserChannel:(UserChannel *)channel;

@end

@interface ChannelView : PSTCollectionViewCell

@property (nonatomic, unsafe_unretained) id<ChannelViewDelegate> delegate;

-(void) populateWithChannel:(YoutubeChannel *)channel delegate:(id<ChannelViewDelegate>)delegate;
-(void) populateWithUserChannel:(UserChannel *)channel delegate:(id<ChannelViewDelegate>)delegate;

@end
