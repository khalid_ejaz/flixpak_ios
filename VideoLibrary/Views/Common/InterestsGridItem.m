//
//  InterestsGridItem.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-16.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "InterestsGridItem.h"

@interface InterestsGridItem()

@property (weak, nonatomic) IBOutlet UILabel *tickLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (nonatomic, strong) NSNumber *categoryId;

@end

@implementation InterestsGridItem

- (void)setSelectedInterest {
    self.tickLabel.hidden = NO;
}

- (void)setDeSelectedInterest {
    self.tickLabel.hidden = YES;
}

- (void)setInterestTitle:(NSString *)title subtitle:(NSString *)subtitle categoryId:(NSNumber *)categoryId {
    self.titleLabel.text = title;
    self.subtitleLabel.text = subtitle;
    _categoryId = categoryId;
}

@end
