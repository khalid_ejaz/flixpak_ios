//
//  PlayerOverlayVideoCell.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-17.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerOverlayVideoCell : UICollectionViewCell

- (void)populateWithTitle:(NSString *)title subtitle:(NSString *)subtitle rating:(NSString *)rating thb:(NSString *)thbUrl vid:(NSString *)vid;
- (void)setPlaying:(BOOL)playing;

@end
