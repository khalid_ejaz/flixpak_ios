//
//  PlayerOverlayVideoCell.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2017-01-17.
//  Copyright © 2017 Malik Khalid Ejaz. All rights reserved.
//

#import "PlayerOverlayVideoCell.h"
#import <UIImageView+AFNetworking.h>
#import "DataManager.h"

@interface PlayerOverlayVideoCell()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet UILabel *playingLabel;
@property (weak, nonatomic) IBOutlet UIView *subtitleView;
@property (weak, nonatomic) IBOutlet UIView *titleView;

@property (nonatomic, strong) NSString *vid;

@end

@implementation PlayerOverlayVideoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setPlaying:(BOOL)playing {
    self.playingLabel.hidden = !playing;
    self.titleView.hidden = self.subtitleView.hidden = !self.playingLabel.hidden;
    if (!playing) {
        [self showOrHideTitleView];
    }
}

- (void)showOrHideTitleView {
    if (!self.titleLabel.text || [self.titleLabel.text isEqualToString:@""]) self.titleView.hidden = YES;
    else self.titleView.hidden = NO;
}

- (void)populateWithTitle:(NSString *)title subtitle:(NSString *)subtitle rating:(NSNumber *)rating thb:(NSString *)thbUrl vid:(NSString *)vid {
    self.titleLabel.text = title;
    [self showOrHideTitleView];
    if (!rating) self.ratingLabel.text = @"";
    else self.ratingLabel.text = [NSString stringWithFormat:@"%@", rating];
    if (!subtitle) subtitle = @"";
    self.subtitleLabel.text = subtitle;
    _vid = vid;
    if (!thbUrl || [thbUrl isEqualToString:@""]) {
        thbUrl = [NSString stringWithFormat:@"https://i1.ytimg.com/vi/%@/mqdefault.jpg",vid];
    }
    self.imageView.image = nil;
    [self.imageView setImageWithURL:[NSURL URLWithString:thbUrl] placeholderImage:nil];
}

@end
