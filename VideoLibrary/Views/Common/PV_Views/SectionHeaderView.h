//
//  SectionHeaderView.h
//  PakVideos
//
//  Created by Malik Ejaz on 2016-09-15.
//  Copyright © 2016 Malik Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectionHeaderView : UIView

- (void)setHeading:(NSString *)heading;

@end
