//
//  SectionRow.m
//  PakVideos
//
//  Created by Malik Ejaz on 2016-09-15.
//  Copyright © 2016 Malik Ejaz. All rights reserved.
//

#import "SectionRow.h"
#import "constants.h"
#import "VideoItemView.h"

#import <UIImageView+AFNetworking.h>

@interface SectionRow()
@property (nonatomic, strong) UICollectionView *collection;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@property (nonatomic, assign) BOOL grayBackground;
@end

@implementation SectionRow

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
//    if (self.collection) {
//        if (IS_PORTRAIT) {
//            [self setDefaultLayout];
//        } else {
//            if (self.layoutVerticalForLandscape) {
//                UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
//                layout.sectionInset = UIEdgeInsetsMake(VIDEO_VIEW_MARGIN, VIDEO_VIEW_MARGIN, VIDEO_VIEW_MARGIN, VIDEO_VIEW_MARGIN);
//                CGFloat allowedWidth = self.widthForVerticalLayout - 20;
//                layout.itemSize = CGSizeMake(allowedWidth, (allowedWidth*0.5625)+60);
//                layout.scrollDirection = UICollectionViewScrollDirectionVertical;
//                [self.collection setCollectionViewLayout:layout animated:YES];
//                
//            } else if (self.layoutHorizontalForLandscape) {
//                UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
//                layout.sectionInset = UIEdgeInsetsMake(VIDEO_VIEW_MARGIN, VIDEO_VIEW_MARGIN, VIDEO_VIEW_MARGIN, VIDEO_VIEW_MARGIN);
//                CGFloat allowedItemHeight = self.heightForHorizontalLayout - 21;
//                layout.itemSize = CGSizeMake((allowedItemHeight-39)*1.77, allowedItemHeight);
//                layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
//                [self.collection setCollectionViewLayout:layout animated:YES];
//                
//            } else [self setDefaultLayout];
//        }
//        
//    }
}

- (void)setupCollectionView {
    if (!self.collection) {
        // create collection view and add to row.
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.sectionInset = UIEdgeInsetsMake(0, 5, 0, 0);
        layout.itemSize = CGSizeMake(IS_IPAD?kIPAD_VIDEO_VIEW_WIDTH:kIPHONE_VIDEO_VIEW_WIDTH, IS_IPAD?kIPAD_VIDEO_VIEW_HEIGHT:kIPHONE_VIDEO_VIEW_HEIGHT);
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumInteritemSpacing = 0;//VIDEO_VIEW_MARGIN;
        layout.minimumLineSpacing = 5;//VIDEO_VIEW_MARGIN;
        UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        [collectionView registerNib:[UINib nibWithNibName:@"VideoItemView" bundle:nil] forCellWithReuseIdentifier:COLLECTION_VIEW_CELL_REUSE_ID];
        collectionView.showsHorizontalScrollIndicator = NO;
        collectionView.delegate = self;
        collectionView.dataSource = self;
        collectionView.backgroundColor = [UIColor blackColor];
        collectionView.frame = self.bounds;
        collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _collection = collectionView;
        [self.contentView addSubview:self.collection];
        [self.contentView bringSubviewToFront:self.spinner];
        [self.collection setBounces:NO];
    }
    // populate a UICollectionView with videos from this section.
    [self.collection reloadData];
}

- (void)setSection:(NSDictionary *)section {
    _section = section;
    
    if ([self videosInSection]) {
        [self.spinner stopAnimating];
    } else [self.spinner startAnimating];
    
    // setup collection view if not already done.
    [self setupCollectionView];
    
    // if youtube video section. start loading videos from youtube and populate section dictionary.
}

- (NSArray *)videosInSection {
    NSArray *videos = self.section[@"videos"];
    return videos;
}

- (BOOL)hasSectionData {
    if (_section) {
        return YES;
    }
    return NO;
}

#pragma mark - collection view

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self videosInSection].count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    VideoItemView *cell = (VideoItemView *)[collectionView dequeueReusableCellWithReuseIdentifier:COLLECTION_VIEW_CELL_REUSE_ID forIndexPath:indexPath];
    
    NSArray *videos = [self videosInSection];
    NSDictionary *video = videos[indexPath.row];
    
    cell.title.text = video[@"title"];
    cell.subtitle.text = video[@"subtitle"];
    int rating = [(NSNumber *)video[@"ratings"] intValue];
    NSString *thumbnailURL = video[@"thb"];
    if (!thumbnailURL) {
        thumbnailURL = [NSString stringWithFormat:@"https://i.ytimg.com/vi/%@/mqdefault.jpg", video[@"vid"]];
    }
    NSURL *imgRUL = [NSURL URLWithString:thumbnailURL];
    
    NSString *ratingText = @"";
    cell.playingLabel.text = @"";
    if (![video.allKeys containsObject:@"fromChannel"]) {
        if (rating == 5) {
            ratingText = @"★★★★★";
        } else if (rating == 4) {
            ratingText = @"★★★★☆";
        } else if (rating == 3) {
            ratingText = @"★★★☆☆";
        } else if (rating == 2) {
            ratingText = @"★★☆☆☆";
        } else if (rating == 1) {
            ratingText = @"★☆☆☆☆";
        } else if (rating == 0) {
            ratingText = @"☆☆☆☆☆";
        }
    } else {
        NSString *age = video[@"added_on"];
        if (age) {
            if (![age isEqualToString:@""]) {
                age = [self publishedAtStringFromString:age];
                cell.playingLabel.text = age;
            }
        }
    }
    cell.ratingLabel.text = ratingText;
    cell.imageView.image = nil;
    [cell.imageView setImageWithURL:imgRUL];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate sectionRowSelectedVideoAtIndex:indexPath.row inSection:self.section];
}

- (NSDate*)dateWithJSONString:(NSString *)jsonDate {
    [NSDateFormatter setDefaultFormatterBehavior:
     NSDateFormatterBehavior10_4];
    NSDateFormatter *dateFormatter =
    [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSz"];
    [dateFormatter setTimeZone:[NSTimeZone
                                timeZoneForSecondsFromGMT:0]];
    [dateFormatter setCalendar:[[NSCalendar alloc]
                                initWithCalendarIdentifier:NSCalendarIdentifierGregorian]];
    
    NSDate *date = [[NSDate alloc] init];
    date = [dateFormatter dateFromString:jsonDate];
    return date;
}

-(NSString *) publishedAtStringFromString:(NSString *)dateString {
    NSDate *date = [self dateWithJSONString:dateString];
    
    NSTimeInterval secs = [[NSDate date] timeIntervalSinceDate:date];
    if (secs == 0) return @"";
    
    if (secs < 60) {
        return [NSString stringWithFormat:@"%.0f sec ago", secs];
    }
    
    NSInteger minutes = secs/60;
    if (minutes < 120) {
        if (minutes > 1) {
            return [NSString stringWithFormat:@"%ld mins ago", (long)minutes];
        } else return [NSString stringWithFormat:@"%ld min ago", (long)minutes];
    }
    
    NSInteger hours = minutes/60;
    if (hours < 24) {
        if (hours > 1) {
            return [NSString stringWithFormat:@"%ld hours ago", (long)hours];
        } else return [NSString stringWithFormat:@"%ld hour ago", (long)hours];
    }
    
    NSInteger days = hours/24;
    if (days < 30) {
        if (days > 1) {
            return [NSString stringWithFormat:@"%ld days ago", (long)days];
        } else return [NSString stringWithFormat:@"%ld day ago", (long)days];
    }
    
    NSInteger months = days/30;
    if (months < 12) {
        if (months > 1) {
            return [NSString stringWithFormat:@"%ld months ago", (long)months];
        } else return [NSString stringWithFormat:@"%ld month ago", (long)months];
    }
    
    NSInteger years = months/12;
    if (years > 1) {
        return [NSString stringWithFormat:@"%ld years ago", (long)years];
    } else return [NSString stringWithFormat:@"%ld year ago", (long)years];
}

#pragma mark - Youtube video channel related.
// if the given section dictionary does not contain videos, then need to load videos from provided id in section. This id is uploads id for the channel given as heading. populate section dictionary with videos so can be passed on in delegate methods.

@end
