//
//  SectionRow.h
//  PakVideos
//
//  Created by Malik Ejaz on 2016-09-15.
//  Copyright © 2016 Malik Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SectionRowDelegate <NSObject>

// when user taps on a video thumbnail to start playing a video.
- (void)sectionRowSelectedVideoAtIndex:(NSInteger)videoIndex inSection:(NSDictionary *)section;

@optional
// when user taps on the heading of the section to view detail screen for this section.
- (void)sectionRowHeaderTapped:(NSDictionary *)section;

@end

@interface SectionRow : UITableViewCell <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, unsafe_unretained) id<SectionRowDelegate> delegate;
@property (nonatomic, strong) NSDictionary * section;

@property (nonatomic, assign) CGFloat widthForVerticalLayout;
@property (nonatomic, assign) BOOL layoutVerticalForLandscape;
@property (nonatomic, assign) CGFloat heightForHorizontalLayout;
@property (nonatomic, assign) BOOL layoutHorizontalForLandscape;

- (BOOL)hasSectionData;
@end
