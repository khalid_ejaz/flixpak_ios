//
//  SectionHeaderView.m
//  PakVideos
//
//  Created by Malik Ejaz on 2016-09-15.
//  Copyright © 2016 Malik Ejaz. All rights reserved.
//

#import "SectionHeaderView.h"

@interface SectionHeaderView()

@property (weak, nonatomic) IBOutlet UIButton *sectionHeadingButton;
@property (weak, nonatomic) IBOutlet UIButton *sortButton;
@property (weak, nonatomic) IBOutlet UILabel *sectionTitleLabel;
@end

@implementation SectionHeaderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (IBAction)sortAction:(id)sender {
//    NSNotification *notif = [NSNotification notificationWithName:@"remove_sections" object:nil userInfo:nil];
//    [[NSNotificationCenter defaultCenter] postNotification:notif];
}
- (IBAction)headingAction:(id)sender {
//    NSNotification *notif = [NSNotification notificationWithName:@"reload_sections" object:nil userInfo:nil];
//    [[NSNotificationCenter defaultCenter] postNotification:notif];
}

- (void)setHeading:(NSString *)heading {
    self.sectionTitleLabel.text = heading;
//    [self.sectionTitleLabel setTextColor:[UIColor lightGrayColor]];
//    [self.sectionHeadingButton setTitle:heading forState:UIControlStateNormal];
//    [self.sectionHeadingButton sizeToFit];
//    CGRect frame = self.sectionHeadingButton.frame;
//    frame.size.width += 16;
//    frame.origin.y = 8;
//    self.sectionHeadingButton.frame = frame;
}

@end
