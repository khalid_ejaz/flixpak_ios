//
//  playerView.m
//  PakVideos
//
//  Created by Malik Ejaz on 2016-10-11.
//  Copyright © 2016 Malik Ejaz. All rights reserved.
//

#import "PlayerView.h"
#import "constants.h"

@interface PlayerView()

@property (weak, nonatomic) IBOutlet UIView *controlsView;
@property (weak, nonatomic) IBOutlet UIView *titleView;
@property (weak, nonatomic) IBOutlet UILabel *videoTitleLabel;
@property (weak, nonatomic) IBOutlet YTPlayerView *playerView;
@property (weak, nonatomic) IBOutlet UIView *moreView;
@property (weak, nonatomic) IBOutlet UISwitch *autoPlaySwitch;
@property (weak, nonatomic) IBOutlet UIButton *fullscreenButton;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

// rating star buttons.
@property (weak, nonatomic) IBOutlet UIButton *oneStarRatingButton;
@property (weak, nonatomic) IBOutlet UIButton *twoStarRatingButton;
@property (weak, nonatomic) IBOutlet UIButton *threeStarRatingButton;
@property (weak, nonatomic) IBOutlet UIButton *fourStarRatingButton;
@property (weak, nonatomic) IBOutlet UIButton *fiveStarRatingButton;
@property (nonatomic, strong) NSDictionary *section;
@property NSInteger videoIndex;
@property (nonatomic, assign) BOOL playerLoaded;
@end

@implementation PlayerView

- (void)playVideoAtIndex:(NSInteger)index section:(NSDictionary *)section {
    _videoIndex = index;
    _section = section;
    [self.spinner startAnimating];
    [self prepareViews];
}

- (void)prepareViews {
    // arrange views on the screen based on orientation.
    [self layoutViews];
    
//    if ([self.section[@"kind"] isEqualToString:@"YouTube Channel"]) {
//        self.favButton.hidden = YES;
//    }
    self.playerView.delegate = self;
    // start video playback.
    self.videoIndex--;
    [self playNextVideo];
}

- (void)playNextVideo {
    self.videoIndex ++;
    if (self.videoIndex < self.allVideos.count) {
        NSDictionary * video = self.allVideos[_videoIndex];
        self.videoTitleLabel.text = video[@"title"];
        [self playVideo:video[@"vid"]];
        [self showUserRatings];
    } else self.videoIndex = -1;
}

- (void)layoutViews {
    CGRect frame;
    
    self.moreView.hidden = YES;
    
    frame.origin.x = 0;
    frame.origin.y = 0;
    frame.size.width = self.bounds.size.width;
    frame.size.height = frame.size.width * 0.5625;
    self.playerView.frame = frame;
    
    self.controlsView.hidden = NO;
    frame = self.controlsView.frame;
    frame.origin.x = 0;
    frame.origin.y = self.playerView.bounds.size.height;
    frame.size.width = self.playerView.frame.size.width;
    self.controlsView.frame = frame;
    
    frame = self.titleView.frame;
    frame.origin.x = self.playerView.frame.origin.x;
    frame.origin.y = self.playerView.frame.origin.y;
    frame.size.width = self.playerView.frame.size.width;
    self.titleView.frame = frame;
    
//    if (IS_PORTRAIT) {
//        frame.origin.x = 0;
//        frame.origin.y = CGRectGetMaxY(self.controlsView.frame);
//        frame.size.width = self.view.bounds.size.width;
//        frame.size.height = self.view.bounds.size.height - self.playerView.bounds.size.height - self.controlsView.bounds.size.height - [[MEAdManager sharedManager] adHeight];
//        self.sectionsVC.view.frame = frame;
//        self.sectionsVC.view.hidden = NO;
//    } else {
//        self.sectionsVC.view.hidden = YES;
//    }
}

#pragma mark - ratings related

- (IBAction)rateOneStarAction:(id)sender {
}

- (IBAction)rateTwoStarAction:(id)sender {
}

- (IBAction)rateThreeStarAction:(id)sender {
}
- (IBAction)rateFourStarAction:(id)sender {
}

- (IBAction)rateFiveStarAction:(id)sender {
}

-(NSArray *)starButtons {
    return @[self.oneStarRatingButton, self.twoStarRatingButton, self.threeStarRatingButton, self.fourStarRatingButton, self.fiveStarRatingButton];
}

-(void) disableStarButtons {
    NSArray *starButtons = [self starButtons];
    for (UIButton *button in starButtons) {
        button.enabled = NO;
    }
}

-(void) resetStarButtons {
    NSArray *starButtons = [self starButtons];
    for (UIButton *button in starButtons) {
        button.enabled = YES;
        [button setTitle:@"☆" forState:UIControlStateNormal];
    }
}

- (void)showStars:(int)stars {
    if (stars >= 5)
        stars = 5;
    
    NSArray *buttons = [self starButtons];;
    for (int i=0; i<stars; i++) {
        UIButton *b = buttons[i];
        [b setTitle:@"★" forState:UIControlStateNormal];
    }
    
    for (int i=stars; i<5; i++) {
        UIButton *b = buttons[i];
        [b setTitle:@"☆" forState:UIControlStateNormal];
    }
}

-(void) showUserRatings {
    // TODO: if user has not rated for this video yet, show avg rating by other users.
    [self resetStarButtons];
    // else show user's own ratings and disable buttons as user already rated and can not rate again.
    [self disableStarButtons];
    
    //    int userRating = [[UserManager sharedManager] userRatingsForEpisode:self.episode];
    //    if (userRating > 0) {
    //        [self showStarsWithRatings:userRating];
    //        [self disableStarButtons];
    //    } else {
    //        [self showStarsWithRatings:[self.episode getAverageRating]];
    //        [self enableStarButtons];
    //    }
    
    //    [button setTitle:@"☆" forState:UIControlStateNormal];
    
    if (![self.section[@"kind"] isEqualToString:@"App Category"]) {
        self.oneStarRatingButton.hidden = self.twoStarRatingButton.hidden = self.threeStarRatingButton.hidden = self.fourStarRatingButton.hidden = self.fiveStarRatingButton.hidden = YES;
    } else {
        self.oneStarRatingButton.hidden = self.twoStarRatingButton.hidden = self.threeStarRatingButton.hidden = self.fourStarRatingButton.hidden = self.fiveStarRatingButton.hidden = NO;
        NSNumber *rat = [[self allVideos] objectAtIndex:self.videoIndex][@"rating"];
        int rating = [rat intValue];
        [self showStars:rating];
        //        if (rating >= 5) {
        //            self.oneStarRatingButton.selected = self.twoStarRatingButton.selected = self.threeStarRatingButton.selected = self.fourStarRatingButton.selected = self.fiveStarRatingButton.selected = YES;
        //        } else if (rating >= 4) {
        //            self.oneStarRatingButton.selected = self.twoStarRatingButton.selected = self.threeStarRatingButton.selected = self.fourStarRatingButton.selected = YES;
        //            self.fiveStarRatingButton.selected = NO;
        //        } else if (rating >= 3) {
        //            self.oneStarRatingButton.selected = self.twoStarRatingButton.selected = self.threeStarRatingButton.selected = YES;
        //            self.fourStarRatingButton.selected = self.fiveStarRatingButton.selected = NO;
        //        } else if (rating >= 2) {
        //            self.oneStarRatingButton.selected = self.twoStarRatingButton.selected = YES;
        //            self.threeStarRatingButton.selected = self.fourStarRatingButton.selected = self.fiveStarRatingButton.selected = NO;
        //        } else if (rating >= 1) {
        //            self.oneStarRatingButton.selected = YES;
        //            self.twoStarRatingButton.selected = self.threeStarRatingButton.selected = self.fourStarRatingButton.selected = self.fiveStarRatingButton.selected = NO;
        //        } else self.oneStarRatingButton.selected = self.twoStarRatingButton.selected = self.threeStarRatingButton.selected = self.fourStarRatingButton.selected = self.fiveStarRatingButton.selected = NO;
    }
}

- (IBAction)replay:(id)sender {
    float time = [self.playerView currentTime] - 7;
    [self.playerView seekToSeconds:time allowSeekAhead:YES];
}
- (IBAction)skip:(id)sender {
    float time = [self.playerView currentTime] + 7;
    [self.playerView seekToSeconds:time allowSeekAhead:YES];
}
- (IBAction)maximize:(id)sender {
}
- (IBAction)closeAction:(id)sender {
    [self.playerView stopVideo];
    self.playerView.hidden = YES;
    [self.delegate playerViewClosedByUser];
}

- (NSArray *)allVideos {
    return self.section[@"videos"];
}

#pragma mark - video playback related.

- (void)playVideo:(NSString *)vid {
    NSDictionary *playerVars = @{
                                 @"playsinline" : @1,
                                 @"rel":@0,
                                 @"fs":@0,
                                 @"showinfo":@1,
                                 @"modestbranding":@0,
                                 @"controls":@1
                                 };
    if (self.playerLoaded) {
        [self.playerView cueVideoById:vid startSeconds:0 suggestedQuality:kYTPlaybackQualityAuto];
        [self.playerView playVideo];
    } else {
        [self.playerView loadWithVideoId:vid playerVars:playerVars];
    }
}

#pragma mark - Youtube Player View delegate methods.

- (void)playerViewDidBecomeReady:(YTPlayerView *)playerView {
    self.playerLoaded = true;
    [playerView playVideo];
}

- (void)playerView:(nonnull YTPlayerView *)playerView didChangeToState:(YTPlayerState)state {
    if (state == kYTPlayerStateEnded) {
        [self playNextVideo];
    } else if (state == kYTPlayerStatePlaying) {
        [self.spinner stopAnimating];
        self.playerView.hidden = NO;
    }
}

@end
