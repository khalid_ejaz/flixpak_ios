//
//  playerView.h
//  PakVideos
//
//  Created by Malik Ejaz on 2016-10-11.
//  Copyright © 2016 Malik Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YTPlayerView.h"

@protocol PlayerViewDelegate <NSObject>

// when user taps on a video thumbnail to start playing a video.
- (void)playerViewClosedByUser;

@end

@interface PlayerView : UIView <YTPlayerViewDelegate>
@property (nonatomic, unsafe_unretained) id<PlayerViewDelegate> delegate;

- (void)playVideoAtIndex:(NSInteger)index section:(NSDictionary *)section;

@end
