//
//  VideoItemView.h
//  PakVideos
//
//  Created by Malik Ejaz on 2016-09-09.
//  Copyright © 2016 Malik Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoItemView : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitle;
@property (weak, nonatomic) IBOutlet UILabel *playingLabel;

@end
