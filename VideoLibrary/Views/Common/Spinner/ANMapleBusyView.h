//
//  ANMapleBusyView.h
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2013-09-06.
//
//

#import <UIKit/UIKit.h>

@interface ANMapleBusyView : UIView

-(void) stopSpinner;
-(void) startSpinner;

@end
