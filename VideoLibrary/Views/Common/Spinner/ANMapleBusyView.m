//
//  ANMapleBusyView.m
//  PakTVShows
//
//  Created by Malik Khalid Ejaz on 2013-09-06.
//
//

#import "ANMapleBusyView.h"
#import <QuartzCore/QuartzCore.h>

@interface ANMapleBusyView ()


@property (unsafe_unretained, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *appImageView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *backgroundView;

@end

@implementation ANMapleBusyView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void) awakeFromNib {
    [super awakeFromNib];
    self.appImageView.layer.cornerRadius = 36;
    self.appImageView.layer.masksToBounds = YES;
//    self.appImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    self.appImageView.layer.borderWidth = 5.0;
    
    [self.activityIndicator setTransform:CGAffineTransformMakeScale(2.0, 2.0)];
//    [self.activityIndicator setTransform:CGAffineTransformMakeScale(3.7, 3.7)];
//    self.backgroundView.layer.cornerRadius = 10;
//    self.backgroundView.layer.masksToBounds = YES;
}

-(void) startSpinner {

    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0.8;
    }];
}

-(void) stopSpinner {
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end
