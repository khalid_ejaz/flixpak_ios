//
//  YCSearchResultView.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/30/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "YCSearchResultView.h"
#import "YCSearchResult.h"
#import "DataManager.h"
#import "Utility.h"
#import "LoadMoreCell.h"
#import "UserManager.h"
#import "SwitchOption.h"

#define CELL_ID             @"cellId"
#define LOAD_MORE_CELL_ID   @"loadMoreCellId"
#define CELL_HEIGHT         121

@interface YCSearchResultView()

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *resultInfoLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *nextButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *prevButton;
@property (unsafe_unretained, nonatomic) IBOutlet UITableView *resultTable;
@property (nonatomic, strong) NSString *sortOrder;

@end

@implementation YCSearchResultView

-(void) setSearchResult:(YCSearchResult *)searchResult {
    _searchResult = searchResult;
    if (!self.sortOrder) {
        self.sortOrder = @"relevance";
    }
    [self.resultTable registerNib:[UINib nibWithNibName:@"YCSearchResultItemCell" bundle:nil] forCellReuseIdentifier:CELL_ID];
    [self.resultTable registerNib:[UINib nibWithNibName:@"LoadMoreCell" bundle:nil] forCellReuseIdentifier:LOAD_MORE_CELL_ID];

    [self showSearchResults];
//    [self addSwitchControl];
}

-(void) setSortedResult:(YCSearchResult *)searchResult {
    _searchResult = searchResult;
    [self showSearchResults];
}

-(void) addSwitchControl {
    SwitchOption *opt1 = [[SwitchOption alloc] initWithDisplayText:@"Relevance" value:@"relevance" ascending:YES];
    SwitchOption *opt2 = [[SwitchOption alloc] initWithDisplayText:@"Video Count" value:@"videoCount" ascending:YES];
    SwitchOption *opt3 = [[SwitchOption alloc] initWithDisplayText:@"View Count" value:@"viewCount" ascending:YES];
    
    SwitchControl *control = [[NSBundle mainBundle] loadNibNamed:@"SwitchControl" owner:self options:nil][0];
    [control setFrame:CGRectMake(8, 8, self.resultTable.bounds.size.width-16, 33)];
    [control populateWithSwitchOptions:@[opt1, opt2, opt3] controlBackColor:GRAY_WITH_PERCENT(15) optionBackColor:GRAY_WITH_PERCENT(15) textColor:GRAY_WITH_PERCENT(60) showingSort:NO];
    control.delegate = self;
    control.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    self.resultTable.tableHeaderView = control;
    [self.resultTable setContentOffset:CGPointMake(0, 33) animated:NO];
}

-(void) showSearchResults {
    
    [self.resultTable reloadData];
}

-(void) showResultsWithPageToken:(NSString *)pageToken {
    [[DataManager sharedManager] findChannelsByName:self.searchResult.searchQuery order:self.sortOrder pageToken:pageToken completion:^(YCSearchResult *result, NSError *error) {
        if (error) {
            [Utility showError:error];
        } else {
            self.searchResult.nextPageToken = result.nextPageToken;
            [self.searchResult.resultItems addObjectsFromArray:result.resultItems];
            [self.resultTable reloadData];
        }
    }];
}

#pragma mark - IBActions

- (IBAction)nextAction:(id)sender {
    [self showResultsWithPageToken:self.searchResult.nextPageToken];
}

- (IBAction)prevAction:(id)sender {
    [self showResultsWithPageToken:self.searchResult.prevPageToken];
}

#pragma mark - table view data source

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.searchResult.resultItems.count) {
        return 44;
    }
    return CELL_HEIGHT;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if (self.searchResult.nextPageToken) {
//        return self.searchResult.resultItems.count + 1;
//    }
    return self.searchResult.resultItems.count;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == self.searchResult.resultItems.count) {
        LoadMoreCell *cell = [tableView dequeueReusableCellWithIdentifier:LOAD_MORE_CELL_ID];
        [cell.loadMoreButton removeTarget:self action:@selector(nextAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.loadMoreButton addTarget:self action:@selector(nextAction:) forControlEvents:UIControlEventTouchUpInside];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.loadMoreButton.tag = indexPath.section;
        cell.loadMoreButton.hidden = NO;
        [cell.activityIndicator stopAnimating];
        [cell.loadMoreButton setTitle:@"More..." forState:UIControlStateNormal];
        return cell;
    }
    
    YCSearchResultItemCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_ID];
    [cell populateWithResultItem:[self.searchResult.resultItems objectAtIndex:indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    YCSearchResultItem *resultItem = self.searchResult.resultItems[indexPath.row];
    [self.delegate selectedResultItem:resultItem];
}

#pragma mark - switch control delegate.

-(void) switchControl:(id)control selectedOption:(SwitchOption *)option {
    self.sortOrder = option.value;
    [[DataManager sharedManager] findChannelsByName:self.searchResult.searchQuery order:self.sortOrder pageToken:nil completion:^(YCSearchResult *result, NSError *error) {
        if (error) {
            [Utility showError:error];
        } else {
            _searchResult = result;
            [self.resultTable reloadData];
        }
    }];
}

-(void) scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(hideKeyboard)]) {
            [self.delegate hideKeyboard];
        }
    }
}

@end
