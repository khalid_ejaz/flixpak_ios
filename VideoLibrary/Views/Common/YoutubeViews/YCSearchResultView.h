//
//  YCSearchResultView.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/30/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YCSearchResultItemCell.h"
#import "SwitchControl.h"

@class YCSearchResult;

@protocol YCSearchResultItemDelegate <NSObject>

-(void) selectedResultItem:(YCSearchResultItem *)resultItem;
@optional
-(void) hideKeyboard;
@end

@interface YCSearchResultView : UIView <UITableViewDataSource, UITableViewDelegate, SwitchControlDelegate>

@property (nonatomic, unsafe_unretained) id<YCSearchResultItemDelegate> delegate;

@property (nonatomic, strong) YCSearchResult *searchResult;

@end
