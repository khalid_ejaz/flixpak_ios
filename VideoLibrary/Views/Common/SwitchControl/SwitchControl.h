//
//  SwitchControl.h
//  PakTV
//
//  Created by Malik Khalid Ejaz on 11/15/2013.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwitchOptionView.h"

@protocol SwitchControlDelegate <NSObject>

-(void) switchControl:(id)control selectedOption:(SwitchOption *)option;

@optional
-(void) gotOptionForSwitchControl:(id)control;

@optional
-(void) removedAllOptionsForSwitchControl:(id)control;

@end

@interface SwitchControl : UIView <SwitchOptionViewDelegate>

@property(nonatomic, unsafe_unretained) id<SwitchControlDelegate> delegate;

-(void) populateWithSwitchOptions:(NSArray *)options controlBackColor:(UIColor *)controlBackColor optionBackColor:(UIColor *)optionBackColor textColor:(UIColor *)textColor showingSort:(BOOL)showingSort;
-(void) populateWithSwitchOptions:(NSArray *)options controlBackColor:(UIColor *)controlBackColor optionBackColor:(UIColor *)optionBackColor textColor:(UIColor *)textColor showingSort:(BOOL)showingSort selectIndex:(NSInteger)index;
-(void) changeBackColorTo:(UIColor *)backColor OptionBackColor:(UIColor *)optionBackColor textColor:(UIColor *)textColor;

-(void) reLayout;
-(SwitchOption *)selectedOption;

-(void) addOption:(SwitchOption *)option;

-(void) removeOptionByValue:(NSString *)value;
-(void) removeOption:(SwitchOption *)option;

-(void) selectOptionAtIndex:(NSInteger)index;
-(void) selectOptionByValue:(NSString *)value;

-(BOOL) containsOptionWithValue:(NSString *)value;

@end
