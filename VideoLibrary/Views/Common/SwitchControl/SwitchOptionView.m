//
//  SwitchOptionView.m
//  PakTV
//
//  Created by Malik Khalid Ejaz on 11/15/2013.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import "SwitchOptionView.h"
#import "SwitchOption.h"
#import "Utility.h"

#define ASC_SORT_INDICATOR      @"△"
#define DESC_SORT_INDICATOR     @"▽"

@interface SwitchOptionView()

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *sortIndicatorLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *optionLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *underlineView;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *rightImageView;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *leftImageView;
@property (nonatomic, assign) BOOL showingSort;
@property (nonatomic, assign) BOOL isFirst;
@property (nonatomic, assign) BOOL isLast;

@property (nonatomic, strong) UIColor *backColor;
@property (nonatomic, strong) UIColor *textColor;

@end

@implementation SwitchOptionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void) populateWithSwtichOption:(SwitchOption *)option delegate:(id<SwitchOptionViewDelegate>)delegate backColor:(UIColor *)backColor textColor:(UIColor *)textColor showingSort:(BOOL)showingSort isFirst:(BOOL)isFirst isLast:(BOOL)isLast {

    
    self.optionLabel.shadowColor = [UIColor blackColor];
    self.optionLabel.shadowOffset = CGSizeMake(0.0, 0.0);
    
    self.backgroundColor = backColor;
    
    _option = option;
    _delegate = delegate;
    
    _backColor = backColor;
    _textColor = textColor;
    
    _isFirst = isFirst;
    _isLast = isLast;
    _showingSort = showingSort;
    
    self.optionLabel.text = option.displayText;
    
    if (self.showingSort) {
        // add sort indicator based on option.ascending.
        self.sortIndicatorLabel.hidden = NO;
        [self setLabelWithSortIndicator];
    } else {
        self.sortIndicatorLabel.hidden = YES;
        self.optionLabel.textColor = textColor;
    }
    
    if (isFirst & isLast) {
        // round all corners.
    }
    
    if (isFirst) {
        // round left top & bottom corners.
    }
    
    if (isLast) {
        // round right top & bottom corners.
    }
}

-(CGFloat)requiredWidth {
    [self.optionLabel sizeToFit];
    CGRect frame = self.optionLabel.frame;
    if (frame.size.width > MAX_OPTION_TEXT_WIDTH) {
        frame.size.width = MAX_OPTION_TEXT_WIDTH;
    }
    
    CGFloat centerPoint = self.bounds.size.height/2 - frame.size.height/2;
    frame.origin.y = centerPoint;
    frame.origin.x = 4;
    self.optionLabel.frame = frame;
    
    CGRect wholeViewFrame = self.frame;
    wholeViewFrame.size.width = frame.size.width + 8;
    self.frame = wholeViewFrame;
    
    return wholeViewFrame.size.width;
}

-(void) centerText {
    CGRect textFrame = self.optionLabel.frame;
    textFrame.size.width = self.bounds.size.width - 8;
    textFrame.origin.x = 4;
    self.optionLabel.frame = textFrame;
    [self resizeUnderlineView];
}

-(void) resizeUnderlineView {
    CGRect underlineFrame = self.underlineView.frame;
    underlineFrame.size.width = self.optionLabel.frame.size.width;
    underlineFrame.origin.x = self.optionLabel.frame.origin.x;
    underlineFrame.origin.y = self.bounds.size.height - underlineFrame.size.height - 8;
    self.underlineView.frame = underlineFrame;
}

-(void) addHighlight {
    if (self.showingSort) {
        self.sortIndicatorLabel.hidden = NO;
        self.sortIndicatorLabel.textColor = _backColor;
    }
    self.backgroundColor = _textColor;
    self.optionLabel.textColor = _backColor;
    
    self.optionLabel.shadowOffset = CGSizeMake(0.0, 0.0);
    self.underlineView.hidden = NO;
    
    self.rightImageView.hidden = NO;
    self.leftImageView.hidden = NO;
    
    [self roundTopCorners];
}

- (void)roundTopCorners {
    // Create the path (with only the top-left corner rounded)
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:UIRectCornerTopLeft| UIRectCornerTopRight                                                         cornerRadii:CGSizeMake(10.0, 10.0)];
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    // Set the newly created shape layer as the mask for the image view's layer
    self.layer.mask = maskLayer;
}

- (void)removeRoundedCorners {
    self.layer.mask = nil;
}

-(void) removeHighlight {
    self.sortIndicatorLabel.hidden = YES;
    self.optionLabel.textColor = _textColor;
    self.optionLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    self.backgroundColor = _backColor;
    self.underlineView.hidden = YES;
    self.rightImageView.hidden = YES;
    self.leftImageView.hidden = YES;
    [self removeRoundedCorners];
}

-(void) setLabelWithSortIndicator {
    NSString *text;
    if (self.option.ascending) {
        self.sortIndicatorLabel.text = ASC_SORT_INDICATOR;
        text = [NSString stringWithFormat:@"%@ ", self.option.displayText];
    } else {
        self.sortIndicatorLabel.text = DESC_SORT_INDICATOR;
        text = [NSString stringWithFormat:@"%@ ", self.option.displayText];
    }
    self.optionLabel.text = text;
}

- (IBAction)optionSelected:(id)sender {
    if (self.showingSort) {
        self.option.ascending = !self.option.ascending;
        [self setLabelWithSortIndicator];
    }
    [self.delegate optionSelected:self.option];
}

-(void) setBackColor:(UIColor *)color textColor:(UIColor *)textColor {
    _backColor = color;
    _textColor = textColor;
    
    self.backgroundColor = color;
    self.optionLabel.textColor = textColor;
}

@end
