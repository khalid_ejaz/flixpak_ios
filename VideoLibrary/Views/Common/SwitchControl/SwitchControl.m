//
//  SwitchControl.m
//  PakTV
//
//  Created by Malik Khalid Ejaz on 11/15/2013.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import "SwitchControl.h"
#import "SwitchOption.h"
#import "DataManager.h"

@interface SwitchControl()

@property (unsafe_unretained, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic, strong) NSMutableArray *options;
@property (nonatomic, strong) UIColor *controlBackColor;
@property (nonatomic, strong) UIColor *optionBackColor;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, assign) BOOL showingSort;

@property (nonatomic, strong) SwitchOption *selectedOption;
@property (nonatomic, strong) NSMutableArray *optionViews;

@end

@implementation SwitchControl

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void) populateWithSwitchOptions:(NSArray *)options controlBackColor:(UIColor *)controlBackColor optionBackColor:(UIColor *)optionBackColor textColor:(UIColor *)textColor showingSort:(BOOL)showingSort {
    
    [self populateWithSwitchOptions:options controlBackColor:controlBackColor optionBackColor:optionBackColor textColor:textColor showingSort:showingSort selectIndex:0];
}

-(void) populateWithSwitchOptions:(NSArray *)options controlBackColor:(UIColor *)controlBackColor optionBackColor:(UIColor *)optionBackColor textColor:(UIColor *)textColor showingSort:(BOOL)showingSort selectIndex:(NSInteger)index {
    
    if (!options) {
        return;
    }
    
    _controlBackColor = controlBackColor;
    _optionBackColor = optionBackColor;
    _textColor = textColor;
    _showingSort = showingSort;
    
    self.backgroundColor = controlBackColor;
    
    _options = [options mutableCopy];
    
    //    if (self.showingSort && options.count > 0) {
    //        SwitchOption *option = [options objectAtIndex:0];
    //        option.ascending = !option.ascending;
    //    }
    
    [self setupViews];
    
    if (options.count > index) {
        [self optionSelected:[options objectAtIndex:index]];
    }
}

-(void) layoutSubviews {
    [super layoutSubviews];
    [self reLayout];
}

#pragma mark - Switch Option View Delegate

-(void) optionSelected:(SwitchOption *)option {
    
    if ([_selectedOption.value isEqualToString:option.value] && !self.showingSort) {
        // do not trigger reload of same option.
        return;
    }
    [self.delegate switchControl:self selectedOption:option];
    _selectedOption = option;
    [self highlightViewForOption:option];
}

-(void) highlightViewForOption:(SwitchOption *)option {
    [UIView animateWithDuration:0.2 animations:^{
        for (int i=0; i<self.optionViews.count; i++) {
            SwitchOptionView *view = [self.optionViews objectAtIndex:i];
            if ([view.option.value isEqualToString:option.value]) {
                [view addHighlight];
                [self centerHighlightedView:view];
            } else [view removeHighlight];
        }
    }];
}

-(void) centerHighlightedView:(UIView *)view {
    CGFloat centerX = view.center.x - self.bounds.size.width/2;
    
    if (centerX < 0) {
        centerX = 0.0f;
    } else if (centerX + self.bounds.size.width > self.scrollView.contentSize.width) {
        centerX = self.scrollView.contentSize.width - self.bounds.size.width;
    }
    [self.scrollView setContentOffset:CGPointMake(centerX, 0) animated:YES];
    
}

-(void) setupViews {
    
    for (UIView *view in self.optionViews) {
        [view removeFromSuperview];
    }
    self.optionViews = [NSMutableArray array];
    
    self.scrollView.contentSize = CGSizeMake(1, self.scrollView.bounds.size.height);
    
    CGFloat totalWidth = 1.0;
    for (int i=0; i< self.options.count; i++) {
        SwitchOption *anOption = [self.options objectAtIndex:i];
        
        SwitchOptionView *optionView = [[NSBundle mainBundle] loadNibNamed:@"SwitchOptionView" owner:self options:nil][0];
        
        [optionView populateWithSwtichOption:anOption delegate:self backColor:_optionBackColor textColor:_textColor showingSort:_showingSort isFirst:(i == 0) isLast:(i == (self.options.count-1))];
        
        [optionView removeHighlight];
        
        [self.optionViews addObject:optionView];
        CGFloat requiredOptionWidth = [optionView requiredWidth] + 1;
        CGRect optionViewFrame = optionView.frame;
        optionViewFrame.origin.y = 1;
        optionViewFrame.origin.x = self.scrollView.contentSize.width;
        optionView.frame = optionViewFrame;
        [optionView centerText];
        
        [self.scrollView addSubview:optionView];
        
        self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width + requiredOptionWidth, self.scrollView.contentSize.height);
        totalWidth += requiredOptionWidth;
    }
    
    if (totalWidth < self.bounds.size.width) {
        CGFloat moreAvailableSpace = self.bounds.size.width - totalWidth;
        int availableSpacePerOption = moreAvailableSpace/self.options.count;
        
        CGFloat x = 1;
        for (SwitchOptionView *optionView in self.optionViews) {
            CGRect optionFrame = optionView.frame;
            optionFrame.origin.x = x;
            optionFrame.size.width += availableSpacePerOption;
            optionView.frame = optionFrame;
            x += optionFrame.size.width + 1;
            [optionView centerText];
        }
        self.scrollView.contentSize = CGSizeMake(x, self.scrollView.contentSize.height);
    }
}

// for view frame changes.
-(void) reLayout {
    [self setupViews];
    
    if (self.selectedOption) {
        [self highlightViewForOption:self.selectedOption];
    }
}

-(void) addOption:(SwitchOption *)option {
    if ([self containsOptionWithValue:option.value]) {
        [self selectOptionByValue:option.value];
        return;
    }
    
    if (!self.options) {
        self.options = [NSMutableArray array];
        _selectedOption = option;
    }
    
    [self.options addObject:option];
    self.options = [[[DataManager sharedManager] sortCaseInsensitiveObjects:self.options orderBy:KEY_SWITCH_OPTION_DISPLAY_TEXT ascending:YES] mutableCopy];
    [self reLayout];
    
    if (self.options.count == 1) {
        [self.delegate gotOptionForSwitchControl:self];
        [self selectOption:option];
    }
}

-(void) removeOptionAtIndex:(NSInteger)index {
    
    if (index >= self.options.count) {
        return;
    }
    
    SwitchOption *option = [self.options objectAtIndex:index];
    [self removeOption:option];
}

-(void) removeOptionByValue:(NSString *)value {
    SwitchOption *option = [self optionByValue:value];
    if (option) {
        [self removeOption:option];
    }
}

-(void) removeOption:(SwitchOption *)option {
    NSInteger index = [self indexOfOption:option];
    if (index == -1) {
        return;
    }
    
    if (option == self.selectedOption && self.options.count != 1) {
        int indexToSelect = index;
        
        if (index == self.options.count - 1) {
            indexToSelect = index - 1;
        }
        
        [self.options removeObjectAtIndex:index];
        [self reLayout];
        [self selectOptionAtIndex:indexToSelect];
        
    } else {
        [self.options removeObjectAtIndex:index];
        [self reLayout];
    }
    
    if (self.options.count == 0) {
        [self.delegate removedAllOptionsForSwitchControl:self];
    }
}

-(void) selectOptionAtIndex:(NSInteger)index {
    
    if (index >= self.options.count) {
        return;
    }
    
    SwitchOption *option = [self.options objectAtIndex:index];
    if (option) {
        [self selectOption:option];
    }
}

-(void) selectOption:(SwitchOption *)option {
    
    if ([self.selectedOption.value isEqualToString:option.value]) {
        return;
    }
    
    _selectedOption = option;
    [self highlightViewForOption:option];
    [self.delegate switchControl:self selectedOption:option];
}

-(void) selectOptionByValue:(NSString *)value {
    
    SwitchOption *option = [self optionByValue:value];
    if (option) {
        [self selectOption:option];
    }
}

-(SwitchOption *) optionByValue:(NSString *)value {
    for (int i=0; i<self.options.count; i++) {
        SwitchOption *option = [self.options objectAtIndex:i];
        if ([option.value isEqualToString:value]) {
            return option;
        }
    }
    return nil;
}

-(NSInteger) indexOfOptionByValue:(NSString *)value {
    for (int i=0; i<self.options.count; i++) {
        SwitchOption *option = [self.options objectAtIndex:i];
        if ([option.value isEqualToString:value]) {
            return i;
        }
    }
    return -1;
}

-(NSInteger) indexOfOption:(SwitchOption *)option {
    for (int i=0; i<self.options.count; i++) {
        SwitchOption *anOption = [self.options objectAtIndex:i];
        if ([anOption.value isEqualToString:option.value]) {
            return i;
        }
    }
    return -1;
}

-(BOOL) containsOptionWithValue:(NSString *)value {
    
    for (SwitchOption *option in self.options) {
        if ([option.value isEqualToString:value]) {
            return YES;
        }
    }
    
    return NO;
}

-(void) changeBackColorTo:(UIColor *)backColor OptionBackColor:(UIColor *)optionBackColor textColor:(UIColor *)textColor {
    _controlBackColor = backColor;
    _optionBackColor = optionBackColor;
    
    self.backgroundColor = backColor;
    for (SwitchOptionView *view in self.optionViews) {
        [view setBackColor:optionBackColor textColor:textColor];
    }
    [self highlightViewForOption:self.selectedOption];
}

@end
