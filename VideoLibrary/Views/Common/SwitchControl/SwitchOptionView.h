//
//  SwitchOptionView.h
//  PakTV
//
//  Created by Malik Khalid Ejaz on 11/15/2013.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#define MAX_OPTION_TEXT_WIDTH       280

@class SwitchOption;

@protocol SwitchOptionViewDelegate <NSObject>

-(void)optionSelected:(SwitchOption *)option;

@end

@interface SwitchOptionView : UIView
@property(nonatomic, unsafe_unretained) id<SwitchOptionViewDelegate> delegate;
@property (nonatomic, strong) SwitchOption *option;

-(void) populateWithSwtichOption:(SwitchOption *)option delegate:(id<SwitchOptionViewDelegate>)delegate backColor:(UIColor *)backColor textColor:(UIColor *)textColor showingSort:(BOOL)showingSort isFirst:(BOOL)isFirst isLast:(BOOL)isLast;

-(void) setBackColor:(UIColor *)color textColor:(UIColor *)textColor;

-(CGFloat)requiredWidth;
-(void) centerText;
-(void) addHighlight;
-(void) removeHighlight;


@end
