//
//  ThumbnailShowView.m
//  PakTVShows
//
//  Created by Malik Ejaz on 2014-06-28.
//  Copyright (c) 2014 Malik Khalid Ejaz. All rights reserved.
//

#import "ThumbnailShowView.h"

@interface ThumbnailShowView() {
    
}

@end

@implementation ThumbnailShowView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
