//
//  ChannelView.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/1/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "ChannelView.h"
#import "Utility.h"
#import <UIImageView+AFNetworking.h>
#import "UserManager.h"

@interface ChannelView()

@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *imageView;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *topLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *favButton;

@property (nonatomic, strong) YoutubeChannel *channel;
@property (nonatomic, strong) UserChannel *userChannel;

@end

@implementation ChannelView

-(void) populateWithChannel:(YoutubeChannel *)channel delegate:(id<ChannelViewDelegate>)delegate {
    _channel = channel;
    _delegate = delegate;
//    [self.imageView setImageWithURL:[NSURL URLWithString: channel.channelImageURL] placeholderImage:[Utility placeHolderImage]];
//    
//    self.topLabel.text = channel.channelName;
//    self.favButton.selected = [UserManager isChannelFavouriteAlready:channel.channelId];
}

-(void) populateWithUserChannel:(UserChannel *)channel delegate:(id<ChannelViewDelegate>)delegate {
    _userChannel = channel;
    _delegate = delegate;
//    [self.imageView setImageWithURL:[NSURL URLWithString: channel.channelThumbnail] placeholderImage:[Utility placeHolderImage]];
//    
//    self.topLabel.text = channel.channelName;
//    self.favButton.selected = [UserManager isChannelFavouriteAlready:channel.channelId];
}

- (IBAction)favouriteAction:(id)sender {
    self.favButton.selected = !self.favButton.selected;
    if (_userChannel) {
        [[UserManager sharedManager] setUserChannel:_userChannel asFavourite:self.favButton.selected];
    } else {
        [[UserManager sharedManager] setChannel:_channel asFavourite:self.favButton.selected];
    }
}

- (IBAction)selectAction:(id)sender {
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(channelView:selectedChannel:)]) {
            if (_userChannel) {
                [self.delegate channelView:self selectedUserChannel:_userChannel];
            } else {
                [self.delegate channelView:self selectedChannel:_channel];
            }
        }
    }
}

@end
