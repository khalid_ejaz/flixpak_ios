//
//  ContentViewCell.m
//  PakTV
//
//  Created by Malik Khalid Ejaz on 10/24/2013.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import "ContentViewCell.h"
#import "DataManager.h"
#import <UIImageView+AFNetworking.h>
#import "Utility.h"

@interface ContentViewCell()

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *contentNameLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *contentImageView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *favButton;

@property (nonatomic, strong) Content *content;

@end

@implementation ContentViewCell

//- (void)setSelected:(BOOL)selected animated:(BOOL)animated
//{
//    [super setSelected:selected animated:animated];
//    if (selected) {
//        self.backgroundColor = PLAIN_COLOR_ALMOST_BLACK;
//    } else self.backgroundColor = PLAIN_COLOR_DARKER_BLACK;
//}

-(void) populateWithContent:(Content *)content {
    _content = content;
//    self.contentNameLabel.text = content.name;
//    [self.contentImageView setImageWithURL:[NSURL URLWithString:content.imageURL]];
//    self.favButton.selected = [content isFavourite];
}
- (IBAction)toggleFav:(id)sender {
//    if (self.favButton.selected) {
//        [self.content saveAsNotFavourite];
//    } else [self.content saveAsFavourite];
//    self.favButton.selected = !self.favButton.selected;
//    
//    if (self.favButton.selected) {
//        [self.delegate addParentCategoryAsFavourite];
//    }
    
//    NSNotification *notification = [NSNotification notificationWithName:NOTIFICATION_CHANGED_PERSONALISED_CONTENT object:nil];
//    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

@end
