//
//  ChannelViewCell.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/1/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "ChannelViewCell.h"
#import "Utility.h"
#import <UIImageView+AFNetworking.h>
#import "UserChannel.h"

@interface ChannelViewCell()

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *channelNameLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *channelImgView;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *channelDescLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *chevronLabel;

@property (nonatomic, strong) UserChannel *channel;

@end

@implementation ChannelViewCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    if (selected) {
        self.backgroundColor = PLAIN_COLOR_ALMOST_BLACK;
    } else self.backgroundColor = PLAIN_COLOR_DARKER_BLACK;
}

-(void) populateWithChannel:(UserChannel *)channel {
    [self.channelImgView setImageWithURL:[NSURL URLWithString:channel.channelThumbnail] placeholderImage:[Utility placeHolderImage]];
    self.channelNameLabel.text = channel.channelName;
    self.channelDescLabel.text = channel.channelDesc;
}

-(void) hideChevronLabel {
    self.chevronLabel.hidden = YES;
}

@end
