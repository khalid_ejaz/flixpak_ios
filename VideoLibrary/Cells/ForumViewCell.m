//
//  ForumViewCell.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/13/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "ForumViewCell.h"
#import "Utility.h"

@interface ForumViewCell()

@property (nonatomic, strong) Forum *forum;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *forumNameLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *favButton;

@end

@implementation ForumViewCell

-(void) populateWithForum:(Forum *)forum {
//    _forum = forum;
//    self.forumNameLabel.text = forum.forumName;
//    
//    self.backgroundColor = GRAY_WITH_PERCENT(20);
//    self.selectionStyle = UITableViewCellSelectionStyleNone;
//    self.forumNameLabel.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:16];
//    self.forumNameLabel.textColor = GRAY_WITH_PERCENT(80);
//    self.favButton.selected = [forum isFavourite];
}

- (IBAction)favAction:(id)sender {
//    self.favButton.selected = !self.favButton.selected;
//    if (self.favButton.selected) {
//        [self.forum saveAsFavourite];
//    } else [self.forum saveAsNotFavourite];
//    
//    NSNotification *notification = [NSNotification notificationWithName:NOTIFICATION_TOGGLE_FAV_ON_FORUM object:nil userInfo:@{@"forum":self.forum,@"selected":self.favButton.selected?@"yes":@"no"}];
//    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

@end
