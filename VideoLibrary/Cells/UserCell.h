//
//  UserCell.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/29/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UserCellDelegate <NSObject>

-(void) userCell:(id)cell requestedColorChangeToColor:(UIColor *)color;
-(void) userCellRequestedResetHeaderColor:(id)cell;

@end

@interface UserCell : UITableViewCell

@property (nonatomic, unsafe_unretained) id<UserCellDelegate> delegate;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *userNameLabel;

-(void) setupWithName:(NSString *)name color:(UIColor *)color;

@end
