//
//  LoadMoreCell.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/19/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "LoadMoreCell.h"

@interface LoadMoreCell()


@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *shadowImageView;

@end

@implementation LoadMoreCell

//- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
//{
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//    if (self) {
//        // Initialization code
//    }
//    return self;
//}
//
//- (void)setSelected:(BOOL)selected animated:(BOOL)animated
//{
//    [super setSelected:selected animated:animated];
//}

-(void) reset {
    self.loadMoreButton.hidden = NO;
    [self.activityIndicator stopAnimating];
}

- (IBAction)showActivity:(id)sender {
    [self.activityIndicator startAnimating];
    self.loadMoreButton.hidden = YES;
}

-(void) stopAnimatingWhenFailed {
    [self.activityIndicator stopAnimating];
    self.loadMoreButton.hidden = NO;
}

@end
