//
//  CategoryViewCell.h
//  PakTV
//
//  Created by Malik Khalid Ejaz on 10/24/2013.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PSTCollectionView.h>

@class ContentCategory;

@interface CategoryViewCell : PSTCollectionViewCell

@property (nonatomic, assign) BOOL showingInMenu;

-(void) populateWithCategory:(ContentCategory *)category;

-(void) hideChevronLabel;

@end
