//
//  YCSearchResultItemCell.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/30/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "YCSearchResultItemCell.h"
#import "YCSearchResultItem.h"
#import <UIImageView+AFNetworking.h>
#import "Utility.h"
#import "UserManager.h"
#import "ME_PrefsManager.h"

@interface YCSearchResultItemCell()
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *selectButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *favButton;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *nameLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *publishedAtLabel;

@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *channelImageView;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *descriptionLabel;


@property (nonatomic, strong) YCSearchResultItem *resultItem;

@end

@implementation YCSearchResultItemCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    if (selected) {
        self.contentView.backgroundColor = GRAY_WITH_PERCENT(15);
    } else self.contentView.backgroundColor = GRAY_WITH_PERCENT(20);
}

-(void) populateWithResultItem:(YCSearchResultItem *)resultItem {
    _resultItem = resultItem;
    [self.channelImageView setImageWithURL:[NSURL URLWithString:resultItem.thumbnail] placeholderImage:[Utility placeHolderImage]];
    [self.nameLabel setText:resultItem.channelTitle];
    [self.descriptionLabel setText:resultItem.channelDescription];
    
    self.publishedAtLabel.text = [NSString stringWithFormat:@"Published: %@", [Utility publishedAtStringFromString:resultItem.publishedAt]];
    
    self.favButton.selected = NO;
    NSMutableArray *favs = [[[ME_PrefsManager singleton] arrayPrefsForKey:@"fav-channels"] mutableCopy];
    for (NSDictionary *dict in favs) {
        if ([dict[@"channelId"] isEqualToString:self.resultItem.channelId]) {
            self.favButton.selected = YES;
        }
    }
}

- (IBAction)toggleFav:(id)sender {
    self.favButton.selected = !self.favButton.selected;
    
    NSMutableArray *favs = [[[ME_PrefsManager singleton] arrayPrefsForKey:@"fav-channels"] mutableCopy];
    for (NSDictionary *dict in favs) {
        if ([dict[@"channelId"] isEqualToString:self.resultItem.channelId]) {
            [favs removeObject:dict];
            self.favButton.selected = NO;
            [[ME_PrefsManager singleton] saveArrayPrefs:favs forKey:@"fav-channels"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"reload-tabs" object:nil];
            return;
        }
    }
    
    // not already added. add it now.
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.resultItem.channelId forKey:@"channelId"];
    [dict setObject:self.resultItem.channelTitle forKey:@"channelName"];
    [favs addObject:dict];
    [[ME_PrefsManager singleton] saveArrayPrefs:favs forKey:@"fav-channels"];
    self.favButton.selected = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reload-tabs" object:nil];
    
//    UserChannel *channel= [UserChannel  object];
//    channel.channelId = self.resultItem.channelId;
//    channel.channelName = self.resultItem.channelTitle;
//    channel.channelDesc = self.resultItem.channelDescription;
//    channel.channelThumbnail = self.resultItem.thumbnail;
//    [[UserManager sharedManager] setUserChannel:channel asFavourite:self.favButton.selected];
}

@end
