//
//  LargeYoutubeVideoCell.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/1/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "LargeYoutubeVideoCell.h"
#import "YoutubeVideo.h"
#import <UIImageView+AFNetworking.h>
#import "Utility.h"

@interface LargeYoutubeVideoCell()

@property (unsafe_unretained, nonatomic) IBOutlet UIButton *watchLaterButton;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *watchLaterLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *durationLabel;

@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *imgView1;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *imgView2;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *imgView3;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *titleLabel;

@property (nonatomic, strong) YoutubeVideo *video;

@end

@implementation LargeYoutubeVideoCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void) populateWithYoutubeVideo:(YoutubeVideo *)video {
    _video = video;
    [self.imgView1 setImageWithURL:video.thumbnail1 placeholderImage:[Utility placeHolderImage]];
    [self.imgView2 setImageWithURL:video.thumbnail2 placeholderImage:[Utility placeHolderImage]];
    [self.imgView3 setImageWithURL:video.thumbnail3 placeholderImage:[Utility placeHolderImage]];
    [self.durationLabel setText:video.duration];
    [self.titleLabel setText:[NSString stringWithFormat:@"%@: %@",video.publishedAt, video.title]];
    [self.titleLabel setTextColor:GRAY_WITH_PERCENT(70)];
    [self.titleLabel setTextAlignment:NSTextAlignmentCenter];
}

- (IBAction)watchLaterAction:(id)sender {
}

- (IBAction)playAction:(id)sender {
    NSNotification *notif = [NSNotification notificationWithName:NOTIFICATION_PLAY_VIDEO object:nil userInfo:@{@"video":self.video}];
    [[NSNotificationCenter defaultCenter] postNotification:notif];
}

- (IBAction)previewAction:(id)sender {
    
    NSArray *array = @[self.video.hqThumbnailURL, self.video.thumbnail1, self.video.thumbnail3];
    
    NSNotification *notif = [NSNotification notificationWithName:NOTIFICATION_SHOW_PREVIEW_IMAGES object:nil userInfo:@{@"urls":array, @"title":self.video.title}];
    [[NSNotificationCenter defaultCenter] postNotification:notif];
}

@end

