//
//  LargeYoutubeVideoCell.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/1/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YoutubeVideo;

@interface LargeYoutubeVideoCell : UITableViewCell

-(void) populateWithYoutubeVideo:(YoutubeVideo *)video;

@end
