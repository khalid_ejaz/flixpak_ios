//
//  VideoItemCell.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/1/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "VideoItemCell.h"
#import "YoutubeVideo.h"
#import "Utility.h"
#import <UIImageView+AFNetworking.h>
#import "UserManager.h"
#import "StyleManager.h"

#define VIDEO_TILE_BORDER_WIDTH_IPHONE     0
#define VIDEO_TILE_BORDER_WIDTH_IPAD       8

@interface VideoItemCell()

@property (unsafe_unretained, nonatomic) IBOutlet UIButton *channelButton;

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *playBackLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *playButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIScrollView *scrollView;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *episodeImageView;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *thumbView1;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *thumbView2;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *thumbView3;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *playerControlView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *previewButton;
@property (weak, nonatomic) IBOutlet UIImageView *episodeOverlayView;
@property (weak, nonatomic) IBOutlet UIImageView *videoOverlayView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

// option buttons.
@property (weak, nonatomic) IBOutlet UIView *optionsView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *favButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *watchLaterButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *selectContentButton;

// episode related.
@property (unsafe_unretained, nonatomic) IBOutlet UIView *episodeDetailView;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *episodeNameLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *episodeRatingLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *episodeTitleLabel;

// video related.
@property (unsafe_unretained, nonatomic) IBOutlet UIView *videoDetailView;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *durationLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *videoTitleLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *publishedAtLabel;

// cell data related.
@property (nonatomic, strong) Episode *episode;
@property (nonatomic, strong) YoutubeVideo *video;

@property (nonatomic, assign) BOOL episodeImageFailed;
@property (nonatomic, assign) BOOL enableContentButton;
@property (nonatomic, assign) BOOL showEpisodeImage;
@property (nonatomic, assign) BOOL scrollSetupDone;

@property (nonatomic, retain) NSURL *url1;
@property (nonatomic, retain) NSURL *url2;
@property (nonatomic, retain) NSURL *url3;

@end

@implementation VideoItemCell

//- (void)setSelected:(BOOL)selected animated:(BOOL)animated
//{
//    [super setSelected:selected animated:animated];
//}

#pragma mark - Data load/populate

-(void) populateWithEpisode:(Episode *)episode contentButtonEnabled:(BOOL)enabled showContentImage:(BOOL)showEpisodeImage {
//    _episode = episode;
//    _video = nil;
//    _scrollSetupDone = NO;
//    
//    [[StyleManager sharedManager] styleHeaderButton:self.channelButton];
//    [[StyleManager sharedManager] styleHeaderButton:self.selectContentButton];
//    [[StyleManager sharedManager] styleHeaderButton:self.watchLaterButton];
//    [[StyleManager sharedManager] styleHeaderButton:self.favButton];
//    
//    self.favButton.hidden = NO;
//    self.favButton.selected = [Content isFavouriteByContentId:episode.contentId];
//    
//    self.videoDetailView.hidden = YES;
//    self.episodeDetailView.hidden = NO;
//    self.durationLabel.hidden = YES;
//    self.publishedAtLabel.hidden = YES;
//    
//    _enableContentButton = enabled;
//    self.selectContentButton.hidden = !enabled;
//    self.selectContentButton.enabled = enabled;
//    
//    _showEpisodeImage = YES;
//    
//    
//    NSString *youtubeEmbedLink = [[DataManager sharedManager] getYoutubeEmbedLinkFromVID:[episode.ytlinks objectAtIndex:0]];
//    NSURL *url1 = [[DataManager sharedManager] getPreviewMediumQualityImageURLOfYoutubeVideoId:[episode.ytlinks objectAtIndex:0]];
//    NSURL *url2, *url3;
//        url2 = [[DataManager sharedManager] getPreviewImageURLOfYoutubeVideo:youtubeEmbedLink number:2];
//        url3 = [[DataManager sharedManager] getPreviewImageURLOfYoutubeVideo:youtubeEmbedLink number:3];
//    
//    if (![_url1.absoluteString isEqualToString:url1.absoluteString] || self.episodeImageView.image == nil) {
//        [self.episodeImageView setImageWithURL:url1 placeholderImage:[Utility placeHolderImage]];
//    }
//    if (![_url1.absoluteString isEqualToString:url1.absoluteString] || self.thumbView1.image == nil) {
//        [self.thumbView1 setImageWithURL:url1 placeholderImage:[Utility placeHolderImage]];
//    }
//    if (![_url2.absoluteString isEqualToString:url2.absoluteString] || self.thumbView2.image == nil) {
//        [self.thumbView2 setImageWithURL:url2 placeholderImage:[Utility placeHolderImage]];
//    }
//    if (![_url3.absoluteString isEqualToString:url3.absoluteString] || self.thumbView3.image == nil) {
//        [self.thumbView3 setImageWithURL:url3 placeholderImage:[Utility placeHolderImage]];
//    }
//
//    
//    if (episode.detail) {
//        self.episodeTitleLabel.text = [NSString stringWithFormat:@"%@: %@",episode.contentName, episode.detail];
//    } else self.episodeNameLabel.text = episode.contentName;
//    
//    int avgRatings = 0;
//    if ([self.episode.totalRatings integerValue] > 0) {
//        avgRatings = [self.episode.ratingPoints integerValue]/[self.episode.totalRatings integerValue];
//    }
//    self.episodeRatingLabel.text = [Utility ratingStarsStringForRatings:avgRatings];
//    self.episodeTitleLabel.text = [[DataManager sharedManager] shortfriendlyDateFromString:self.episode.title];
//    
//    self.watchLaterButton.selected = [self.episode isFavourite];
//
//    [self setupScroll];
//    [self resetAlphaOnDetailView];
}

-(void) populateWithVideo:(YoutubeVideo *)video showChannelTitleButton:(BOOL)showChannelButton {
    _video = video;
    _episode = nil;
    _scrollSetupDone = NO;
    
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    self.channelButton.hidden = !showChannelButton;
    self.favButton.hidden = YES;
    self.selectContentButton.hidden = YES;
    self.episodeDetailView.hidden = YES;
    self.videoDetailView.hidden = NO;
    self.durationLabel.hidden = NO;
    self.publishedAtLabel.hidden = NO;
    
    [self.channelButton setTitle:video.channelTitle forState:UIControlStateNormal];
    self.publishedAtLabel.text = video.publishedAt;
    
//    if (IS_IPAD) {
        [self.episodeImageView setImageWithURL:self.video.hqThumbnailURL placeholderImage:[Utility placeHolderImage]];
//    } else {
//        self.episodeImageView.hidden = YES;
//    }
    
    [self.thumbView1 setImageWithURL:video.hqThumbnailURL placeholderImage:[Utility placeHolderImage]];
    [self.thumbView2 setImageWithURL:video.thumbnail3 placeholderImage:[Utility placeHolderImage]];
    [self.thumbView3 setImageWithURL:video.thumbnail2 placeholderImage:[Utility placeHolderImage]];
    
    [self.durationLabel setText:video.duration];
    self.videoTitleLabel.text = video.title;
    
//    self.watchLaterButton.selected = [self.video isFavourite];
    
    [self setupScroll];
}

-(void) setEpisodeImageWithURL:(NSURL *)url onImageView:(UIImageView *)imageView {
//    __block UIImageView *imgView = imageView;
//    [imageView setImageWithURLRequest:[NSURLRequest requestWithURL:url] placeholderImage:[Utility placeHolderImage] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
//        if (image) {
//            imgView.image = image;
//            [self setupScroll];
//        }
//    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
//        self.episodeImageFailed = YES;
//        if (IS_IPAD) {
            [self showMediumQualityThumnail];
//        }
        [self setupScroll];
//    }];
}

-(void) showMediumQualityThumnail {
//    NSURL *mediumThumbnailURL = [[DataManager sharedManager] getPreviewMediumQualityImageURLOfYoutubeVideoId:[self.episode.ytlinks objectAtIndex:0]];
//    [self.episodeImageView setImageWithURL:mediumThumbnailURL placeholderImage:[Utility placeHolderImage]];
}

-(void) layoutSubviews {
    [super layoutSubviews];
    [self layoutPlayerControlAndSearchButton];
}

-(void) setupScroll {
    
//    CGFloat borderWidth = IS_IPAD?VIDEO_TILE_BORDER_WIDTH_IPAD:VIDEO_TILE_BORDER_WIDTH_IPHONE;
//    
//    CGFloat width = [self imageWidth] - 2 * borderWidth;
//    CGFloat height = [self imageHeight] - 2 * borderWidth;
//    self.scrollView.frame = CGRectMake(borderWidth, borderWidth, width, height);
//    [self.scrollView setContentOffset:CGPointMake(width, 0)];
//    [self.pageControl setCurrentPage:1];
//    if (_scrollSetupDone) {
//        return;
//    }
//    _scrollSetupDone = YES;
//    self.optionsView.backgroundColor = [[UserManager sharedManager] colorForUser];
//    
//    [self.playButton setTitleColor:[[UserManager sharedManager] lighterColorForUser] forState:UIControlStateHighlighted];
//    
//    CGFloat detailWidth = [self detailViewWidth];
//    CGFloat x = width;    // hardcoded size of options view in video item.
//    
//    if (self.video) {
//        
//        self.videoDetailView.hidden = NO;
//        [self.videoDetailView setFrame:self.bounds];
//        
//        
//        CGRect frame = CGRectMake(x, 0, width, height);
//            self.episodeImageView.frame = frame;
//            x += width;
//            frame.origin.x = x;
//        
//        self.thumbView1.frame = frame;
//        
//        x += width;
//        frame.origin.x = x;
//        self.thumbView2.frame = frame;
//        
//        x += width;
//        frame.origin.x = x;
//        self.thumbView3.frame = frame;
//        
//        CGRect previewButtonFrame = self.thumbView1.frame;
//        previewButtonFrame.size.width *= 3;
//        self.previewButton.frame = previewButtonFrame;
//        
//        x += width;
//        [self.scrollView setContentSize:CGSizeMake(x, height)];
//        
//        [self layoutPlayerControlAndSearchButton];
//        
//    } else if (self.episode) {
//        
//        [self.episodeDetailView setFrame:self.bounds];
//        
//            self.episodeImageView.hidden = NO;
//        
//        CGRect frame = CGRectMake(x, 0, width, height);
//        if (!self.episodeImageView.hidden) {
//            self.episodeImageView.frame = frame;
//            x += width;
//            frame.origin.x = x;
//        }
//        
//        self.thumbView1.frame = frame;
//        
//        x += width;
//        frame.origin.x = x;
//        self.thumbView2.frame = frame;
//        
//        x += width;
//        frame.origin.x = x;
//        self.thumbView3.frame = frame;
//        
//        CGRect previewButtonFrame = self.thumbView1.frame;
//        previewButtonFrame.size.width *= 3;
//        self.previewButton.frame = previewButtonFrame;
//        
//        x += width;
//        [self.scrollView setContentSize:CGSizeMake(x, height)];
//        
//        [self layoutPlayerControlAndSearchButton];
//    }
//    [self.pageControl setCurrentPage:1];
}

-(void) layoutPlayerControlAndSearchButton {
    if (IS_IPAD) {
        self.playBackLabel.font = [UIFont fontWithName:@"Helvetica" size:120];
        [self.playButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:28]];
        CGPoint center = self.playBackLabel.center;
        self.playButton.center = CGPointMake(center.x+2, center.y+5);
    }
    if (self.episodeImageView.hidden) {
        self.playerControlView.center = self.thumbView1.center;
    } else {
        self.playerControlView.center = self.episodeImageView.center;

    }
    
    CGRect previewButtonFrame = self.thumbView1.frame;
    previewButtonFrame.size.width *= 3;
    self.previewButton.frame = previewButtonFrame;
}

-(CGFloat) detailViewWidth {
    return [self imageWidth];
}

-(CGFloat) imageWidth {
    return [VideoItemCell sizeForVideoItem].width;
}

-(CGFloat) imageHeight {
    return [VideoItemCell sizeForVideoItem].height;
}

+(CGFloat)marginForVideoItem {
    if (IS_IPAD) {
        return VIDEO_TILE_MARGIN * 2;
    } return VIDEO_TILE_MARGIN;
}

+(CGSize)sizeForVideoItem {
    int columns = 0;
    int screenWidth = SCREEN_WIDTH;
    
    if (IS_IPAD) {
        if (IS_PORTRAIT) {
            columns = VIDEO_COLUMNS_FOR_IPAD_PORTRAIT;
        } else {
            columns = VIDEO_COLUMNS_FOR_IPAD_LANDSCAPE;
        }
    } else {
        if (IS_PORTRAIT) {
            columns = VIDEO_COLUMNS_FOR_IPHONE_PORTRAIT;
        } else {
            columns = VIDEO_COLUMNS_FOR_IPHONE_LANDSCAPE;
        }
    }
    
    int width = 0;
    int height = 0;
    
    if (columns == 1) {
        width = screenWidth;
    } else if (columns > 1) {
        width = (screenWidth - ((columns + 1) * [VideoItemCell marginForVideoItem]))/columns;
    }
    height = width/2;
    return CGSizeMake(width, height);
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.x <= self.episodeImageView.frame.origin.x) {
        [UIView animateWithDuration:0.3 animations:^{
            [self resetAlphaOnDetailView];
        }];
    } else {
        [UIView animateWithDuration:0.3 animations:^{
            self.episodeDetailView.alpha = self.videoDetailView.alpha = 0.1f;
        }];
    }
}

- (void)resetAlphaOnDetailView {
    if (self.video) {
        self.videoDetailView.alpha = 1.0f;
    } else {
        self.episodeDetailView.alpha = 1.0f;
    }
}

#pragma mark - IBActions

- (IBAction)toggleFavAction:(id)sender {
//    self.favButton.selected = !self.favButton.selected;
//    if (self.favButton.selected) {
//        [[UserManager sharedManager] addContentAsFavouriteById:self.episode.contentId];
//    } else {
//        [Content saveAsNotFavouriteByContentId:self.episode.contentId];
//    }
}

- (IBAction)selectChannelAction:(id)sender {
    NSString *channelId = self.video.channelId;
    NSString *channelTitle = self.video.channelTitle;
    
    if (!channelTitle) {
        channelTitle = @"-";
    }
    
    if (channelId) {
        NSNotification *notif = [NSNotification notificationWithName:@"NOTIFICATION_SHOW_CHANNEL" object:nil userInfo:@{@"channelId":channelId, @"channelTitle":channelTitle}];
        [[NSNotificationCenter defaultCenter] postNotification:notif];
    }
}

- (IBAction)selectContentAction:(id)sender {
    if (self.episode) {
        NSNotification *notif = [NSNotification notificationWithName:@"NOTIFICATION_SHOW_ALL_EPISODES" object:nil userInfo:@{@"Episode":self.episode}];
        [[NSNotificationCenter defaultCenter] postNotification:notif];
    }
}

- (IBAction)watchLaterAction:(id)sender {
//    if (self.episode) {
//        if (self.watchLaterButton.selected) {
//            [self.episode saveAsNotFavourite];
//        } else {
//            [self.episode saveAsFavourite];
//            [self sendNotificationForShowingTipForWatchLater];
//        }
//    } else if (self.video) {
//        // TODO: tag/untag videos.
//        if (self.watchLaterButton.selected) {
//            [self.video saveAsNotFavourite];
//        } else {
//            [self.video saveAsFavourite];
//            [self sendNotificationForShowingTipForWatchLater];
//        }
//    }
//    
//    self.watchLaterButton.selected = !self.watchLaterButton.selected;
}

-(void) sendNotificationForShowingTipForWatchLater {
    NSNotification *notif = [NSNotification notificationWithName:@"NOTIFICATION_ADDED_TO_WATCH_LATER" object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotification:notif];
}

- (IBAction)showPreview {
//    NSString *youtubeEmbedLink = [[DataManager sharedManager] getYoutubeEmbedLinkFromVID:[self.episode.ytlinks objectAtIndex:0]];
//    
//    NSURL *url1 = [[DataManager sharedManager] getPreviewImageURLOfYoutubeVideo:youtubeEmbedLink number:0];
//    NSURL *url2 = [[DataManager sharedManager] getPreviewImageURLOfYoutubeVideo:youtubeEmbedLink number:1];
//    NSURL *url3 = [[DataManager sharedManager] getPreviewImageURLOfYoutubeVideo:youtubeEmbedLink number:3];
//    
//    NSArray *array = @[url1, url2, url3];
//    
//    NSDictionary *userInfo;
//    if (self.episode) {
//        userInfo = @{@"urls":array, @"title":self.episode.title, @"name":self.episode.contentName};
//    } else if (self.video) {
//        userInfo = @{@"urls":@[self.video.hqThumbnailURL, self.video.thumbnail1, self.video.thumbnail3], @"title":self.video.title};
//    }
//    
//    if (userInfo) {
//        NSNotification *notif = [NSNotification notificationWithName:NOTIFICATION_SHOW_PREVIEW_IMAGES object:nil userInfo:userInfo];
//        [[NSNotificationCenter defaultCenter] postNotification:notif];
//    }
}

- (IBAction)playAction:(id)sender {
    if (self.episode) {
        NSNotification *notif = [NSNotification notificationWithName:@"NOTIFICATION_PLAY_EPISODE" object:nil userInfo:@{@"Episode":self.episode}];
        [[NSNotificationCenter defaultCenter] postNotification:notif];
    } else if (self.video) {
        NSNotification *notif = [NSNotification notificationWithName:NOTIFICATION_PLAY_VIDEO object:nil userInfo:@{@"video":self.video}];
        [[NSNotificationCenter defaultCenter] postNotification:notif];
    }
}

-(void) hideFavButton {
    self.favButton.hidden = YES;
}

#pragma mark -
#pragma mark - UIScrollViewDelegate and related methods.

-(void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    if (targetContentOffset->x == 0) {
        [self.pageControl setCurrentPage:0];
    } else if (targetContentOffset->x == self.episodeImageView.frame.origin.x) {
        [self.pageControl setCurrentPage:1];
    } else if (targetContentOffset->x == self.thumbView1.frame.origin.x) {
        [self.pageControl setCurrentPage:2];
    } else if (targetContentOffset->x == self.thumbView2.frame.origin.x) {
        [self.pageControl setCurrentPage:3];
    } else if (targetContentOffset->x == self.thumbView3.frame.origin.x) {
        [self.pageControl setCurrentPage:4];
    }
}


// enabled paging on scroll view so this is not needed.


//
//-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
//    
//    if (decelerate) {
//        return;
//    }
//    [self snapScrollView:scrollView];
//}
//
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
//    [self snapScrollView:scrollView];
//}
//
//-(void)snapScrollView:(UIScrollView *)scrollView {
//    if (scrollView.contentOffset.x > self.optionsView.bounds.size.width/2 && scrollView.contentOffset.x < self.optionsView.bounds.size.width) {
//        [scrollView setContentOffset:CGPointMake(self.optionsView.bounds.size.width, 0) animated:YES];
//    } else if (scrollView.contentOffset.x <= self.optionsView.bounds.size.width/2 && scrollView.contentOffset.x > 0) {
//        [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
//    }
//}

@end
