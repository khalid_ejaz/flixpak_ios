//
//  YCSearchResultItemCell.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/30/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YCSearchResultItem;

@interface YCSearchResultItemCell : UITableViewCell

-(void) populateWithResultItem:(YCSearchResultItem *)resultItem;

@end
