//
//  YoutubeVideoCell.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/1/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "YoutubeVideoCell.h"
#import "YoutubeVideo.h"
#import <UIImageView+AFNetworking.h>
#import "Utility.h"

@interface YoutubeVideoCell()


@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *imgView1;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *imgView2;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *imgView3;
@property (unsafe_unretained, nonatomic) IBOutlet UITextView *titleView;

@property (nonatomic, strong) YoutubeVideo *video;

@end

@implementation YoutubeVideoCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void) populateWithYoutubeVideo:(YoutubeVideo *)video {
    _video = video;
    [self.imgView1 setImageWithURL:video.thumbnail1 placeholderImage:[Utility placeHolderImage]];
    [self.imgView2 setImageWithURL:video.thumbnail2 placeholderImage:[Utility placeHolderImage]];
    [self.imgView3 setImageWithURL:video.thumbnail3 placeholderImage:[Utility placeHolderImage]];
    
    [self.titleView setText:[NSString stringWithFormat:@"%@: %@",video.publishedAt, video.title]];
    [self.titleView setTextColor:GRAY_WITH_PERCENT(70)];
    [self.titleView setTextAlignment:NSTextAlignmentCenter];
}

- (IBAction)playAction:(id)sender {
    NSNotification *notif = [NSNotification notificationWithName:NOTIFICATION_PLAY_VIDEO object:nil userInfo:@{@"video":self.video}];
    [[NSNotificationCenter defaultCenter] postNotification:notif];
}

- (IBAction)previewAction:(id)sender {
    
    NSArray *array = @[self.video.hqThumbnailURL, self.video.thumbnail1, self.video.thumbnail3];
    
    NSNotification *notif = [NSNotification notificationWithName:NOTIFICATION_SHOW_PREVIEW_IMAGES object:nil userInfo:@{@"urls":array, @"title":self.video.title}];
    [[NSNotificationCenter defaultCenter] postNotification:notif];
}

@end
