//
//  VideoItemCell.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/1/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PSTCollectionView.h>

#define VIDEO_COLUMNS_FOR_IPHONE_PORTRAIT       1
#define VIDEO_COLUMNS_FOR_IPHONE_LANDSCAPE      2

#define VIDEO_COLUMNS_FOR_IPAD_PORTRAIT         2
#define VIDEO_COLUMNS_FOR_IPAD_LANDSCAPE        3

@class Episode, YoutubeVideo;

@interface VideoItemCell : PSTCollectionViewCell <UIScrollViewDelegate>

-(void) populateWithEpisode:(Episode *)episode contentButtonEnabled:(BOOL)enabled showContentImage:(BOOL)showEpisodeImage;

-(void) populateWithVideo:(YoutubeVideo *)video showChannelTitleButton:(BOOL)showChannelButton;

-(void) hideFavButton;

+(CGSize)sizeForVideoItem;
+(CGFloat)marginForVideoItem;

@end
