//
//  UserCell.m
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/29/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import "UserCell.h"
#import "Utility.h"
#import "UserManager.h"

@interface UserCell()

@property (unsafe_unretained, nonatomic) IBOutlet UIButton *upButton;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *userColorLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *colorsView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *but1;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *but2;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *but3;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *but4;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *but5;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *but6;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *but7;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *but8;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *but9;

@property (nonatomic, assign) BOOL buttonsReady;

@end

@implementation UserCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    self.backgroundColor = highlighted?GRAY_WITH_PERCENT(33):GRAY_WITH_PERCENT(70);
}

-(void) setupWithName:(NSString *)name color:(UIColor *)color {
    self.userColorLabel.textColor = color;
    self.userNameLabel.text = name;
}

- (IBAction)showColorsAction:(id)sender {
    CGRect frame = self.colorsView.frame;
    frame.origin.y = 0 - self.bounds.size.height;
    self.colorsView.frame = frame;
    self.colorsView.hidden = NO;
    frame.origin.y = 0;
    [UIView animateWithDuration:0.3 animations:^{
        self.colorsView.frame = frame;
    }];
    [self.upButton setTitleColor:self.userColorLabel.textColor forState:UIControlStateNormal];
    [self setupButtons];
}

-(void) setupButtons {
    if (self.buttonsReady) {
        return;
    } else {
        NSArray *buttons = [self colorButtons];
        int count = 1;
        for (UIButton *button in buttons) {
            button.tag = count;
            [button addTarget:self action:@selector(chooseColor:) forControlEvents:UIControlEventTouchUpInside];
            [button setTitleColor:[[UserManager sharedManager] colorForNumber:count] forState:UIControlStateNormal];
            count ++;
        }
        self.buttonsReady = YES;
    }
}

-(void) chooseColor:(id)sender {
    int tag = [(UIButton *)sender tag];
    UIColor *color = [[UserManager sharedManager] colorForNumber:tag];
    [[UserManager sharedManager] changeColorForUser:self.userNameLabel.text toColor:color];
    self.userColorLabel.textColor = color;
    [self.delegate userCell:self requestedColorChangeToColor:color];
}

- (IBAction)hideColorsView:(id)sender {
    CGRect frame = self.colorsView.frame;
    frame.origin.y = 0;
    self.colorsView.frame = frame;
    frame.origin.y = 0 - self.bounds.size.height;
    [UIView animateWithDuration:0.3 animations:^{
        self.colorsView.frame = frame;
    } completion:^(BOOL finished) {
        self.colorsView.hidden = YES;
        [self.delegate userCellRequestedResetHeaderColor:self];
    }];
}

-(NSArray *) colorButtons {
    return @[self.but1, self.but2, self.but3, self.but4, self.but5, self.but6, self.but7, self.but8, self.but9];
}

@end
