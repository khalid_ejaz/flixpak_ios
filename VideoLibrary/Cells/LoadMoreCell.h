//
//  LoadMoreCell.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/19/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PSTCollectionView.h>

@interface LoadMoreCell : PSTCollectionViewCell

@property (unsafe_unretained, nonatomic) IBOutlet UIButton *loadMoreButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

-(void) stopAnimatingWhenFailed;
-(void) reset;

@end
