//
//  ForumViewCell.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 12/13/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Forum;

@interface ForumViewCell : UITableViewCell

-(void) populateWithForum:(Forum *)forum;

@end
