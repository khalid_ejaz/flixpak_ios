//
//  ContentViewCell.h
//  PakTV
//
//  Created by Malik Khalid Ejaz on 10/24/2013.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PSTCollectionView.h>

@class Content;

@protocol ContentViewCellDelegate <NSObject>

-(void) addParentCategoryAsFavourite;

@end

@interface ContentViewCell : PSTCollectionViewCell

@property (nonatomic, unsafe_unretained) id<ContentViewCellDelegate> delegate;

-(void) populateWithContent:(Content *)content;

@end
