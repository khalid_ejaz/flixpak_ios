//
//  SideMenuCell.h
//  PakTV
//
//  Created by Malik Khalid Ejaz on 2013-09-24.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuCell : UITableViewCell

@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *menuColorView;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *menuBackgroundView;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *menuArtView;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *menuTextLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *symbolLabel;

@end
