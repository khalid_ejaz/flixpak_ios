//
//  CategoryViewCell.m
//  PakTV
//
//  Created by Malik Khalid Ejaz on 10/24/2013.
//  Copyright (c) 2013 A & N Maple Inc. All rights reserved.
//

#import "CategoryViewCell.h"
#import "DataManager.h"
#import "Utility.h"

@interface CategoryViewCell()

@property (unsafe_unretained, nonatomic) IBOutlet UIButton *exploreButton;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *chevronLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *categoryColorView;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *categoryNameLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *favButton;

@property (nonatomic, strong) ContentCategory *contentCategory;

@end

@implementation CategoryViewCell

//- (void)setSelected:(BOOL)selected animated:(BOOL)animated
//{
//    [super setSelected:selected animated:animated];
//    if (selected && !self.chevronLabel.hidden) {
//        self.backgroundColor = PLAIN_COLOR_ALMOST_BLACK;
//    } else self.backgroundColor = PLAIN_COLOR_DARKER_BLACK;
//    // Configure the view for the selected state
//}

-(void) populateWithCategory:(ContentCategory *)category {
//    _contentCategory = category;
//
//    self.categoryNameLabel.text = category.categoryName;
//    
//    self.favButton.selected = [category isFavourite];
//    [self.favButton setHidden:[category.contents isEqualToNumber:[NSNumber numberWithInteger:0]]?YES:NO];
}

- (IBAction)exploreAction:(id)sender {
    NSNotification *notification = [NSNotification notificationWithName:NOTIFICATION_EXPLORE_SELECTED_FROM_CAT_CELL object:nil userInfo:@{@"category":self.contentCategory}];
    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

-(void) hideChevronLabel {
    self.chevronLabel.hidden = YES;
    self.exploreButton.hidden = NO;
}

- (IBAction)toggleFav:(id)sender {
//    if (self.favButton.selected) {
//        [self.contentCategory saveAsNotFavourite];
//    } else [self.contentCategory saveAsFavourite];
//    self.favButton.selected = !self.favButton.selected;
//    
//        NSNotification *notification = [NSNotification notificationWithName:NOTIFICATION_CHANGED_FAVS_FROM_SIDE_MENU object:nil userInfo:@{@"category":self.contentCategory,@"selected":self.favButton.selected?@"yes":@"no"}];
//        [[NSNotificationCenter defaultCenter] postNotification:notification];
}

@end
