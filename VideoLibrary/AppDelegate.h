//
//  AppDelegate.h
//  VideoLibrary
//
//  Created by Malik Khalid Ejaz on 11/17/2013.
//  Copyright (c) 2013 Malik Khalid Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end


/*
 
 Message to Youtube Channel owners on Facebook or if they are not on facebook, then on youtube.
 Need to have social media accounts created for this before sending invitations.
 Update credit card info on google for malikejaz.com and for digital occean.
 Get a business credit card so all business related payments are sent through that.
 Register the company with the name FlixPak. Renew domain names needed for this.
 All of this if apple accepts the app.
 
 For pre-launch:
 
    Hi there! We are reaching out to you because you have some great video content on your YouTube channel. More and more quality Pakistani content is being created every day but the revenues are not increasing at the same pace.  We have taken the initiative to promote original Pakistani content production by bringing it to more viewers worldwide. We are calling it Project FlixPak. Our motto is 'watch Pakistani'. And it's free for you and the viewers. This is a time limited, special pre-launch invitation that is only being offered to select few channels. But, if you would like to wait till project launch, you don't have to do anything. After launch, we will have a registration and review process that you can use to apply to join and start reaching more people. More details will be made available after launch at FlixPak.com. All you have to do is reply to this message with 'Yes'. The videos on your channel will be viewed by Pakistani's around the world. It's that easy. You will start seeing more views on your channel videos and hence increased ad revenues. Pakistanis will be encouraged to make more original content and all invisible bunnies will dance with joy. So reply with 'Yes' to get started.

 App name: Pakistani videos
 
 Don't put comment on tv channel's videos. Share with them a web link that gives them the information on how the app works video. let them come to app instead of telling them about the app.
 
 Contact as many video bloggers on their facebook pages as possible. Have them pre-signup for video sharing.
 Write a template message to send as facebook message or share with them as a post.
 Start a facebook ad campaign to 'calling all creatives' to encourage adoption.
 
 App store description:
 
    What's chirping in your head?
 
    Watch great original Pakistani content produced by individuals or by large media houses or by anyone in between. This app is a place to appreciate, enjoy and encourage Pakistani videos and promote high quality production. Individual video bloggers to professional content producers, everyone's work is welcome here.
 
    Watch the rising stars rise, and the superstars shine.
 
    Or become the most glamorous star yourself. 
 
    A song unsung, a story untold or a play never played. Whatever is in your heart, maybe its time to bring it to reality. 
 
    Yes, with this app, you can become the news anchor in highest demand, an artist much loved or whatever else you have the creativity for and we can not even imagine. 
 
    Grab a camera (even mobile camera), make original content and share with all Pakistanis and watch what others are sharing. Pakistanis are one of the most talented people in the world. Let's light it up with awesome.
 
    Why video?
        Video is our most lifelike expression. We maybe far and wide, but our hearts dream the same dreams, our heads move to same tunes and we all pray for Pakistan. We are more like each other than we realize. In today's world, there is no better medium than video to express and to make a difference. Whether its a joke that brightens someone's day, a story that puts a child to sleep or a talk show that brings matters into attention, video is the best medium to capture curosity. So play your part. And remember, everyone has a right to privacy, please ensure that all people in your video are ok with it. Practice good manners and use common sense. And don't copy. Be real.
 
    Share your video (Menu option)
    Anyone can share a video after verification.
 
    Consistent high quality content is required to be able to promote a channel. Can not promote a channel with less than 50 quality videos.
 
    change app rating to 18+
 
    User video categories:
 
    Dramatiks:
        Gather your siblings, cousins, buddies, dudes, besties, saheliyan and jigars. Take a pen and paper. Draw a pinch of dreams, mix some twist and turns, romance hasb-e-zaiqa, add some thrills and scares or a full tub of laughters. Show your acting skills and play your parts. clip and trim as needed. Weldone. Now let me see it.
 
    Let's talk issues:
        Start a talk show. speak your own opinion or listen to other people. Get a discussion going. Politics, fashion, sports or society taboos. Whatever is an issue. talk about it.
 
    Elder eyes: 
        Interview your grand parents, see from their eyes. Build a video log of their life stories. The fun and the sad times. The traditions long forgotten. Listen to their perspective. Record it for generations to come.
 
    Teller's desk:
        A poem unwritten, a song unsung, a story untold or a play never played. We would love to hear it.
 
    Children's corner:
        Tell your favourite bedtime stories from your childhood or act the silliest songs. This is the place to be a kid again.
 
    Documentaries:
        What's your home town like? what's special about it? What is the origin of a tradition? From history to latest trends, if you are the inquisitive type, inquire and enlighten.
 
    Prettify:
        Tips and tricks to enhance your looks.
 
    Lifestyle:
        Home to Yoga to diet to whatever else.
 
    Home Maker:
        It's not just the dishes and the cleaning. Share your tips to make a house a home.
 
    Hall of fame:   (Menu option)
        Every month, one video most loved will be added to the hall of fame for each category. To stay there forever. Can deny videos from hall of fame due to language or the type of content or behavior that we don't want to encourage.
 
    Find your voice, give it a shape and come share with us. Let everyone hear your soul.
 
    If you have anything interesting in mind or if you want to watch more interesting people. You are in the right spot. This app provides content that is different, interesting and original. Because this app is for ordinary people. Tell your stories, share your dreams and
 
    Wouldn't it be great if there was a single place to watch any Pakistani video? Whether its the latest red hot drama, a heated political discussion
 
 
 For video content publishers (before launch):
 
     More and more Pakistanis around the world are watching Pakistani videos online. Video sharing sites are enabling a new era of oppertunities. The viewers are increasing at an exponential rate.
     
     The current state of technology allows us to produce and share content with such ease that there is no reason to not see the growth we all deserve.
     
     But all is not well. Original content producers are not increasing in Pakistan as they should. On the other side, great potential is being left unappreciated. And the ad revenues on video sharing sites are not increasing at the rates that they can.
     
     In case of Pakistan, the reason for this is the lack of a centralised platform to showcase and watch Pakistani content. While major media producers are able to reach greater audience on television, video sharing websites and social media, there is need for more and there is potential for better.
     
     Whether it's the difficulty in preserving the copyrighted content or the ever strong ad blocking plugins reducing revenues. Big and small producers are not seeing the growth that they could.
     
     Fortunately, more and more people are using mobile apps. The ease of access and mobility enables content consumption like never before. Mobile devices have started to eat into the share of desktop video viewing and while these devices offer video ad revenues, there are no dreaded ad blocking plugins and content theft risks.
     
     With all this in mind, we are undertaking a major initiative to bring all Pakistani content in one place. And bring all Pakistani content lovers to that place. Our goal is to be able to preserve all the great content that is being produced and then bring this content to the audience that appreciates it and hence encourage production of higher quality original content. And we will do it for free for everyone.
     
     Today, a cell phone camera is enough to showcase the talent. Whether its an individual, a group of people working on a project or professionals, we hope to provide everyone with a platform to show what they got and reach the people who can appreciate and reward the potential.
     
     We are calling this platform 'FlixPak'. And our motto is 'watch pakistani'. You can continue sharing your content whereever you like. Our mission and hope is to bring you more viewers where you are already sharing content and encourage quality work. We have made it easier than ever to share your video channels with people who want to watch them. And our promise is that we will continue to improve and encourage.
     
     Let's enjoy great content together.
 
 For video content publishers (after launch):
 
    More and more Pakistanis around the world are watching Pakistani videos online. Video sharing sites are enabling a new era of oppertunities. The viewers are increasing at an exponential rate. 
 
    The current state of technology allows us to produce and share content with such ease that there is no reason to not see the growth we all deserve. A few years ago, one would need expensive video streaming servers that were out of reach of all but the biggest media producers. Today there are free video sharing sites and even individuals are able to have their own content showcased at a global scale and for free.
 
    But all is not well. Original content producers are not increasing in Pakistan as they should. On the other side, great potential is being left unappreciated. And the ad revenues on free video sharing sites are not increasing at the rates that they can.
 
    In case of Pakistan, the reason for this is the lack of a centralised platform to showcase and watch Pakistani content. While major media producers are able to reach greater audience on television, video sharing websites and social media, there is need for more and there is potential for better.
 
    While social media and video sharing sites are providing great potential. These are not without limitations and disadvantages. Whether it be the difficult in preserving the copyrighted content or the ever strong ad blocking plugins reducing revenues. Big and small producers are not seeing the growth that they deserve.
 
    Fortunately, more and more people are using mobile apps. The ease of access and mobility enables content consumption like never before. Mobile devices have started to eat into the share of desktop video viewing and while these devices offer video ad revenues, there are no dreaded ad blocking plugins and theft risks.
 
    We are undertaking a major initiative to bring all Pakistani content in one place. And bring all Pakistani content lovers in one place. Our goal is to be able to preserve all the great content that is being produced and then bring this content to the audience that appreciates it and hence encourage production of higher quality original content. And we want to do it for free for everyone.
 
    Today, a cell phone camera is enough to showcase the talent. Whether its an individual, a group of people working on a project or professionals, we hope to provide everyone with a platform to show what they got and reach the people who can appreciate and reward the potential.
 
    We are calling this platform 'FlixPak'. And our motto is 'watch pakistani'. You can continue sharing your content whereever you like. Our mission and hope is to bring you more viewers and encourage quality work. We have made it easier than ever to share your video channels with people who want to watch them. And our promise is that we will continue to improve and encourage.
 
    Let's enjoy great content together. Bacuse Pakistanis are the most talented people in the world, we can't wait to see all the creativity unleash.
 
*/
